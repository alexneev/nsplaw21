function valid_email_address(e) {
    var t = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    return t.test(e)
}! function(e, t) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function(e) {
        if (!e.document) throw new Error("jQuery requires a window with a document");
        return t(e)
    } : t(e)
}("undefined" != typeof window ? window : this, function(e, t) {
    function n(e) {
        var t = !!e && "length" in e && e.length,
            n = re.type(e);
        return "function" !== n && !re.isWindow(e) && ("array" === n || 0 === t || "number" == typeof t && t > 0 && t - 1 in e)
    }

    function i(e, t, n) {
        if (re.isFunction(t)) return re.grep(e, function(e, i) {
            return !!t.call(e, i, e) !== n
        });
        if (t.nodeType) return re.grep(e, function(e) {
            return e === t !== n
        });
        if ("string" == typeof t) {
            if (me.test(t)) return re.filter(t, e, n);
            t = re.filter(t, e)
        }
        return re.grep(e, function(e) {
            return Z.call(t, e) > -1 !== n
        })
    }

    function o(e, t) {
        for (;
            (e = e[t]) && 1 !== e.nodeType;);
        return e
    }

    function r(e) {
        var t = {};
        return re.each(e.match(xe) || [], function(e, n) {
            t[n] = !0
        }), t
    }

    function s() {
        G.removeEventListener("DOMContentLoaded", s), e.removeEventListener("load", s), re.ready()
    }

    function a() {
        this.expando = re.expando + a.uid++
    }

    function l(e, t, n) {
        var i;
        if (void 0 === n && 1 === e.nodeType)
            if (i = "data-" + t.replace(Ie, "-$&").toLowerCase(), n = e.getAttribute(i), "string" == typeof n) {
                try {
                    n = "true" === n || "false" !== n && ("null" === n ? null : +n + "" === n ? +n : Se.test(n) ? re.parseJSON(n) : n)
                } catch (e) {}
                je.set(e, t, n)
            } else n = void 0;
        return n
    }

    function c(e, t, n, i) {
        var o, r = 1,
            s = 20,
            a = i ? function() {
                return i.cur()
            } : function() {
                return re.css(e, t, "")
            },
            l = a(),
            c = n && n[3] || (re.cssNumber[t] ? "" : "px"),
            u = (re.cssNumber[t] || "px" !== c && +l) && Ee.exec(re.css(e, t));
        if (u && u[3] !== c) {
            c = c || u[3], n = n || [], u = +l || 1;
            do r = r || ".5", u /= r, re.style(e, t, u + c); while (r !== (r = a() / l) && 1 !== r && --s)
        }
        return n && (u = +u || +l || 0, o = n[1] ? u + (n[1] + 1) * n[2] : +n[2], i && (i.unit = c, i.start = u, i.end = o)), o
    }

    function u(e, t) {
        var n = "undefined" != typeof e.getElementsByTagName ? e.getElementsByTagName(t || "*") : "undefined" != typeof e.querySelectorAll ? e.querySelectorAll(t || "*") : [];
        return void 0 === t || t && re.nodeName(e, t) ? re.merge([e], n) : n
    }

    function d(e, t) {
        for (var n = 0, i = e.length; n < i; n++) _e.set(e[n], "globalEval", !t || _e.get(t[n], "globalEval"))
    }

    function p(e, t, n, i, o) {
        for (var r, s, a, l, c, p, h = t.createDocumentFragment(), f = [], m = 0, g = e.length; m < g; m++)
            if (r = e[m], r || 0 === r)
                if ("object" === re.type(r)) re.merge(f, r.nodeType ? [r] : r);
                else if (ze.test(r)) {
            for (s = s || h.appendChild(t.createElement("div")), a = (qe.exec(r) || ["", ""])[1].toLowerCase(), l = Le[a] || Le._default, s.innerHTML = l[1] + re.htmlPrefilter(r) + l[2], p = l[0]; p--;) s = s.lastChild;
            re.merge(f, s.childNodes), s = h.firstChild, s.textContent = ""
        } else f.push(t.createTextNode(r));
        for (h.textContent = "", m = 0; r = f[m++];)
            if (i && re.inArray(r, i) > -1) o && o.push(r);
            else if (c = re.contains(r.ownerDocument, r), s = u(h.appendChild(r), "script"), c && d(s), n)
            for (p = 0; r = s[p++];) Ne.test(r.type || "") && n.push(r);
        return h
    }

    function h() {
        return !0
    }

    function f() {
        return !1
    }

    function m() {
        try {
            return G.activeElement
        } catch (e) {}
    }

    function g(e, t, n, i, o, r) {
        var s, a;
        if ("object" == typeof t) {
            "string" != typeof n && (i = i || n, n = void 0);
            for (a in t) g(e, a, n, i, t[a], r);
            return e
        }
        if (null == i && null == o ? (o = n, i = n = void 0) : null == o && ("string" == typeof n ? (o = i, i = void 0) : (o = i, i = n, n = void 0)), o === !1) o = f;
        else if (!o) return e;
        return 1 === r && (s = o, o = function(e) {
            return re().off(e), s.apply(this, arguments)
        }, o.guid = s.guid || (s.guid = re.guid++)), e.each(function() {
            re.event.add(this, t, o, i, n)
        })
    }

    function v(e, t) {
        return re.nodeName(e, "table") && re.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e
    }

    function y(e) {
        return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
    }

    function w(e) {
        var t = Fe.exec(e.type);
        return t ? e.type = t[1] : e.removeAttribute("type"), e
    }

    function b(e, t) {
        var n, i, o, r, s, a, l, c;
        if (1 === t.nodeType) {
            if (_e.hasData(e) && (r = _e.access(e), s = _e.set(t, r), c = r.events)) {
                delete s.handle, s.events = {};
                for (o in c)
                    for (n = 0, i = c[o].length; n < i; n++) re.event.add(t, o, c[o][n])
            }
            je.hasData(e) && (a = je.access(e), l = re.extend({}, a), je.set(t, l))
        }
    }

    function x(e, t) {
        var n = t.nodeName.toLowerCase();
        "input" === n && Ae.test(e.type) ? t.checked = e.checked : "input" !== n && "textarea" !== n || (t.defaultValue = e.defaultValue)
    }

    function C(e, t, n, i) {
        t = K.apply([], t);
        var o, r, s, a, l, c, d = 0,
            h = e.length,
            f = h - 1,
            m = t[0],
            g = re.isFunction(m);
        if (g || h > 1 && "string" == typeof m && !ie.checkClone && Be.test(m)) return e.each(function(o) {
            var r = e.eq(o);
            g && (t[0] = m.call(this, o, r.html())), C(r, t, n, i)
        });
        if (h && (o = p(t, e[0].ownerDocument, !1, e, i), r = o.firstChild, 1 === o.childNodes.length && (o = r), r || i)) {
            for (s = re.map(u(o, "script"), y), a = s.length; d < h; d++) l = o, d !== f && (l = re.clone(l, !0, !0), a && re.merge(s, u(l, "script"))), n.call(e[d], l, d);
            if (a)
                for (c = s[s.length - 1].ownerDocument, re.map(s, w), d = 0; d < a; d++) l = s[d], Ne.test(l.type || "") && !_e.access(l, "globalEval") && re.contains(c, l) && (l.src ? re._evalUrl && re._evalUrl(l.src) : re.globalEval(l.textContent.replace(Xe, "")))
        }
        return e
    }

    function T(e, t, n) {
        for (var i, o = t ? re.filter(t, e) : e, r = 0; null != (i = o[r]); r++) n || 1 !== i.nodeType || re.cleanData(u(i)), i.parentNode && (n && re.contains(i.ownerDocument, i) && d(u(i, "script")), i.parentNode.removeChild(i));
        return e
    }

    function k(e, t) {
        var n = re(t.createElement(e)).appendTo(t.body),
            i = re.css(n[0], "display");
        return n.detach(), i
    }

    function _(e) {
        var t = G,
            n = Ye[e];
        return n || (n = k(e, t), "none" !== n && n || (Ve = (Ve || re("<iframe frameborder='0' width='0' height='0'/>")).appendTo(t.documentElement), t = Ve[0].contentDocument, t.write(), t.close(), n = k(e, t), Ve.detach()), Ye[e] = n), n
    }

    function j(e, t, n) {
        var i, o, r, s, a = e.style;
        return n = n || Qe(e), s = n ? n.getPropertyValue(t) || n[t] : void 0, "" !== s && void 0 !== s || re.contains(e.ownerDocument, e) || (s = re.style(e, t)), n && !ie.pixelMarginRight() && Ge.test(s) && Ue.test(t) && (i = a.width, o = a.minWidth, r = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = n.width, a.width = i, a.minWidth = o, a.maxWidth = r), void 0 !== s ? s + "" : s
    }

    function S(e, t) {
        return {
            get: function() {
                return e() ? void delete this.get : (this.get = t).apply(this, arguments)
            }
        }
    }

    function I(e) {
        if (e in it) return e;
        for (var t = e[0].toUpperCase() + e.slice(1), n = nt.length; n--;)
            if (e = nt[n] + t, e in it) return e
    }

    function $(e, t, n) {
        var i = Ee.exec(t);
        return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : t
    }

    function E(e, t, n, i, o) {
        for (var r = n === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0, s = 0; r < 4; r += 2) "margin" === n && (s += re.css(e, n + De[r], !0, o)), i ? ("content" === n && (s -= re.css(e, "padding" + De[r], !0, o)), "margin" !== n && (s -= re.css(e, "border" + De[r] + "Width", !0, o))) : (s += re.css(e, "padding" + De[r], !0, o), "padding" !== n && (s += re.css(e, "border" + De[r] + "Width", !0, o)));
        return s
    }

    function D(e, t, n) {
        var i = !0,
            o = "width" === t ? e.offsetWidth : e.offsetHeight,
            r = Qe(e),
            s = "border-box" === re.css(e, "boxSizing", !1, r);
        if (o <= 0 || null == o) {
            if (o = j(e, t, r), (o < 0 || null == o) && (o = e.style[t]), Ge.test(o)) return o;
            i = s && (ie.boxSizingReliable() || o === e.style[t]), o = parseFloat(o) || 0
        }
        return o + E(e, t, n || (s ? "border" : "content"), i, r) + "px"
    }

    function P(e, t) {
        for (var n, i, o, r = [], s = 0, a = e.length; s < a; s++) i = e[s], i.style && (r[s] = _e.get(i, "olddisplay"), n = i.style.display, t ? (r[s] || "none" !== n || (i.style.display = ""), "" === i.style.display && Pe(i) && (r[s] = _e.access(i, "olddisplay", _(i.nodeName)))) : (o = Pe(i), "none" === n && o || _e.set(i, "olddisplay", o ? n : re.css(i, "display"))));
        for (s = 0; s < a; s++) i = e[s], i.style && (t && "none" !== i.style.display && "" !== i.style.display || (i.style.display = t ? r[s] || "" : "none"));
        return e
    }

    function A(e, t, n, i, o) {
        return new A.prototype.init(e, t, n, i, o)
    }

    function q() {
        return e.setTimeout(function() {
            ot = void 0
        }), ot = re.now()
    }

    function N(e, t) {
        var n, i = 0,
            o = {
                height: e
            };
        for (t = t ? 1 : 0; i < 4; i += 2 - t) n = De[i], o["margin" + n] = o["padding" + n] = e;
        return t && (o.opacity = o.width = e), o
    }

    function L(e, t, n) {
        for (var i, o = (O.tweeners[t] || []).concat(O.tweeners["*"]), r = 0, s = o.length; r < s; r++)
            if (i = o[r].call(n, t, e)) return i
    }

    function z(e, t, n) {
        var i, o, r, s, a, l, c, u, d = this,
            p = {},
            h = e.style,
            f = e.nodeType && Pe(e),
            m = _e.get(e, "fxshow");
        n.queue || (a = re._queueHooks(e, "fx"), null == a.unqueued && (a.unqueued = 0, l = a.empty.fire, a.empty.fire = function() {
            a.unqueued || l()
        }), a.unqueued++, d.always(function() {
            d.always(function() {
                a.unqueued--, re.queue(e, "fx").length || a.empty.fire()
            })
        })), 1 === e.nodeType && ("height" in t || "width" in t) && (n.overflow = [h.overflow, h.overflowX, h.overflowY], c = re.css(e, "display"), u = "none" === c ? _e.get(e, "olddisplay") || _(e.nodeName) : c, "inline" === u && "none" === re.css(e, "float") && (h.display = "inline-block")), n.overflow && (h.overflow = "hidden", d.always(function() {
            h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2]
        }));
        for (i in t)
            if (o = t[i], st.exec(o)) {
                if (delete t[i], r = r || "toggle" === o, o === (f ? "hide" : "show")) {
                    if ("show" !== o || !m || void 0 === m[i]) continue;
                    f = !0
                }
                p[i] = m && m[i] || re.style(e, i)
            } else c = void 0;
        if (re.isEmptyObject(p)) "inline" === ("none" === c ? _(e.nodeName) : c) && (h.display = c);
        else {
            m ? "hidden" in m && (f = m.hidden) : m = _e.access(e, "fxshow", {}), r && (m.hidden = !f), f ? re(e).show() : d.done(function() {
                re(e).hide()
            }), d.done(function() {
                var t;
                _e.remove(e, "fxshow");
                for (t in p) re.style(e, t, p[t])
            });
            for (i in p) s = L(f ? m[i] : 0, i, d), i in m || (m[i] = s.start, f && (s.end = s.start, s.start = "width" === i || "height" === i ? 1 : 0))
        }
    }

    function W(e, t) {
        var n, i, o, r, s;
        for (n in e)
            if (i = re.camelCase(n), o = t[i], r = e[n], re.isArray(r) && (o = r[1], r = e[n] = r[0]), n !== i && (e[i] = r, delete e[n]), s = re.cssHooks[i], s && "expand" in s) {
                r = s.expand(r), delete e[i];
                for (n in r) n in e || (e[n] = r[n], t[n] = o)
            } else t[i] = o
    }

    function O(e, t, n) {
        var i, o, r = 0,
            s = O.prefilters.length,
            a = re.Deferred().always(function() {
                delete l.elem
            }),
            l = function() {
                if (o) return !1;
                for (var t = ot || q(), n = Math.max(0, c.startTime + c.duration - t), i = n / c.duration || 0, r = 1 - i, s = 0, l = c.tweens.length; s < l; s++) c.tweens[s].run(r);
                return a.notifyWith(e, [c, r, n]), r < 1 && l ? n : (a.resolveWith(e, [c]), !1)
            },
            c = a.promise({
                elem: e,
                props: re.extend({}, t),
                opts: re.extend(!0, {
                    specialEasing: {},
                    easing: re.easing._default
                }, n),
                originalProperties: t,
                originalOptions: n,
                startTime: ot || q(),
                duration: n.duration,
                tweens: [],
                createTween: function(t, n) {
                    var i = re.Tween(e, c.opts, t, n, c.opts.specialEasing[t] || c.opts.easing);
                    return c.tweens.push(i), i
                },
                stop: function(t) {
                    var n = 0,
                        i = t ? c.tweens.length : 0;
                    if (o) return this;
                    for (o = !0; n < i; n++) c.tweens[n].run(1);
                    return t ? (a.notifyWith(e, [c, 1, 0]), a.resolveWith(e, [c, t])) : a.rejectWith(e, [c, t]), this
                }
            }),
            u = c.props;
        for (W(u, c.opts.specialEasing); r < s; r++)
            if (i = O.prefilters[r].call(c, e, u, c.opts)) return re.isFunction(i.stop) && (re._queueHooks(c.elem, c.opts.queue).stop = re.proxy(i.stop, i)), i;
        return re.map(u, L, c), re.isFunction(c.opts.start) && c.opts.start.call(e, c), re.fx.timer(re.extend(l, {
            elem: e,
            anim: c,
            queue: c.opts.queue
        })), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always)
    }

    function H(e) {
        return e.getAttribute && e.getAttribute("class") || ""
    }

    function M(e) {
        return function(t, n) {
            "string" != typeof t && (n = t, t = "*");
            var i, o = 0,
                r = t.toLowerCase().match(xe) || [];
            if (re.isFunction(n))
                for (; i = r[o++];) "+" === i[0] ? (i = i.slice(1) || "*", (e[i] = e[i] || []).unshift(n)) : (e[i] = e[i] || []).push(n)
        }
    }

    function R(e, t, n, i) {
        function o(a) {
            var l;
            return r[a] = !0, re.each(e[a] || [], function(e, a) {
                var c = a(t, n, i);
                return "string" != typeof c || s || r[c] ? s ? !(l = c) : void 0 : (t.dataTypes.unshift(c), o(c), !1)
            }), l
        }
        var r = {},
            s = e === jt;
        return o(t.dataTypes[0]) || !r["*"] && o("*")
    }

    function B(e, t) {
        var n, i, o = re.ajaxSettings.flatOptions || {};
        for (n in t) void 0 !== t[n] && ((o[n] ? e : i || (i = {}))[n] = t[n]);
        return i && re.extend(!0, e, i), e
    }

    function F(e, t, n) {
        for (var i, o, r, s, a = e.contents, l = e.dataTypes;
            "*" === l[0];) l.shift(), void 0 === i && (i = e.mimeType || t.getResponseHeader("Content-Type"));
        if (i)
            for (o in a)
                if (a[o] && a[o].test(i)) {
                    l.unshift(o);
                    break
                } if (l[0] in n) r = l[0];
        else {
            for (o in n) {
                if (!l[0] || e.converters[o + " " + l[0]]) {
                    r = o;
                    break
                }
                s || (s = o)
            }
            r = r || s
        }
        if (r) return r !== l[0] && l.unshift(r), n[r]
    }

    function X(e, t, n, i) {
        var o, r, s, a, l, c = {},
            u = e.dataTypes.slice();
        if (u[1])
            for (s in e.converters) c[s.toLowerCase()] = e.converters[s];
        for (r = u.shift(); r;)
            if (e.responseFields[r] && (n[e.responseFields[r]] = t), !l && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = r, r = u.shift())
                if ("*" === r) r = l;
                else if ("*" !== l && l !== r) {
            if (s = c[l + " " + r] || c["* " + r], !s)
                for (o in c)
                    if (a = o.split(" "), a[1] === r && (s = c[l + " " + a[0]] || c["* " + a[0]])) {
                        s === !0 ? s = c[o] : c[o] !== !0 && (r = a[0], u.unshift(a[1]));
                        break
                    } if (s !== !0)
                if (s && e.throws) t = s(t);
                else try {
                    t = s(t)
                } catch (e) {
                    return {
                        state: "parsererror",
                        error: s ? e : "No conversion from " + l + " to " + r
                    }
                }
        }
        return {
            state: "success",
            data: t
        }
    }

    function V(e, t, n, i) {
        var o;
        if (re.isArray(t)) re.each(t, function(t, o) {
            n || Et.test(e) ? i(e, o) : V(e + "[" + ("object" == typeof o && null != o ? t : "") + "]", o, n, i)
        });
        else if (n || "object" !== re.type(t)) i(e, t);
        else
            for (o in t) V(e + "[" + o + "]", t[o], n, i)
    }

    function Y(e) {
        return re.isWindow(e) ? e : 9 === e.nodeType && e.defaultView
    }
    var U = [],
        G = e.document,
        Q = U.slice,
        K = U.concat,
        J = U.push,
        Z = U.indexOf,
        ee = {},
        te = ee.toString,
        ne = ee.hasOwnProperty,
        ie = {},
        oe = "2.2.4",
        re = function(e, t) {
            return new re.fn.init(e, t)
        },
        se = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        ae = /^-ms-/,
        le = /-([\da-z])/gi,
        ce = function(e, t) {
            return t.toUpperCase()
        };
    re.fn = re.prototype = {
        jquery: oe,
        constructor: re,
        selector: "",
        length: 0,
        toArray: function() {
            return Q.call(this)
        },
        get: function(e) {
            return null != e ? e < 0 ? this[e + this.length] : this[e] : Q.call(this)
        },
        pushStack: function(e) {
            var t = re.merge(this.constructor(), e);
            return t.prevObject = this, t.context = this.context, t
        },
        each: function(e) {
            return re.each(this, e)
        },
        map: function(e) {
            return this.pushStack(re.map(this, function(t, n) {
                return e.call(t, n, t)
            }))
        },
        slice: function() {
            return this.pushStack(Q.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(e) {
            var t = this.length,
                n = +e + (e < 0 ? t : 0);
            return this.pushStack(n >= 0 && n < t ? [this[n]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor()
        },
        push: J,
        sort: U.sort,
        splice: U.splice
    }, re.extend = re.fn.extend = function() {
        var e, t, n, i, o, r, s = arguments[0] || {},
            a = 1,
            l = arguments.length,
            c = !1;
        for ("boolean" == typeof s && (c = s, s = arguments[a] || {}, a++), "object" == typeof s || re.isFunction(s) || (s = {}), a === l && (s = this, a--); a < l; a++)
            if (null != (e = arguments[a]))
                for (t in e) n = s[t], i = e[t], s !== i && (c && i && (re.isPlainObject(i) || (o = re.isArray(i))) ? (o ? (o = !1, r = n && re.isArray(n) ? n : []) : r = n && re.isPlainObject(n) ? n : {}, s[t] = re.extend(c, r, i)) : void 0 !== i && (s[t] = i));
        return s
    }, re.extend({
        expando: "jQuery" + (oe + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(e) {
            throw new Error(e)
        },
        noop: function() {},
        isFunction: function(e) {
            return "function" === re.type(e)
        },
        isArray: Array.isArray,
        isWindow: function(e) {
            return null != e && e === e.window
        },
        isNumeric: function(e) {
            var t = e && e.toString();
            return !re.isArray(e) && t - parseFloat(t) + 1 >= 0
        },
        isPlainObject: function(e) {
            var t;
            if ("object" !== re.type(e) || e.nodeType || re.isWindow(e)) return !1;
            if (e.constructor && !ne.call(e, "constructor") && !ne.call(e.constructor.prototype || {}, "isPrototypeOf")) return !1;
            for (t in e);
            return void 0 === t || ne.call(e, t)
        },
        isEmptyObject: function(e) {
            var t;
            for (t in e) return !1;
            return !0
        },
        type: function(e) {
            return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? ee[te.call(e)] || "object" : typeof e
        },
        globalEval: function(e) {
            var t, n = eval;
            e = re.trim(e), e && (1 === e.indexOf("use strict") ? (t = G.createElement("script"), t.text = e, G.head.appendChild(t).parentNode.removeChild(t)) : n(e))
        },
        camelCase: function(e) {
            return e.replace(ae, "ms-").replace(le, ce)
        },
        nodeName: function(e, t) {
            return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
        },
        each: function(e, t) {
            var i, o = 0;
            if (n(e))
                for (i = e.length; o < i && t.call(e[o], o, e[o]) !== !1; o++);
            else
                for (o in e)
                    if (t.call(e[o], o, e[o]) === !1) break;
            return e
        },
        trim: function(e) {
            return null == e ? "" : (e + "").replace(se, "")
        },
        makeArray: function(e, t) {
            var i = t || [];
            return null != e && (n(Object(e)) ? re.merge(i, "string" == typeof e ? [e] : e) : J.call(i, e)), i
        },
        inArray: function(e, t, n) {
            return null == t ? -1 : Z.call(t, e, n)
        },
        merge: function(e, t) {
            for (var n = +t.length, i = 0, o = e.length; i < n; i++) e[o++] = t[i];
            return e.length = o, e
        },
        grep: function(e, t, n) {
            for (var i, o = [], r = 0, s = e.length, a = !n; r < s; r++) i = !t(e[r], r), i !== a && o.push(e[r]);
            return o
        },
        map: function(e, t, i) {
            var o, r, s = 0,
                a = [];
            if (n(e))
                for (o = e.length; s < o; s++) r = t(e[s], s, i), null != r && a.push(r);
            else
                for (s in e) r = t(e[s], s, i), null != r && a.push(r);
            return K.apply([], a)
        },
        guid: 1,
        proxy: function(e, t) {
            var n, i, o;
            if ("string" == typeof t && (n = e[t], t = e, e = n), re.isFunction(e)) return i = Q.call(arguments, 2), o = function() {
                return e.apply(t || this, i.concat(Q.call(arguments)))
            }, o.guid = e.guid = e.guid || re.guid++, o
        },
        now: Date.now,
        support: ie
    }), "function" == typeof Symbol && (re.fn[Symbol.iterator] = U[Symbol.iterator]), re.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(e, t) {
        ee["[object " + t + "]"] = t.toLowerCase()
    });
    var ue = function(e) {
        function t(e, t, n, i) {
            var o, r, s, a, l, c, d, h, f = t && t.ownerDocument,
                m = t ? t.nodeType : 9;
            if (n = n || [], "string" != typeof e || !e || 1 !== m && 9 !== m && 11 !== m) return n;
            if (!i && ((t ? t.ownerDocument || t : H) !== P && D(t), t = t || P, q)) {
                if (11 !== m && (c = ve.exec(e)))
                    if (o = c[1]) {
                        if (9 === m) {
                            if (!(s = t.getElementById(o))) return n;
                            if (s.id === o) return n.push(s), n
                        } else if (f && (s = f.getElementById(o)) && W(t, s) && s.id === o) return n.push(s), n
                    } else {
                        if (c[2]) return J.apply(n, t.getElementsByTagName(e)), n;
                        if ((o = c[3]) && x.getElementsByClassName && t.getElementsByClassName) return J.apply(n, t.getElementsByClassName(o)), n
                    } if (x.qsa && !X[e + " "] && (!N || !N.test(e))) {
                    if (1 !== m) f = t, h = e;
                    else if ("object" !== t.nodeName.toLowerCase()) {
                        for ((a = t.getAttribute("id")) ? a = a.replace(we, "\\$&") : t.setAttribute("id", a = O), d = _(e), r = d.length, l = pe.test(a) ? "#" + a : "[id='" + a + "']"; r--;) d[r] = l + " " + p(d[r]);
                        h = d.join(","), f = ye.test(e) && u(t.parentNode) || t
                    }
                    if (h) try {
                        return J.apply(n, f.querySelectorAll(h)), n
                    } catch (e) {} finally {
                        a === O && t.removeAttribute("id")
                    }
                }
            }
            return S(e.replace(ae, "$1"), t, n, i)
        }

        function n() {
            function e(n, i) {
                return t.push(n + " ") > C.cacheLength && delete e[t.shift()], e[n + " "] = i
            }
            var t = [];
            return e
        }

        function i(e) {
            return e[O] = !0, e
        }

        function o(e) {
            var t = P.createElement("div");
            try {
                return !!e(t)
            } catch (e) {
                return !1
            } finally {
                t.parentNode && t.parentNode.removeChild(t), t = null
            }
        }

        function r(e, t) {
            for (var n = e.split("|"), i = n.length; i--;) C.attrHandle[n[i]] = t
        }

        function s(e, t) {
            var n = t && e,
                i = n && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || Y) - (~e.sourceIndex || Y);
            if (i) return i;
            if (n)
                for (; n = n.nextSibling;)
                    if (n === t) return -1;
            return e ? 1 : -1
        }

        function a(e) {
            return function(t) {
                var n = t.nodeName.toLowerCase();
                return "input" === n && t.type === e
            }
        }

        function l(e) {
            return function(t) {
                var n = t.nodeName.toLowerCase();
                return ("input" === n || "button" === n) && t.type === e
            }
        }

        function c(e) {
            return i(function(t) {
                return t = +t, i(function(n, i) {
                    for (var o, r = e([], n.length, t), s = r.length; s--;) n[o = r[s]] && (n[o] = !(i[o] = n[o]))
                })
            })
        }

        function u(e) {
            return e && "undefined" != typeof e.getElementsByTagName && e
        }

        function d() {}

        function p(e) {
            for (var t = 0, n = e.length, i = ""; t < n; t++) i += e[t].value;
            return i
        }

        function h(e, t, n) {
            var i = t.dir,
                o = n && "parentNode" === i,
                r = R++;
            return t.first ? function(t, n, r) {
                for (; t = t[i];)
                    if (1 === t.nodeType || o) return e(t, n, r)
            } : function(t, n, s) {
                var a, l, c, u = [M, r];
                if (s) {
                    for (; t = t[i];)
                        if ((1 === t.nodeType || o) && e(t, n, s)) return !0
                } else
                    for (; t = t[i];)
                        if (1 === t.nodeType || o) {
                            if (c = t[O] || (t[O] = {}), l = c[t.uniqueID] || (c[t.uniqueID] = {}), (a = l[i]) && a[0] === M && a[1] === r) return u[2] = a[2];
                            if (l[i] = u, u[2] = e(t, n, s)) return !0
                        }
            }
        }

        function f(e) {
            return e.length > 1 ? function(t, n, i) {
                for (var o = e.length; o--;)
                    if (!e[o](t, n, i)) return !1;
                return !0
            } : e[0]
        }

        function m(e, n, i) {
            for (var o = 0, r = n.length; o < r; o++) t(e, n[o], i);
            return i
        }

        function g(e, t, n, i, o) {
            for (var r, s = [], a = 0, l = e.length, c = null != t; a < l; a++)(r = e[a]) && (n && !n(r, i, o) || (s.push(r), c && t.push(a)));
            return s
        }

        function v(e, t, n, o, r, s) {
            return o && !o[O] && (o = v(o)), r && !r[O] && (r = v(r, s)), i(function(i, s, a, l) {
                var c, u, d, p = [],
                    h = [],
                    f = s.length,
                    v = i || m(t || "*", a.nodeType ? [a] : a, []),
                    y = !e || !i && t ? v : g(v, p, e, a, l),
                    w = n ? r || (i ? e : f || o) ? [] : s : y;
                if (n && n(y, w, a, l), o)
                    for (c = g(w, h), o(c, [], a, l), u = c.length; u--;)(d = c[u]) && (w[h[u]] = !(y[h[u]] = d));
                if (i) {
                    if (r || e) {
                        if (r) {
                            for (c = [], u = w.length; u--;)(d = w[u]) && c.push(y[u] = d);
                            r(null, w = [], c, l)
                        }
                        for (u = w.length; u--;)(d = w[u]) && (c = r ? ee(i, d) : p[u]) > -1 && (i[c] = !(s[c] = d))
                    }
                } else w = g(w === s ? w.splice(f, w.length) : w), r ? r(null, s, w, l) : J.apply(s, w)
            })
        }

        function y(e) {
            for (var t, n, i, o = e.length, r = C.relative[e[0].type], s = r || C.relative[" "], a = r ? 1 : 0, l = h(function(e) {
                    return e === t
                }, s, !0), c = h(function(e) {
                    return ee(t, e) > -1
                }, s, !0), u = [function(e, n, i) {
                    var o = !r && (i || n !== I) || ((t = n).nodeType ? l(e, n, i) : c(e, n, i));
                    return t = null, o
                }]; a < o; a++)
                if (n = C.relative[e[a].type]) u = [h(f(u), n)];
                else {
                    if (n = C.filter[e[a].type].apply(null, e[a].matches), n[O]) {
                        for (i = ++a; i < o && !C.relative[e[i].type]; i++);
                        return v(a > 1 && f(u), a > 1 && p(e.slice(0, a - 1).concat({
                            value: " " === e[a - 2].type ? "*" : ""
                        })).replace(ae, "$1"), n, a < i && y(e.slice(a, i)), i < o && y(e = e.slice(i)), i < o && p(e))
                    }
                    u.push(n)
                } return f(u)
        }

        function w(e, n) {
            var o = n.length > 0,
                r = e.length > 0,
                s = function(i, s, a, l, c) {
                    var u, d, p, h = 0,
                        f = "0",
                        m = i && [],
                        v = [],
                        y = I,
                        w = i || r && C.find.TAG("*", c),
                        b = M += null == y ? 1 : Math.random() || .1,
                        x = w.length;
                    for (c && (I = s === P || s || c); f !== x && null != (u = w[f]); f++) {
                        if (r && u) {
                            for (d = 0, s || u.ownerDocument === P || (D(u), a = !q); p = e[d++];)
                                if (p(u, s || P, a)) {
                                    l.push(u);
                                    break
                                } c && (M = b)
                        }
                        o && ((u = !p && u) && h--, i && m.push(u))
                    }
                    if (h += f, o && f !== h) {
                        for (d = 0; p = n[d++];) p(m, v, s, a);
                        if (i) {
                            if (h > 0)
                                for (; f--;) m[f] || v[f] || (v[f] = Q.call(l));
                            v = g(v)
                        }
                        J.apply(l, v), c && !i && v.length > 0 && h + n.length > 1 && t.uniqueSort(l)
                    }
                    return c && (M = b, I = y), m
                };
            return o ? i(s) : s
        }
        var b, x, C, T, k, _, j, S, I, $, E, D, P, A, q, N, L, z, W, O = "sizzle" + 1 * new Date,
            H = e.document,
            M = 0,
            R = 0,
            B = n(),
            F = n(),
            X = n(),
            V = function(e, t) {
                return e === t && (E = !0), 0
            },
            Y = 1 << 31,
            U = {}.hasOwnProperty,
            G = [],
            Q = G.pop,
            K = G.push,
            J = G.push,
            Z = G.slice,
            ee = function(e, t) {
                for (var n = 0, i = e.length; n < i; n++)
                    if (e[n] === t) return n;
                return -1
            },
            te = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            ne = "[\\x20\\t\\r\\n\\f]",
            ie = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
            oe = "\\[" + ne + "*(" + ie + ")(?:" + ne + "*([*^$|!~]?=)" + ne + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + ie + "))|)" + ne + "*\\]",
            re = ":(" + ie + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + oe + ")*)|.*)\\)|)",
            se = new RegExp(ne + "+", "g"),
            ae = new RegExp("^" + ne + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ne + "+$", "g"),
            le = new RegExp("^" + ne + "*," + ne + "*"),
            ce = new RegExp("^" + ne + "*([>+~]|" + ne + ")" + ne + "*"),
            ue = new RegExp("=" + ne + "*([^\\]'\"]*?)" + ne + "*\\]", "g"),
            de = new RegExp(re),
            pe = new RegExp("^" + ie + "$"),
            he = {
                ID: new RegExp("^#(" + ie + ")"),
                CLASS: new RegExp("^\\.(" + ie + ")"),
                TAG: new RegExp("^(" + ie + "|[*])"),
                ATTR: new RegExp("^" + oe),
                PSEUDO: new RegExp("^" + re),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ne + "*(even|odd|(([+-]|)(\\d*)n|)" + ne + "*(?:([+-]|)" + ne + "*(\\d+)|))" + ne + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + te + ")$", "i"),
                needsContext: new RegExp("^" + ne + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ne + "*((?:-\\d)?\\d*)" + ne + "*\\)|)(?=[^-]|$)", "i")
            },
            fe = /^(?:input|select|textarea|button)$/i,
            me = /^h\d$/i,
            ge = /^[^{]+\{\s*\[native \w/,
            ve = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            ye = /[+~]/,
            we = /'|\\/g,
            be = new RegExp("\\\\([\\da-f]{1,6}" + ne + "?|(" + ne + ")|.)", "ig"),
            xe = function(e, t, n) {
                var i = "0x" + t - 65536;
                return i !== i || n ? t : i < 0 ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
            },
            Ce = function() {
                D()
            };
        try {
            J.apply(G = Z.call(H.childNodes), H.childNodes), G[H.childNodes.length].nodeType
        } catch (e) {
            J = {
                apply: G.length ? function(e, t) {
                    K.apply(e, Z.call(t))
                } : function(e, t) {
                    for (var n = e.length, i = 0; e[n++] = t[i++];);
                    e.length = n - 1
                }
            }
        }
        x = t.support = {}, k = t.isXML = function(e) {
            var t = e && (e.ownerDocument || e).documentElement;
            return !!t && "HTML" !== t.nodeName
        }, D = t.setDocument = function(e) {
            var t, n, i = e ? e.ownerDocument || e : H;
            return i !== P && 9 === i.nodeType && i.documentElement ? (P = i, A = P.documentElement, q = !k(P), (n = P.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", Ce, !1) : n.attachEvent && n.attachEvent("onunload", Ce)), x.attributes = o(function(e) {
                return e.className = "i", !e.getAttribute("className")
            }), x.getElementsByTagName = o(function(e) {
                return e.appendChild(P.createComment("")), !e.getElementsByTagName("*").length
            }), x.getElementsByClassName = ge.test(P.getElementsByClassName), x.getById = o(function(e) {
                return A.appendChild(e).id = O, !P.getElementsByName || !P.getElementsByName(O).length
            }), x.getById ? (C.find.ID = function(e, t) {
                if ("undefined" != typeof t.getElementById && q) {
                    var n = t.getElementById(e);
                    return n ? [n] : []
                }
            }, C.filter.ID = function(e) {
                var t = e.replace(be, xe);
                return function(e) {
                    return e.getAttribute("id") === t
                }
            }) : (delete C.find.ID, C.filter.ID = function(e) {
                var t = e.replace(be, xe);
                return function(e) {
                    var n = "undefined" != typeof e.getAttributeNode && e.getAttributeNode("id");
                    return n && n.value === t
                }
            }), C.find.TAG = x.getElementsByTagName ? function(e, t) {
                return "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e) : x.qsa ? t.querySelectorAll(e) : void 0
            } : function(e, t) {
                var n, i = [],
                    o = 0,
                    r = t.getElementsByTagName(e);
                if ("*" === e) {
                    for (; n = r[o++];) 1 === n.nodeType && i.push(n);
                    return i
                }
                return r
            }, C.find.CLASS = x.getElementsByClassName && function(e, t) {
                if ("undefined" != typeof t.getElementsByClassName && q) return t.getElementsByClassName(e)
            }, L = [], N = [], (x.qsa = ge.test(P.querySelectorAll)) && (o(function(e) {
                A.appendChild(e).innerHTML = "<a id='" + O + "'></a><select id='" + O + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && N.push("[*^$]=" + ne + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || N.push("\\[" + ne + "*(?:value|" + te + ")"), e.querySelectorAll("[id~=" + O + "-]").length || N.push("~="), e.querySelectorAll(":checked").length || N.push(":checked"), e.querySelectorAll("a#" + O + "+*").length || N.push(".#.+[+~]")
            }), o(function(e) {
                var t = P.createElement("input");
                t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && N.push("name" + ne + "*[*^$|!~]?="), e.querySelectorAll(":enabled").length || N.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), N.push(",.*:")
            })), (x.matchesSelector = ge.test(z = A.matches || A.webkitMatchesSelector || A.mozMatchesSelector || A.oMatchesSelector || A.msMatchesSelector)) && o(function(e) {
                x.disconnectedMatch = z.call(e, "div"), z.call(e, "[s!='']:x"), L.push("!=", re)
            }), N = N.length && new RegExp(N.join("|")), L = L.length && new RegExp(L.join("|")), t = ge.test(A.compareDocumentPosition), W = t || ge.test(A.contains) ? function(e, t) {
                var n = 9 === e.nodeType ? e.documentElement : e,
                    i = t && t.parentNode;
                return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i)))
            } : function(e, t) {
                if (t)
                    for (; t = t.parentNode;)
                        if (t === e) return !0;
                return !1
            }, V = t ? function(e, t) {
                if (e === t) return E = !0, 0;
                var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                return n ? n : (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1, 1 & n || !x.sortDetached && t.compareDocumentPosition(e) === n ? e === P || e.ownerDocument === H && W(H, e) ? -1 : t === P || t.ownerDocument === H && W(H, t) ? 1 : $ ? ee($, e) - ee($, t) : 0 : 4 & n ? -1 : 1)
            } : function(e, t) {
                if (e === t) return E = !0, 0;
                var n, i = 0,
                    o = e.parentNode,
                    r = t.parentNode,
                    a = [e],
                    l = [t];
                if (!o || !r) return e === P ? -1 : t === P ? 1 : o ? -1 : r ? 1 : $ ? ee($, e) - ee($, t) : 0;
                if (o === r) return s(e, t);
                for (n = e; n = n.parentNode;) a.unshift(n);
                for (n = t; n = n.parentNode;) l.unshift(n);
                for (; a[i] === l[i];) i++;
                return i ? s(a[i], l[i]) : a[i] === H ? -1 : l[i] === H ? 1 : 0
            }, P) : P
        }, t.matches = function(e, n) {
            return t(e, null, null, n)
        }, t.matchesSelector = function(e, n) {
            if ((e.ownerDocument || e) !== P && D(e), n = n.replace(ue, "='$1']"), x.matchesSelector && q && !X[n + " "] && (!L || !L.test(n)) && (!N || !N.test(n))) try {
                var i = z.call(e, n);
                if (i || x.disconnectedMatch || e.document && 11 !== e.document.nodeType) return i
            } catch (e) {}
            return t(n, P, null, [e]).length > 0
        }, t.contains = function(e, t) {
            return (e.ownerDocument || e) !== P && D(e), W(e, t)
        }, t.attr = function(e, t) {
            (e.ownerDocument || e) !== P && D(e);
            var n = C.attrHandle[t.toLowerCase()],
                i = n && U.call(C.attrHandle, t.toLowerCase()) ? n(e, t, !q) : void 0;
            return void 0 !== i ? i : x.attributes || !q ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
        }, t.error = function(e) {
            throw new Error("Syntax error, unrecognized expression: " + e)
        }, t.uniqueSort = function(e) {
            var t, n = [],
                i = 0,
                o = 0;
            if (E = !x.detectDuplicates, $ = !x.sortStable && e.slice(0), e.sort(V), E) {
                for (; t = e[o++];) t === e[o] && (i = n.push(o));
                for (; i--;) e.splice(n[i], 1)
            }
            return $ = null, e
        }, T = t.getText = function(e) {
            var t, n = "",
                i = 0,
                o = e.nodeType;
            if (o) {
                if (1 === o || 9 === o || 11 === o) {
                    if ("string" == typeof e.textContent) return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling) n += T(e)
                } else if (3 === o || 4 === o) return e.nodeValue
            } else
                for (; t = e[i++];) n += T(t);
            return n
        }, C = t.selectors = {
            cacheLength: 50,
            createPseudo: i,
            match: he,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(e) {
                    return e[1] = e[1].replace(be, xe), e[3] = (e[3] || e[4] || e[5] || "").replace(be, xe), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                },
                CHILD: function(e) {
                    return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || t.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && t.error(e[0]), e
                },
                PSEUDO: function(e) {
                    var t, n = !e[6] && e[2];
                    return he.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && de.test(n) && (t = _(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                }
            },
            filter: {
                TAG: function(e) {
                    var t = e.replace(be, xe).toLowerCase();
                    return "*" === e ? function() {
                        return !0
                    } : function(e) {
                        return e.nodeName && e.nodeName.toLowerCase() === t
                    }
                },
                CLASS: function(e) {
                    var t = B[e + " "];
                    return t || (t = new RegExp("(^|" + ne + ")" + e + "(" + ne + "|$)")) && B(e, function(e) {
                        return t.test("string" == typeof e.className && e.className || "undefined" != typeof e.getAttribute && e.getAttribute("class") || "")
                    })
                },
                ATTR: function(e, n, i) {
                    return function(o) {
                        var r = t.attr(o, e);
                        return null == r ? "!=" === n : !n || (r += "", "=" === n ? r === i : "!=" === n ? r !== i : "^=" === n ? i && 0 === r.indexOf(i) : "*=" === n ? i && r.indexOf(i) > -1 : "$=" === n ? i && r.slice(-i.length) === i : "~=" === n ? (" " + r.replace(se, " ") + " ").indexOf(i) > -1 : "|=" === n && (r === i || r.slice(0, i.length + 1) === i + "-"))
                    }
                },
                CHILD: function(e, t, n, i, o) {
                    var r = "nth" !== e.slice(0, 3),
                        s = "last" !== e.slice(-4),
                        a = "of-type" === t;
                    return 1 === i && 0 === o ? function(e) {
                        return !!e.parentNode
                    } : function(t, n, l) {
                        var c, u, d, p, h, f, m = r !== s ? "nextSibling" : "previousSibling",
                            g = t.parentNode,
                            v = a && t.nodeName.toLowerCase(),
                            y = !l && !a,
                            w = !1;
                        if (g) {
                            if (r) {
                                for (; m;) {
                                    for (p = t; p = p[m];)
                                        if (a ? p.nodeName.toLowerCase() === v : 1 === p.nodeType) return !1;
                                    f = m = "only" === e && !f && "nextSibling"
                                }
                                return !0
                            }
                            if (f = [s ? g.firstChild : g.lastChild], s && y) {
                                for (p = g, d = p[O] || (p[O] = {}), u = d[p.uniqueID] || (d[p.uniqueID] = {}), c = u[e] || [], h = c[0] === M && c[1], w = h && c[2], p = h && g.childNodes[h]; p = ++h && p && p[m] || (w = h = 0) || f.pop();)
                                    if (1 === p.nodeType && ++w && p === t) {
                                        u[e] = [M, h, w];
                                        break
                                    }
                            } else if (y && (p = t, d = p[O] || (p[O] = {}), u = d[p.uniqueID] || (d[p.uniqueID] = {}), c = u[e] || [], h = c[0] === M && c[1], w = h), w === !1)
                                for (;
                                    (p = ++h && p && p[m] || (w = h = 0) || f.pop()) && ((a ? p.nodeName.toLowerCase() !== v : 1 !== p.nodeType) || !++w || (y && (d = p[O] || (p[O] = {}), u = d[p.uniqueID] || (d[p.uniqueID] = {}), u[e] = [M, w]), p !== t)););
                            return w -= o, w === i || w % i === 0 && w / i >= 0
                        }
                    }
                },
                PSEUDO: function(e, n) {
                    var o, r = C.pseudos[e] || C.setFilters[e.toLowerCase()] || t.error("unsupported pseudo: " + e);
                    return r[O] ? r(n) : r.length > 1 ? (o = [e, e, "", n], C.setFilters.hasOwnProperty(e.toLowerCase()) ? i(function(e, t) {
                        for (var i, o = r(e, n), s = o.length; s--;) i = ee(e, o[s]), e[i] = !(t[i] = o[s])
                    }) : function(e) {
                        return r(e, 0, o)
                    }) : r
                }
            },
            pseudos: {
                not: i(function(e) {
                    var t = [],
                        n = [],
                        o = j(e.replace(ae, "$1"));
                    return o[O] ? i(function(e, t, n, i) {
                        for (var r, s = o(e, null, i, []), a = e.length; a--;)(r = s[a]) && (e[a] = !(t[a] = r))
                    }) : function(e, i, r) {
                        return t[0] = e, o(t, null, r, n), t[0] = null, !n.pop()
                    }
                }),
                has: i(function(e) {
                    return function(n) {
                        return t(e, n).length > 0
                    }
                }),
                contains: i(function(e) {
                    return e = e.replace(be, xe),
                        function(t) {
                            return (t.textContent || t.innerText || T(t)).indexOf(e) > -1
                        }
                }),
                lang: i(function(e) {
                    return pe.test(e || "") || t.error("unsupported lang: " + e), e = e.replace(be, xe).toLowerCase(),
                        function(t) {
                            var n;
                            do
                                if (n = q ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return n = n.toLowerCase(), n === e || 0 === n.indexOf(e + "-"); while ((t = t.parentNode) && 1 === t.nodeType);
                            return !1
                        }
                }),
                target: function(t) {
                    var n = e.location && e.location.hash;
                    return n && n.slice(1) === t.id
                },
                root: function(e) {
                    return e === A
                },
                focus: function(e) {
                    return e === P.activeElement && (!P.hasFocus || P.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                },
                enabled: function(e) {
                    return e.disabled === !1
                },
                disabled: function(e) {
                    return e.disabled === !0
                },
                checked: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && !!e.checked || "option" === t && !!e.selected
                },
                selected: function(e) {
                    return e.parentNode && e.parentNode.selectedIndex, e.selected === !0
                },
                empty: function(e) {
                    for (e = e.firstChild; e; e = e.nextSibling)
                        if (e.nodeType < 6) return !1;
                    return !0
                },
                parent: function(e) {
                    return !C.pseudos.empty(e)
                },
                header: function(e) {
                    return me.test(e.nodeName)
                },
                input: function(e) {
                    return fe.test(e.nodeName)
                },
                button: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && "button" === e.type || "button" === t
                },
                text: function(e) {
                    var t;
                    return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                },
                first: c(function() {
                    return [0]
                }),
                last: c(function(e, t) {
                    return [t - 1]
                }),
                eq: c(function(e, t, n) {
                    return [n < 0 ? n + t : n]
                }),
                even: c(function(e, t) {
                    for (var n = 0; n < t; n += 2) e.push(n);
                    return e
                }),
                odd: c(function(e, t) {
                    for (var n = 1; n < t; n += 2) e.push(n);
                    return e
                }),
                lt: c(function(e, t, n) {
                    for (var i = n < 0 ? n + t : n; --i >= 0;) e.push(i);
                    return e
                }),
                gt: c(function(e, t, n) {
                    for (var i = n < 0 ? n + t : n; ++i < t;) e.push(i);
                    return e
                })
            }
        }, C.pseudos.nth = C.pseudos.eq;
        for (b in {
                radio: !0,
                checkbox: !0,
                file: !0,
                password: !0,
                image: !0
            }) C.pseudos[b] = a(b);
        for (b in {
                submit: !0,
                reset: !0
            }) C.pseudos[b] = l(b);
        return d.prototype = C.filters = C.pseudos, C.setFilters = new d, _ = t.tokenize = function(e, n) {
            var i, o, r, s, a, l, c, u = F[e + " "];
            if (u) return n ? 0 : u.slice(0);
            for (a = e, l = [], c = C.preFilter; a;) {
                i && !(o = le.exec(a)) || (o && (a = a.slice(o[0].length) || a), l.push(r = [])), i = !1, (o = ce.exec(a)) && (i = o.shift(), r.push({
                    value: i,
                    type: o[0].replace(ae, " ")
                }), a = a.slice(i.length));
                for (s in C.filter) !(o = he[s].exec(a)) || c[s] && !(o = c[s](o)) || (i = o.shift(), r.push({
                    value: i,
                    type: s,
                    matches: o
                }), a = a.slice(i.length));
                if (!i) break
            }
            return n ? a.length : a ? t.error(e) : F(e, l).slice(0)
        }, j = t.compile = function(e, t) {
            var n, i = [],
                o = [],
                r = X[e + " "];
            if (!r) {
                for (t || (t = _(e)), n = t.length; n--;) r = y(t[n]), r[O] ? i.push(r) : o.push(r);
                r = X(e, w(o, i)), r.selector = e
            }
            return r
        }, S = t.select = function(e, t, n, i) {
            var o, r, s, a, l, c = "function" == typeof e && e,
                d = !i && _(e = c.selector || e);
            if (n = n || [], 1 === d.length) {
                if (r = d[0] = d[0].slice(0), r.length > 2 && "ID" === (s = r[0]).type && x.getById && 9 === t.nodeType && q && C.relative[r[1].type]) {
                    if (t = (C.find.ID(s.matches[0].replace(be, xe), t) || [])[0], !t) return n;
                    c && (t = t.parentNode), e = e.slice(r.shift().value.length)
                }
                for (o = he.needsContext.test(e) ? 0 : r.length; o-- && (s = r[o], !C.relative[a = s.type]);)
                    if ((l = C.find[a]) && (i = l(s.matches[0].replace(be, xe), ye.test(r[0].type) && u(t.parentNode) || t))) {
                        if (r.splice(o, 1), e = i.length && p(r), !e) return J.apply(n, i), n;
                        break
                    }
            }
            return (c || j(e, d))(i, t, !q, n, !t || ye.test(e) && u(t.parentNode) || t), n
        }, x.sortStable = O.split("").sort(V).join("") === O, x.detectDuplicates = !!E, D(), x.sortDetached = o(function(e) {
            return 1 & e.compareDocumentPosition(P.createElement("div"))
        }), o(function(e) {
            return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
        }) || r("type|href|height|width", function(e, t, n) {
            if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
        }), x.attributes && o(function(e) {
            return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
        }) || r("value", function(e, t, n) {
            if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue
        }), o(function(e) {
            return null == e.getAttribute("disabled")
        }) || r(te, function(e, t, n) {
            var i;
            if (!n) return e[t] === !0 ? t.toLowerCase() : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
        }), t
    }(e);
    re.find = ue, re.expr = ue.selectors, re.expr[":"] = re.expr.pseudos, re.uniqueSort = re.unique = ue.uniqueSort, re.text = ue.getText, re.isXMLDoc = ue.isXML, re.contains = ue.contains;
    var de = function(e, t, n) {
            for (var i = [], o = void 0 !== n;
                (e = e[t]) && 9 !== e.nodeType;)
                if (1 === e.nodeType) {
                    if (o && re(e).is(n)) break;
                    i.push(e)
                } return i
        },
        pe = function(e, t) {
            for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
            return n
        },
        he = re.expr.match.needsContext,
        fe = /^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,
        me = /^.[^:#\[\.,]*$/;
    re.filter = function(e, t, n) {
        var i = t[0];
        return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === i.nodeType ? re.find.matchesSelector(i, e) ? [i] : [] : re.find.matches(e, re.grep(t, function(e) {
            return 1 === e.nodeType
        }))
    }, re.fn.extend({
        find: function(e) {
            var t, n = this.length,
                i = [],
                o = this;
            if ("string" != typeof e) return this.pushStack(re(e).filter(function() {
                for (t = 0; t < n; t++)
                    if (re.contains(o[t], this)) return !0
            }));
            for (t = 0; t < n; t++) re.find(e, o[t], i);
            return i = this.pushStack(n > 1 ? re.unique(i) : i), i.selector = this.selector ? this.selector + " " + e : e, i
        },
        filter: function(e) {
            return this.pushStack(i(this, e || [], !1))
        },
        not: function(e) {
            return this.pushStack(i(this, e || [], !0))
        },
        is: function(e) {
            return !!i(this, "string" == typeof e && he.test(e) ? re(e) : e || [], !1).length
        }
    });
    var ge, ve = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
        ye = re.fn.init = function(e, t, n) {
            var i, o;
            if (!e) return this;
            if (n = n || ge, "string" == typeof e) {
                if (i = "<" === e[0] && ">" === e[e.length - 1] && e.length >= 3 ? [null, e, null] : ve.exec(e), !i || !i[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
                if (i[1]) {
                    if (t = t instanceof re ? t[0] : t, re.merge(this, re.parseHTML(i[1], t && t.nodeType ? t.ownerDocument || t : G, !0)), fe.test(i[1]) && re.isPlainObject(t))
                        for (i in t) re.isFunction(this[i]) ? this[i](t[i]) : this.attr(i, t[i]);
                    return this
                }
                return o = G.getElementById(i[2]), o && o.parentNode && (this.length = 1, this[0] = o), this.context = G, this.selector = e, this
            }
            return e.nodeType ? (this.context = this[0] = e, this.length = 1, this) : re.isFunction(e) ? void 0 !== n.ready ? n.ready(e) : e(re) : (void 0 !== e.selector && (this.selector = e.selector, this.context = e.context), re.makeArray(e, this))
        };
    ye.prototype = re.fn, ge = re(G);
    var we = /^(?:parents|prev(?:Until|All))/,
        be = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    re.fn.extend({
        has: function(e) {
            var t = re(e, this),
                n = t.length;
            return this.filter(function() {
                for (var e = 0; e < n; e++)
                    if (re.contains(this, t[e])) return !0
            })
        },
        closest: function(e, t) {
            for (var n, i = 0, o = this.length, r = [], s = he.test(e) || "string" != typeof e ? re(e, t || this.context) : 0; i < o; i++)
                for (n = this[i]; n && n !== t; n = n.parentNode)
                    if (n.nodeType < 11 && (s ? s.index(n) > -1 : 1 === n.nodeType && re.find.matchesSelector(n, e))) {
                        r.push(n);
                        break
                    } return this.pushStack(r.length > 1 ? re.uniqueSort(r) : r)
        },
        index: function(e) {
            return e ? "string" == typeof e ? Z.call(re(e), this[0]) : Z.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(e, t) {
            return this.pushStack(re.uniqueSort(re.merge(this.get(), re(e, t))))
        },
        addBack: function(e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }), re.each({
        parent: function(e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        },
        parents: function(e) {
            return de(e, "parentNode")
        },
        parentsUntil: function(e, t, n) {
            return de(e, "parentNode", n)
        },
        next: function(e) {
            return o(e, "nextSibling")
        },
        prev: function(e) {
            return o(e, "previousSibling")
        },
        nextAll: function(e) {
            return de(e, "nextSibling")
        },
        prevAll: function(e) {
            return de(e, "previousSibling")
        },
        nextUntil: function(e, t, n) {
            return de(e, "nextSibling", n)
        },
        prevUntil: function(e, t, n) {
            return de(e, "previousSibling", n)
        },
        siblings: function(e) {
            return pe((e.parentNode || {}).firstChild, e)
        },
        children: function(e) {
            return pe(e.firstChild)
        },
        contents: function(e) {
            return e.contentDocument || re.merge([], e.childNodes)
        }
    }, function(e, t) {
        re.fn[e] = function(n, i) {
            var o = re.map(this, t, n);
            return "Until" !== e.slice(-5) && (i = n), i && "string" == typeof i && (o = re.filter(i, o)), this.length > 1 && (be[e] || re.uniqueSort(o), we.test(e) && o.reverse()), this.pushStack(o)
        }
    });
    var xe = /\S+/g;
    re.Callbacks = function(e) {
        e = "string" == typeof e ? r(e) : re.extend({}, e);
        var t, n, i, o, s = [],
            a = [],
            l = -1,
            c = function() {
                for (o = e.once, i = t = !0; a.length; l = -1)
                    for (n = a.shift(); ++l < s.length;) s[l].apply(n[0], n[1]) === !1 && e.stopOnFalse && (l = s.length, n = !1);
                e.memory || (n = !1), t = !1, o && (s = n ? [] : "")
            },
            u = {
                add: function() {
                    return s && (n && !t && (l = s.length - 1, a.push(n)), function t(n) {
                        re.each(n, function(n, i) {
                            re.isFunction(i) ? e.unique && u.has(i) || s.push(i) : i && i.length && "string" !== re.type(i) && t(i)
                        })
                    }(arguments), n && !t && c()), this
                },
                remove: function() {
                    return re.each(arguments, function(e, t) {
                        for (var n;
                            (n = re.inArray(t, s, n)) > -1;) s.splice(n, 1), n <= l && l--
                    }), this
                },
                has: function(e) {
                    return e ? re.inArray(e, s) > -1 : s.length > 0
                },
                empty: function() {
                    return s && (s = []), this
                },
                disable: function() {
                    return o = a = [], s = n = "", this
                },
                disabled: function() {
                    return !s
                },
                lock: function() {
                    return o = a = [], n || (s = n = ""), this
                },
                locked: function() {
                    return !!o
                },
                fireWith: function(e, n) {
                    return o || (n = n || [], n = [e, n.slice ? n.slice() : n], a.push(n), t || c()), this
                },
                fire: function() {
                    return u.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!i
                }
            };
        return u
    }, re.extend({
        Deferred: function(e) {
            var t = [
                    ["resolve", "done", re.Callbacks("once memory"), "resolved"],
                    ["reject", "fail", re.Callbacks("once memory"), "rejected"],
                    ["notify", "progress", re.Callbacks("memory")]
                ],
                n = "pending",
                i = {
                    state: function() {
                        return n
                    },
                    always: function() {
                        return o.done(arguments).fail(arguments), this
                    },
                    then: function() {
                        var e = arguments;
                        return re.Deferred(function(n) {
                            re.each(t, function(t, r) {
                                var s = re.isFunction(e[t]) && e[t];
                                o[r[1]](function() {
                                    var e = s && s.apply(this, arguments);
                                    e && re.isFunction(e.promise) ? e.promise().progress(n.notify).done(n.resolve).fail(n.reject) : n[r[0] + "With"](this === i ? n.promise() : this, s ? [e] : arguments)
                                })
                            }), e = null
                        }).promise()
                    },
                    promise: function(e) {
                        return null != e ? re.extend(e, i) : i
                    }
                },
                o = {};
            return i.pipe = i.then, re.each(t, function(e, r) {
                var s = r[2],
                    a = r[3];
                i[r[1]] = s.add, a && s.add(function() {
                    n = a
                }, t[1 ^ e][2].disable, t[2][2].lock), o[r[0]] = function() {
                    return o[r[0] + "With"](this === o ? i : this, arguments), this
                }, o[r[0] + "With"] = s.fireWith
            }), i.promise(o), e && e.call(o, o), o
        },
        when: function(e) {
            var t, n, i, o = 0,
                r = Q.call(arguments),
                s = r.length,
                a = 1 !== s || e && re.isFunction(e.promise) ? s : 0,
                l = 1 === a ? e : re.Deferred(),
                c = function(e, n, i) {
                    return function(o) {
                        n[e] = this, i[e] = arguments.length > 1 ? Q.call(arguments) : o, i === t ? l.notifyWith(n, i) : --a || l.resolveWith(n, i)
                    }
                };
            if (s > 1)
                for (t = new Array(s), n = new Array(s), i = new Array(s); o < s; o++) r[o] && re.isFunction(r[o].promise) ? r[o].promise().progress(c(o, n, t)).done(c(o, i, r)).fail(l.reject) : --a;
            return a || l.resolveWith(i, r), l.promise()
        }
    });
    var Ce;
    re.fn.ready = function(e) {
        return re.ready.promise().done(e), this
    }, re.extend({
        isReady: !1,
        readyWait: 1,
        holdReady: function(e) {
            e ? re.readyWait++ : re.ready(!0)
        },
        ready: function(e) {
            (e === !0 ? --re.readyWait : re.isReady) || (re.isReady = !0, e !== !0 && --re.readyWait > 0 || (Ce.resolveWith(G, [re]), re.fn.triggerHandler && (re(G).triggerHandler("ready"), re(G).off("ready"))))
        }
    }), re.ready.promise = function(t) {
        return Ce || (Ce = re.Deferred(), "complete" === G.readyState || "loading" !== G.readyState && !G.documentElement.doScroll ? e.setTimeout(re.ready) : (G.addEventListener("DOMContentLoaded", s), e.addEventListener("load", s))), Ce.promise(t)
    }, re.ready.promise();
    var Te = function(e, t, n, i, o, r, s) {
            var a = 0,
                l = e.length,
                c = null == n;
            if ("object" === re.type(n)) {
                o = !0;
                for (a in n) Te(e, t, a, n[a], !0, r, s)
            } else if (void 0 !== i && (o = !0, re.isFunction(i) || (s = !0), c && (s ? (t.call(e, i), t = null) : (c = t, t = function(e, t, n) {
                    return c.call(re(e), n)
                })), t))
                for (; a < l; a++) t(e[a], n, s ? i : i.call(e[a], a, t(e[a], n)));
            return o ? e : c ? t.call(e) : l ? t(e[0], n) : r
        },
        ke = function(e) {
            return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
        };
    a.uid = 1, a.prototype = {
        register: function(e, t) {
            var n = t || {};
            return e.nodeType ? e[this.expando] = n : Object.defineProperty(e, this.expando, {
                value: n,
                writable: !0,
                configurable: !0
            }), e[this.expando]
        },
        cache: function(e) {
            if (!ke(e)) return {};
            var t = e[this.expando];
            return t || (t = {}, ke(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
                value: t,
                configurable: !0
            }))), t
        },
        set: function(e, t, n) {
            var i, o = this.cache(e);
            if ("string" == typeof t) o[t] = n;
            else
                for (i in t) o[i] = t[i];
            return o
        },
        get: function(e, t) {
            return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][t]
        },
        access: function(e, t, n) {
            var i;
            return void 0 === t || t && "string" == typeof t && void 0 === n ? (i = this.get(e, t), void 0 !== i ? i : this.get(e, re.camelCase(t))) : (this.set(e, t, n), void 0 !== n ? n : t)
        },
        remove: function(e, t) {
            var n, i, o, r = e[this.expando];
            if (void 0 !== r) {
                if (void 0 === t) this.register(e);
                else {
                    re.isArray(t) ? i = t.concat(t.map(re.camelCase)) : (o = re.camelCase(t), t in r ? i = [t, o] : (i = o, i = i in r ? [i] : i.match(xe) || [])), n = i.length;
                    for (; n--;) delete r[i[n]]
                }(void 0 === t || re.isEmptyObject(r)) && (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando])
            }
        },
        hasData: function(e) {
            var t = e[this.expando];
            return void 0 !== t && !re.isEmptyObject(t)
        }
    };
    var _e = new a,
        je = new a,
        Se = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        Ie = /[A-Z]/g;
    re.extend({
        hasData: function(e) {
            return je.hasData(e) || _e.hasData(e)
        },
        data: function(e, t, n) {
            return je.access(e, t, n)
        },
        removeData: function(e, t) {
            je.remove(e, t)
        },
        _data: function(e, t, n) {
            return _e.access(e, t, n)
        },
        _removeData: function(e, t) {
            _e.remove(e, t)
        }
    }), re.fn.extend({
        data: function(e, t) {
            var n, i, o, r = this[0],
                s = r && r.attributes;
            if (void 0 === e) {
                if (this.length && (o = je.get(r), 1 === r.nodeType && !_e.get(r, "hasDataAttrs"))) {
                    for (n = s.length; n--;) s[n] && (i = s[n].name, 0 === i.indexOf("data-") && (i = re.camelCase(i.slice(5)), l(r, i, o[i])));
                    _e.set(r, "hasDataAttrs", !0)
                }
                return o
            }
            return "object" == typeof e ? this.each(function() {
                je.set(this, e)
            }) : Te(this, function(t) {
                var n, i;
                if (r && void 0 === t) {
                    if (n = je.get(r, e) || je.get(r, e.replace(Ie, "-$&").toLowerCase()), void 0 !== n) return n;
                    if (i = re.camelCase(e), n = je.get(r, i), void 0 !== n) return n;
                    if (n = l(r, i, void 0), void 0 !== n) return n
                } else i = re.camelCase(e), this.each(function() {
                    var n = je.get(this, i);
                    je.set(this, i, t), e.indexOf("-") > -1 && void 0 !== n && je.set(this, e, t)
                })
            }, null, t, arguments.length > 1, null, !0)
        },
        removeData: function(e) {
            return this.each(function() {
                je.remove(this, e)
            })
        }
    }), re.extend({
        queue: function(e, t, n) {
            var i;
            if (e) return t = (t || "fx") + "queue", i = _e.get(e, t), n && (!i || re.isArray(n) ? i = _e.access(e, t, re.makeArray(n)) : i.push(n)), i || []
        },
        dequeue: function(e, t) {
            t = t || "fx";
            var n = re.queue(e, t),
                i = n.length,
                o = n.shift(),
                r = re._queueHooks(e, t),
                s = function() {
                    re.dequeue(e, t)
                };
            "inprogress" === o && (o = n.shift(), i--), o && ("fx" === t && n.unshift("inprogress"), delete r.stop, o.call(e, s, r)), !i && r && r.empty.fire()
        },
        _queueHooks: function(e, t) {
            var n = t + "queueHooks";
            return _e.get(e, n) || _e.access(e, n, {
                empty: re.Callbacks("once memory").add(function() {
                    _e.remove(e, [t + "queue", n])
                })
            })
        }
    }), re.fn.extend({
        queue: function(e, t) {
            var n = 2;
            return "string" != typeof e && (t = e, e = "fx", n--), arguments.length < n ? re.queue(this[0], e) : void 0 === t ? this : this.each(function() {
                var n = re.queue(this, e, t);
                re._queueHooks(this, e), "fx" === e && "inprogress" !== n[0] && re.dequeue(this, e)
            })
        },
        dequeue: function(e) {
            return this.each(function() {
                re.dequeue(this, e)
            })
        },
        clearQueue: function(e) {
            return this.queue(e || "fx", [])
        },
        promise: function(e, t) {
            var n, i = 1,
                o = re.Deferred(),
                r = this,
                s = this.length,
                a = function() {
                    --i || o.resolveWith(r, [r])
                };
            for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; s--;) n = _e.get(r[s], e + "queueHooks"), n && n.empty && (i++, n.empty.add(a));
            return a(), o.promise(t)
        }
    });
    var $e = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        Ee = new RegExp("^(?:([+-])=|)(" + $e + ")([a-z%]*)$", "i"),
        De = ["Top", "Right", "Bottom", "Left"],
        Pe = function(e, t) {
            return e = t || e, "none" === re.css(e, "display") || !re.contains(e.ownerDocument, e)
        },
        Ae = /^(?:checkbox|radio)$/i,
        qe = /<([\w:-]+)/,
        Ne = /^$|\/(?:java|ecma)script/i,
        Le = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            thead: [1, "<table>", "</table>"],
            col: [2, "<table><colgroup>", "</colgroup></table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: [0, "", ""]
        };
    Le.optgroup = Le.option, Le.tbody = Le.tfoot = Le.colgroup = Le.caption = Le.thead, Le.th = Le.td;
    var ze = /<|&#?\w+;/;
    ! function() {
        var e = G.createDocumentFragment(),
            t = e.appendChild(G.createElement("div")),
            n = G.createElement("input");
        n.setAttribute("type", "radio"), n.setAttribute("checked", "checked"), n.setAttribute("name", "t"), t.appendChild(n), ie.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, t.innerHTML = "<textarea>x</textarea>", ie.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue
    }();
    var We = /^key/,
        Oe = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        He = /^([^.]*)(?:\.(.+)|)/;
    re.event = {
        global: {},
        add: function(e, t, n, i, o) {
            var r, s, a, l, c, u, d, p, h, f, m, g = _e.get(e);
            if (g)
                for (n.handler && (r = n, n = r.handler, o = r.selector), n.guid || (n.guid = re.guid++), (l = g.events) || (l = g.events = {}), (s = g.handle) || (s = g.handle = function(t) {
                        return "undefined" != typeof re && re.event.triggered !== t.type ? re.event.dispatch.apply(e, arguments) : void 0
                    }), t = (t || "").match(xe) || [""], c = t.length; c--;) a = He.exec(t[c]) || [], h = m = a[1], f = (a[2] || "").split(".").sort(), h && (d = re.event.special[h] || {}, h = (o ? d.delegateType : d.bindType) || h, d = re.event.special[h] || {}, u = re.extend({
                    type: h,
                    origType: m,
                    data: i,
                    handler: n,
                    guid: n.guid,
                    selector: o,
                    needsContext: o && re.expr.match.needsContext.test(o),
                    namespace: f.join(".")
                }, r), (p = l[h]) || (p = l[h] = [], p.delegateCount = 0, d.setup && d.setup.call(e, i, f, s) !== !1 || e.addEventListener && e.addEventListener(h, s)), d.add && (d.add.call(e, u), u.handler.guid || (u.handler.guid = n.guid)), o ? p.splice(p.delegateCount++, 0, u) : p.push(u), re.event.global[h] = !0)
        },
        remove: function(e, t, n, i, o) {
            var r, s, a, l, c, u, d, p, h, f, m, g = _e.hasData(e) && _e.get(e);
            if (g && (l = g.events)) {
                for (t = (t || "").match(xe) || [""], c = t.length; c--;)
                    if (a = He.exec(t[c]) || [], h = m = a[1], f = (a[2] || "").split(".").sort(), h) {
                        for (d = re.event.special[h] || {}, h = (i ? d.delegateType : d.bindType) || h, p = l[h] || [], a = a[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), s = r = p.length; r--;) u = p[r], !o && m !== u.origType || n && n.guid !== u.guid || a && !a.test(u.namespace) || i && i !== u.selector && ("**" !== i || !u.selector) || (p.splice(r, 1), u.selector && p.delegateCount--, d.remove && d.remove.call(e, u));
                        s && !p.length && (d.teardown && d.teardown.call(e, f, g.handle) !== !1 || re.removeEvent(e, h, g.handle), delete l[h])
                    } else
                        for (h in l) re.event.remove(e, h + t[c], n, i, !0);
                re.isEmptyObject(l) && _e.remove(e, "handle events")
            }
        },
        dispatch: function(e) {
            e = re.event.fix(e);
            var t, n, i, o, r, s = [],
                a = Q.call(arguments),
                l = (_e.get(this, "events") || {})[e.type] || [],
                c = re.event.special[e.type] || {};
            if (a[0] = e, e.delegateTarget = this, !c.preDispatch || c.preDispatch.call(this, e) !== !1) {
                for (s = re.event.handlers.call(this, e, l), t = 0;
                    (o = s[t++]) && !e.isPropagationStopped();)
                    for (e.currentTarget = o.elem, n = 0;
                        (r = o.handlers[n++]) && !e.isImmediatePropagationStopped();) e.rnamespace && !e.rnamespace.test(r.namespace) || (e.handleObj = r, e.data = r.data, i = ((re.event.special[r.origType] || {}).handle || r.handler).apply(o.elem, a), void 0 !== i && (e.result = i) === !1 && (e.preventDefault(), e.stopPropagation()));
                return c.postDispatch && c.postDispatch.call(this, e), e.result
            }
        },
        handlers: function(e, t) {
            var n, i, o, r, s = [],
                a = t.delegateCount,
                l = e.target;
            if (a && l.nodeType && ("click" !== e.type || isNaN(e.button) || e.button < 1))
                for (; l !== this; l = l.parentNode || this)
                    if (1 === l.nodeType && (l.disabled !== !0 || "click" !== e.type)) {
                        for (i = [], n = 0; n < a; n++) r = t[n], o = r.selector + " ", void 0 === i[o] && (i[o] = r.needsContext ? re(o, this).index(l) > -1 : re.find(o, this, null, [l]).length), i[o] && i.push(r);
                        i.length && s.push({
                            elem: l,
                            handlers: i
                        })
                    } return a < t.length && s.push({
                elem: this,
                handlers: t.slice(a)
            }), s
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(e, t) {
                return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(e, t) {
                var n, i, o, r = t.button;
                return null == e.pageX && null != t.clientX && (n = e.target.ownerDocument || G, i = n.documentElement, o = n.body, e.pageX = t.clientX + (i && i.scrollLeft || o && o.scrollLeft || 0) - (i && i.clientLeft || o && o.clientLeft || 0), e.pageY = t.clientY + (i && i.scrollTop || o && o.scrollTop || 0) - (i && i.clientTop || o && o.clientTop || 0)), e.which || void 0 === r || (e.which = 1 & r ? 1 : 2 & r ? 3 : 4 & r ? 2 : 0), e
            }
        },
        fix: function(e) {
            if (e[re.expando]) return e;
            var t, n, i, o = e.type,
                r = e,
                s = this.fixHooks[o];
            for (s || (this.fixHooks[o] = s = Oe.test(o) ? this.mouseHooks : We.test(o) ? this.keyHooks : {}), i = s.props ? this.props.concat(s.props) : this.props, e = new re.Event(r), t = i.length; t--;) n = i[t], e[n] = r[n];
            return e.target || (e.target = G), 3 === e.target.nodeType && (e.target = e.target.parentNode), s.filter ? s.filter(e, r) : e
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== m() && this.focus) return this.focus(), !1
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    if (this === m() && this.blur) return this.blur(), !1
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    if ("checkbox" === this.type && this.click && re.nodeName(this, "input")) return this.click(), !1
                },
                _default: function(e) {
                    return re.nodeName(e.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(e) {
                    void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                }
            }
        }
    }, re.removeEvent = function(e, t, n) {
        e.removeEventListener && e.removeEventListener(t, n)
    }, re.Event = function(e, t) {
        return this instanceof re.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && e.returnValue === !1 ? h : f) : this.type = e, t && re.extend(this, t), this.timeStamp = e && e.timeStamp || re.now(), void(this[re.expando] = !0)) : new re.Event(e, t)
    }, re.Event.prototype = {
        constructor: re.Event,
        isDefaultPrevented: f,
        isPropagationStopped: f,
        isImmediatePropagationStopped: f,
        isSimulated: !1,
        preventDefault: function() {
            var e = this.originalEvent;
            this.isDefaultPrevented = h, e && !this.isSimulated && e.preventDefault()
        },
        stopPropagation: function() {
            var e = this.originalEvent;
            this.isPropagationStopped = h, e && !this.isSimulated && e.stopPropagation()
        },
        stopImmediatePropagation: function() {
            var e = this.originalEvent;
            this.isImmediatePropagationStopped = h, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
        }
    }, re.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(e, t) {
        re.event.special[e] = {
            delegateType: t,
            bindType: t,
            handle: function(e) {
                var n, i = this,
                    o = e.relatedTarget,
                    r = e.handleObj;
                return o && (o === i || re.contains(i, o)) || (e.type = r.origType, n = r.handler.apply(this, arguments), e.type = t), n
            }
        }
    }), re.fn.extend({
        on: function(e, t, n, i) {
            return g(this, e, t, n, i)
        },
        one: function(e, t, n, i) {
            return g(this, e, t, n, i, 1)
        },
        off: function(e, t, n) {
            var i, o;
            if (e && e.preventDefault && e.handleObj) return i = e.handleObj, re(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
            if ("object" == typeof e) {
                for (o in e) this.off(o, t, e[o]);
                return this
            }
            return t !== !1 && "function" != typeof t || (n = t, t = void 0), n === !1 && (n = f), this.each(function() {
                re.event.remove(this, e, n, t)
            })
        }
    });
    var Me = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,
        Re = /<script|<style|<link/i,
        Be = /checked\s*(?:[^=]|=\s*.checked.)/i,
        Fe = /^true\/(.*)/,
        Xe = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
    re.extend({
        htmlPrefilter: function(e) {
            return e.replace(Me, "<$1></$2>")
        },
        clone: function(e, t, n) {
            var i, o, r, s, a = e.cloneNode(!0),
                l = re.contains(e.ownerDocument, e);
            if (!(ie.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || re.isXMLDoc(e)))
                for (s = u(a), r = u(e), i = 0, o = r.length; i < o; i++) x(r[i], s[i]);
            if (t)
                if (n)
                    for (r = r || u(e), s = s || u(a), i = 0, o = r.length; i < o; i++) b(r[i], s[i]);
                else b(e, a);
            return s = u(a, "script"), s.length > 0 && d(s, !l && u(e, "script")), a
        },
        cleanData: function(e) {
            for (var t, n, i, o = re.event.special, r = 0; void 0 !== (n = e[r]); r++)
                if (ke(n)) {
                    if (t = n[_e.expando]) {
                        if (t.events)
                            for (i in t.events) o[i] ? re.event.remove(n, i) : re.removeEvent(n, i, t.handle);
                        n[_e.expando] = void 0
                    }
                    n[je.expando] && (n[je.expando] = void 0)
                }
        }
    }), re.fn.extend({
        domManip: C,
        detach: function(e) {
            return T(this, e, !0)
        },
        remove: function(e) {
            return T(this, e)
        },
        text: function(e) {
            return Te(this, function(e) {
                return void 0 === e ? re.text(this) : this.empty().each(function() {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
                })
            }, null, e, arguments.length)
        },
        append: function() {
            return C(this, arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = v(this, e);
                    t.appendChild(e)
                }
            })
        },
        prepend: function() {
            return C(this, arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = v(this, e);
                    t.insertBefore(e, t.firstChild)
                }
            })
        },
        before: function() {
            return C(this, arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        },
        after: function() {
            return C(this, arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        },
        empty: function() {
            for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (re.cleanData(u(e, !1)), e.textContent = "");
            return this
        },
        clone: function(e, t) {
            return e = null != e && e, t = null == t ? e : t, this.map(function() {
                return re.clone(this, e, t)
            })
        },
        html: function(e) {
            return Te(this, function(e) {
                var t = this[0] || {},
                    n = 0,
                    i = this.length;
                if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
                if ("string" == typeof e && !Re.test(e) && !Le[(qe.exec(e) || ["", ""])[1].toLowerCase()]) {
                    e = re.htmlPrefilter(e);
                    try {
                        for (; n < i; n++) t = this[n] || {}, 1 === t.nodeType && (re.cleanData(u(t, !1)), t.innerHTML = e);
                        t = 0
                    } catch (e) {}
                }
                t && this.empty().append(e)
            }, null, e, arguments.length)
        },
        replaceWith: function() {
            var e = [];
            return C(this, arguments, function(t) {
                var n = this.parentNode;
                re.inArray(this, e) < 0 && (re.cleanData(u(this)), n && n.replaceChild(t, this))
            }, e)
        }
    }), re.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(e, t) {
        re.fn[e] = function(e) {
            for (var n, i = [], o = re(e), r = o.length - 1, s = 0; s <= r; s++) n = s === r ? this : this.clone(!0), re(o[s])[t](n), J.apply(i, n.get());
            return this.pushStack(i)
        }
    });
    var Ve, Ye = {
            HTML: "block",
            BODY: "block"
        },
        Ue = /^margin/,
        Ge = new RegExp("^(" + $e + ")(?!px)[a-z%]+$", "i"),
        Qe = function(t) {
            var n = t.ownerDocument.defaultView;
            return n && n.opener || (n = e), n.getComputedStyle(t)
        },
        Ke = function(e, t, n, i) {
            var o, r, s = {};
            for (r in t) s[r] = e.style[r], e.style[r] = t[r];
            o = n.apply(e, i || []);
            for (r in t) e.style[r] = s[r];
            return o
        },
        Je = G.documentElement;
    ! function() {
        function t() {
            a.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", a.innerHTML = "", Je.appendChild(s);
            var t = e.getComputedStyle(a);
            n = "1%" !== t.top, r = "2px" === t.marginLeft, i = "4px" === t.width, a.style.marginRight = "50%", o = "4px" === t.marginRight, Je.removeChild(s)
        }
        var n, i, o, r, s = G.createElement("div"),
            a = G.createElement("div");
        a.style && (a.style.backgroundClip = "content-box", a.cloneNode(!0).style.backgroundClip = "", ie.clearCloneStyle = "content-box" === a.style.backgroundClip, s.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", s.appendChild(a), re.extend(ie, {
            pixelPosition: function() {
                return t(), n
            },
            boxSizingReliable: function() {
                return null == i && t(), i
            },
            pixelMarginRight: function() {
                return null == i && t(), o
            },
            reliableMarginLeft: function() {
                return null == i && t(), r
            },
            reliableMarginRight: function() {
                var t, n = a.appendChild(G.createElement("div"));
                return n.style.cssText = a.style.cssText = "-webkit-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", n.style.marginRight = n.style.width = "0", a.style.width = "1px", Je.appendChild(s), t = !parseFloat(e.getComputedStyle(n).marginRight), Je.removeChild(s), a.removeChild(n), t
            }
        }))
    }();
    var Ze = /^(none|table(?!-c[ea]).+)/,
        et = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        tt = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        nt = ["Webkit", "O", "Moz", "ms"],
        it = G.createElement("div").style;
    re.extend({
        cssHooks: {
            opacity: {
                get: function(e, t) {
                    if (t) {
                        var n = j(e, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            float: "cssFloat"
        },
        style: function(e, t, n, i) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var o, r, s, a = re.camelCase(t),
                    l = e.style;
                return t = re.cssProps[a] || (re.cssProps[a] = I(a) || a), s = re.cssHooks[t] || re.cssHooks[a], void 0 === n ? s && "get" in s && void 0 !== (o = s.get(e, !1, i)) ? o : l[t] : (r = typeof n, "string" === r && (o = Ee.exec(n)) && o[1] && (n = c(e, t, o), r = "number"), null != n && n === n && ("number" === r && (n += o && o[3] || (re.cssNumber[a] ? "" : "px")), ie.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"), s && "set" in s && void 0 === (n = s.set(e, n, i)) || (l[t] = n)), void 0)
            }
        },
        css: function(e, t, n, i) {
            var o, r, s, a = re.camelCase(t);
            return t = re.cssProps[a] || (re.cssProps[a] = I(a) || a), s = re.cssHooks[t] || re.cssHooks[a], s && "get" in s && (o = s.get(e, !0, n)), void 0 === o && (o = j(e, t, i)), "normal" === o && t in tt && (o = tt[t]), "" === n || n ? (r = parseFloat(o), n === !0 || isFinite(r) ? r || 0 : o) : o
        }
    }), re.each(["height", "width"], function(e, t) {
        re.cssHooks[t] = {
            get: function(e, n, i) {
                if (n) return Ze.test(re.css(e, "display")) && 0 === e.offsetWidth ? Ke(e, et, function() {
                    return D(e, t, i)
                }) : D(e, t, i)
            },
            set: function(e, n, i) {
                var o, r = i && Qe(e),
                    s = i && E(e, t, i, "border-box" === re.css(e, "boxSizing", !1, r), r);
                return s && (o = Ee.exec(n)) && "px" !== (o[3] || "px") && (e.style[t] = n, n = re.css(e, t)), $(e, n, s)
            }
        }
    }), re.cssHooks.marginLeft = S(ie.reliableMarginLeft, function(e, t) {
        if (t) return (parseFloat(j(e, "marginLeft")) || e.getBoundingClientRect().left - Ke(e, {
            marginLeft: 0
        }, function() {
            return e.getBoundingClientRect().left
        })) + "px"
    }), re.cssHooks.marginRight = S(ie.reliableMarginRight, function(e, t) {
        if (t) return Ke(e, {
            display: "inline-block"
        }, j, [e, "marginRight"])
    }), re.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(e, t) {
        re.cssHooks[e + t] = {
            expand: function(n) {
                for (var i = 0, o = {}, r = "string" == typeof n ? n.split(" ") : [n]; i < 4; i++) o[e + De[i] + t] = r[i] || r[i - 2] || r[0];
                return o
            }
        }, Ue.test(e) || (re.cssHooks[e + t].set = $)
    }), re.fn.extend({
        css: function(e, t) {
            return Te(this, function(e, t, n) {
                var i, o, r = {},
                    s = 0;
                if (re.isArray(t)) {
                    for (i = Qe(e), o = t.length; s < o; s++) r[t[s]] = re.css(e, t[s], !1, i);
                    return r
                }
                return void 0 !== n ? re.style(e, t, n) : re.css(e, t)
            }, e, t, arguments.length > 1)
        },
        show: function() {
            return P(this, !0)
        },
        hide: function() {
            return P(this)
        },
        toggle: function(e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                Pe(this) ? re(this).show() : re(this).hide()
            })
        }
    }), re.Tween = A, A.prototype = {
        constructor: A,
        init: function(e, t, n, i, o, r) {
            this.elem = e, this.prop = n, this.easing = o || re.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = i, this.unit = r || (re.cssNumber[n] ? "" : "px")
        },
        cur: function() {
            var e = A.propHooks[this.prop];
            return e && e.get ? e.get(this) : A.propHooks._default.get(this)
        },
        run: function(e) {
            var t, n = A.propHooks[this.prop];
            return this.options.duration ? this.pos = t = re.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : A.propHooks._default.set(this), this
        }
    }, A.prototype.init.prototype = A.prototype, A.propHooks = {
        _default: {
            get: function(e) {
                var t;
                return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = re.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0)
            },
            set: function(e) {
                re.fx.step[e.prop] ? re.fx.step[e.prop](e) : 1 !== e.elem.nodeType || null == e.elem.style[re.cssProps[e.prop]] && !re.cssHooks[e.prop] ? e.elem[e.prop] = e.now : re.style(e.elem, e.prop, e.now + e.unit)
            }
        }
    }, A.propHooks.scrollTop = A.propHooks.scrollLeft = {
        set: function(e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, re.easing = {
        linear: function(e) {
            return e
        },
        swing: function(e) {
            return .5 - Math.cos(e * Math.PI) / 2
        },
        _default: "swing"
    }, re.fx = A.prototype.init, re.fx.step = {};
    var ot, rt, st = /^(?:toggle|show|hide)$/,
        at = /queueHooks$/;
    re.Animation = re.extend(O, {
            tweeners: {
                "*": [function(e, t) {
                    var n = this.createTween(e, t);
                    return c(n.elem, e, Ee.exec(t), n), n
                }]
            },
            tweener: function(e, t) {
                re.isFunction(e) ? (t = e, e = ["*"]) : e = e.match(xe);
                for (var n, i = 0, o = e.length; i < o; i++) n = e[i], O.tweeners[n] = O.tweeners[n] || [], O.tweeners[n].unshift(t)
            },
            prefilters: [z],
            prefilter: function(e, t) {
                t ? O.prefilters.unshift(e) : O.prefilters.push(e)
            }
        }), re.speed = function(e, t, n) {
            var i = e && "object" == typeof e ? re.extend({}, e) : {
                complete: n || !n && t || re.isFunction(e) && e,
                duration: e,
                easing: n && t || t && !re.isFunction(t) && t
            };
            return i.duration = re.fx.off ? 0 : "number" == typeof i.duration ? i.duration : i.duration in re.fx.speeds ? re.fx.speeds[i.duration] : re.fx.speeds._default, null != i.queue && i.queue !== !0 || (i.queue = "fx"), i.old = i.complete, i.complete = function() {
                re.isFunction(i.old) && i.old.call(this), i.queue && re.dequeue(this, i.queue)
            }, i
        }, re.fn.extend({
            fadeTo: function(e, t, n, i) {
                return this.filter(Pe).css("opacity", 0).show().end().animate({
                    opacity: t
                }, e, n, i)
            },
            animate: function(e, t, n, i) {
                var o = re.isEmptyObject(e),
                    r = re.speed(t, n, i),
                    s = function() {
                        var t = O(this, re.extend({}, e), r);
                        (o || _e.get(this, "finish")) && t.stop(!0)
                    };
                return s.finish = s, o || r.queue === !1 ? this.each(s) : this.queue(r.queue, s)
            },
            stop: function(e, t, n) {
                var i = function(e) {
                    var t = e.stop;
                    delete e.stop, t(n)
                };
                return "string" != typeof e && (n = t, t = e, e = void 0), t && e !== !1 && this.queue(e || "fx", []), this.each(function() {
                    var t = !0,
                        o = null != e && e + "queueHooks",
                        r = re.timers,
                        s = _e.get(this);
                    if (o) s[o] && s[o].stop && i(s[o]);
                    else
                        for (o in s) s[o] && s[o].stop && at.test(o) && i(s[o]);
                    for (o = r.length; o--;) r[o].elem !== this || null != e && r[o].queue !== e || (r[o].anim.stop(n), t = !1, r.splice(o, 1));
                    !t && n || re.dequeue(this, e)
                })
            },
            finish: function(e) {
                return e !== !1 && (e = e || "fx"), this.each(function() {
                    var t, n = _e.get(this),
                        i = n[e + "queue"],
                        o = n[e + "queueHooks"],
                        r = re.timers,
                        s = i ? i.length : 0;
                    for (n.finish = !0, re.queue(this, e, []), o && o.stop && o.stop.call(this, !0), t = r.length; t--;) r[t].elem === this && r[t].queue === e && (r[t].anim.stop(!0), r.splice(t, 1));
                    for (t = 0; t < s; t++) i[t] && i[t].finish && i[t].finish.call(this);
                    delete n.finish
                })
            }
        }), re.each(["toggle", "show", "hide"], function(e, t) {
            var n = re.fn[t];
            re.fn[t] = function(e, i, o) {
                return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(N(t, !0), e, i, o)
            }
        }), re.each({
            slideDown: N("show"),
            slideUp: N("hide"),
            slideToggle: N("toggle"),
            fadeIn: {
                opacity: "show"
            },
            fadeOut: {
                opacity: "hide"
            },
            fadeToggle: {
                opacity: "toggle"
            }
        }, function(e, t) {
            re.fn[e] = function(e, n, i) {
                return this.animate(t, e, n, i)
            }
        }), re.timers = [], re.fx.tick = function() {
            var e, t = 0,
                n = re.timers;
            for (ot = re.now(); t < n.length; t++) e = n[t], e() || n[t] !== e || n.splice(t--, 1);
            n.length || re.fx.stop(), ot = void 0
        }, re.fx.timer = function(e) {
            re.timers.push(e), e() ? re.fx.start() : re.timers.pop()
        }, re.fx.interval = 13, re.fx.start = function() {
            rt || (rt = e.setInterval(re.fx.tick, re.fx.interval))
        }, re.fx.stop = function() {
            e.clearInterval(rt), rt = null
        }, re.fx.speeds = {
            slow: 600,
            fast: 200,
            _default: 400
        }, re.fn.delay = function(t, n) {
            return t = re.fx ? re.fx.speeds[t] || t : t, n = n || "fx", this.queue(n, function(n, i) {
                var o = e.setTimeout(n, t);
                i.stop = function() {
                    e.clearTimeout(o)
                }
            })
        },
        function() {
            var e = G.createElement("input"),
                t = G.createElement("select"),
                n = t.appendChild(G.createElement("option"));
            e.type = "checkbox", ie.checkOn = "" !== e.value, ie.optSelected = n.selected, t.disabled = !0, ie.optDisabled = !n.disabled, e = G.createElement("input"), e.value = "t", e.type = "radio", ie.radioValue = "t" === e.value
        }();
    var lt, ct = re.expr.attrHandle;
    re.fn.extend({
        attr: function(e, t) {
            return Te(this, re.attr, e, t, arguments.length > 1)
        },
        removeAttr: function(e) {
            return this.each(function() {
                re.removeAttr(this, e)
            })
        }
    }), re.extend({
        attr: function(e, t, n) {
            var i, o, r = e.nodeType;
            if (3 !== r && 8 !== r && 2 !== r) return "undefined" == typeof e.getAttribute ? re.prop(e, t, n) : (1 === r && re.isXMLDoc(e) || (t = t.toLowerCase(), o = re.attrHooks[t] || (re.expr.match.bool.test(t) ? lt : void 0)), void 0 !== n ? null === n ? void re.removeAttr(e, t) : o && "set" in o && void 0 !== (i = o.set(e, n, t)) ? i : (e.setAttribute(t, n + ""), n) : o && "get" in o && null !== (i = o.get(e, t)) ? i : (i = re.find.attr(e, t), null == i ? void 0 : i))
        },
        attrHooks: {
            type: {
                set: function(e, t) {
                    if (!ie.radioValue && "radio" === t && re.nodeName(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t
                    }
                }
            }
        },
        removeAttr: function(e, t) {
            var n, i, o = 0,
                r = t && t.match(xe);
            if (r && 1 === e.nodeType)
                for (; n = r[o++];) i = re.propFix[n] || n, re.expr.match.bool.test(n) && (e[i] = !1), e.removeAttribute(n)
        }
    }), lt = {
        set: function(e, t, n) {
            return t === !1 ? re.removeAttr(e, n) : e.setAttribute(n, n), n
        }
    }, re.each(re.expr.match.bool.source.match(/\w+/g), function(e, t) {
        var n = ct[t] || re.find.attr;
        ct[t] = function(e, t, i) {
            var o, r;
            return i || (r = ct[t], ct[t] = o, o = null != n(e, t, i) ? t.toLowerCase() : null, ct[t] = r), o
        }
    });
    var ut = /^(?:input|select|textarea|button)$/i,
        dt = /^(?:a|area)$/i;
    re.fn.extend({
        prop: function(e, t) {
            return Te(this, re.prop, e, t, arguments.length > 1)
        },
        removeProp: function(e) {
            return this.each(function() {
                delete this[re.propFix[e] || e]
            })
        }
    }), re.extend({
        prop: function(e, t, n) {
            var i, o, r = e.nodeType;
            if (3 !== r && 8 !== r && 2 !== r) return 1 === r && re.isXMLDoc(e) || (t = re.propFix[t] || t, o = re.propHooks[t]), void 0 !== n ? o && "set" in o && void 0 !== (i = o.set(e, n, t)) ? i : e[t] = n : o && "get" in o && null !== (i = o.get(e, t)) ? i : e[t]
        },
        propHooks: {
            tabIndex: {
                get: function(e) {
                    var t = re.find.attr(e, "tabindex");
                    return t ? parseInt(t, 10) : ut.test(e.nodeName) || dt.test(e.nodeName) && e.href ? 0 : -1
                }
            }
        },
        propFix: {
            for: "htmlFor",
            class: "className"
        }
    }), ie.optSelected || (re.propHooks.selected = {
        get: function(e) {
            var t = e.parentNode;
            return t && t.parentNode && t.parentNode.selectedIndex, null
        },
        set: function(e) {
            var t = e.parentNode;
            t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
        }
    }), re.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        re.propFix[this.toLowerCase()] = this
    });
    var pt = /[\t\r\n\f]/g;
    re.fn.extend({
        addClass: function(e) {
            var t, n, i, o, r, s, a, l = 0;
            if (re.isFunction(e)) return this.each(function(t) {
                re(this).addClass(e.call(this, t, H(this)))
            });
            if ("string" == typeof e && e)
                for (t = e.match(xe) || []; n = this[l++];)
                    if (o = H(n), i = 1 === n.nodeType && (" " + o + " ").replace(pt, " ")) {
                        for (s = 0; r = t[s++];) i.indexOf(" " + r + " ") < 0 && (i += r + " ");
                        a = re.trim(i), o !== a && n.setAttribute("class", a)
                    } return this
        },
        removeClass: function(e) {
            var t, n, i, o, r, s, a, l = 0;
            if (re.isFunction(e)) return this.each(function(t) {
                re(this).removeClass(e.call(this, t, H(this)))
            });
            if (!arguments.length) return this.attr("class", "");
            if ("string" == typeof e && e)
                for (t = e.match(xe) || []; n = this[l++];)
                    if (o = H(n), i = 1 === n.nodeType && (" " + o + " ").replace(pt, " ")) {
                        for (s = 0; r = t[s++];)
                            for (; i.indexOf(" " + r + " ") > -1;) i = i.replace(" " + r + " ", " ");
                        a = re.trim(i), o !== a && n.setAttribute("class", a)
                    } return this
        },
        toggleClass: function(e, t) {
            var n = typeof e;
            return "boolean" == typeof t && "string" === n ? t ? this.addClass(e) : this.removeClass(e) : re.isFunction(e) ? this.each(function(n) {
                re(this).toggleClass(e.call(this, n, H(this), t), t)
            }) : this.each(function() {
                var t, i, o, r;
                if ("string" === n)
                    for (i = 0, o = re(this), r = e.match(xe) || []; t = r[i++];) o.hasClass(t) ? o.removeClass(t) : o.addClass(t);
                else void 0 !== e && "boolean" !== n || (t = H(this), t && _e.set(this, "__className__", t), this.setAttribute && this.setAttribute("class", t || e === !1 ? "" : _e.get(this, "__className__") || ""))
            })
        },
        hasClass: function(e) {
            var t, n, i = 0;
            for (t = " " + e + " "; n = this[i++];)
                if (1 === n.nodeType && (" " + H(n) + " ").replace(pt, " ").indexOf(t) > -1) return !0;
            return !1
        }
    });
    var ht = /\r/g,
        ft = /[\x20\t\r\n\f]+/g;
    re.fn.extend({
        val: function(e) {
            var t, n, i, o = this[0]; {
                if (arguments.length) return i = re.isFunction(e), this.each(function(n) {
                    var o;
                    1 === this.nodeType && (o = i ? e.call(this, n, re(this).val()) : e, null == o ? o = "" : "number" == typeof o ? o += "" : re.isArray(o) && (o = re.map(o, function(e) {
                        return null == e ? "" : e + ""
                    })), t = re.valHooks[this.type] || re.valHooks[this.nodeName.toLowerCase()], t && "set" in t && void 0 !== t.set(this, o, "value") || (this.value = o))
                });
                if (o) return t = re.valHooks[o.type] || re.valHooks[o.nodeName.toLowerCase()], t && "get" in t && void 0 !== (n = t.get(o, "value")) ? n : (n = o.value, "string" == typeof n ? n.replace(ht, "") : null == n ? "" : n)
            }
        }
    }), re.extend({
        valHooks: {
            option: {
                get: function(e) {
                    var t = re.find.attr(e, "value");
                    return null != t ? t : re.trim(re.text(e)).replace(ft, " ")
                }
            },
            select: {
                get: function(e) {
                    for (var t, n, i = e.options, o = e.selectedIndex, r = "select-one" === e.type || o < 0, s = r ? null : [], a = r ? o + 1 : i.length, l = o < 0 ? a : r ? o : 0; l < a; l++)
                        if (n = i[l], (n.selected || l === o) && (ie.optDisabled ? !n.disabled : null === n.getAttribute("disabled")) && (!n.parentNode.disabled || !re.nodeName(n.parentNode, "optgroup"))) {
                            if (t = re(n).val(), r) return t;
                            s.push(t)
                        } return s
                },
                set: function(e, t) {
                    for (var n, i, o = e.options, r = re.makeArray(t), s = o.length; s--;) i = o[s], (i.selected = re.inArray(re.valHooks.option.get(i), r) > -1) && (n = !0);
                    return n || (e.selectedIndex = -1), r
                }
            }
        }
    }), re.each(["radio", "checkbox"], function() {
        re.valHooks[this] = {
            set: function(e, t) {
                if (re.isArray(t)) return e.checked = re.inArray(re(e).val(), t) > -1
            }
        }, ie.checkOn || (re.valHooks[this].get = function(e) {
            return null === e.getAttribute("value") ? "on" : e.value
        })
    });
    var mt = /^(?:focusinfocus|focusoutblur)$/;
    re.extend(re.event, {
        trigger: function(t, n, i, o) {
            var r, s, a, l, c, u, d, p = [i || G],
                h = ne.call(t, "type") ? t.type : t,
                f = ne.call(t, "namespace") ? t.namespace.split(".") : [];
            if (s = a = i = i || G, 3 !== i.nodeType && 8 !== i.nodeType && !mt.test(h + re.event.triggered) && (h.indexOf(".") > -1 && (f = h.split("."), h = f.shift(), f.sort()), c = h.indexOf(":") < 0 && "on" + h, t = t[re.expando] ? t : new re.Event(h, "object" == typeof t && t), t.isTrigger = o ? 2 : 3, t.namespace = f.join("."), t.rnamespace = t.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = i), n = null == n ? [t] : re.makeArray(n, [t]), d = re.event.special[h] || {}, o || !d.trigger || d.trigger.apply(i, n) !== !1)) {
                if (!o && !d.noBubble && !re.isWindow(i)) {
                    for (l = d.delegateType || h, mt.test(l + h) || (s = s.parentNode); s; s = s.parentNode) p.push(s), a = s;
                    a === (i.ownerDocument || G) && p.push(a.defaultView || a.parentWindow || e)
                }
                for (r = 0;
                    (s = p[r++]) && !t.isPropagationStopped();) t.type = r > 1 ? l : d.bindType || h, u = (_e.get(s, "events") || {})[t.type] && _e.get(s, "handle"), u && u.apply(s, n), u = c && s[c], u && u.apply && ke(s) && (t.result = u.apply(s, n), t.result === !1 && t.preventDefault());
                return t.type = h, o || t.isDefaultPrevented() || d._default && d._default.apply(p.pop(), n) !== !1 || !ke(i) || c && re.isFunction(i[h]) && !re.isWindow(i) && (a = i[c], a && (i[c] = null), re.event.triggered = h, i[h](), re.event.triggered = void 0, a && (i[c] = a)), t.result
            }
        },
        simulate: function(e, t, n) {
            var i = re.extend(new re.Event, n, {
                type: e,
                isSimulated: !0
            });
            re.event.trigger(i, null, t)
        }
    }), re.fn.extend({
        trigger: function(e, t) {
            return this.each(function() {
                re.event.trigger(e, t, this)
            })
        },
        triggerHandler: function(e, t) {
            var n = this[0];
            if (n) return re.event.trigger(e, t, n, !0)
        }
    }), re.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(e, t) {
        re.fn[t] = function(e, n) {
            return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
        }
    }), re.fn.extend({
        hover: function(e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        }
    }), ie.focusin = "onfocusin" in e, ie.focusin || re.each({
        focus: "focusin",
        blur: "focusout"
    }, function(e, t) {
        var n = function(e) {
            re.event.simulate(t, e.target, re.event.fix(e))
        };
        re.event.special[t] = {
            setup: function() {
                var i = this.ownerDocument || this,
                    o = _e.access(i, t);
                o || i.addEventListener(e, n, !0), _e.access(i, t, (o || 0) + 1)
            },
            teardown: function() {
                var i = this.ownerDocument || this,
                    o = _e.access(i, t) - 1;
                o ? _e.access(i, t, o) : (i.removeEventListener(e, n, !0), _e.remove(i, t))
            }
        }
    });
    var gt = e.location,
        vt = re.now(),
        yt = /\?/;
    re.parseJSON = function(e) {
        return JSON.parse(e + "")
    }, re.parseXML = function(t) {
        var n;
        if (!t || "string" != typeof t) return null;
        try {
            n = (new e.DOMParser).parseFromString(t, "text/xml")
        } catch (e) {
            n = void 0
        }
        return n && !n.getElementsByTagName("parsererror").length || re.error("Invalid XML: " + t), n
    };
    var wt = /#.*$/,
        bt = /([?&])_=[^&]*/,
        xt = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        Ct = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        Tt = /^(?:GET|HEAD)$/,
        kt = /^\/\//,
        _t = {},
        jt = {},
        St = "*/".concat("*"),
        It = G.createElement("a");
    It.href = gt.href, re.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: gt.href,
            type: "GET",
            isLocal: Ct.test(gt.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": St,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /\bxml\b/,
                html: /\bhtml/,
                json: /\bjson\b/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": re.parseJSON,
                "text xml": re.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(e, t) {
            return t ? B(B(e, re.ajaxSettings), t) : B(re.ajaxSettings, e)
        },
        ajaxPrefilter: M(_t),
        ajaxTransport: M(jt),
        ajax: function(t, n) {
            function i(t, n, i, a) {
                var c, d, y, w, x, T = n;
                2 !== b && (b = 2, l && e.clearTimeout(l), o = void 0, s = a || "", C.readyState = t > 0 ? 4 : 0, c = t >= 200 && t < 300 || 304 === t, i && (w = F(p, C, i)), w = X(p, w, C, c), c ? (p.ifModified && (x = C.getResponseHeader("Last-Modified"), x && (re.lastModified[r] = x), x = C.getResponseHeader("etag"), x && (re.etag[r] = x)), 204 === t || "HEAD" === p.type ? T = "nocontent" : 304 === t ? T = "notmodified" : (T = w.state, d = w.data, y = w.error, c = !y)) : (y = T, !t && T || (T = "error", t < 0 && (t = 0))), C.status = t, C.statusText = (n || T) + "", c ? m.resolveWith(h, [d, T, C]) : m.rejectWith(h, [C, T, y]), C.statusCode(v), v = void 0, u && f.trigger(c ? "ajaxSuccess" : "ajaxError", [C, p, c ? d : y]), g.fireWith(h, [C, T]), u && (f.trigger("ajaxComplete", [C, p]), --re.active || re.event.trigger("ajaxStop")))
            }
            "object" == typeof t && (n = t, t = void 0), n = n || {};
            var o, r, s, a, l, c, u, d, p = re.ajaxSetup({}, n),
                h = p.context || p,
                f = p.context && (h.nodeType || h.jquery) ? re(h) : re.event,
                m = re.Deferred(),
                g = re.Callbacks("once memory"),
                v = p.statusCode || {},
                y = {},
                w = {},
                b = 0,
                x = "canceled",
                C = {
                    readyState: 0,
                    getResponseHeader: function(e) {
                        var t;
                        if (2 === b) {
                            if (!a)
                                for (a = {}; t = xt.exec(s);) a[t[1].toLowerCase()] = t[2];
                            t = a[e.toLowerCase()]
                        }
                        return null == t ? null : t
                    },
                    getAllResponseHeaders: function() {
                        return 2 === b ? s : null
                    },
                    setRequestHeader: function(e, t) {
                        var n = e.toLowerCase();
                        return b || (e = w[n] = w[n] || e, y[e] = t), this
                    },
                    overrideMimeType: function(e) {
                        return b || (p.mimeType = e), this
                    },
                    statusCode: function(e) {
                        var t;
                        if (e)
                            if (b < 2)
                                for (t in e) v[t] = [v[t], e[t]];
                            else C.always(e[C.status]);
                        return this
                    },
                    abort: function(e) {
                        var t = e || x;
                        return o && o.abort(t), i(0, t), this
                    }
                };
            if (m.promise(C).complete = g.add, C.success = C.done, C.error = C.fail, p.url = ((t || p.url || gt.href) + "").replace(wt, "").replace(kt, gt.protocol + "//"), p.type = n.method || n.type || p.method || p.type, p.dataTypes = re.trim(p.dataType || "*").toLowerCase().match(xe) || [""], null == p.crossDomain) {
                c = G.createElement("a");
                try {
                    c.href = p.url, c.href = c.href, p.crossDomain = It.protocol + "//" + It.host != c.protocol + "//" + c.host
                } catch (e) {
                    p.crossDomain = !0
                }
            }
            if (p.data && p.processData && "string" != typeof p.data && (p.data = re.param(p.data, p.traditional)), R(_t, p, n, C), 2 === b) return C;
            u = re.event && p.global, u && 0 === re.active++ && re.event.trigger("ajaxStart"), p.type = p.type.toUpperCase(), p.hasContent = !Tt.test(p.type), r = p.url, p.hasContent || (p.data && (r = p.url += (yt.test(r) ? "&" : "?") + p.data, delete p.data), p.cache === !1 && (p.url = bt.test(r) ? r.replace(bt, "$1_=" + vt++) : r + (yt.test(r) ? "&" : "?") + "_=" + vt++)), p.ifModified && (re.lastModified[r] && C.setRequestHeader("If-Modified-Since", re.lastModified[r]), re.etag[r] && C.setRequestHeader("If-None-Match", re.etag[r])), (p.data && p.hasContent && p.contentType !== !1 || n.contentType) && C.setRequestHeader("Content-Type", p.contentType), C.setRequestHeader("Accept", p.dataTypes[0] && p.accepts[p.dataTypes[0]] ? p.accepts[p.dataTypes[0]] + ("*" !== p.dataTypes[0] ? ", " + St + "; q=0.01" : "") : p.accepts["*"]);
            for (d in p.headers) C.setRequestHeader(d, p.headers[d]);
            if (p.beforeSend && (p.beforeSend.call(h, C, p) === !1 || 2 === b)) return C.abort();
            x = "abort";
            for (d in {
                    success: 1,
                    error: 1,
                    complete: 1
                }) C[d](p[d]);
            if (o = R(jt, p, n, C)) {
                if (C.readyState = 1, u && f.trigger("ajaxSend", [C, p]), 2 === b) return C;
                p.async && p.timeout > 0 && (l = e.setTimeout(function() {
                    C.abort("timeout")
                }, p.timeout));
                try {
                    b = 1, o.send(y, i)
                } catch (e) {
                    if (!(b < 2)) throw e;
                    i(-1, e)
                }
            } else i(-1, "No Transport");
            return C
        },
        getJSON: function(e, t, n) {
            return re.get(e, t, n, "json")
        },
        getScript: function(e, t) {
            return re.get(e, void 0, t, "script")
        }
    }), re.each(["get", "post"], function(e, t) {
        re[t] = function(e, n, i, o) {
            return re.isFunction(n) && (o = o || i, i = n, n = void 0), re.ajax(re.extend({
                url: e,
                type: t,
                dataType: o,
                data: n,
                success: i
            }, re.isPlainObject(e) && e))
        }
    }), re._evalUrl = function(e) {
        return re.ajax({
            url: e,
            type: "GET",
            dataType: "script",
            async: !1,
            global: !1,
            throws: !0
        })
    }, re.fn.extend({
        wrapAll: function(e) {
            var t;
            return re.isFunction(e) ? this.each(function(t) {
                re(this).wrapAll(e.call(this, t))
            }) : (this[0] && (t = re(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                for (var e = this; e.firstElementChild;) e = e.firstElementChild;
                return e
            }).append(this)), this)
        },
        wrapInner: function(e) {
            return re.isFunction(e) ? this.each(function(t) {
                re(this).wrapInner(e.call(this, t))
            }) : this.each(function() {
                var t = re(this),
                    n = t.contents();
                n.length ? n.wrapAll(e) : t.append(e)
            })
        },
        wrap: function(e) {
            var t = re.isFunction(e);
            return this.each(function(n) {
                re(this).wrapAll(t ? e.call(this, n) : e)
            })
        },
        unwrap: function() {
            return this.parent().each(function() {
                re.nodeName(this, "body") || re(this).replaceWith(this.childNodes)
            }).end()
        }
    }), re.expr.filters.hidden = function(e) {
        return !re.expr.filters.visible(e)
    }, re.expr.filters.visible = function(e) {
        return e.offsetWidth > 0 || e.offsetHeight > 0 || e.getClientRects().length > 0
    };
    var $t = /%20/g,
        Et = /\[\]$/,
        Dt = /\r?\n/g,
        Pt = /^(?:submit|button|image|reset|file)$/i,
        At = /^(?:input|select|textarea|keygen)/i;
    re.param = function(e, t) {
        var n, i = [],
            o = function(e, t) {
                t = re.isFunction(t) ? t() : null == t ? "" : t, i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
            };
        if (void 0 === t && (t = re.ajaxSettings && re.ajaxSettings.traditional), re.isArray(e) || e.jquery && !re.isPlainObject(e)) re.each(e, function() {
            o(this.name, this.value)
        });
        else
            for (n in e) V(n, e[n], t, o);
        return i.join("&").replace($t, "+")
    }, re.fn.extend({
        serialize: function() {
            return re.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var e = re.prop(this, "elements");
                return e ? re.makeArray(e) : this
            }).filter(function() {
                var e = this.type;
                return this.name && !re(this).is(":disabled") && At.test(this.nodeName) && !Pt.test(e) && (this.checked || !Ae.test(e))
            }).map(function(e, t) {
                var n = re(this).val();
                return null == n ? null : re.isArray(n) ? re.map(n, function(e) {
                    return {
                        name: t.name,
                        value: e.replace(Dt, "\r\n")
                    }
                }) : {
                    name: t.name,
                    value: n.replace(Dt, "\r\n")
                }
            }).get()
        }
    }), re.ajaxSettings.xhr = function() {
        try {
            return new e.XMLHttpRequest
        } catch (e) {}
    };
    var qt = {
            0: 200,
            1223: 204
        },
        Nt = re.ajaxSettings.xhr();
    ie.cors = !!Nt && "withCredentials" in Nt, ie.ajax = Nt = !!Nt, re.ajaxTransport(function(t) {
        var n, i;
        if (ie.cors || Nt && !t.crossDomain) return {
            send: function(o, r) {
                var s, a = t.xhr();
                if (a.open(t.type, t.url, t.async, t.username, t.password), t.xhrFields)
                    for (s in t.xhrFields) a[s] = t.xhrFields[s];
                t.mimeType && a.overrideMimeType && a.overrideMimeType(t.mimeType), t.crossDomain || o["X-Requested-With"] || (o["X-Requested-With"] = "XMLHttpRequest");
                for (s in o) a.setRequestHeader(s, o[s]);
                n = function(e) {
                    return function() {
                        n && (n = i = a.onload = a.onerror = a.onabort = a.onreadystatechange = null, "abort" === e ? a.abort() : "error" === e ? "number" != typeof a.status ? r(0, "error") : r(a.status, a.statusText) : r(qt[a.status] || a.status, a.statusText, "text" !== (a.responseType || "text") || "string" != typeof a.responseText ? {
                            binary: a.response
                        } : {
                            text: a.responseText
                        }, a.getAllResponseHeaders()))
                    }
                }, a.onload = n(), i = a.onerror = n("error"), void 0 !== a.onabort ? a.onabort = i : a.onreadystatechange = function() {
                    4 === a.readyState && e.setTimeout(function() {
                        n && i()
                    })
                }, n = n("abort");
                try {
                    a.send(t.hasContent && t.data || null)
                } catch (e) {
                    if (n) throw e
                }
            },
            abort: function() {
                n && n()
            }
        }
    }), re.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /\b(?:java|ecma)script\b/
        },
        converters: {
            "text script": function(e) {
                return re.globalEval(e), e
            }
        }
    }), re.ajaxPrefilter("script", function(e) {
        void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
    }), re.ajaxTransport("script", function(e) {
        if (e.crossDomain) {
            var t, n;
            return {
                send: function(i, o) {
                    t = re("<script>").prop({
                        charset: e.scriptCharset,
                        src: e.url
                    }).on("load error", n = function(e) {
                        t.remove(), n = null, e && o("error" === e.type ? 404 : 200, e.type)
                    }), G.head.appendChild(t[0])
                },
                abort: function() {
                    n && n()
                }
            }
        }
    });
    var Lt = [],
        zt = /(=)\?(?=&|$)|\?\?/;
    re.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var e = Lt.pop() || re.expando + "_" + vt++;
            return this[e] = !0, e
        }
    }), re.ajaxPrefilter("json jsonp", function(t, n, i) {
        var o, r, s, a = t.jsonp !== !1 && (zt.test(t.url) ? "url" : "string" == typeof t.data && 0 === (t.contentType || "").indexOf("application/x-www-form-urlencoded") && zt.test(t.data) && "data");
        if (a || "jsonp" === t.dataTypes[0]) return o = t.jsonpCallback = re.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, a ? t[a] = t[a].replace(zt, "$1" + o) : t.jsonp !== !1 && (t.url += (yt.test(t.url) ? "&" : "?") + t.jsonp + "=" + o), t.converters["script json"] = function() {
            return s || re.error(o + " was not called"), s[0]
        }, t.dataTypes[0] = "json", r = e[o], e[o] = function() {
            s = arguments
        }, i.always(function() {
            void 0 === r ? re(e).removeProp(o) : e[o] = r, t[o] && (t.jsonpCallback = n.jsonpCallback, Lt.push(o)), s && re.isFunction(r) && r(s[0]), s = r = void 0
        }), "script"
    }), re.parseHTML = function(e, t, n) {
        if (!e || "string" != typeof e) return null;
        "boolean" == typeof t && (n = t, t = !1), t = t || G;
        var i = fe.exec(e),
            o = !n && [];
        return i ? [t.createElement(i[1])] : (i = p([e], t, o), o && o.length && re(o).remove(), re.merge([], i.childNodes))
    };
    var Wt = re.fn.load;
    re.fn.load = function(e, t, n) {
        if ("string" != typeof e && Wt) return Wt.apply(this, arguments);
        var i, o, r, s = this,
            a = e.indexOf(" ");
        return a > -1 && (i = re.trim(e.slice(a)), e = e.slice(0, a)), re.isFunction(t) ? (n = t, t = void 0) : t && "object" == typeof t && (o = "POST"), s.length > 0 && re.ajax({
            url: e,
            type: o || "GET",
            dataType: "html",
            data: t
        }).done(function(e) {
            r = arguments, s.html(i ? re("<div>").append(re.parseHTML(e)).find(i) : e)
        }).always(n && function(e, t) {
            s.each(function() {
                n.apply(this, r || [e.responseText, t, e])
            })
        }), this
    }, re.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
        re.fn[t] = function(e) {
            return this.on(t, e)
        }
    }), re.expr.filters.animated = function(e) {
        return re.grep(re.timers, function(t) {
            return e === t.elem
        }).length
    }, re.offset = {
        setOffset: function(e, t, n) {
            var i, o, r, s, a, l, c, u = re.css(e, "position"),
                d = re(e),
                p = {};
            "static" === u && (e.style.position = "relative"), a = d.offset(), r = re.css(e, "top"), l = re.css(e, "left"), c = ("absolute" === u || "fixed" === u) && (r + l).indexOf("auto") > -1, c ? (i = d.position(), s = i.top, o = i.left) : (s = parseFloat(r) || 0, o = parseFloat(l) || 0), re.isFunction(t) && (t = t.call(e, n, re.extend({}, a))), null != t.top && (p.top = t.top - a.top + s), null != t.left && (p.left = t.left - a.left + o), "using" in t ? t.using.call(e, p) : d.css(p)
        }
    }, re.fn.extend({
        offset: function(e) {
            if (arguments.length) return void 0 === e ? this : this.each(function(t) {
                re.offset.setOffset(this, e, t)
            });
            var t, n, i = this[0],
                o = {
                    top: 0,
                    left: 0
                },
                r = i && i.ownerDocument;
            if (r) return t = r.documentElement, re.contains(t, i) ? (o = i.getBoundingClientRect(), n = Y(r), {
                top: o.top + n.pageYOffset - t.clientTop,
                left: o.left + n.pageXOffset - t.clientLeft
            }) : o
        },
        position: function() {
            if (this[0]) {
                var e, t, n = this[0],
                    i = {
                        top: 0,
                        left: 0
                    };
                return "fixed" === re.css(n, "position") ? t = n.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), re.nodeName(e[0], "html") || (i = e.offset()), i.top += re.css(e[0], "borderTopWidth", !0), i.left += re.css(e[0], "borderLeftWidth", !0)), {
                    top: t.top - i.top - re.css(n, "marginTop", !0),
                    left: t.left - i.left - re.css(n, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var e = this.offsetParent; e && "static" === re.css(e, "position");) e = e.offsetParent;
                return e || Je
            })
        }
    }), re.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(e, t) {
        var n = "pageYOffset" === t;
        re.fn[e] = function(i) {
            return Te(this, function(e, i, o) {
                var r = Y(e);
                return void 0 === o ? r ? r[t] : e[i] : void(r ? r.scrollTo(n ? r.pageXOffset : o, n ? o : r.pageYOffset) : e[i] = o)
            }, e, i, arguments.length)
        }
    }), re.each(["top", "left"], function(e, t) {
        re.cssHooks[t] = S(ie.pixelPosition, function(e, n) {
            if (n) return n = j(e, t), Ge.test(n) ? re(e).position()[t] + "px" : n
        })
    }), re.each({
        Height: "height",
        Width: "width"
    }, function(e, t) {
        re.each({
            padding: "inner" + e,
            content: t,
            "": "outer" + e
        }, function(n, i) {
            re.fn[i] = function(i, o) {
                var r = arguments.length && (n || "boolean" != typeof i),
                    s = n || (i === !0 || o === !0 ? "margin" : "border");
                return Te(this, function(t, n, i) {
                    var o;
                    return re.isWindow(t) ? t.document.documentElement["client" + e] : 9 === t.nodeType ? (o = t.documentElement, Math.max(t.body["scroll" + e], o["scroll" + e], t.body["offset" + e], o["offset" + e], o["client" + e])) : void 0 === i ? re.css(t, n, s) : re.style(t, n, i, s)
                }, t, r ? i : void 0, r, null)
            }
        })
    }), re.fn.extend({
        bind: function(e, t, n) {
            return this.on(e, null, t, n)
        },
        unbind: function(e, t) {
            return this.off(e, null, t)
        },
        delegate: function(e, t, n, i) {
            return this.on(t, e, n, i)
        },
        undelegate: function(e, t, n) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
        },
        size: function() {
            return this.length
        }
    }), re.fn.andSelf = re.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function() {
        return re
    });
    var Ot = e.jQuery,
        Ht = e.$;
    return re.noConflict = function(t) {
        return e.$ === re && (e.$ = Ht), t && e.jQuery === re && (e.jQuery = Ot), re
    }, t || (e.jQuery = e.$ = re), re
}), ! function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
}(function(e) {
    "use strict";

    function t(t, n) {
        this.element = t, this.options = e.extend({}, o, n), this.init()
    }

    function n(t) {
        if (!e(t.target).parents().hasClass("jq-selectbox") && "OPTION" != t.target.nodeName && e("div.jq-selectbox.opened").length) {
            var n = e("div.jq-selectbox.opened"),
                o = e("div.jq-selectbox__search input", n),
                r = e("div.jq-selectbox__dropdown", n),
                s = n.find("select").data("_" + i).options;
            s.onSelectClosed.call(n), o.length && o.val("").keyup(), r.hide().find("li.sel").addClass("selected"), n.removeClass("focused opened dropup dropdown")
        }
    }
    var i = "styler",
        o = {
            idSuffix: "-styler",
            filePlaceholder: "Файл не выбран",
            fileBrowse: "Обзор...",
            fileNumber: "Выбрано файлов: %s",
            selectPlaceholder: "Выберите...",
            selectSearch: !1,
            selectSearchLimit: 10,
            selectSearchNotFound: "Совпадений не найдено",
            selectSearchPlaceholder: "Поиск...",
            selectVisibleOptions: 0,
            singleSelectzIndex: "100",
            selectSmartPositioning: !0,
            onSelectOpened: function() {},
            onSelectClosed: function() {},
            onFormStyled: function() {}
        };
    t.prototype = {
        init: function() {
            function t() {
                void 0 !== i.attr("id") && "" !== i.attr("id") && (this.id = i.attr("id") + o.idSuffix), this.title = i.attr("title"), this.classes = i.attr("class"), this.data = i.data()
            }
            var i = e(this.element),
                o = this.options,
                r = !(!navigator.userAgent.match(/(iPad|iPhone|iPod)/i) || navigator.userAgent.match(/(Windows\sPhone)/i)),
                s = !(!navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/(Windows\sPhone)/i));
            if (i.is(":checkbox")) {
                var a = function() {
                    var n = new t,
                        o = e('<div class="jq-checkbox"><div class="jq-checkbox__div"></div></div>').attr({
                            id: n.id,
                            title: n.title
                        }).addClass(n.classes).data(n.data);
                    i.css({
                        position: "absolute",
                        zIndex: "-1",
                        opacity: 0,
                        margin: 0,
                        padding: 0
                    }).after(o).prependTo(o), o.attr("unselectable", "on").css({
                        "-webkit-user-select": "none",
                        "-moz-user-select": "none",
                        "-ms-user-select": "none",
                        "-o-user-select": "none",
                        "user-select": "none",
                        display: "inline-block",
                        position: "relative",
                        overflow: "hidden"
                    }), i.is(":checked") && o.addClass("checked"), i.is(":disabled") && o.addClass("disabled"), o.click(function(e) {
                        e.preventDefault(), o.is(".disabled") || (i.is(":checked") ? (i.prop("checked", !1), o.removeClass("checked")) : (i.prop("checked", !0), o.addClass("checked")), i.focus().change())
                    }), i.closest("label").add('label[for="' + i.attr("id") + '"]').on("click.styler", function(t) {
                        e(t.target).is("a") || e(t.target).closest(o).length || (o.triggerHandler("click"), t.preventDefault())
                    }), i.on("change.styler", function() {
                        i.is(":checked") ? o.addClass("checked") : o.removeClass("checked")
                    }).on("keydown.styler", function(e) {
                        32 == e.which && o.click()
                    }).on("focus.styler", function() {
                        o.is(".disabled") || o.addClass("focused")
                    }).on("blur.styler", function() {
                        o.removeClass("focused")
                    })
                };
                a(), i.on("refresh", function() {
                    i.closest("label").add('label[for="' + i.attr("id") + '"]').off(".styler"), i.off(".styler").parent().before(i).remove(), a()
                })
            } else if (i.is(":radio")) {
                var l = function() {
                    var n = new t,
                        o = e('<div class="jq-radio"><div class="jq-radio__div"></div></div>').attr({
                            id: n.id,
                            title: n.title
                        }).addClass(n.classes).data(n.data);
                    i.css({
                        position: "absolute",
                        zIndex: "-1",
                        opacity: 0,
                        margin: 0,
                        padding: 0
                    }).after(o).prependTo(o), o.attr("unselectable", "on").css({
                        "-webkit-user-select": "none",
                        "-moz-user-select": "none",
                        "-ms-user-select": "none",
                        "-o-user-select": "none",
                        "user-select": "none",
                        display: "inline-block",
                        position: "relative"
                    }), i.is(":checked") && o.addClass("checked"), i.is(":disabled") && o.addClass("disabled"), e.fn.commonParents = function() {
                        var t = this;
                        return t.first().parents().filter(function() {
                            return e(this).find(t).length === t.length
                        })
                    }, e.fn.commonParent = function() {
                        return e(this).commonParents().first()
                    }, o.click(function(t) {
                        if (t.preventDefault(), !o.is(".disabled")) {
                            var n = e('input[name="' + i.attr("name") + '"]');
                            n.commonParent().find(n).prop("checked", !1).parent().removeClass("checked"), i.prop("checked", !0).parent().addClass("checked"), i.focus().change()
                        }
                    }), i.closest("label").add('label[for="' + i.attr("id") + '"]').on("click.styler", function(t) {
                        e(t.target).is("a") || e(t.target).closest(o).length || (o.triggerHandler("click"), t.preventDefault())
                    }), i.on("change.styler", function() {
                        i.parent().addClass("checked")
                    }).on("focus.styler", function() {
                        o.is(".disabled") || o.addClass("focused")
                    }).on("blur.styler", function() {
                        o.removeClass("focused")
                    })
                };
                l(), i.on("refresh", function() {
                    i.closest("label").add('label[for="' + i.attr("id") + '"]').off(".styler"), i.off(".styler").parent().before(i).remove(), l()
                })
            } else if (i.is(":file")) {
                i.css({
                    position: "absolute",
                    top: 0,
                    right: 0,
                    margin: 0,
                    padding: 0,
                    opacity: 0,
                    fontSize: "100px"
                });
                var c = function() {
                    var n = new t,
                        r = i.data("placeholder");
                    void 0 === r && (r = o.filePlaceholder);
                    var s = i.data("browse");
                    void 0 !== s && "" !== s || (s = o.fileBrowse);
                    var a = e('<div class="jq-file"><div class="jq-file__name">' + r + '</div><div class="jq-file__browse">' + s + "</div></div>").css({
                        display: "inline-block",
                        position: "relative",
                        overflow: "hidden"
                    }).attr({
                        id: n.id,
                        title: n.title
                    }).addClass(n.classes).data(n.data);
                    i.after(a).appendTo(a), i.is(":disabled") && a.addClass("disabled"), i.on("change.styler", function() {
                        var t = i.val(),
                            n = e("div.jq-file__name", a);
                        if (i.is("[multiple]")) {
                            t = "";
                            var s = i[0].files.length;
                            if (s > 0) {
                                var l = i.data("number");
                                void 0 === l && (l = o.fileNumber), l = l.replace("%s", s), t = l
                            }
                        }
                        n.text(t.replace(/.+[\\\/]/, "")), "" === t ? (n.text(r), a.removeClass("changed")) : a.addClass("changed")
                    }).on("focus.styler", function() {
                        a.addClass("focused")
                    }).on("blur.styler", function() {
                        a.removeClass("focused")
                    }).on("click.styler", function() {
                        a.removeClass("focused")
                    })
                };
                c(), i.on("refresh", function() {
                    i.off(".styler").parent().before(i).remove(), c()
                })
            } else if (i.is('input[type="number"]')) {
                var u = function() {
                    var n = new t,
                        o = e('<div class="jq-number"><div class="jq-number__spin minus"></div><div class="jq-number__spin plus"></div></div>').attr({
                            id: n.id,
                            title: n.title
                        }).addClass(n.classes).data(n.data);
                    i.after(o).prependTo(o).wrap('<div class="jq-number__field"></div>'), i.is(":disabled") && o.addClass("disabled");
                    var r, s, a, l = null,
                        c = null;
                    void 0 !== i.attr("min") && (r = i.attr("min")), void 0 !== i.attr("max") && (s = i.attr("max")), a = void 0 !== i.attr("step") && e.isNumeric(i.attr("step")) ? Number(i.attr("step")) : Number(1);
                    var u = function(t) {
                        var n, o = i.val();
                        e.isNumeric(o) || (o = 0, i.val("0")), t.is(".minus") ? n = Number(o) - a : t.is(".plus") && (n = Number(o) + a);
                        var l = (a.toString().split(".")[1] || []).length;
                        if (l > 0) {
                            for (var c = "1"; c.length <= l;) c += "0";
                            n = Math.round(n * c) / c
                        }
                        e.isNumeric(r) && e.isNumeric(s) ? n >= r && s >= n && i.val(n) : e.isNumeric(r) && !e.isNumeric(s) ? n >= r && i.val(n) : !e.isNumeric(r) && e.isNumeric(s) ? s >= n && i.val(n) : i.val(n)
                    };
                    o.is(".disabled") || (o.on("mousedown", "div.jq-number__spin", function() {
                        var t = e(this);
                        u(t), l = setTimeout(function() {
                            c = setInterval(function() {
                                u(t)
                            }, 40)
                        }, 350)
                    }).on("mouseup mouseout", "div.jq-number__spin", function() {
                        clearTimeout(l), clearInterval(c)
                    }).on("mouseup", "div.jq-number__spin", function() {
                        i.change()
                    }), i.on("focus.styler", function() {
                        o.addClass("focused")
                    }).on("blur.styler", function() {
                        o.removeClass("focused")
                    }))
                };
                u(), i.on("refresh", function() {
                    i.off(".styler").closest(".jq-number").before(i).remove(), u()
                })
            } else if (i.is("select")) {
                var d = function() {
                    function a(t) {
                        t.off("mousewheel DOMMouseScroll").on("mousewheel DOMMouseScroll", function(t) {
                            var n = null;
                            "mousewheel" == t.type ? n = -1 * t.originalEvent.wheelDelta : "DOMMouseScroll" == t.type && (n = 40 * t.originalEvent.detail), n && (t.stopPropagation(), t.preventDefault(), e(this).scrollTop(n + e(this).scrollTop()))
                        })
                    }

                    function l() {
                        for (var e = 0; e < d.length; e++) {
                            var t = d.eq(e),
                                n = "",
                                i = "",
                                r = "",
                                s = "",
                                a = "",
                                l = "",
                                c = "",
                                u = "",
                                h = "",
                                f = "disabled",
                                m = "selected sel disabled";
                            t.prop("selected") && (i = "selected sel"), t.is(":disabled") && (i = f), t.is(":selected:disabled") && (i = m), void 0 !== t.attr("id") && "" !== t.attr("id") && (s = ' id="' + t.attr("id") + o.idSuffix + '"'), void 0 !== t.attr("title") && "" !== d.attr("title") && (a = ' title="' + t.attr("title") + '"'), void 0 !== t.attr("class") && (c = " " + t.attr("class"), h = ' data-jqfs-class="' + t.attr("class") + '"');
                            var g = t.data();
                            for (var v in g) "" !== g[v] && (l += " data-" + v + '="' + g[v] + '"');
                            i + c !== "" && (r = ' class="' + i + c + '"'), n = "<li" + h + l + r + a + s + ">" + t.html() + "</li>", t.parent().is("optgroup") && (void 0 !== t.parent().attr("class") && (u = " " + t.parent().attr("class")), n = "<li" + h + l + ' class="' + i + c + " option" + u + '"' + a + s + ">" + t.html() + "</li>", t.is(":first-child") && (n = '<li class="optgroup' + u + '">' + t.parent().attr("label") + "</li>" + n)), p += n
                        }
                    }

                    function c() {
                        var s = new t,
                            c = "",
                            u = i.data("placeholder"),
                            h = i.data("search"),
                            f = i.data("search-limit"),
                            m = i.data("search-not-found"),
                            g = i.data("search-placeholder"),
                            v = i.data("z-index"),
                            y = i.data("smart-positioning");
                        void 0 === u && (u = o.selectPlaceholder), void 0 !== h && "" !== h || (h = o.selectSearch), void 0 !== f && "" !== f || (f = o.selectSearchLimit), void 0 !== m && "" !== m || (m = o.selectSearchNotFound), void 0 === g && (g = o.selectSearchPlaceholder), void 0 !== v && "" !== v || (v = o.singleSelectzIndex), void 0 !== y && "" !== y || (y = o.selectSmartPositioning);
                        var w = e('<div class="jq-selectbox jqselect"><div class="jq-selectbox__select" style="position: relative"><div class="jq-selectbox__select-text"></div><div class="jq-selectbox__trigger"><div class="jq-selectbox__trigger-arrow"></div></div></div></div>').css({
                            display: "inline-block",
                            position: "relative",
                            zIndex: v
                        }).attr({
                            id: s.id,
                            title: s.title
                        }).addClass(s.classes).data(s.data);
                        i.css({
                            margin: 0,
                            padding: 0
                        }).after(w).prependTo(w);
                        var b = e("div.jq-selectbox__select", w),
                            x = e("div.jq-selectbox__select-text", w),
                            C = d.filter(":selected");
                        l(), h && (c = '<div class="jq-selectbox__search"><input type="search" autocomplete="off" placeholder="' + g + '"></div><div class="jq-selectbox__not-found">' + m + "</div>");
                        var T = e('<div class="jq-selectbox__dropdown" style="position: absolute">' + c + '<ul style="position: relative; list-style: none; overflow: auto; overflow-x: hidden">' + p + "</ul></div>");
                        w.append(T);
                        var k = e("ul", T),
                            _ = e("li", T),
                            j = e("input", T),
                            S = e("div.jq-selectbox__not-found", T).hide();
                        _.length < f && j.parent().hide(), "" === d.first().text() && d.first().is(":selected") ? x.text(u).addClass("placeholder") : x.text(C.text());
                        var I = 0,
                            $ = 0;
                        if (_.css({
                                display: "inline-block"
                            }), _.each(function() {
                                var t = e(this);
                                t.innerWidth() > I && (I = t.innerWidth(), $ = t.width())
                            }), _.css({
                                display: ""
                            }), x.is(".placeholder") && x.width() > I) x.width(x.width());
                        else {
                            var E = w.clone().appendTo("body").width("auto"),
                                D = E.outerWidth();
                            E.remove(), D == w.outerWidth() && x.width($)
                        }
                        I > w.width() && T.width(I), "" === d.first().text() && "" !== i.data("placeholder") && _.first().hide(), i.css({
                            position: "absolute",
                            left: 0,
                            top: 0,
                            width: "100%",
                            height: "100%",
                            opacity: 0
                        });
                        var P = w.outerHeight(!0),
                            A = j.parent().outerHeight(!0),
                            q = k.css("max-height"),
                            N = _.filter(".selected");
                        N.length < 1 && _.first().addClass("selected sel"), void 0 === _.data("li-height") && _.data("li-height", _.outerHeight());
                        var L = T.css("top");
                        if ("auto" == T.css("left") && T.css({
                                left: 0
                            }), "auto" == T.css("top") && (T.css({
                                top: P
                            }), L = P), T.hide(), N.length && (d.first().text() != C.text() && w.addClass("changed"), w.data("jqfs-class", N.data("jqfs-class")), w.addClass(N.data("jqfs-class"))), i.is(":disabled")) return w.addClass("disabled"), !1;
                        b.click(function() {
                            if (e("div.jq-selectbox").filter(".opened").length && o.onSelectClosed.call(e("div.jq-selectbox").filter(".opened")), i.focus(), !r) {
                                var t = e(window),
                                    n = _.data("li-height"),
                                    s = w.offset().top,
                                    l = t.height() - P - (s - t.scrollTop()),
                                    c = i.data("visible-options");
                                void 0 !== c && "" !== c || (c = o.selectVisibleOptions);
                                var u = 5 * n,
                                    p = n * c;
                                c > 0 && 6 > c && (u = p), 0 === c && (p = "auto");
                                var h = function() {
                                        T.height("auto").css({
                                            bottom: "auto",
                                            top: L
                                        });
                                        var e = function() {
                                            k.css("max-height", Math.floor((l - 20 - A) / n) * n)
                                        };
                                        e(), k.css("max-height", p), "none" != q && k.css("max-height", q), l < T.outerHeight() + 20 && e()
                                    },
                                    f = function() {
                                        T.height("auto").css({
                                            top: "auto",
                                            bottom: L
                                        });
                                        var e = function() {
                                            k.css("max-height", Math.floor((s - t.scrollTop() - 20 - A) / n) * n)
                                        };
                                        e(), k.css("max-height", p), "none" != q && k.css("max-height", q), s - t.scrollTop() - 20 < T.outerHeight() + 20 && e()
                                    };
                                y === !0 || 1 === y ? l > u + A + 20 ? (h(), w.removeClass("dropup").addClass("dropdown")) : (f(), w.removeClass("dropdown").addClass("dropup")) : y !== !1 && 0 !== y || l > u + A + 20 && (h(), w.removeClass("dropup").addClass("dropdown")), w.offset().left + T.outerWidth() > t.width() && T.css({
                                    left: "auto",
                                    right: 0
                                }), e("div.jqselect").css({
                                    zIndex: v - 1
                                }).removeClass("opened"), w.css({
                                    zIndex: v
                                }), T.is(":hidden") ? (e("div.jq-selectbox__dropdown:visible").hide(), T.show(), w.addClass("opened focused"), o.onSelectOpened.call(w)) : (T.hide(), w.removeClass("opened dropup dropdown"), e("div.jq-selectbox").filter(".opened").length && o.onSelectClosed.call(w)), j.length && (j.val("").keyup(), S.hide(), j.keyup(function() {
                                    var t = e(this).val();
                                    _.each(function() {
                                        e(this).html().match(new RegExp(".*?" + t + ".*?", "i")) ? e(this).show() : e(this).hide()
                                    }), "" === d.first().text() && "" !== i.data("placeholder") && _.first().hide(), _.filter(":visible").length < 1 ? S.show() : S.hide()
                                })), _.filter(".selected").length && ("" === i.val() ? k.scrollTop(0) : (k.innerHeight() / n % 2 !== 0 && (n /= 2), k.scrollTop(k.scrollTop() + _.filter(".selected").position().top - k.innerHeight() / 2 + n))), a(k)
                            }
                        }), _.hover(function() {
                            e(this).siblings().removeClass("selected")
                        });
                        var z = _.filter(".selected").text();
                        _.filter(":not(.disabled):not(.optgroup)").click(function() {
                            i.focus();
                            var t = e(this),
                                n = t.text();
                            if (!t.is(".selected")) {
                                var r = t.index();
                                r -= t.prevAll(".optgroup").length, t.addClass("selected sel").siblings().removeClass("selected sel"), d.prop("selected", !1).eq(r).prop("selected", !0), z = n, x.text(n), w.data("jqfs-class") && w.removeClass(w.data("jqfs-class")), w.data("jqfs-class", t.data("jqfs-class")), w.addClass(t.data("jqfs-class")), i.change()
                            }
                            T.hide(), w.removeClass("opened dropup dropdown"), o.onSelectClosed.call(w)
                        }), T.mouseout(function() {
                            e("li.sel", T).addClass("selected")
                        }), i.on("change.styler", function() {
                            x.text(d.filter(":selected").text()).removeClass("placeholder"), _.removeClass("selected sel").not(".optgroup").eq(i[0].selectedIndex).addClass("selected sel"), d.first().text() != _.filter(".selected").text() ? w.addClass("changed") : w.removeClass("changed")
                        }).on("focus.styler", function() {
                            w.addClass("focused"), e("div.jqselect").not(".focused").removeClass("opened dropup dropdown").find("div.jq-selectbox__dropdown").hide()
                        }).on("blur.styler", function() {
                            w.removeClass("focused")
                        }).on("keydown.styler keyup.styler", function(e) {
                            var t = _.data("li-height");
                            "" === i.val() ? x.text(u).addClass("placeholder") : x.text(d.filter(":selected").text()), _.removeClass("selected sel").not(".optgroup").eq(i[0].selectedIndex).addClass("selected sel"), 38 != e.which && 37 != e.which && 33 != e.which && 36 != e.which || ("" === i.val() ? k.scrollTop(0) : k.scrollTop(k.scrollTop() + _.filter(".selected").position().top)), 40 != e.which && 39 != e.which && 34 != e.which && 35 != e.which || k.scrollTop(k.scrollTop() + _.filter(".selected").position().top - k.innerHeight() + t), 13 == e.which && (e.preventDefault(), T.hide(), w.removeClass("opened dropup dropdown"), o.onSelectClosed.call(w))
                        }).on("keydown.styler", function(e) {
                            32 == e.which && (e.preventDefault(), b.click())
                        }), n.registered || (e(document).on("click", n), n.registered = !0)
                    }

                    function u() {
                        var n = new t,
                            o = e('<div class="jq-select-multiple jqselect"></div>').css({
                                display: "inline-block",
                                position: "relative"
                            }).attr({
                                id: n.id,
                                title: n.title
                            }).addClass(n.classes).data(n.data);
                        i.css({
                            margin: 0,
                            padding: 0
                        }).after(o), l(), o.append("<ul>" + p + "</ul>");
                        var r = e("ul", o).css({
                                position: "relative",
                                "overflow-x": "hidden",
                                "-webkit-overflow-scrolling": "touch"
                            }),
                            s = e("li", o).attr("unselectable", "on"),
                            c = i.attr("size"),
                            u = r.outerHeight(),
                            h = s.outerHeight();
                        void 0 !== c && c > 0 ? r.css({
                            height: h * c
                        }) : r.css({
                            height: 4 * h
                        }), u > o.height() && (r.css("overflowY", "scroll"), a(r), s.filter(".selected").length && r.scrollTop(r.scrollTop() + s.filter(".selected").position().top)), i.prependTo(o).css({
                            position: "absolute",
                            left: 0,
                            top: 0,
                            width: "100%",
                            height: "100%",
                            opacity: 0
                        }), i.is(":disabled") ? (o.addClass("disabled"), d.each(function() {
                            e(this).is(":selected") && s.eq(e(this).index()).addClass("selected")
                        })) : (s.filter(":not(.disabled):not(.optgroup)").click(function(t) {
                            i.focus();
                            var n = e(this);
                            if (t.ctrlKey || t.metaKey || n.addClass("selected"), t.shiftKey || n.addClass("first"), t.ctrlKey || t.metaKey || t.shiftKey || n.siblings().removeClass("selected first"), (t.ctrlKey || t.metaKey) && (n.is(".selected") ? n.removeClass("selected first") : n.addClass("selected first"), n.siblings().removeClass("first")), t.shiftKey) {
                                var o = !1,
                                    r = !1;
                                n.siblings().removeClass("selected").siblings(".first").addClass("selected"), n.prevAll().each(function() {
                                    e(this).is(".first") && (o = !0)
                                }), n.nextAll().each(function() {
                                    e(this).is(".first") && (r = !0)
                                }), o && n.prevAll().each(function() {
                                    return !e(this).is(".selected") && void e(this).not(".disabled, .optgroup").addClass("selected")
                                }), r && n.nextAll().each(function() {
                                    return !e(this).is(".selected") && void e(this).not(".disabled, .optgroup").addClass("selected")
                                }), 1 == s.filter(".selected").length && n.addClass("first")
                            }
                            d.prop("selected", !1), s.filter(".selected").each(function() {
                                var t = e(this),
                                    n = t.index();
                                t.is(".option") && (n -= t.prevAll(".optgroup").length), d.eq(n).prop("selected", !0)
                            }), i.change()
                        }), d.each(function(t) {
                            e(this).data("optionIndex", t)
                        }), i.on("change.styler", function() {
                            s.removeClass("selected");
                            var t = [];
                            d.filter(":selected").each(function() {
                                t.push(e(this).data("optionIndex"))
                            }), s.not(".optgroup").filter(function(n) {
                                return e.inArray(n, t) > -1
                            }).addClass("selected")
                        }).on("focus.styler", function() {
                            o.addClass("focused")
                        }).on("blur.styler", function() {
                            o.removeClass("focused")
                        }), u > o.height() && i.on("keydown.styler", function(e) {
                            38 != e.which && 37 != e.which && 33 != e.which || r.scrollTop(r.scrollTop() + s.filter(".selected").position().top - h), 40 != e.which && 39 != e.which && 34 != e.which || r.scrollTop(r.scrollTop() + s.filter(".selected:last").position().top - r.innerHeight() + 2 * h)
                        }))
                    }
                    var d = e("option", i),
                        p = "";
                    if (i.is("[multiple]")) {
                        if (s || r) return;
                        u()
                    } else c()
                };
                d(), i.on("refresh", function() {
                    i.off(".styler").parent().before(i).remove(), d()
                })
            } else i.is(":reset") && i.on("click", function() {
                setTimeout(function() {
                    i.closest("form").find("input, select").trigger("refresh")
                }, 1)
            })
        },
        destroy: function() {
            var t = e(this.element);
            t.is(":checkbox") || t.is(":radio") ? (t.removeData("_" + i).off(".styler refresh").removeAttr("style").parent().before(t).remove(), t.closest("label").add('label[for="' + t.attr("id") + '"]').off(".styler")) : t.is('input[type="number"]') ? t.removeData("_" + i).off(".styler refresh").closest(".jq-number").before(t).remove() : (t.is(":file") || t.is("select")) && t.removeData("_" + i).off(".styler refresh").removeAttr("style").parent().before(t).remove()
        }
    }, e.fn[i] = function(n) {
        var o = arguments;
        if (void 0 === n || "object" == typeof n) return this.each(function() {
            e.data(this, "_" + i) || e.data(this, "_" + i, new t(this, n))
        }).promise().done(function() {
            var t = e(this[0]).data("_" + i);
            t && t.options.onFormStyled.call()
        }), this;
        if ("string" == typeof n && "_" !== n[0] && "init" !== n) {
            var r;
            return this.each(function() {
                var s = e.data(this, "_" + i);
                s instanceof t && "function" == typeof s[n] && (r = s[n].apply(s, Array.prototype.slice.call(o, 1)))
            }), void 0 !== r ? r : this
        }
    }, n.registered = !1
}), "function" != typeof Object.create && (Object.create = function(e) {
        function t() {}
        return t.prototype = e, new t
    }),
    function(e, t, n) {
        var i = {
            init: function(t, n) {
                var i = this;
                i.$elem = e(n), i.options = e.extend({}, e.fn.owlCarousel.options, i.$elem.data(), t), i.userOptions = t, i.loadContent()
            },
            loadContent: function() {
                function t(e) {
                    var t, n = "";
                    if ("function" == typeof i.options.jsonSuccess) i.options.jsonSuccess.apply(this, [e]);
                    else {
                        for (t in e.owl) e.owl.hasOwnProperty(t) && (n += e.owl[t].item);
                        i.$elem.html(n)
                    }
                    i.logIn()
                }
                var n, i = this;
                "function" == typeof i.options.beforeInit && i.options.beforeInit.apply(this, [i.$elem]), "string" == typeof i.options.jsonPath ? (n = i.options.jsonPath, e.getJSON(n, t)) : i.logIn()
            },
            logIn: function() {
                var e = this;
                e.$elem.data("owl-originalStyles", e.$elem.attr("style")).data("owl-originalClasses", e.$elem.attr("class")), e.$elem.css({
                    opacity: 0
                }), e.orignalItems = e.options.items, e.checkBrowser(), e.wrapperWidth = 0, e.checkVisible = null, e.setVars()
            },
            setVars: function() {
                var e = this;
                return 0 !== e.$elem.children().length && (e.baseClass(), e.eventTypes(), e.$userItems = e.$elem.children(), e.itemsAmount = e.$userItems.length, e.wrapItems(), e.$owlItems = e.$elem.find(".owl-item"), e.$owlWrapper = e.$elem.find(".owl-wrapper"), e.playDirection = "next", e.prevItem = 0, e.prevArr = [0], e.currentItem = 0, e.customEvents(), void e.onStartup())
            },
            onStartup: function() {
                var e = this;
                e.updateItems(), e.calculateAll(), e.buildControls(), e.updateControls(), e.response(), e.moveEvents(), e.stopOnHover(), e.owlStatus(), e.options.transitionStyle !== !1 && e.transitionTypes(e.options.transitionStyle), e.options.autoPlay === !0 && (e.options.autoPlay = 5e3), e.play(), e.$elem.find(".owl-wrapper").css("display", "block"), e.$elem.is(":visible") ? e.$elem.css("opacity", 1) : e.watchVisibility(), e.onstartup = !1, e.eachMoveUpdate(), "function" == typeof e.options.afterInit && e.options.afterInit.apply(this, [e.$elem])
            },
            eachMoveUpdate: function() {
                var e = this;
                e.options.lazyLoad === !0 && e.lazyLoad(), e.options.autoHeight === !0 && e.autoHeight(), e.onVisibleItems(), "function" == typeof e.options.afterAction && e.options.afterAction.apply(this, [e.$elem])
            },
            updateVars: function() {
                var e = this;
                "function" == typeof e.options.beforeUpdate && e.options.beforeUpdate.apply(this, [e.$elem]), e.watchVisibility(), e.updateItems(), e.calculateAll(), e.updatePosition(), e.updateControls(), e.eachMoveUpdate(), "function" == typeof e.options.afterUpdate && e.options.afterUpdate.apply(this, [e.$elem])
            },
            reload: function() {
                var e = this;
                t.setTimeout(function() {
                    e.updateVars()
                }, 0)
            },
            watchVisibility: function() {
                var e = this;
                return e.$elem.is(":visible") === !1 && (e.$elem.css({
                    opacity: 0
                }), t.clearInterval(e.autoPlayInterval), t.clearInterval(e.checkVisible), void(e.checkVisible = t.setInterval(function() {
                    e.$elem.is(":visible") && (e.reload(), e.$elem.animate({
                        opacity: 1
                    }, 200), t.clearInterval(e.checkVisible))
                }, 500)))
            },
            wrapItems: function() {
                var e = this;
                e.$userItems.wrapAll('<div class="owl-wrapper">').wrap('<div class="owl-item"></div>'), e.$elem.find(".owl-wrapper").wrap('<div class="owl-wrapper-outer">'), e.wrapperOuter = e.$elem.find(".owl-wrapper-outer"), e.$elem.css("display", "block")
            },
            baseClass: function() {
                var e = this,
                    t = e.$elem.hasClass(e.options.baseClass),
                    n = e.$elem.hasClass(e.options.theme);
                t || e.$elem.addClass(e.options.baseClass), n || e.$elem.addClass(e.options.theme)
            },
            updateItems: function() {
                var t, n, i = this;
                if (i.options.responsive === !1) return !1;
                if (i.options.singleItem === !0) return i.options.items = i.orignalItems = 1, i.options.itemsCustom = !1, i.options.itemsDesktop = !1, i.options.itemsDesktopSmall = !1, i.options.itemsTablet = !1, i.options.itemsTabletSmall = !1, i.options.itemsMobile = !1, !1;
                if (t = e(i.options.responsiveBaseWidth).width(), t > (i.options.itemsDesktop[0] || i.orignalItems) && (i.options.items = i.orignalItems), i.options.itemsCustom !== !1)
                    for (i.options.itemsCustom.sort(function(e, t) {
                            return e[0] - t[0]
                        }), n = 0; n < i.options.itemsCustom.length; n += 1) i.options.itemsCustom[n][0] <= t && (i.options.items = i.options.itemsCustom[n][1]);
                else t <= i.options.itemsDesktop[0] && i.options.itemsDesktop !== !1 && (i.options.items = i.options.itemsDesktop[1]), t <= i.options.itemsDesktopSmall[0] && i.options.itemsDesktopSmall !== !1 && (i.options.items = i.options.itemsDesktopSmall[1]), t <= i.options.itemsTablet[0] && i.options.itemsTablet !== !1 && (i.options.items = i.options.itemsTablet[1]), t <= i.options.itemsTabletSmall[0] && i.options.itemsTabletSmall !== !1 && (i.options.items = i.options.itemsTabletSmall[1]), t <= i.options.itemsMobile[0] && i.options.itemsMobile !== !1 && (i.options.items = i.options.itemsMobile[1]);
                i.options.items > i.itemsAmount && i.options.itemsScaleUp === !0 && (i.options.items = i.itemsAmount)
            },
            response: function() {
                var n, i, o = this;
                return o.options.responsive === !0 && (i = e(t).width(), o.resizer = function() {
                    e(t).width() !== i && (o.options.autoPlay !== !1 && t.clearInterval(o.autoPlayInterval), t.clearTimeout(n), n = t.setTimeout(function() {
                        i = e(t).width(), o.updateVars()
                    }, o.options.responsiveRefreshRate))
                }, void e(t).resize(o.resizer))
            },
            updatePosition: function() {
                var e = this;
                e.jumpTo(e.currentItem), e.options.autoPlay !== !1 && e.checkAp()
            },
            appendItemsSizes: function() {
                var t = this,
                    n = 0,
                    i = t.itemsAmount - t.options.items;
                t.$owlItems.each(function(o) {
                    var r = e(this);
                    r.css({
                        width: t.itemWidth
                    }).data("owl-item", Number(o)), o % t.options.items !== 0 && o !== i || o > i || (n += 1), r.data("owl-roundPages", n)
                })
            },
            appendWrapperSizes: function() {
                var e = this,
                    t = e.$owlItems.length * e.itemWidth;
                e.$owlWrapper.css({
                    width: 2 * t,
                    left: 0
                }), e.appendItemsSizes()
            },
            calculateAll: function() {
                var e = this;
                e.calculateWidth(), e.appendWrapperSizes(), e.loops(), e.max()
            },
            calculateWidth: function() {
                var e = this;
                e.itemWidth = Math.round(e.$elem.width() / e.options.items)
            },
            max: function() {
                var e = this,
                    t = (e.itemsAmount * e.itemWidth - e.options.items * e.itemWidth) * -1;
                return e.options.items > e.itemsAmount ? (e.maximumItem = 0, t = 0, e.maximumPixels = 0) : (e.maximumItem = e.itemsAmount - e.options.items, e.maximumPixels = t), t
            },
            min: function() {
                return 0
            },
            loops: function() {
                var t, n, i, o = this,
                    r = 0,
                    s = 0;
                for (o.positionsInArray = [0], o.pagesInArray = [], t = 0; t < o.itemsAmount; t += 1) s += o.itemWidth, o.positionsInArray.push(-s), o.options.scrollPerPage === !0 && (n = e(o.$owlItems[t]), i = n.data("owl-roundPages"), i !== r && (o.pagesInArray[r] = o.positionsInArray[t], r = i))
            },
            buildControls: function() {
                var t = this;
                t.options.navigation !== !0 && t.options.pagination !== !0 || (t.owlControls = e('<div class="owl-controls"/>').toggleClass("clickable", !t.browser.isTouch).appendTo(t.$elem)), t.options.pagination === !0 && t.buildPagination(), t.options.navigation === !0 && t.buildButtons()
            },
            buildButtons: function() {
                var t = this,
                    n = e('<div class="owl-buttons"/>');
                t.owlControls.append(n), t.buttonPrev = e("<div/>", {
                    class: "owl-prev",
                    html: t.options.navigationText[0] || ""
                }), t.buttonNext = e("<div/>", {
                    class: "owl-next",
                    html: t.options.navigationText[1] || ""
                }), n.append(t.buttonPrev).append(t.buttonNext), n.on("touchstart.owlControls mousedown.owlControls", 'div[class^="owl"]', function(e) {
                    e.preventDefault()
                }), n.on("touchend.owlControls mouseup.owlControls", 'div[class^="owl"]', function(n) {
                    n.preventDefault(), e(this).hasClass("owl-next") ? t.next() : t.prev()
                })
            },
            buildPagination: function() {
                var t = this;
                t.paginationWrapper = e('<div class="owl-pagination"/>'), t.owlControls.append(t.paginationWrapper), t.paginationWrapper.on("touchend.owlControls mouseup.owlControls", ".owl-page", function(n) {
                    n.preventDefault(), Number(e(this).data("owl-page")) !== t.currentItem && t.goTo(Number(e(this).data("owl-page")), !0)
                })
            },
            updatePagination: function() {
                var t, n, i, o, r, s, a = this;
                if (a.options.pagination === !1) return !1;
                for (a.paginationWrapper.html(""), t = 0, n = a.itemsAmount - a.itemsAmount % a.options.items, o = 0; o < a.itemsAmount; o += 1) o % a.options.items === 0 && (t += 1, n === o && (i = a.itemsAmount - a.options.items), r = e("<div/>", {
                    class: "owl-page"
                }), s = e("<span></span>", {
                    text: a.options.paginationNumbers === !0 ? t : "",
                    class: a.options.paginationNumbers === !0 ? "owl-numbers" : ""
                }), r.append(s), r.data("owl-page", n === o ? i : o), r.data("owl-roundPages", t), a.paginationWrapper.append(r));
                a.checkPagination()
            },
            checkPagination: function() {
                var t = this;
                return t.options.pagination !== !1 && void t.paginationWrapper.find(".owl-page").each(function() {
                    e(this).data("owl-roundPages") === e(t.$owlItems[t.currentItem]).data("owl-roundPages") && (t.paginationWrapper.find(".owl-page").removeClass("active"), e(this).addClass("active"))
                })
            },
            checkNavigation: function() {
                var e = this;
                return e.options.navigation !== !1 && void(e.options.rewindNav === !1 && (0 === e.currentItem && 0 === e.maximumItem ? (e.buttonPrev.addClass("disabled"), e.buttonNext.addClass("disabled")) : 0 === e.currentItem && 0 !== e.maximumItem ? (e.buttonPrev.addClass("disabled"), e.buttonNext.removeClass("disabled")) : e.currentItem === e.maximumItem ? (e.buttonPrev.removeClass("disabled"), e.buttonNext.addClass("disabled")) : 0 !== e.currentItem && e.currentItem !== e.maximumItem && (e.buttonPrev.removeClass("disabled"), e.buttonNext.removeClass("disabled"))))
            },
            updateControls: function() {
                var e = this;
                e.updatePagination(), e.checkNavigation(), e.owlControls && (e.options.items >= e.itemsAmount ? e.owlControls.hide() : e.owlControls.show())
            },
            destroyControls: function() {
                var e = this;
                e.owlControls && e.owlControls.remove()
            },
            next: function(e) {
                var t = this;
                if (t.isTransition) return !1;
                if (t.currentItem += t.options.scrollPerPage === !0 ? t.options.items : 1, t.currentItem > t.maximumItem + (t.options.scrollPerPage === !0 ? t.options.items - 1 : 0)) {
                    if (t.options.rewindNav !== !0) return t.currentItem = t.maximumItem, !1;
                    t.currentItem = 0, e = "rewind"
                }
                t.goTo(t.currentItem, e)
            },
            prev: function(e) {
                var t = this;
                if (t.isTransition) return !1;
                if (t.options.scrollPerPage === !0 && t.currentItem > 0 && t.currentItem < t.options.items ? t.currentItem = 0 : t.currentItem -= t.options.scrollPerPage === !0 ? t.options.items : 1, t.currentItem < 0) {
                    if (t.options.rewindNav !== !0) return t.currentItem = 0, !1;
                    t.currentItem = t.maximumItem, e = "rewind"
                }
                t.goTo(t.currentItem, e)
            },
            goTo: function(e, n, i) {
                var o, r = this;
                return !r.isTransition && ("function" == typeof r.options.beforeMove && r.options.beforeMove.apply(this, [r.$elem]), e >= r.maximumItem ? e = r.maximumItem : e <= 0 && (e = 0), r.currentItem = r.owl.currentItem = e, r.options.transitionStyle !== !1 && "drag" !== i && 1 === r.options.items && r.browser.support3d === !0 ? (r.swapSpeed(0), r.browser.support3d === !0 ? r.transition3d(r.positionsInArray[e]) : r.css2slide(r.positionsInArray[e], 1), r.afterGo(), r.singleItemTransition(), !1) : (o = r.positionsInArray[e], r.browser.support3d === !0 ? (r.isCss3Finish = !1, n === !0 ? (r.swapSpeed("paginationSpeed"), t.setTimeout(function() {
                    r.isCss3Finish = !0
                }, r.options.paginationSpeed)) : "rewind" === n ? (r.swapSpeed(r.options.rewindSpeed), t.setTimeout(function() {
                    r.isCss3Finish = !0
                }, r.options.rewindSpeed)) : (r.swapSpeed("slideSpeed"), t.setTimeout(function() {
                    r.isCss3Finish = !0
                }, r.options.slideSpeed)), r.transition3d(o)) : n === !0 ? r.css2slide(o, r.options.paginationSpeed) : "rewind" === n ? r.css2slide(o, r.options.rewindSpeed) : r.css2slide(o, r.options.slideSpeed), void r.afterGo()))
            },
            jumpTo: function(e) {
                var t = this;
                "function" == typeof t.options.beforeMove && t.options.beforeMove.apply(this, [t.$elem]), e >= t.maximumItem || e === -1 ? e = t.maximumItem : e <= 0 && (e = 0), t.swapSpeed(0), t.browser.support3d === !0 ? t.transition3d(t.positionsInArray[e]) : t.css2slide(t.positionsInArray[e], 1), t.currentItem = t.owl.currentItem = e, t.afterGo()
            },
            afterGo: function() {
                var e = this;
                e.prevArr.push(e.currentItem), e.prevItem = e.owl.prevItem = e.prevArr[e.prevArr.length - 2], e.prevArr.shift(0), e.prevItem !== e.currentItem && (e.checkPagination(), e.checkNavigation(), e.eachMoveUpdate(), e.options.autoPlay !== !1 && e.checkAp()), "function" == typeof e.options.afterMove && e.prevItem !== e.currentItem && e.options.afterMove.apply(this, [e.$elem])
            },
            stop: function() {
                var e = this;
                e.apStatus = "stop", t.clearInterval(e.autoPlayInterval)
            },
            checkAp: function() {
                var e = this;
                "stop" !== e.apStatus && e.play()
            },
            play: function() {
                var e = this;
                return e.apStatus = "play", e.options.autoPlay !== !1 && (t.clearInterval(e.autoPlayInterval), void(e.autoPlayInterval = t.setInterval(function() {
                    e.next(!0)
                }, e.options.autoPlay)))
            },
            swapSpeed: function(e) {
                var t = this;
                "slideSpeed" === e ? t.$owlWrapper.css(t.addCssSpeed(t.options.slideSpeed)) : "paginationSpeed" === e ? t.$owlWrapper.css(t.addCssSpeed(t.options.paginationSpeed)) : "string" != typeof e && t.$owlWrapper.css(t.addCssSpeed(e))
            },
            addCssSpeed: function(e) {
                return {
                    "-webkit-transition": "all " + e + "ms ease",
                    "-moz-transition": "all " + e + "ms ease",
                    "-o-transition": "all " + e + "ms ease",
                    transition: "all " + e + "ms ease"
                }
            },
            removeTransition: function() {
                return {
                    "-webkit-transition": "",
                    "-moz-transition": "",
                    "-o-transition": "",
                    transition: ""
                }
            },
            doTranslate: function(e) {
                return {
                    "-webkit-transform": "translate3d(" + e + "px, 0px, 0px)",
                    "-moz-transform": "translate3d(" + e + "px, 0px, 0px)",
                    "-o-transform": "translate3d(" + e + "px, 0px, 0px)",
                    "-ms-transform": "translate3d(" + e + "px, 0px, 0px)",
                    transform: "translate3d(" + e + "px, 0px,0px)"
                }
            },
            transition3d: function(e) {
                var t = this;
                t.$owlWrapper.css(t.doTranslate(e))
            },
            css2move: function(e) {
                var t = this;
                t.$owlWrapper.css({
                    left: e
                })
            },
            css2slide: function(e, t) {
                var n = this;
                n.isCssFinish = !1, n.$owlWrapper.stop(!0, !0).animate({
                    left: e
                }, {
                    duration: t || n.options.slideSpeed,
                    complete: function() {
                        n.isCssFinish = !0
                    }
                })
            },
            checkBrowser: function() {
                var e, i, o, r, s = this,
                    a = "translate3d(0px, 0px, 0px)",
                    l = n.createElement("div");
                l.style.cssText = "  -moz-transform:" + a + "; -ms-transform:" + a + "; -o-transform:" + a + "; -webkit-transform:" + a + "; transform:" + a, e = /translate3d\(0px, 0px, 0px\)/g, i = l.style.cssText.match(e), o = null !== i && 1 === i.length, r = "ontouchstart" in t || t.navigator.msMaxTouchPoints, s.browser = {
                    support3d: o,
                    isTouch: r
                }
            },
            moveEvents: function() {
                var e = this;
                e.options.mouseDrag === !1 && e.options.touchDrag === !1 || (e.gestures(), e.disabledEvents())
            },
            eventTypes: function() {
                var e = this,
                    t = ["s", "e", "x"];
                e.ev_types = {}, e.options.mouseDrag === !0 && e.options.touchDrag === !0 ? t = ["touchstart.owl mousedown.owl", "touchmove.owl mousemove.owl", "touchend.owl touchcancel.owl mouseup.owl"] : e.options.mouseDrag === !1 && e.options.touchDrag === !0 ? t = ["touchstart.owl", "touchmove.owl", "touchend.owl touchcancel.owl"] : e.options.mouseDrag === !0 && e.options.touchDrag === !1 && (t = ["mousedown.owl", "mousemove.owl", "mouseup.owl"]), e.ev_types.start = t[0], e.ev_types.move = t[1], e.ev_types.end = t[2]
            },
            disabledEvents: function() {
                var t = this;
                t.$elem.on("dragstart.owl", function(e) {
                    e.preventDefault()
                }), t.$elem.on("mousedown.disableTextSelect", function(t) {
                    return e(t.target).is("input, textarea, select, option")
                })
            },
            gestures: function() {
                function i(e) {
                    if (void 0 !== e.touches) return {
                        x: e.touches[0].pageX,
                        y: e.touches[0].pageY
                    };
                    if (void 0 === e.touches) {
                        if (void 0 !== e.pageX) return {
                            x: e.pageX,
                            y: e.pageY
                        };
                        if (void 0 === e.pageX) return {
                            x: e.clientX,
                            y: e.clientY
                        }
                    }
                }

                function o(t) {
                    "on" === t ? (e(n).on(l.ev_types.move, s), e(n).on(l.ev_types.end, a)) : "off" === t && (e(n).off(l.ev_types.move), e(n).off(l.ev_types.end))
                }

                function r(n) {
                    var r, s = n.originalEvent || n || t.event;
                    if (3 === s.which) return !1;
                    if (!(l.itemsAmount <= l.options.items)) {
                        if (l.isCssFinish === !1 && !l.options.dragBeforeAnimFinish) return !1;
                        if (l.isCss3Finish === !1 && !l.options.dragBeforeAnimFinish) return !1;
                        l.options.autoPlay !== !1 && t.clearInterval(l.autoPlayInterval), l.browser.isTouch === !0 || l.$owlWrapper.hasClass("grabbing") || l.$owlWrapper.addClass("grabbing"), l.newPosX = 0, l.newRelativeX = 0, e(this).css(l.removeTransition()), r = e(this).position(), c.relativePos = r.left, c.offsetX = i(s).x - r.left, c.offsetY = i(s).y - r.top, o("on"), c.sliding = !1, c.targetElement = s.target || s.srcElement
                    }
                }

                function s(o) {
                    var r, s, a = o.originalEvent || o || t.event;
                    l.newPosX = i(a).x - c.offsetX, l.newPosY = i(a).y - c.offsetY, l.newRelativeX = l.newPosX - c.relativePos, "function" == typeof l.options.startDragging && c.dragging !== !0 && 0 !== l.newRelativeX && (c.dragging = !0, l.options.startDragging.apply(l, [l.$elem])), (l.newRelativeX > 8 || l.newRelativeX < -8) && l.browser.isTouch === !0 && (void 0 !== a.preventDefault ? a.preventDefault() : a.returnValue = !1, c.sliding = !0), (l.newPosY > 10 || l.newPosY < -10) && c.sliding === !1 && e(n).off("touchmove.owl"), r = function() {
                        return l.newRelativeX / 5
                    }, s = function() {
                        return l.maximumPixels + l.newRelativeX / 5
                    }, l.newPosX = Math.max(Math.min(l.newPosX, r()), s()), l.browser.support3d === !0 ? l.transition3d(l.newPosX) : l.css2move(l.newPosX)
                }

                function a(n) {
                    var i, r, s, a = n.originalEvent || n || t.event;
                    a.target = a.target || a.srcElement, c.dragging = !1, l.browser.isTouch !== !0 && l.$owlWrapper.removeClass("grabbing"), l.newRelativeX < 0 ? l.dragDirection = l.owl.dragDirection = "left" : l.dragDirection = l.owl.dragDirection = "right", 0 !== l.newRelativeX && (i = l.getNewPosition(), l.goTo(i, !1, "drag"), c.targetElement === a.target && l.browser.isTouch !== !0 && (e(a.target).on("click.disable", function(t) {
                        t.stopImmediatePropagation(), t.stopPropagation(), t.preventDefault(), e(t.target).off("click.disable")
                    }), r = e._data(a.target, "events").click, s = r.pop(), r.splice(0, 0, s))), o("off")
                }
                var l = this,
                    c = {
                        offsetX: 0,
                        offsetY: 0,
                        baseElWidth: 0,
                        relativePos: 0,
                        position: null,
                        minSwipe: null,
                        maxSwipe: null,
                        sliding: null,
                        dargging: null,
                        targetElement: null
                    };
                l.isCssFinish = !0, l.$elem.on(l.ev_types.start, ".owl-wrapper", r)
            },
            getNewPosition: function() {
                var e = this,
                    t = e.closestItem();
                return t > e.maximumItem ? (e.currentItem = e.maximumItem, t = e.maximumItem) : e.newPosX >= 0 && (t = 0, e.currentItem = 0), t
            },
            closestItem: function() {
                var t = this,
                    n = t.options.scrollPerPage === !0 ? t.pagesInArray : t.positionsInArray,
                    i = t.newPosX,
                    o = null;
                return e.each(n, function(r, s) {
                    i - t.itemWidth / 20 > n[r + 1] && i - t.itemWidth / 20 < s && "left" === t.moveDirection() ? (o = s, t.options.scrollPerPage === !0 ? t.currentItem = e.inArray(o, t.positionsInArray) : t.currentItem = r) : i + t.itemWidth / 20 < s && i + t.itemWidth / 20 > (n[r + 1] || n[r] - t.itemWidth) && "right" === t.moveDirection() && (t.options.scrollPerPage === !0 ? (o = n[r + 1] || n[n.length - 1], t.currentItem = e.inArray(o, t.positionsInArray)) : (o = n[r + 1], t.currentItem = r + 1))
                }), t.currentItem
            },
            moveDirection: function() {
                var e, t = this;
                return t.newRelativeX < 0 ? (e = "right", t.playDirection = "next") : (e = "left", t.playDirection = "prev"), e
            },
            customEvents: function() {
                var e = this;
                e.$elem.on("owl.next", function() {
                    e.next()
                }), e.$elem.on("owl.prev", function() {
                    e.prev()
                }), e.$elem.on("owl.play", function(t, n) {
                    e.options.autoPlay = n, e.play(), e.hoverStatus = "play"
                }), e.$elem.on("owl.stop", function() {
                    e.stop(), e.hoverStatus = "stop"
                }), e.$elem.on("owl.goTo", function(t, n) {
                    e.goTo(n)
                }), e.$elem.on("owl.jumpTo", function(t, n) {
                    e.jumpTo(n)
                })
            },
            stopOnHover: function() {
                var e = this;
                e.options.stopOnHover === !0 && e.browser.isTouch !== !0 && e.options.autoPlay !== !1 && (e.$elem.on("mouseover", function() {
                    e.stop()
                }), e.$elem.on("mouseout", function() {
                    "stop" !== e.hoverStatus && e.play()
                }))
            },
            lazyLoad: function() {
                var t, n, i, o, r, s = this;
                if (s.options.lazyLoad === !1) return !1;
                for (t = 0; t < s.itemsAmount; t += 1) n = e(s.$owlItems[t]), "loaded" !== n.data("owl-loaded") && (i = n.data("owl-item"), o = n.find(".lazyOwl"), "string" == typeof o.data("src") ? (void 0 === n.data("owl-loaded") && (o.hide(), n.addClass("loading").data("owl-loaded", "checked")), r = s.options.lazyFollow !== !0 || i >= s.currentItem, r && i < s.currentItem + s.options.items && o.length && s.lazyPreload(n, o)) : n.data("owl-loaded", "loaded"))
            },
            lazyPreload: function(e, n) {
                function i() {
                    e.data("owl-loaded", "loaded").removeClass("loading"), n.removeAttr("data-src"), "fade" === s.options.lazyEffect ? n.fadeIn(400) : n.show(), "function" == typeof s.options.afterLazyLoad && s.options.afterLazyLoad.apply(this, [s.$elem])
                }

                function o() {
                    a += 1, s.completeImg(n.get(0)) || r === !0 ? i() : a <= 100 ? t.setTimeout(o, 100) : i()
                }
                var r, s = this,
                    a = 0;
                "DIV" === n.prop("tagName") ? (n.css("background-image", "url(" + n.data("src") + ")"), r = !0) : n[0].src = n.data("src"), o()
            },
            autoHeight: function() {
                function n() {
                    var n = e(r.$owlItems[r.currentItem]).height();
                    r.wrapperOuter.css("height", n + "px"), r.wrapperOuter.hasClass("autoHeight") || t.setTimeout(function() {
                        r.wrapperOuter.addClass("autoHeight")
                    }, 0)
                }

                function i() {
                    o += 1, r.completeImg(s.get(0)) ? n() : o <= 100 ? t.setTimeout(i, 100) : r.wrapperOuter.css("height", "")
                }
                var o, r = this,
                    s = e(r.$owlItems[r.currentItem]).find("img");
                void 0 !== s.get(0) ? (o = 0, i()) : n()
            },
            completeImg: function(e) {
                var t;
                return !!e.complete && (t = typeof e.naturalWidth, "undefined" === t || 0 !== e.naturalWidth)
            },
            onVisibleItems: function() {
                var t, n = this;
                for (n.options.addClassActive === !0 && n.$owlItems.removeClass("active"), n.visibleItems = [], t = n.currentItem; t < n.currentItem + n.options.items; t += 1) n.visibleItems.push(t), n.options.addClassActive === !0 && e(n.$owlItems[t]).addClass("active");
                n.owl.visibleItems = n.visibleItems
            },
            transitionTypes: function(e) {
                var t = this;
                t.outClass = "owl-" + e + "-out", t.inClass = "owl-" + e + "-in"
            },
            singleItemTransition: function() {
                function e(e) {
                    return {
                        position: "relative",
                        left: e + "px"
                    }
                }
                var t = this,
                    n = t.outClass,
                    i = t.inClass,
                    o = t.$owlItems.eq(t.currentItem),
                    r = t.$owlItems.eq(t.prevItem),
                    s = Math.abs(t.positionsInArray[t.currentItem]) + t.positionsInArray[t.prevItem],
                    a = Math.abs(t.positionsInArray[t.currentItem]) + t.itemWidth / 2,
                    l = "webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend";
                t.isTransition = !0, t.$owlWrapper.addClass("owl-origin").css({
                    "-webkit-transform-origin": a + "px",
                    "-moz-perspective-origin": a + "px",
                    "perspective-origin": a + "px"
                }), r.css(e(s, 10)).addClass(n).on(l, function() {
                    t.endPrev = !0, r.off(l), t.clearTransStyle(r, n)
                }), o.addClass(i).on(l, function() {
                    t.endCurrent = !0, o.off(l), t.clearTransStyle(o, i)
                })
            },
            clearTransStyle: function(e, t) {
                var n = this;
                e.css({
                    position: "",
                    left: ""
                }).removeClass(t), n.endPrev && n.endCurrent && (n.$owlWrapper.removeClass("owl-origin"), n.endPrev = !1, n.endCurrent = !1, n.isTransition = !1)
            },
            owlStatus: function() {
                var e = this;
                e.owl = {
                    userOptions: e.userOptions,
                    baseElement: e.$elem,
                    userItems: e.$userItems,
                    owlItems: e.$owlItems,
                    currentItem: e.currentItem,
                    prevItem: e.prevItem,
                    visibleItems: e.visibleItems,
                    isTouch: e.browser.isTouch,
                    browser: e.browser,
                    dragDirection: e.dragDirection
                }
            },
            clearEvents: function() {
                var i = this;
                i.$elem.off(".owl owl mousedown.disableTextSelect"), e(n).off(".owl owl"), e(t).off("resize", i.resizer)
            },
            unWrap: function() {
                var e = this;
                0 !== e.$elem.children().length && (e.$owlWrapper.unwrap(), e.$userItems.unwrap().unwrap(), e.owlControls && e.owlControls.remove()), e.clearEvents(), e.$elem.attr("style", e.$elem.data("owl-originalStyles") || "").attr("class", e.$elem.data("owl-originalClasses"))
            },
            destroy: function() {
                var e = this;
                e.stop(), t.clearInterval(e.checkVisible), e.unWrap(), e.$elem.removeData()
            },
            reinit: function(t) {
                var n = this,
                    i = e.extend({}, n.userOptions, t);
                n.unWrap(), n.init(i, n.$elem)
            },
            addItem: function(e, t) {
                var n, i = this;
                return !!e && (0 === i.$elem.children().length ? (i.$elem.append(e), i.setVars(), !1) : (i.unWrap(), n = void 0 === t || t === -1 ? -1 : t, n >= i.$userItems.length || n === -1 ? i.$userItems.eq(-1).after(e) : i.$userItems.eq(n).before(e), void i.setVars()))
            },
            removeItem: function(e) {
                var t, n = this;
                return 0 !== n.$elem.children().length && (t = void 0 === e || e === -1 ? -1 : e, n.unWrap(), n.$userItems.eq(t).remove(), void n.setVars())
            }
        };
        e.fn.owlCarousel = function(t) {
            return this.each(function() {
                if (e(this).data("owl-init") === !0) return !1;
                e(this).data("owl-init", !0);
                var n = Object.create(i);
                n.init(t, this), e.data(this, "owlCarousel", n)
            })
        }, e.fn.owlCarousel.options = {
            items: 5,
            itemsCustom: !1,
            itemsDesktop: [1199, 4],
            itemsDesktopSmall: [979, 3],
            itemsTablet: [768, 2],
            itemsTabletSmall: !1,
            itemsMobile: [479, 1],
            singleItem: !1,
            itemsScaleUp: !1,
            slideSpeed: 200,
            paginationSpeed: 800,
            rewindSpeed: 1e3,
            autoPlay: !1,
            stopOnHover: !1,
            navigation: !1,
            navigationText: ["prev", "next"],
            rewindNav: !0,
            scrollPerPage: !1,
            pagination: !0,
            paginationNumbers: !1,
            responsive: !0,
            responsiveRefreshRate: 200,
            responsiveBaseWidth: t,
            baseClass: "owl-carousel",
            theme: "owl-theme",
            lazyLoad: !1,
            lazyFollow: !0,
            lazyEffect: "fade",
            autoHeight: !1,
            jsonPath: !1,
            jsonSuccess: !1,
            dragBeforeAnimFinish: !0,
            mouseDrag: !0,
            touchDrag: !0,
            addClassActive: !1,
            transitionStyle: !1,
            beforeUpdate: !1,
            afterUpdate: !1,
            beforeInit: !1,
            afterInit: !1,
            beforeMove: !1,
            afterMove: !1,
            afterAction: !1,
            startDragging: !1,
            afterLazyLoad: !1
        }
    }(jQuery, window, document),
    function(e, t) {
        "use strict";
        "function" == typeof define && define.amd ? define("jquery-bridget/jquery-bridget", ["jquery"], function(n) {
            t(e, n)
        }) : "object" == typeof module && module.exports ? module.exports = t(e, require("jquery")) : e.jQueryBridget = t(e, e.jQuery)
    }(window, function(e, t) {
        "use strict";

        function n(n, r, a) {
            function l(e, t, i) {
                var o, r = "$()." + n + '("' + t + '")';
                return e.each(function(e, l) {
                    var c = a.data(l, n);
                    if (!c) return void s(n + " not initialized. Cannot call methods, i.e. " + r);
                    var u = c[t];
                    if (!u || "_" == t.charAt(0)) return void s(r + " is not a valid method");
                    var d = u.apply(c, i);
                    o = void 0 === o ? d : o
                }), void 0 !== o ? o : e
            }

            function c(e, t) {
                e.each(function(e, i) {
                    var o = a.data(i, n);
                    o ? (o.option(t), o._init()) : (o = new r(i, t), a.data(i, n, o))
                })
            }
            a = a || t || e.jQuery, a && (r.prototype.option || (r.prototype.option = function(e) {
                a.isPlainObject(e) && (this.options = a.extend(!0, this.options, e))
            }), a.fn[n] = function(e) {
                if ("string" == typeof e) {
                    var t = o.call(arguments, 1);
                    return l(this, e, t)
                }
                return c(this, e), this
            }, i(a))
        }

        function i(e) {
            !e || e && e.bridget || (e.bridget = n)
        }
        var o = Array.prototype.slice,
            r = e.console,
            s = "undefined" == typeof r ? function() {} : function(e) {
                r.error(e)
            };
        return i(t || e.jQuery), n
    }),
    function(e, t) {
        "use strict";
        "function" == typeof define && define.amd ? define("get-size/get-size", [], function() {
            return t()
        }) : "object" == typeof module && module.exports ? module.exports = t() : e.getSize = t()
    }(window, function() {
        "use strict";

        function e(e) {
            var t = parseFloat(e),
                n = e.indexOf("%") == -1 && !isNaN(t);
            return n && t
        }

        function t() {}

        function n() {
            for (var e = {
                    width: 0,
                    height: 0,
                    innerWidth: 0,
                    innerHeight: 0,
                    outerWidth: 0,
                    outerHeight: 0
                }, t = 0; t < c; t++) {
                var n = l[t];
                e[n] = 0
            }
            return e
        }

        function i(e) {
            var t = getComputedStyle(e);
            return t || a("Style returned " + t + ". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"), t
        }

        function o() {
            if (!u) {
                u = !0;
                var t = document.createElement("div");
                t.style.width = "200px", t.style.padding = "1px 2px 3px 4px", t.style.borderStyle = "solid", t.style.borderWidth = "1px 2px 3px 4px", t.style.boxSizing = "border-box";
                var n = document.body || document.documentElement;
                n.appendChild(t);
                var o = i(t);
                r.isBoxSizeOuter = s = 200 == e(o.width), n.removeChild(t)
            }
        }

        function r(t) {
            if (o(), "string" == typeof t && (t = document.querySelector(t)), t && "object" == typeof t && t.nodeType) {
                var r = i(t);
                if ("none" == r.display) return n();
                var a = {};
                a.width = t.offsetWidth, a.height = t.offsetHeight;
                for (var u = a.isBorderBox = "border-box" == r.boxSizing, d = 0; d < c; d++) {
                    var p = l[d],
                        h = r[p],
                        f = parseFloat(h);
                    a[p] = isNaN(f) ? 0 : f
                }
                var m = a.paddingLeft + a.paddingRight,
                    g = a.paddingTop + a.paddingBottom,
                    v = a.marginLeft + a.marginRight,
                    y = a.marginTop + a.marginBottom,
                    w = a.borderLeftWidth + a.borderRightWidth,
                    b = a.borderTopWidth + a.borderBottomWidth,
                    x = u && s,
                    C = e(r.width);
                C !== !1 && (a.width = C + (x ? 0 : m + w));
                var T = e(r.height);
                return T !== !1 && (a.height = T + (x ? 0 : g + b)), a.innerWidth = a.width - (m + w), a.innerHeight = a.height - (g + b), a.outerWidth = a.width + v, a.outerHeight = a.height + y, a
            }
        }
        var s, a = "undefined" == typeof console ? t : function(e) {
                console.error(e)
            },
            l = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"],
            c = l.length,
            u = !1;
        return r
    }),
    function(e, t) {
        "function" == typeof define && define.amd ? define("ev-emitter/ev-emitter", t) : "object" == typeof module && module.exports ? module.exports = t() : e.EvEmitter = t()
    }(this, function() {
        function e() {}
        var t = e.prototype;
        return t.on = function(e, t) {
            if (e && t) {
                var n = this._events = this._events || {},
                    i = n[e] = n[e] || [];
                return i.indexOf(t) == -1 && i.push(t), this
            }
        }, t.once = function(e, t) {
            if (e && t) {
                this.on(e, t);
                var n = this._onceEvents = this._onceEvents || {},
                    i = n[e] = n[e] || {};
                return i[t] = !0, this
            }
        }, t.off = function(e, t) {
            var n = this._events && this._events[e];
            if (n && n.length) {
                var i = n.indexOf(t);
                return i != -1 && n.splice(i, 1), this
            }
        }, t.emitEvent = function(e, t) {
            var n = this._events && this._events[e];
            if (n && n.length) {
                var i = 0,
                    o = n[i];
                t = t || [];
                for (var r = this._onceEvents && this._onceEvents[e]; o;) {
                    var s = r && r[o];
                    s && (this.off(e, o), delete r[o]), o.apply(this, t), i += s ? 0 : 1, o = n[i]
                }
                return this
            }
        }, e
    }),
    function(e, t) {
        "use strict";
        "function" == typeof define && define.amd ? define("desandro-matches-selector/matches-selector", t) : "object" == typeof module && module.exports ? module.exports = t() : e.matchesSelector = t()
    }(window, function() {
        "use strict";
        var e = function() {
            var e = Element.prototype;
            if (e.matches) return "matches";
            if (e.matchesSelector) return "matchesSelector";
            for (var t = ["webkit", "moz", "ms", "o"], n = 0; n < t.length; n++) {
                var i = t[n],
                    o = i + "MatchesSelector";
                if (e[o]) return o
            }
        }();
        return function(t, n) {
            return t[e](n)
        }
    }),
    function(e, t) {
        "function" == typeof define && define.amd ? define("fizzy-ui-utils/utils", ["desandro-matches-selector/matches-selector"], function(n) {
            return t(e, n)
        }) : "object" == typeof module && module.exports ? module.exports = t(e, require("desandro-matches-selector")) : e.fizzyUIUtils = t(e, e.matchesSelector)
    }(window, function(e, t) {
        var n = {};
        n.extend = function(e, t) {
            for (var n in t) e[n] = t[n];
            return e
        }, n.modulo = function(e, t) {
            return (e % t + t) % t
        }, n.makeArray = function(e) {
            var t = [];
            if (Array.isArray(e)) t = e;
            else if (e && "number" == typeof e.length)
                for (var n = 0; n < e.length; n++) t.push(e[n]);
            else t.push(e);
            return t
        }, n.removeFrom = function(e, t) {
            var n = e.indexOf(t);
            n != -1 && e.splice(n, 1)
        }, n.getParent = function(e, n) {
            for (; e != document.body;)
                if (e = e.parentNode, t(e, n)) return e
        }, n.getQueryElement = function(e) {
            return "string" == typeof e ? document.querySelector(e) : e
        }, n.handleEvent = function(e) {
            var t = "on" + e.type;
            this[t] && this[t](e)
        }, n.filterFindElements = function(e, i) {
            e = n.makeArray(e);
            var o = [];
            return e.forEach(function(e) {
                if (e instanceof HTMLElement) {
                    if (!i) return void o.push(e);
                    t(e, i) && o.push(e);
                    for (var n = e.querySelectorAll(i), r = 0; r < n.length; r++) o.push(n[r])
                }
            }), o
        }, n.debounceMethod = function(e, t, n) {
            var i = e.prototype[t],
                o = t + "Timeout";
            e.prototype[t] = function() {
                var e = this[o];
                e && clearTimeout(e);
                var t = arguments,
                    r = this;
                this[o] = setTimeout(function() {
                    i.apply(r, t), delete r[o]
                }, n || 100)
            }
        }, n.docReady = function(e) {
            "complete" == document.readyState ? e() : document.addEventListener("DOMContentLoaded", e)
        }, n.toDashed = function(e) {
            return e.replace(/(.)([A-Z])/g, function(e, t, n) {
                return t + "-" + n
            }).toLowerCase()
        };
        var i = e.console;
        return n.htmlInit = function(t, o) {
            n.docReady(function() {
                var r = n.toDashed(o),
                    s = "data-" + r,
                    a = document.querySelectorAll("[" + s + "]"),
                    l = document.querySelectorAll(".js-" + r),
                    c = n.makeArray(a).concat(n.makeArray(l)),
                    u = s + "-options",
                    d = e.jQuery;
                c.forEach(function(e) {
                    var n, r = e.getAttribute(s) || e.getAttribute(u);
                    try {
                        n = r && JSON.parse(r)
                    } catch (t) {
                        return void(i && i.error("Error parsing " + s + " on " + e.className + ": " + t))
                    }
                    var a = new t(e, n);
                    d && d.data(e, o, a)
                })
            })
        }, n
    }),
    function(e, t) {
        "function" == typeof define && define.amd ? define("outlayer/item", ["ev-emitter/ev-emitter", "get-size/get-size"], t) : "object" == typeof module && module.exports ? module.exports = t(require("ev-emitter"), require("get-size")) : (e.Outlayer = {}, e.Outlayer.Item = t(e.EvEmitter, e.getSize))
    }(window, function(e, t) {
        "use strict";

        function n(e) {
            for (var t in e) return !1;
            return t = null, !0
        }

        function i(e, t) {
            e && (this.element = e, this.layout = t, this.position = {
                x: 0,
                y: 0
            }, this._create())
        }

        function o(e) {
            return e.replace(/([A-Z])/g, function(e) {
                return "-" + e.toLowerCase()
            })
        }
        var r = document.documentElement.style,
            s = "string" == typeof r.transition ? "transition" : "WebkitTransition",
            a = "string" == typeof r.transform ? "transform" : "WebkitTransform",
            l = {
                WebkitTransition: "webkitTransitionEnd",
                transition: "transitionend"
            } [s],
            c = {
                transform: a,
                transition: s,
                transitionDuration: s + "Duration",
                transitionProperty: s + "Property",
                transitionDelay: s + "Delay"
            },
            u = i.prototype = Object.create(e.prototype);
        u.constructor = i, u._create = function() {
            this._transn = {
                ingProperties: {},
                clean: {},
                onEnd: {}
            }, this.css({
                position: "absolute"
            })
        }, u.handleEvent = function(e) {
            var t = "on" + e.type;
            this[t] && this[t](e)
        }, u.getSize = function() {
            this.size = t(this.element)
        }, u.css = function(e) {
            var t = this.element.style;
            for (var n in e) {
                var i = c[n] || n;
                t[i] = e[n]
            }
        }, u.getPosition = function() {
            var e = getComputedStyle(this.element),
                t = this.layout._getOption("originLeft"),
                n = this.layout._getOption("originTop"),
                i = e[t ? "left" : "right"],
                o = e[n ? "top" : "bottom"],
                r = this.layout.size,
                s = i.indexOf("%") != -1 ? parseFloat(i) / 100 * r.width : parseInt(i, 10),
                a = o.indexOf("%") != -1 ? parseFloat(o) / 100 * r.height : parseInt(o, 10);
            s = isNaN(s) ? 0 : s, a = isNaN(a) ? 0 : a, s -= t ? r.paddingLeft : r.paddingRight, a -= n ? r.paddingTop : r.paddingBottom, this.position.x = s, this.position.y = a
        }, u.layoutPosition = function() {
            var e = this.layout.size,
                t = {},
                n = this.layout._getOption("originLeft"),
                i = this.layout._getOption("originTop"),
                o = n ? "paddingLeft" : "paddingRight",
                r = n ? "left" : "right",
                s = n ? "right" : "left",
                a = this.position.x + e[o];
            t[r] = this.getXValue(a), t[s] = "";
            var l = i ? "paddingTop" : "paddingBottom",
                c = i ? "top" : "bottom",
                u = i ? "bottom" : "top",
                d = this.position.y + e[l];
            t[c] = this.getYValue(d), t[u] = "", this.css(t), this.emitEvent("layout", [this])
        }, u.getXValue = function(e) {
            var t = this.layout._getOption("horizontal");
            return this.layout.options.percentPosition && !t ? e / this.layout.size.width * 100 + "%" : e + "px"
        }, u.getYValue = function(e) {
            var t = this.layout._getOption("horizontal");
            return this.layout.options.percentPosition && t ? e / this.layout.size.height * 100 + "%" : e + "px"
        }, u._transitionTo = function(e, t) {
            this.getPosition();
            var n = this.position.x,
                i = this.position.y,
                o = parseInt(e, 10),
                r = parseInt(t, 10),
                s = o === this.position.x && r === this.position.y;
            if (this.setPosition(e, t), s && !this.isTransitioning) return void this.layoutPosition();
            var a = e - n,
                l = t - i,
                c = {};
            c.transform = this.getTranslate(a, l), this.transition({
                to: c,
                onTransitionEnd: {
                    transform: this.layoutPosition
                },
                isCleaning: !0
            })
        }, u.getTranslate = function(e, t) {
            var n = this.layout._getOption("originLeft"),
                i = this.layout._getOption("originTop");
            return e = n ? e : -e, t = i ? t : -t, "translate3d(" + e + "px, " + t + "px, 0)"
        }, u.goTo = function(e, t) {
            this.setPosition(e, t), this.layoutPosition()
        }, u.moveTo = u._transitionTo, u.setPosition = function(e, t) {
            this.position.x = parseInt(e, 10), this.position.y = parseInt(t, 10)
        }, u._nonTransition = function(e) {
            this.css(e.to), e.isCleaning && this._removeStyles(e.to);
            for (var t in e.onTransitionEnd) e.onTransitionEnd[t].call(this)
        }, u.transition = function(e) {
            if (!parseFloat(this.layout.options.transitionDuration)) return void this._nonTransition(e);
            var t = this._transn;
            for (var n in e.onTransitionEnd) t.onEnd[n] = e.onTransitionEnd[n];
            for (n in e.to) t.ingProperties[n] = !0, e.isCleaning && (t.clean[n] = !0);
            if (e.from) {
                this.css(e.from);
                var i = this.element.offsetHeight;
                i = null
            }
            this.enableTransition(e.to), this.css(e.to), this.isTransitioning = !0
        };
        var d = "opacity," + o(a);
        u.enableTransition = function() {
            if (!this.isTransitioning) {
                var e = this.layout.options.transitionDuration;
                e = "number" == typeof e ? e + "ms" : e, this.css({
                    transitionProperty: d,
                    transitionDuration: e,
                    transitionDelay: this.staggerDelay || 0
                }), this.element.addEventListener(l, this, !1)
            }
        }, u.onwebkitTransitionEnd = function(e) {
            this.ontransitionend(e)
        }, u.onotransitionend = function(e) {
            this.ontransitionend(e)
        };
        var p = {
            "-webkit-transform": "transform"
        };
        u.ontransitionend = function(e) {
            if (e.target === this.element) {
                var t = this._transn,
                    i = p[e.propertyName] || e.propertyName;
                if (delete t.ingProperties[i], n(t.ingProperties) && this.disableTransition(), i in t.clean && (this.element.style[e.propertyName] = "", delete t.clean[i]), i in t.onEnd) {
                    var o = t.onEnd[i];
                    o.call(this), delete t.onEnd[i]
                }
                this.emitEvent("transitionEnd", [this])
            }
        }, u.disableTransition = function() {
            this.removeTransitionStyles(), this.element.removeEventListener(l, this, !1), this.isTransitioning = !1
        }, u._removeStyles = function(e) {
            var t = {};
            for (var n in e) t[n] = "";
            this.css(t)
        };
        var h = {
            transitionProperty: "",
            transitionDuration: "",
            transitionDelay: ""
        };
        return u.removeTransitionStyles = function() {
            this.css(h)
        }, u.stagger = function(e) {
            e = isNaN(e) ? 0 : e, this.staggerDelay = e + "ms"
        }, u.removeElem = function() {
            this.element.parentNode.removeChild(this.element), this.css({
                display: ""
            }), this.emitEvent("remove", [this])
        }, u.remove = function() {
            return s && parseFloat(this.layout.options.transitionDuration) ? (this.once("transitionEnd", function() {
                this.removeElem()
            }), void this.hide()) : void this.removeElem()
        }, u.reveal = function() {
            delete this.isHidden, this.css({
                display: ""
            });
            var e = this.layout.options,
                t = {},
                n = this.getHideRevealTransitionEndProperty("visibleStyle");
            t[n] = this.onRevealTransitionEnd, this.transition({
                from: e.hiddenStyle,
                to: e.visibleStyle,
                isCleaning: !0,
                onTransitionEnd: t
            })
        }, u.onRevealTransitionEnd = function() {
            this.isHidden || this.emitEvent("reveal")
        }, u.getHideRevealTransitionEndProperty = function(e) {
            var t = this.layout.options[e];
            if (t.opacity) return "opacity";
            for (var n in t) return n
        }, u.hide = function() {
            this.isHidden = !0, this.css({
                display: ""
            });
            var e = this.layout.options,
                t = {},
                n = this.getHideRevealTransitionEndProperty("hiddenStyle");
            t[n] = this.onHideTransitionEnd, this.transition({
                from: e.visibleStyle,
                to: e.hiddenStyle,
                isCleaning: !0,
                onTransitionEnd: t
            })
        }, u.onHideTransitionEnd = function() {
            this.isHidden && (this.css({
                display: "none"
            }), this.emitEvent("hide"))
        }, u.destroy = function() {
            this.css({
                position: "",
                left: "",
                right: "",
                top: "",
                bottom: "",
                transition: "",
                transform: ""
            })
        }, i
    }),
    function(e, t) {
        "use strict";
        "function" == typeof define && define.amd ? define("outlayer/outlayer", ["ev-emitter/ev-emitter", "get-size/get-size", "fizzy-ui-utils/utils", "./item"], function(n, i, o, r) {
            return t(e, n, i, o, r)
        }) : "object" == typeof module && module.exports ? module.exports = t(e, require("ev-emitter"), require("get-size"), require("fizzy-ui-utils"), require("./item")) : e.Outlayer = t(e, e.EvEmitter, e.getSize, e.fizzyUIUtils, e.Outlayer.Item)
    }(window, function(e, t, n, i, o) {
        "use strict";

        function r(e, t) {
            var n = i.getQueryElement(e);
            if (!n) return void(l && l.error("Bad element for " + this.constructor.namespace + ": " + (n || e)));
            this.element = n, c && (this.$element = c(this.element)), this.options = i.extend({}, this.constructor.defaults), this.option(t);
            var o = ++d;
            this.element.outlayerGUID = o, p[o] = this, this._create();
            var r = this._getOption("initLayout");
            r && this.layout()
        }

        function s(e) {
            function t() {
                e.apply(this, arguments)
            }
            return t.prototype = Object.create(e.prototype), t.prototype.constructor = t, t
        }

        function a(e) {
            if ("number" == typeof e) return e;
            var t = e.match(/(^\d*\.?\d*)(\w*)/),
                n = t && t[1],
                i = t && t[2];
            if (!n.length) return 0;
            n = parseFloat(n);
            var o = f[i] || 1;
            return n * o
        }
        var l = e.console,
            c = e.jQuery,
            u = function() {},
            d = 0,
            p = {};
        r.namespace = "outlayer", r.Item = o, r.defaults = {
            containerStyle: {
                position: "relative"
            },
            initLayout: !0,
            originLeft: !0,
            originTop: !0,
            resize: !0,
            resizeContainer: !0,
            transitionDuration: "0.4s",
            hiddenStyle: {
                opacity: 0,
                transform: "scale(0.001)"
            },
            visibleStyle: {
                opacity: 1,
                transform: "scale(1)"
            }
        };
        var h = r.prototype;
        i.extend(h, t.prototype), h.option = function(e) {
            i.extend(this.options, e)
        }, h._getOption = function(e) {
            var t = this.constructor.compatOptions[e];
            return t && void 0 !== this.options[t] ? this.options[t] : this.options[e]
        }, r.compatOptions = {
            initLayout: "isInitLayout",
            horizontal: "isHorizontal",
            layoutInstant: "isLayoutInstant",
            originLeft: "isOriginLeft",
            originTop: "isOriginTop",
            resize: "isResizeBound",
            resizeContainer: "isResizingContainer"
        }, h._create = function() {
            this.reloadItems(), this.stamps = [], this.stamp(this.options.stamp), i.extend(this.element.style, this.options.containerStyle);
            var e = this._getOption("resize");
            e && this.bindResize()
        }, h.reloadItems = function() {
            this.items = this._itemize(this.element.children)
        }, h._itemize = function(e) {
            for (var t = this._filterFindItemElements(e), n = this.constructor.Item, i = [], o = 0; o < t.length; o++) {
                var r = t[o],
                    s = new n(r, this);
                i.push(s)
            }
            return i
        }, h._filterFindItemElements = function(e) {
            return i.filterFindElements(e, this.options.itemSelector)
        }, h.getItemElements = function() {
            return this.items.map(function(e) {
                return e.element
            })
        }, h.layout = function() {
            this._resetLayout(), this._manageStamps();
            var e = this._getOption("layoutInstant"),
                t = void 0 !== e ? e : !this._isLayoutInited;
            this.layoutItems(this.items, t), this._isLayoutInited = !0
        }, h._init = h.layout, h._resetLayout = function() {
            this.getSize()
        }, h.getSize = function() {
            this.size = n(this.element)
        }, h._getMeasurement = function(e, t) {
            var i, o = this.options[e];
            o ? ("string" == typeof o ? i = this.element.querySelector(o) : o instanceof HTMLElement && (i = o), this[e] = i ? n(i)[t] : o) : this[e] = 0
        }, h.layoutItems = function(e, t) {
            e = this._getItemsForLayout(e), this._layoutItems(e, t), this._postLayout()
        }, h._getItemsForLayout = function(e) {
            return e.filter(function(e) {
                return !e.isIgnored
            })
        }, h._layoutItems = function(e, t) {
            if (this._emitCompleteOnItems("layout", e), e && e.length) {
                var n = [];
                e.forEach(function(e) {
                    var i = this._getItemLayoutPosition(e);
                    i.item = e, i.isInstant = t || e.isLayoutInstant, n.push(i)
                }, this), this._processLayoutQueue(n)
            }
        }, h._getItemLayoutPosition = function() {
            return {
                x: 0,
                y: 0
            }
        }, h._processLayoutQueue = function(e) {
            this.updateStagger(), e.forEach(function(e, t) {
                this._positionItem(e.item, e.x, e.y, e.isInstant, t)
            }, this)
        }, h.updateStagger = function() {
            var e = this.options.stagger;
            return null === e || void 0 === e ? void(this.stagger = 0) : (this.stagger = a(e), this.stagger)
        }, h._positionItem = function(e, t, n, i, o) {
            i ? e.goTo(t, n) : (e.stagger(o * this.stagger), e.moveTo(t, n))
        }, h._postLayout = function() {
            this.resizeContainer()
        }, h.resizeContainer = function() {
            var e = this._getOption("resizeContainer");
            if (e) {
                var t = this._getContainerSize();
                t && (this._setContainerMeasure(t.width, !0), this._setContainerMeasure(t.height, !1))
            }
        }, h._getContainerSize = u, h._setContainerMeasure = function(e, t) {
            if (void 0 !== e) {
                var n = this.size;
                n.isBorderBox && (e += t ? n.paddingLeft + n.paddingRight + n.borderLeftWidth + n.borderRightWidth : n.paddingBottom + n.paddingTop + n.borderTopWidth + n.borderBottomWidth), e = Math.max(e, 0), this.element.style[t ? "width" : "height"] = e + "px"
            }
        }, h._emitCompleteOnItems = function(e, t) {
            function n() {
                o.dispatchEvent(e + "Complete", null, [t])
            }

            function i() {
                s++, s == r && n()
            }
            var o = this,
                r = t.length;
            if (!t || !r) return void n();
            var s = 0;
            t.forEach(function(t) {
                t.once(e, i)
            })
        }, h.dispatchEvent = function(e, t, n) {
            var i = t ? [t].concat(n) : n;
            if (this.emitEvent(e, i), c)
                if (this.$element = this.$element || c(this.element), t) {
                    var o = c.Event(t);
                    o.type = e, this.$element.trigger(o, n)
                } else this.$element.trigger(e, n)
        }, h.ignore = function(e) {
            var t = this.getItem(e);
            t && (t.isIgnored = !0)
        }, h.unignore = function(e) {
            var t = this.getItem(e);
            t && delete t.isIgnored
        }, h.stamp = function(e) {
            e = this._find(e), e && (this.stamps = this.stamps.concat(e), e.forEach(this.ignore, this))
        }, h.unstamp = function(e) {
            e = this._find(e), e && e.forEach(function(e) {
                i.removeFrom(this.stamps, e), this.unignore(e)
            }, this)
        }, h._find = function(e) {
            if (e) return "string" == typeof e && (e = this.element.querySelectorAll(e)), e = i.makeArray(e)
        }, h._manageStamps = function() {
            this.stamps && this.stamps.length && (this._getBoundingRect(), this.stamps.forEach(this._manageStamp, this))
        }, h._getBoundingRect = function() {
            var e = this.element.getBoundingClientRect(),
                t = this.size;
            this._boundingRect = {
                left: e.left + t.paddingLeft + t.borderLeftWidth,
                top: e.top + t.paddingTop + t.borderTopWidth,
                right: e.right - (t.paddingRight + t.borderRightWidth),
                bottom: e.bottom - (t.paddingBottom + t.borderBottomWidth)
            }
        }, h._manageStamp = u, h._getElementOffset = function(e) {
            var t = e.getBoundingClientRect(),
                i = this._boundingRect,
                o = n(e),
                r = {
                    left: t.left - i.left - o.marginLeft,
                    top: t.top - i.top - o.marginTop,
                    right: i.right - t.right - o.marginRight,
                    bottom: i.bottom - t.bottom - o.marginBottom
                };
            return r
        }, h.handleEvent = i.handleEvent, h.bindResize = function() {
            e.addEventListener("resize", this), this.isResizeBound = !0
        }, h.unbindResize = function() {
            e.removeEventListener("resize", this), this.isResizeBound = !1
        }, h.onresize = function() {
            this.resize()
        }, i.debounceMethod(r, "onresize", 100), h.resize = function() {
            this.isResizeBound && this.needsResizeLayout() && this.layout()
        }, h.needsResizeLayout = function() {
            var e = n(this.element),
                t = this.size && e;
            return t && e.innerWidth !== this.size.innerWidth
        }, h.addItems = function(e) {
            var t = this._itemize(e);
            return t.length && (this.items = this.items.concat(t)), t
        }, h.appended = function(e) {
            var t = this.addItems(e);
            t.length && (this.layoutItems(t, !0), this.reveal(t))
        }, h.prepended = function(e) {
            var t = this._itemize(e);
            if (t.length) {
                var n = this.items.slice(0);
                this.items = t.concat(n), this._resetLayout(), this._manageStamps(), this.layoutItems(t, !0), this.reveal(t), this.layoutItems(n)
            }
        }, h.reveal = function(e) {
            if (this._emitCompleteOnItems("reveal", e), e && e.length) {
                var t = this.updateStagger();
                e.forEach(function(e, n) {
                    e.stagger(n * t), e.reveal()
                })
            }
        }, h.hide = function(e) {
            if (this._emitCompleteOnItems("hide", e), e && e.length) {
                var t = this.updateStagger();
                e.forEach(function(e, n) {
                    e.stagger(n * t), e.hide()
                })
            }
        }, h.revealItemElements = function(e) {
            var t = this.getItems(e);
            this.reveal(t)
        }, h.hideItemElements = function(e) {
            var t = this.getItems(e);
            this.hide(t)
        }, h.getItem = function(e) {
            for (var t = 0; t < this.items.length; t++) {
                var n = this.items[t];
                if (n.element == e) return n
            }
        }, h.getItems = function(e) {
            e = i.makeArray(e);
            var t = [];
            return e.forEach(function(e) {
                var n = this.getItem(e);
                n && t.push(n)
            }, this), t
        }, h.remove = function(e) {
            var t = this.getItems(e);
            this._emitCompleteOnItems("remove", t), t && t.length && t.forEach(function(e) {
                e.remove(), i.removeFrom(this.items, e)
            }, this)
        }, h.destroy = function() {
            var e = this.element.style;
            e.height = "", e.position = "", e.width = "", this.items.forEach(function(e) {
                e.destroy()
            }), this.unbindResize();
            var t = this.element.outlayerGUID;
            delete p[t], delete this.element.outlayerGUID, c && c.removeData(this.element, this.constructor.namespace)
        }, r.data = function(e) {
            e = i.getQueryElement(e);
            var t = e && e.outlayerGUID;
            return t && p[t]
        }, r.create = function(e, t) {
            var n = s(r);
            return n.defaults = i.extend({}, r.defaults), i.extend(n.defaults, t), n.compatOptions = i.extend({}, r.compatOptions), n.namespace = e, n.data = r.data, n.Item = s(o), i.htmlInit(n, e), c && c.bridget && c.bridget(e, n), n
        };
        var f = {
            ms: 1,
            s: 1e3
        };
        return r.Item = o, r
    }),
    function(e, t) {
        "function" == typeof define && define.amd ? define("packery/js/rect", t) : "object" == typeof module && module.exports ? module.exports = t() : (e.Packery = e.Packery || {}, e.Packery.Rect = t())
    }(window, function() {
        "use strict";

        function e(t) {
            for (var n in e.defaults) this[n] = e.defaults[n];
            for (n in t) this[n] = t[n]
        }
        e.defaults = {
            x: 0,
            y: 0,
            width: 0,
            height: 0
        };
        var t = e.prototype;
        return t.contains = function(e) {
            var t = e.width || 0,
                n = e.height || 0;
            return this.x <= e.x && this.y <= e.y && this.x + this.width >= e.x + t && this.y + this.height >= e.y + n
        }, t.overlaps = function(e) {
            var t = this.x + this.width,
                n = this.y + this.height,
                i = e.x + e.width,
                o = e.y + e.height;
            return this.x < i && t > e.x && this.y < o && n > e.y
        }, t.getMaximalFreeRects = function(t) {
            if (!this.overlaps(t)) return !1;
            var n, i = [],
                o = this.x + this.width,
                r = this.y + this.height,
                s = t.x + t.width,
                a = t.y + t.height;
            return this.y < t.y && (n = new e({
                x: this.x,
                y: this.y,
                width: this.width,
                height: t.y - this.y
            }), i.push(n)), o > s && (n = new e({
                x: s,
                y: this.y,
                width: o - s,
                height: this.height
            }), i.push(n)), r > a && (n = new e({
                x: this.x,
                y: a,
                width: this.width,
                height: r - a
            }), i.push(n)), this.x < t.x && (n = new e({
                x: this.x,
                y: this.y,
                width: t.x - this.x,
                height: this.height
            }), i.push(n)), i
        }, t.canFit = function(e) {
            return this.width >= e.width && this.height >= e.height
        }, e
    }),
    function(e, t) {
        if ("function" == typeof define && define.amd) define("packery/js/packer", ["./rect"], t);
        else if ("object" == typeof module && module.exports) module.exports = t(require("./rect"));
        else {
            var n = e.Packery = e.Packery || {};
            n.Packer = t(n.Rect)
        }
    }(window, function(e) {
        "use strict";

        function t(e, t, n) {
            this.width = e || 0, this.height = t || 0, this.sortDirection = n || "downwardLeftToRight", this.reset()
        }
        var n = t.prototype;
        n.reset = function() {
            this.spaces = [];
            var t = new e({
                x: 0,
                y: 0,
                width: this.width,
                height: this.height
            });
            this.spaces.push(t), this.sorter = i[this.sortDirection] || i.downwardLeftToRight
        }, n.pack = function(e) {
            for (var t = 0; t < this.spaces.length; t++) {
                var n = this.spaces[t];
                if (n.canFit(e)) {
                    this.placeInSpace(e, n);
                    break
                }
            }
        }, n.columnPack = function(e) {
            for (var t = 0; t < this.spaces.length; t++) {
                var n = this.spaces[t],
                    i = n.x <= e.x && n.x + n.width >= e.x + e.width && n.height >= e.height - .01;
                if (i) {
                    e.y = n.y, this.placed(e);
                    break
                }
            }
        }, n.rowPack = function(e) {
            for (var t = 0; t < this.spaces.length; t++) {
                var n = this.spaces[t],
                    i = n.y <= e.y && n.y + n.height >= e.y + e.height && n.width >= e.width - .01;
                if (i) {
                    e.x = n.x, this.placed(e);
                    break
                }
            }
        }, n.placeInSpace = function(e, t) {
            e.x = t.x, e.y = t.y, this.placed(e)
        }, n.placed = function(e) {
            for (var t = [], n = 0; n < this.spaces.length; n++) {
                var i = this.spaces[n],
                    o = i.getMaximalFreeRects(e);
                o ? t.push.apply(t, o) : t.push(i)
            }
            this.spaces = t, this.mergeSortSpaces()
        }, n.mergeSortSpaces = function() {
            t.mergeRects(this.spaces), this.spaces.sort(this.sorter)
        }, n.addSpace = function(e) {
            this.spaces.push(e), this.mergeSortSpaces()
        }, t.mergeRects = function(e) {
            var t = 0,
                n = e[t];
            e: for (; n;) {
                for (var i = 0, o = e[t + i]; o;) {
                    if (o == n) i++;
                    else {
                        if (o.contains(n)) {
                            e.splice(t, 1), n = e[t];
                            continue e
                        }
                        n.contains(o) ? e.splice(t + i, 1) : i++
                    }
                    o = e[t + i]
                }
                t++, n = e[t]
            }
            return e
        };
        var i = {
            downwardLeftToRight: function(e, t) {
                return e.y - t.y || e.x - t.x
            },
            rightwardTopToBottom: function(e, t) {
                return e.x - t.x || e.y - t.y
            }
        };
        return t
    }),
    function(e, t) {
        "function" == typeof define && define.amd ? define("packery/js/item", ["outlayer/outlayer", "./rect"], t) : "object" == typeof module && module.exports ? module.exports = t(require("outlayer"), require("./rect")) : e.Packery.Item = t(e.Outlayer, e.Packery.Rect)
    }(window, function(e, t) {
        "use strict";
        var n = document.documentElement.style,
            i = "string" == typeof n.transform ? "transform" : "WebkitTransform",
            o = function() {
                e.Item.apply(this, arguments)
            },
            r = o.prototype = Object.create(e.Item.prototype),
            s = r._create;
        r._create = function() {
            s.call(this), this.rect = new t
        };
        var a = r.moveTo;
        return r.moveTo = function(e, t) {
            var n = Math.abs(this.position.x - e),
                i = Math.abs(this.position.y - t),
                o = this.layout.dragItemCount && !this.isPlacing && !this.isTransitioning && n < 1 && i < 1;
            return o ? void this.goTo(e, t) : void a.apply(this, arguments)
        }, r.enablePlacing = function() {
            this.removeTransitionStyles(), this.isTransitioning && i && (this.element.style[i] = "none"), this.isTransitioning = !1, this.getSize(), this.layout._setRectSize(this.element, this.rect), this.isPlacing = !0
        }, r.disablePlacing = function() {
            this.isPlacing = !1
        }, r.removeElem = function() {
            this.element.parentNode.removeChild(this.element), this.layout.packer.addSpace(this.rect), this.emitEvent("remove", [this])
        }, r.showDropPlaceholder = function() {
            var e = this.dropPlaceholder;
            e || (e = this.dropPlaceholder = document.createElement("div"), e.className = "packery-drop-placeholder", e.style.position = "absolute"), e.style.width = this.size.width + "px", e.style.height = this.size.height + "px", this.positionDropPlaceholder(), this.layout.element.appendChild(e)
        }, r.positionDropPlaceholder = function() {
            this.dropPlaceholder.style[i] = "translate(" + this.rect.x + "px, " + this.rect.y + "px)"
        }, r.hideDropPlaceholder = function() {
            var e = this.dropPlaceholder.parentNode;
            e && e.removeChild(this.dropPlaceholder)
        }, o
    }),
    function(e, t) {
        "function" == typeof define && define.amd ? define(["get-size/get-size", "outlayer/outlayer", "packery/js/rect", "packery/js/packer", "packery/js/item"], t) : "object" == typeof module && module.exports ? module.exports = t(require("get-size"), require("outlayer"), require("./rect"), require("./packer"), require("./item")) : e.Packery = t(e.getSize, e.Outlayer, e.Packery.Rect, e.Packery.Packer, e.Packery.Item)
    }(window, function(e, t, n, i, o) {
        "use strict";

        function r(e, t) {
            return e.position.y - t.position.y || e.position.x - t.position.x
        }

        function s(e, t) {
            return e.position.x - t.position.x || e.position.y - t.position.y
        }

        function a(e, t) {
            var n = t.x - e.x,
                i = t.y - e.y;
            return Math.sqrt(n * n + i * i)
        }
        n.prototype.canFit = function(e) {
            return this.width >= e.width - 1 && this.height >= e.height - 1
        };
        var l = t.create("packery");
        l.Item = o;
        var c = l.prototype;
        c._create = function() {
            t.prototype._create.call(this), this.packer = new i, this.shiftPacker = new i, this.isEnabled = !0, this.dragItemCount = 0;
            var e = this;
            this.handleDraggabilly = {
                dragStart: function() {
                    e.itemDragStart(this.element)
                },
                dragMove: function() {
                    e.itemDragMove(this.element, this.position.x, this.position.y)
                },
                dragEnd: function() {
                    e.itemDragEnd(this.element)
                }
            }, this.handleUIDraggable = {
                start: function(t, n) {
                    n && e.itemDragStart(t.currentTarget)
                },
                drag: function(t, n) {
                    n && e.itemDragMove(t.currentTarget, n.position.left, n.position.top)
                },
                stop: function(t, n) {
                    n && e.itemDragEnd(t.currentTarget)
                }
            }
        }, c._resetLayout = function() {
            this.getSize(), this._getMeasurements();
            var e, t, n;
            this._getOption("horizontal") ? (e = 1 / 0, t = this.size.innerHeight + this.gutter, n = "rightwardTopToBottom") : (e = this.size.innerWidth + this.gutter, t = 1 / 0, n = "downwardLeftToRight"), this.packer.width = this.shiftPacker.width = e, this.packer.height = this.shiftPacker.height = t, this.packer.sortDirection = this.shiftPacker.sortDirection = n, this.packer.reset(), this.maxY = 0, this.maxX = 0
        }, c._getMeasurements = function() {
            this._getMeasurement("columnWidth", "width"), this._getMeasurement("rowHeight", "height"), this._getMeasurement("gutter", "width")
        }, c._getItemLayoutPosition = function(e) {
            if (this._setRectSize(e.element, e.rect), this.isShifting || this.dragItemCount > 0) {
                var t = this._getPackMethod();
                this.packer[t](e.rect)
            } else this.packer.pack(e.rect);
            return this._setMaxXY(e.rect), e.rect
        }, c.shiftLayout = function() {
            this.isShifting = !0, this.layout(), delete this.isShifting
        }, c._getPackMethod = function() {
            return this._getOption("horizontal") ? "rowPack" : "columnPack"
        }, c._setMaxXY = function(e) {
            this.maxX = Math.max(e.x + e.width, this.maxX), this.maxY = Math.max(e.y + e.height, this.maxY)
        }, c._setRectSize = function(t, n) {
            var i = e(t),
                o = i.outerWidth,
                r = i.outerHeight;
            (o || r) && (o = this._applyGridGutter(o, this.columnWidth), r = this._applyGridGutter(r, this.rowHeight)), n.width = Math.min(o, this.packer.width), n.height = Math.min(r, this.packer.height)
        }, c._applyGridGutter = function(e, t) {
            if (!t) return e + this.gutter;
            t += this.gutter;
            var n = e % t,
                i = n && n < 1 ? "round" : "ceil";
            return e = Math[i](e / t) * t
        }, c._getContainerSize = function() {
            return this._getOption("horizontal") ? {
                width: this.maxX - this.gutter
            } : {
                height: this.maxY - this.gutter
            }
        }, c._manageStamp = function(e) {
            var t, i = this.getItem(e);
            if (i && i.isPlacing) t = i.rect;
            else {
                var o = this._getElementOffset(e);
                t = new n({
                    x: this._getOption("originLeft") ? o.left : o.right,
                    y: this._getOption("originTop") ? o.top : o.bottom
                })
            }
            this._setRectSize(e, t), this.packer.placed(t), this._setMaxXY(t)
        }, c.sortItemsByPosition = function() {
            var e = this._getOption("horizontal") ? s : r;
            this.items.sort(e)
        }, c.fit = function(e, t, n) {
            var i = this.getItem(e);
            i && (this.stamp(i.element), i.enablePlacing(), this.updateShiftTargets(i), t = void 0 === t ? i.rect.x : t, n = void 0 === n ? i.rect.y : n, this.shift(i, t, n), this._bindFitEvents(i), i.moveTo(i.rect.x, i.rect.y), this.shiftLayout(), this.unstamp(i.element), this.sortItemsByPosition(), i.disablePlacing())
        }, c._bindFitEvents = function(e) {
            function t() {
                i++, 2 == i && n.dispatchEvent("fitComplete", null, [e])
            }
            var n = this,
                i = 0;
            e.once("layout", t), this.once("layoutComplete", t)
        }, c.resize = function() {
            this.isResizeBound && this.needsResizeLayout() && (this.options.shiftPercentResize ? this.resizeShiftPercentLayout() : this.layout())
        }, c.needsResizeLayout = function() {
            var t = e(this.element),
                n = this._getOption("horizontal") ? "innerHeight" : "innerWidth";
            return t[n] != this.size[n]
        }, c.resizeShiftPercentLayout = function() {
            var t = this._getItemsForLayout(this.items),
                n = this._getOption("horizontal"),
                i = n ? "y" : "x",
                o = n ? "height" : "width",
                r = n ? "rowHeight" : "columnWidth",
                s = n ? "innerHeight" : "innerWidth",
                a = this[r];
            if (a = a && a + this.gutter) {
                this._getMeasurements();
                var l = this[r] + this.gutter;
                t.forEach(function(e) {
                    var t = Math.round(e.rect[i] / a);
                    e.rect[i] = t * l
                })
            } else {
                var c = e(this.element)[s] + this.gutter,
                    u = this.packer[o];
                t.forEach(function(e) {
                    e.rect[i] = e.rect[i] / u * c
                })
            }
            this.shiftLayout()
        }, c.itemDragStart = function(e) {
            if (this.isEnabled) {
                this.stamp(e);
                var t = this.getItem(e);
                t && (t.enablePlacing(), t.showDropPlaceholder(), this.dragItemCount++, this.updateShiftTargets(t))
            }
        }, c.updateShiftTargets = function(e) {
            this.shiftPacker.reset(), this._getBoundingRect();
            var t = this._getOption("originLeft"),
                i = this._getOption("originTop");
            this.stamps.forEach(function(e) {
                var o = this.getItem(e);
                if (!o || !o.isPlacing) {
                    var r = this._getElementOffset(e),
                        s = new n({
                            x: t ? r.left : r.right,
                            y: i ? r.top : r.bottom
                        });
                    this._setRectSize(e, s), this.shiftPacker.placed(s)
                }
            }, this);
            var o = this._getOption("horizontal"),
                r = o ? "rowHeight" : "columnWidth",
                s = o ? "height" : "width";
            this.shiftTargetKeys = [], this.shiftTargets = [];
            var a, l = this[r];
            if (l = l && l + this.gutter) {
                var c = Math.ceil(e.rect[s] / l),
                    u = Math.floor((this.shiftPacker[s] + this.gutter) / l);
                a = (u - c) * l;
                for (var d = 0; d < u; d++) {
                    var p = o ? 0 : d * l,
                        h = o ? d * l : 0;
                    this._addShiftTarget(p, h, a)
                }
            } else a = this.shiftPacker[s] + this.gutter - e.rect[s], this._addShiftTarget(0, 0, a);
            var f = this._getItemsForLayout(this.items),
                m = this._getPackMethod();
            f.forEach(function(e) {
                var t = e.rect;
                this._setRectSize(e.element, t), this.shiftPacker[m](t), this._addShiftTarget(t.x, t.y, a);
                var n = o ? t.x + t.width : t.x,
                    i = o ? t.y : t.y + t.height;
                if (this._addShiftTarget(n, i, a), l)
                    for (var r = Math.round(t[s] / l), c = 1; c < r; c++) {
                        var u = o ? n : t.x + l * c,
                            d = o ? t.y + l * c : i;
                        this._addShiftTarget(u, d, a)
                    }
            }, this)
        }, c._addShiftTarget = function(e, t, n) {
            var i = this._getOption("horizontal") ? t : e;
            if (!(0 !== i && i > n)) {
                var o = e + "," + t,
                    r = this.shiftTargetKeys.indexOf(o) != -1;
                r || (this.shiftTargetKeys.push(o), this.shiftTargets.push({
                    x: e,
                    y: t
                }))
            }
        }, c.shift = function(e, t, n) {
            var i, o = 1 / 0,
                r = {
                    x: t,
                    y: n
                };
            this.shiftTargets.forEach(function(e) {
                var t = a(e, r);
                t < o && (i = e, o = t)
            }), e.rect.x = i.x, e.rect.y = i.y
        };
        var u = 120;
        c.itemDragMove = function(e, t, n) {
            function i() {
                r.shift(o, t, n), o.positionDropPlaceholder(), r.layout()
            }
            var o = this.isEnabled && this.getItem(e);
            if (o) {
                t -= this.size.paddingLeft, n -= this.size.paddingTop;
                var r = this,
                    s = new Date;
                this._itemDragTime && s - this._itemDragTime < u ? (clearTimeout(this.dragTimeout), this.dragTimeout = setTimeout(i, u)) : (i(),
                    this._itemDragTime = s)
            }
        }, c.itemDragEnd = function(e) {
            function t() {
                i++, 2 == i && (n.element.classList.remove("is-positioning-post-drag"), n.hideDropPlaceholder(), o.dispatchEvent("dragItemPositioned", null, [n]))
            }
            var n = this.isEnabled && this.getItem(e);
            if (n) {
                clearTimeout(this.dragTimeout), n.element.classList.add("is-positioning-post-drag");
                var i = 0,
                    o = this;
                n.once("layout", t), this.once("layoutComplete", t), n.moveTo(n.rect.x, n.rect.y), this.layout(), this.dragItemCount = Math.max(0, this.dragItemCount - 1), this.sortItemsByPosition(), n.disablePlacing(), this.unstamp(n.element)
            }
        }, c.bindDraggabillyEvents = function(e) {
            this._bindDraggabillyEvents(e, "on")
        }, c.unbindDraggabillyEvents = function(e) {
            this._bindDraggabillyEvents(e, "off")
        }, c._bindDraggabillyEvents = function(e, t) {
            var n = this.handleDraggabilly;
            e[t]("dragStart", n.dragStart), e[t]("dragMove", n.dragMove), e[t]("dragEnd", n.dragEnd)
        }, c.bindUIDraggableEvents = function(e) {
            this._bindUIDraggableEvents(e, "on")
        }, c.unbindUIDraggableEvents = function(e) {
            this._bindUIDraggableEvents(e, "off")
        }, c._bindUIDraggableEvents = function(e, t) {
            var n = this.handleUIDraggable;
            e[t]("dragstart", n.start)[t]("drag", n.drag)[t]("dragstop", n.stop)
        };
        var d = c.destroy;
        return c.destroy = function() {
            d.apply(this, arguments), this.isEnabled = !1
        }, l.Rect = n, l.Packer = i, l
    }), $(document).ready(function() {
        function closeMenu() {
            $(".header").removeClass("opened-menu"), $(".main-nav").removeClass("main-nav_visible"), setTimeout(function() {
                $(".main-nav").removeClass("main-nav_showed")
            }, 500)
        }

        function videoCenter(e) {
            var t = e / 2;
            if (word_1Width > t) {
                var n = word_1Width - t;
                word_1.css("left", t - n + "px"), word_2.css("left", t - n + 50 + "px"), word_3.css("left", t - n + 50 + word_2Width + 50 + "px")
            } else if (word_1Width + 50 > t);
            else if (word_1Width + 50 + word_2Width > t) {
                var n = word_1Width + 50 + word_2Width - t;
                word_2.css("left", t - word_2Width + n + "px"), word_1.css("left", t - word_2Width + n - 50 - word_1Width + "px"), word_3.css("left", t + n + 50 + "px")
            } else if (word_1Width + 50 + word_2Width + 50 > t);
            else if (word_1Width + 50 + word_2Width + 50 + word_3Width > t) {
                var n = word_1Width - t;
                word_1.css("left", t - n + "px"), word_2.css("left", t - n + 50 + "px"), word_3.css("left", t - n + 50 + word_2Width + 50 + "px")
            }
        }

        function promoAnimate() {
            $(".main-promo").addClass("in-progress"), step1()
        }

        function step1() {
            var e = $(".main-promo");
            e.removeClass("step6").removeClass("step5").removeClass("step4").removeClass("step3").removeClass("step2"), e.addClass("step1").data("active", "1"), $(".progress-bar__we").addClass("active"), document.getElementById("video1").play(), $(".progress-bar__we .progress-line").css("width", "0"), $(".progress-bar__do .progress-line").css("width", "0"), $(".progress-bar__more .progress-line").css("width", "0"), $("#video1").addClass("shown"), $(".progress-bar__we .progress-line").animate({
                width: "90%"
            }, durationVideo1, function() {
                $(".progress-bar__we").hasClass("active") && (step2(e), $(".progress-bar__we").removeClass("active"))
            })
        }

        function step2() {
            var e = $(".main-promo");
            e.removeClass("step6").removeClass("step5").removeClass("step4").removeClass("step3").addClass("step2").data("active", "2"), $(".progress-bar__do").addClass("active"), document.getElementById("video1").pause(), document.getElementById("video1").currentTime = 0, document.getElementById("video2").play(), $("video").removeClass("shown"), $(".progress-bar__we .progress-line").css("width", "90%"), $(".progress-bar__do .progress-line").css("width", "0"), $(".progress-bar__more .progress-line").css("width", "0"), $("#video2").addClass("shown"), $(".progress-bar__do .progress-line").animate({
                width: "90%"
            }, durationVideo2, function() {
                $(".progress-bar__do").hasClass("active") && (step3(e), $(".progress-bar__do").removeClass("active"))
            })
        }

        function step3() {
            var e = $(".main-promo");
            e.removeClass("step2").removeClass("step6").removeClass("step5").removeClass("step4"), e.addClass("step3").data("active", "3"), $(".progress-bar__more").addClass("active"), document.getElementById("video2").pause(), document.getElementById("video2").currentTime = 0, document.getElementById("video3").play(), $("video").removeClass("shown"), $(".progress-bar__we .progress-line").css("width", "90%"), $(".progress-bar__do .progress-line").css("width", "90%"), $(".progress-bar__more .progress-line").css("width", "0"), $("#video3").addClass("shown"), $(".progress-bar__more .progress-line").animate({
                width: "33%"
            }, durationVideo3, function() {
                $(".progress-bar__more").hasClass("active") && step4(e)
            })
        }

        function step4() {
            var e = $(".main-promo");
            e.removeClass("step3").removeClass("step6").removeClass("step5").removeClass("step2"), e.addClass("step4").data("active", "4"), document.getElementById("video3").pause(), document.getElementById("video3").currentTime = 0, document.getElementById("video4").play(), $("video").removeClass("shown"), $("#video4").addClass("shown"), $(".progress-bar__more .progress-line").animate({
                width: "66%"
            }, durationVideo4, function() {
                $(".progress-bar__more").hasClass("active") && step5(e)
            })
        }

        function step5() {
            var e = $(".main-promo");
            e.removeClass("step4").removeClass("step6").removeClass("step2").removeClass("step3"), e.addClass("step5").data("active", "5"), document.getElementById("video4").pause(), document.getElementById("video4").currentTime = 0, document.getElementById("video5").play(), $("video").removeClass("shown"), $("#video5").addClass("shown"), $(".progress-bar__more .progress-line").animate({
                width: "90%"
            }, durationVideo5, function() {
                $(".progress-bar__more").hasClass("active") && (step6(e), $(".progress-bar__more").removeClass("active"))
            })
        }

        function step6() {
            $(".main-promo");
            document.getElementById("video5").pause(), document.getElementById("video5").currentTime = 0, $("video").removeClass("shown"), setTimeout(function() {
                $(".main-promo").addClass("test"), $(".main-promo").removeClass("test step5 step1"), $(".main-promo").addClass("step6")
            }, 1e3), setTimeout(function() {
                $(".main-promo").removeClass("in-progress"), $(".progress-line").animate({
                    width: "0"
                })
            }, 2e3)
        }

        function fieldValid() {
            if (globalCurrentStep == countOfQuestions - 1) return !0;
            var e = $('input[name="field' + globalCurrentStep + '"]');
            return !!(e && e.val().length > 2)
        }

        function setActiveStep(e) {
            $(".form-steps__step-item, .nav-step-form").removeClass("active"), $(".error-mess").removeClass("active"), $('.form-steps__step-item[data-step="' + e + '"]').addClass("active"), $('.nav-step-form[data-step="' + e + '"]').addClass("active")
        }

        function validStep(e) {}

        function sendRequest(e) {
            $.ajax({
                method: "POST",
                url: "/ru/formbymail/",
                dataType: "json",
                data: e
            })
        }
        $(document).on("click", ".show-menu", function() {
                $(".header").addClass("opened-menu"), $(this).siblings(".main-nav").addClass("main-nav_showed"), setTimeout(function() {
                    $(".main-nav").addClass("main-nav_visible")
                }, 100)
            }), $(document).on("click", ".close-main-nav", function() {
                closeMenu()
            }), $(document).click(function(e) {
                $(e.target).closest(".show-menu").length || $(e.target).closest(".main-nav").length || (closeMenu(), e.stopPropagation())
            }), $(window).scroll(function() {
                var e = $(this).scrollTop(),
                    t = $(this).height();
                e > 28 && window.innerWidth > 1024 || e > 0 && window.innerWidth < 1025 ? $(".header-bottom").addClass("fixed header_dark") : ($(".header-bottom").removeClass("fixed"), $(".header").hasClass("header-white") && $(".header-bottom").removeClass("header_dark")), e > t && !$(".header-white").hasClass("header_dark") ? $(".header").addClass("header_dark") : e <= t && $(".header-white").hasClass("header_dark") && $(".header").removeClass("header_dark")
            }), $(".js-accordion-trigger").bind("click", function(e) {
                jQuery(this).parent().find(".js-accordion-content").hasClass("footer__nav") && $(window).width() < 768 && (jQuery(this).parent().find(".js-accordion-content").slideToggle("fast"), jQuery(this).parent().toggleClass("is-expanded"), e.preventDefault()), $(this).closest(".accordion_benefit").length && (jQuery(this).parent().find(".js-accordion-content").slideToggle("fast"), jQuery(this).parent().toggleClass("is-expanded"), e.preventDefault())
            }),
            function() {
                var e = function() {
                    $(".clock-wrapper").each(function(e) {
                        var t = new Date,
                            n = t.getMinutes(),
                            i = t.getHours(),
                            o = $(this),
                            r = o.data("zone");
                        switch (r) {
                            case "moskow":
                                break;
                            case "london":
                                i -= 3;
                                break;
                            case "pekin":
                                i += 5;
                                break;
						case "newyork":
                                i += 4;
                                break;
                            case "bern":
                                i -= 1
                        }
                        o.find(".clock__hours").attr("data-value", i), o.find(".clock__minutes").attr("data-value", n)
                    })
                };
                setInterval(e, 100)
            }(), $(".is-accordion").on("click", ".tab-link", function(e) {
                if ($(this).hasClass("is-active")) e.preventDefault();
                else {
                    $(window).width() > 768 && e.preventDefault();
                    var t = $(this).closest(".main-accordion-tabs");
                    t.find(".is-open").removeClass("is-open").hide(), $(this).next().toggleClass("is-open").show(), t.find(".is-active").removeClass("is-active"), $(this).addClass("is-active")
                }
            }), $(".is-tab").on("click", ".tab-link", function(e) {
                if (e.preventDefault(), !$(this).hasClass("is-active")) {
                    var t = $(this).attr("href").slice(1);
                    $(".tab-container").find(".is-open").removeClass("is-open").hide(), $(".tab-content[id = " + t + "]").toggleClass("is-open").show(), $(".main-accordion-tabs.is-tab").find(".is-active").removeClass("is-active"), $(this).addClass("is-active")
                }
            }), $(".expertise__lnk").on("click", function(e) {
                e.preventDefault();
                var t = $(this).data("expertise"),
                    n = $(this).data("bg");
                t && ($(".expertise-doc").removeClass("active"), $("#" + t).addClass("active"), $(".expertise__lnk").parent().removeClass("active"), $(this).parent().addClass("active"), console.log($(this).closest("expertise-box")), $(".expertise-box").css("background", "url(" + n + ")"), $(".expertise-box").css("background-size", "cover"))
            }), $(document).on("click", ".open-popup-publication", function() {
                var e = ($(this).data("name"), $(this).data("id")),
                    t = $(document).width();
                $("body").addClass("no-scroll");
                var n = $(document).width();
                $("body").css({
                    "padding-right": n - t,
                    transition: "padding 0s"
                }), $('input[name="id-publication"]').val(e), $("#popup-publication").addClass("visible")
            }), $(document).on("click", ".popup-wrap, .btn-close, .close-popup", function() {
                $("#popup").removeClass("visible"), $("#popup-publication").removeClass("visible"), setTimeout(function() {
                    $("body").removeClass("no-scroll"), $("body").css({
                        "padding-right": "0",
                        transition: "padding 0s"
                    })
                }, 500)
            }), $(document).on("click", ".popup", function(e) {
                e.stopPropagation()
            }), equalheight = function(e) {
                var t, n = 0,
                    i = 0,
                    o = new Array;
                $(e).each(function() {
                    if (t = $(this), $(t).height("auto"), topPostion = t.position().top, i != topPostion) {
                        for (currentDiv = 0; currentDiv < o.length; currentDiv++) o[currentDiv].height(n);
                        o.length = 0, i = topPostion, n = t.height(), o.push(t)
                    } else o.push(t), n = n < t.height() ? t.height() : n;
                    for (currentDiv = 0; currentDiv < o.length; currentDiv++) o[currentDiv].height(n)
                })
            };
        var hasIE = window.eval && eval("/*@cc_on 1;@*/") && (/msie (\d+)/i.exec(navigator.userAgent) || [, !0])[1];
        hasIE < 11 && $(".old-popop").addClass("showed");
        var res = navigator.userAgent.match(/; CPU.*OS (\d_\d)/);
        if (res) {
            var strVer = res[res.length - 1];
            strVer = strVer.replace("_", "."), version = 1 * strVer, version < 9 && $(".old-popop").addClass("showed")
        }
        $(".promo-btn").click(function() {
            if (!($(".promo-btn").length && $(window).width() < 1024 && $(window).height() < 701)) {
                $(".main-promo");
                !$(".main-promo.in-progress").length > 0 && promoAnimate()
            }
        });
        var durationVideo1 = 8500,
            durationVideo2 = 8e3,
            durationVideo3 = 5e3,
            durationVideo4 = 5e3,
            durationVideo5 = 5e3,
            currentStep1, currentStep2;
        setTimeout(function() {
            $(".news-press__cell").packery({
                itemSelector: ".news-press__cell-item"
            })
        }, 500), $(".progress-bar__item").click(function() {
            var currentProgress = $(this).data("progress"),
                activeProgress = $(".progress-bar__item.active").data("progress"),
                currentBlock = $(".main-promo").data("active");
            if (currentProgress > activeProgress) $(".progress-bar__item.active .progress-line").stop(!0).css("width", "90%"), $(".progress-bar__item.active").removeClass("active"), $(this).addClass("active"), eval("step" + currentProgress + "()");
            else if (currentProgress < activeProgress) $(".progress-bar__item.active .progress-line").stop(!0).css("width", "90%"), $(".main-promo").removeClass("step" + currentBlock), $(".progress-bar__item.active").removeClass("active"), $(this).addClass("active"), eval("step" + currentProgress + "()");
            else if (currentProgress == activeProgress) return
        });
        var countOfQuestions = $(".form-steps__step-item").length,
            globalCurrentStep = 1;
        $(".form-steps__btn-next, .nav-step-form").click(function(e) {
            if (e.preventDefault(), fieldValid()) {
                if ($(".feedaback-form__page").css("display", "block"), globalCurrentStep == countOfQuestions - 1) {
                    $(".feedaback-form__page").css("display", "none");
                    for (var t = [], n = 1; n < countOfQuestions; n++) t.push({
                        field_name: $('label[for="field' + n + '"]').text(),
                        field_data: $('input[name="field' + n + '"]').val()
                    });
                    sendRequest({
                        csrfmiddlewaretoken: document.getElementsByName("csrfmiddlewaretoken")[0].value,
                        userData: JSON.stringify(t)
                    })
                }
                globalCurrentStep++, setActiveStep("step" + globalCurrentStep)
            }
        }), setTimeout(function() {
            $("select").styler({
                onSelectOpened: function() {
                    $(this).find(".jq-selectbox__trigger-arrow").addClass("opened")
                },
                onSelectClosed: function() {
                    $(this).find(".jq-selectbox__trigger-arrow").removeClass("opened")
                }
            })
        }, 100)
    }), $(window).on("scroll", function(e) {
        var t = $(this).scrollTop(),
            n = $(this).scrollTop() + $(".menu-press").height() + $(".header-bottom").height();
        $(".menu-press__link").removeClass("active"), $(".press-box").each(function() {
            var e = $(this),
                t = e.offset().top - $(".menu-press").outerHeight() - 20,
                i = e.outerHeight() + t;
            if (n > t && n < i) {
                var o = e.attr("id");
                $('.menu-press a[href="#' + o + '"]').addClass("active")
            }
        }), t > 106 && $(".menu-press").length && window.innerWidth > 768 ? ($(".menu-press").addClass("fixed"), $(".wrapper").css("padding-top", $(".menu-press").outerHeight() + 90)) : ($(".menu-press").removeClass("fixed"), $(".wrapper").css("padding-top", "0"))
    }), $(document).on("click", "#send-doc", function() {
        valid_email_address($("#send-doc-email").val()) ? ($(".popup__form__error").removeClass("active"), $("#popup-text").load("ajax/success-send-doc.html .wrapper")) : $(".popup__form__error").addClass("active")
    }), $(document).on("click", ".menu-press a", function(e) {
        var t = $(this).attr("href"),
            n = $(".header-bottom").innerHeight() + $(".menu-press").innerHeight(),
            i = $(t).offset().top,
            o = n;
        i -= o, $("html, body").stop().animate({
            scrollTop: i
        }, 1e3), e.preventDefault()
    }), $(document).on("click", ".main-nav__item--collapse", function(e) {
        e.target != $("#services")[0] && e.target != $("#services_arrow")[0] || (e.preventDefault(), $(".header").toggleClass("top-element"), $(this).toggleClass("collapsed"), $(".main-nav__item__subcontent h4 a") && $(window).width() > 1024 && equalheight(".main-nav__item__subcontent h4 a"))
    }), $(window).load(function() {
        $(window).width() > 1024 && equalheight(".main-nav__item__subcontent h4 a"), $(".main-accordion-tabs.is-tab").is(":visible") && equalheight(".tab-header-and-content")
    }), $(window).resize(function() {
        $(window).width() > 1024 && equalheight(".main-nav__item__subcontent h4 a"), $(".main-accordion-tabs.is-tab").is(":visible") && equalheight(".tab-header-and-content")
    }), ! function(e) {
        "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
    }(function(e) {
        e.fn.jScrollPane = function(t) {
            function n(t, n) {
                function i(n) {
                    var r, a, c, u, d, f, m = !1,
                        g = !1;
                    if (O = n, void 0 === H) d = t.scrollTop(), f = t.scrollLeft(), t.css({
                        overflow: "hidden",
                        padding: 0
                    }), M = t.innerWidth() + ve, R = t.innerHeight(), t.width(M), H = e('<div class="jspPane" />').css("padding", ge).append(t.children()), B = e('<div class="jspContainer" />').css({
                        width: M + "px",
                        height: R + "px"
                    }).append(H).appendTo(t);
                    else {
                        if (t.css("width", ""), m = O.stickToBottom && j(), g = O.stickToRight && S(), u = t.innerWidth() + ve != M || t.outerHeight() != R, u && (M = t.innerWidth() + ve, R = t.innerHeight(), B.css({
                                width: M + "px",
                                height: R + "px"
                            })), !u && ye == F && H.outerHeight() == X) return void t.width(M);
                        ye = F, H.css("width", ""), t.width(M), B.find(">.jspVerticalBar,>.jspHorizontalBar").remove().end()
                    }
                    H.css("overflow", "auto"), F = n.contentWidth ? n.contentWidth : H[0].scrollWidth, X = H[0].scrollHeight, H.css("overflow", ""), V = F / M, Y = X / R, U = Y > 1, G = V > 1, G || U ? (t.addClass("jspScrollable"), r = O.maintainPosition && (J || te), r && (a = k(), c = _()), o(), s(), l(), r && (C(g ? F - M : a, !1), x(m ? X - R : c, !1)), D(), I(), z(), O.enableKeyboardNavigation && A(), O.clickOnTrack && p(), N(), O.hijackInternalLinks && L()) : (t.removeClass("jspScrollable"), H.css({
                        top: 0,
                        left: 0,
                        width: B.width() - ve
                    }), $(), P(), q(), h()), O.autoReinitialise && !me ? me = setInterval(function() {
                        i(O)
                    }, O.autoReinitialiseDelay) : !O.autoReinitialise && me && clearInterval(me), d && t.scrollTop(0) && x(d, !1), f && t.scrollLeft(0) && C(f, !1), t.trigger("jsp-initialised", [G || U])
                }

                function o() {
                    U && (B.append(e('<div class="jspVerticalBar" />').append(e('<div class="jspCap jspCapTop" />'), e('<div class="jspTrack" />').append(e('<div class="jspDrag" />').append(e('<div class="jspDragTop" />'), e('<div class="jspDragBottom" />'))), e('<div class="jspCap jspCapBottom" />'))), ne = B.find(">.jspVerticalBar"), ie = ne.find(">.jspTrack"), Q = ie.find(">.jspDrag"), O.showArrows && (ae = e('<a class="jspArrow jspArrowUp" />').bind("mousedown.jsp", u(0, -1)).bind("click.jsp", E), le = e('<a class="jspArrow jspArrowDown" />').bind("mousedown.jsp", u(0, 1)).bind("click.jsp", E), O.arrowScrollOnHover && (ae.bind("mouseover.jsp", u(0, -1, ae)), le.bind("mouseover.jsp", u(0, 1, le))), c(ie, O.verticalArrowPositions, ae, le)), re = R, B.find(">.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow").each(function() {
                        re -= e(this).outerHeight()
                    }), Q.hover(function() {
                        Q.addClass("jspHover")
                    }, function() {
                        Q.removeClass("jspHover")
                    }).bind("mousedown.jsp", function(t) {
                        e("html").bind("dragstart.jsp selectstart.jsp", E), Q.addClass("jspActive");
                        var n = t.pageY - Q.position().top;
                        return e("html").bind("mousemove.jsp", function(e) {
                            m(e.pageY - n, !1)
                        }).bind("mouseup.jsp mouseleave.jsp", f), !1
                    }), r())
                }

                function r() {
                    ie.height(re + "px"), J = 0, oe = O.verticalGutter + ie.outerWidth(), H.width(M - oe - ve);
                    try {
                        0 === ne.position().left && H.css("margin-left", oe + "px")
                    } catch (e) {}
                }

                function s() {
                    G && (B.append(e('<div class="jspHorizontalBar" />').append(e('<div class="jspCap jspCapLeft" />'), e('<div class="jspTrack" />').append(e('<div class="jspDrag" />').append(e('<div class="jspDragLeft" />'), e('<div class="jspDragRight" />'))), e('<div class="jspCap jspCapRight" />'))), ce = B.find(">.jspHorizontalBar"), ue = ce.find(">.jspTrack"), Z = ue.find(">.jspDrag"), O.showArrows && (he = e('<a class="jspArrow jspArrowLeft" />').bind("mousedown.jsp", u(-1, 0)).bind("click.jsp", E), fe = e('<a class="jspArrow jspArrowRight" />').bind("mousedown.jsp", u(1, 0)).bind("click.jsp", E), O.arrowScrollOnHover && (he.bind("mouseover.jsp", u(-1, 0, he)), fe.bind("mouseover.jsp", u(1, 0, fe))), c(ue, O.horizontalArrowPositions, he, fe)), Z.hover(function() {
                        Z.addClass("jspHover")
                    }, function() {
                        Z.removeClass("jspHover")
                    }).bind("mousedown.jsp", function(t) {
                        e("html").bind("dragstart.jsp selectstart.jsp", E), Z.addClass("jspActive");
                        var n = t.pageX - Z.position().left;
                        return e("html").bind("mousemove.jsp", function(e) {
                            v(e.pageX - n, !1)
                        }).bind("mouseup.jsp mouseleave.jsp", f), !1
                    }), de = B.innerWidth(), a())
                }

                function a() {
                    B.find(">.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow").each(function() {
                        de -= e(this).outerWidth()
                    }), ue.width(de + "px"), te = 0
                }

                function l() {
                    if (G && U) {
                        var t = ue.outerHeight(),
                            n = ie.outerWidth();
                        re -= t, e(ce).find(">.jspCap:visible,>.jspArrow").each(function() {
                            de += e(this).outerWidth()
                        }), de -= n, R -= n, M -= t, ue.parent().append(e('<div class="jspCorner" />').css("width", t + "px")), r(), a()
                    }
                    G && H.width(B.outerWidth() - ve + "px"), X = H.outerHeight(), Y = X / R, G && (pe = Math.ceil(1 / V * de), pe > O.horizontalDragMaxWidth ? pe = O.horizontalDragMaxWidth : pe < O.horizontalDragMinWidth && (pe = O.horizontalDragMinWidth), Z.width(pe + "px"), ee = de - pe, y(te)), U && (se = Math.ceil(1 / Y * re), se > O.verticalDragMaxHeight ? se = O.verticalDragMaxHeight : se < O.verticalDragMinHeight && (se = O.verticalDragMinHeight), Q.height(se + "px"), K = re - se, g(J))
                }

                function c(e, t, n, i) {
                    var o, r = "before",
                        s = "after";
                    "os" == t && (t = /Mac/.test(navigator.platform) ? "after" : "split"), t == r ? s = t : t == s && (r = t, o = n, n = i, i = o), e[r](n)[s](i)
                }

                function u(e, t, n) {
                    return function() {
                        return d(e, t, this, n), this.blur(), !1
                    }
                }

                function d(t, n, i, o) {
                    i = e(i).addClass("jspActive");
                    var r, s, a = !0,
                        l = function() {
                            0 !== t && we.scrollByX(t * O.arrowButtonSpeed), 0 !== n && we.scrollByY(n * O.arrowButtonSpeed), s = setTimeout(l, a ? O.initialDelay : O.arrowRepeatFreq), a = !1
                        };
                    l(), r = o ? "mouseout.jsp" : "mouseup.jsp", o = o || e("html"), o.bind(r, function() {
                        i.removeClass("jspActive"), s && clearTimeout(s), s = null, o.unbind(r)
                    })
                }

                function p() {
                    h(), U && ie.bind("mousedown.jsp", function(t) {
                        if (void 0 === t.originalTarget || t.originalTarget == t.currentTarget) {
                            var n, i = e(this),
                                o = i.offset(),
                                r = t.pageY - o.top - J,
                                s = !0,
                                a = function() {
                                    var e = i.offset(),
                                        o = t.pageY - e.top - se / 2,
                                        c = R * O.scrollPagePercent,
                                        u = K * c / (X - R);
                                    if (0 > r) J - u > o ? we.scrollByY(-c) : m(o);
                                    else {
                                        if (!(r > 0)) return void l();
                                        o > J + u ? we.scrollByY(c) : m(o)
                                    }
                                    n = setTimeout(a, s ? O.initialDelay : O.trackClickRepeatFreq), s = !1
                                },
                                l = function() {
                                    n && clearTimeout(n), n = null, e(document).unbind("mouseup.jsp", l)
                                };
                            return a(), e(document).bind("mouseup.jsp", l), !1
                        }
                    }), G && ue.bind("mousedown.jsp", function(t) {
                        if (void 0 === t.originalTarget || t.originalTarget == t.currentTarget) {
                            var n, i = e(this),
                                o = i.offset(),
                                r = t.pageX - o.left - te,
                                s = !0,
                                a = function() {
                                    var e = i.offset(),
                                        o = t.pageX - e.left - pe / 2,
                                        c = M * O.scrollPagePercent,
                                        u = ee * c / (F - M);
                                    if (0 > r) te - u > o ? we.scrollByX(-c) : v(o);
                                    else {
                                        if (!(r > 0)) return void l();
                                        o > te + u ? we.scrollByX(c) : v(o)
                                    }
                                    n = setTimeout(a, s ? O.initialDelay : O.trackClickRepeatFreq), s = !1
                                },
                                l = function() {
                                    n && clearTimeout(n), n = null, e(document).unbind("mouseup.jsp", l)
                                };
                            return a(), e(document).bind("mouseup.jsp", l), !1
                        }
                    })
                }

                function h() {
                    ue && ue.unbind("mousedown.jsp"), ie && ie.unbind("mousedown.jsp")
                }

                function f() {
                    e("html").unbind("dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp"), Q && Q.removeClass("jspActive"), Z && Z.removeClass("jspActive")
                }

                function m(n, i) {
                    if (U) {
                        0 > n ? n = 0 : n > K && (n = K);
                        var o = new e.Event("jsp-will-scroll-y");
                        if (t.trigger(o, [n]), !o.isDefaultPrevented()) {
                            var r = n || 0,
                                s = 0 === r,
                                a = r == K,
                                l = n / K,
                                c = -l * (X - R);
                            void 0 === i && (i = O.animateScroll), i ? we.animate(Q, "top", n, g, function() {
                                t.trigger("jsp-user-scroll-y", [-c, s, a])
                            }) : (Q.css("top", n), g(n), t.trigger("jsp-user-scroll-y", [-c, s, a]))
                        }
                    }
                }

                function g(e) {
                    void 0 === e && (e = Q.position().top), B.scrollTop(0), J = e || 0;
                    var n = 0 === J,
                        i = J == K,
                        o = e / K,
                        r = -o * (X - R);
                    (be != n || Ce != i) && (be = n, Ce = i, t.trigger("jsp-arrow-change", [be, Ce, xe, Te])), w(n, i), H.css("top", r), t.trigger("jsp-scroll-y", [-r, n, i]).trigger("scroll")
                }

                function v(n, i) {
                    if (G) {
                        0 > n ? n = 0 : n > ee && (n = ee);
                        var o = new e.Event("jsp-will-scroll-x");
                        if (t.trigger(o, [n]), !o.isDefaultPrevented()) {
                            var r = n || 0,
                                s = 0 === r,
                                a = r == ee,
                                l = n / ee,
                                c = -l * (F - M);
                            void 0 === i && (i = O.animateScroll), i ? we.animate(Z, "left", n, y, function() {
                                t.trigger("jsp-user-scroll-x", [-c, s, a])
                            }) : (Z.css("left", n), y(n), t.trigger("jsp-user-scroll-x", [-c, s, a]))
                        }
                    }
                }

                function y(e) {
                    void 0 === e && (e = Z.position().left), B.scrollTop(0), te = e || 0;
                    var n = 0 === te,
                        i = te == ee,
                        o = e / ee,
                        r = -o * (F - M);
                    (xe != n || Te != i) && (xe = n, Te = i, t.trigger("jsp-arrow-change", [be, Ce, xe, Te])), b(n, i), H.css("left", r), t.trigger("jsp-scroll-x", [-r, n, i]).trigger("scroll")
                }

                function w(e, t) {
                    O.showArrows && (ae[e ? "addClass" : "removeClass"]("jspDisabled"), le[t ? "addClass" : "removeClass"]("jspDisabled"))
                }

                function b(e, t) {
                    O.showArrows && (he[e ? "addClass" : "removeClass"]("jspDisabled"), fe[t ? "addClass" : "removeClass"]("jspDisabled"))
                }

                function x(e, t) {
                    var n = e / (X - R);
                    m(n * K, t)
                }

                function C(e, t) {
                    var n = e / (F - M);
                    v(n * ee, t)
                }

                function T(t, n, i) {
                    var o, r, s, a, l, c, u, d, p, h = 0,
                        f = 0;
                    try {
                        o = e(t)
                    } catch (e) {
                        return
                    }
                    for (r = o.outerHeight(), s = o.outerWidth(), B.scrollTop(0), B.scrollLeft(0); !o.is(".jspPane");)
                        if (h += o.position().top, f += o.position().left, o = o.offsetParent(), /^body|html$/i.test(o[0].nodeName)) return;
                    a = _(), c = a + R, a > h || n ? d = h - O.horizontalGutter : h + r > c && (d = h - R + r + O.horizontalGutter), isNaN(d) || x(d, i), l = k(), u = l + M, l > f || n ? p = f - O.horizontalGutter : f + s > u && (p = f - M + s + O.horizontalGutter), isNaN(p) || C(p, i)
                }

                function k() {
                    return -H.position().left
                }

                function _() {
                    return -H.position().top
                }

                function j() {
                    var e = X - R;
                    return e > 20 && e - _() < 10
                }

                function S() {
                    var e = F - M;
                    return e > 20 && e - k() < 10
                }

                function I() {
                    B.unbind(_e).bind(_e, function(e, t, n, i) {
                        te || (te = 0), J || (J = 0);
                        var o = te,
                            r = J,
                            s = e.deltaFactor || O.mouseWheelSpeed;
                        return we.scrollBy(n * s, -i * s, !1), o == te && r == J
                    })
                }

                function $() {
                    B.unbind(_e)
                }

                function E() {
                    return !1
                }

                function D() {
                    H.find(":input,a").unbind("focus.jsp").bind("focus.jsp", function(e) {
                        T(e.target, !1)
                    })
                }

                function P() {
                    H.find(":input,a").unbind("focus.jsp")
                }

                function A() {
                    function n() {
                        var e = te,
                            t = J;
                        switch (i) {
                            case 40:
                                we.scrollByY(O.keyboardSpeed, !1);
                                break;
                            case 38:
                                we.scrollByY(-O.keyboardSpeed, !1);
                                break;
                            case 34:
                            case 32:
                                we.scrollByY(R * O.scrollPagePercent, !1);
                                break;
                            case 33:
                                we.scrollByY(-R * O.scrollPagePercent, !1);
                                break;
                            case 39:
                                we.scrollByX(O.keyboardSpeed, !1);
                                break;
                            case 37:
                                we.scrollByX(-O.keyboardSpeed, !1)
                        }
                        return o = e != te || t != J
                    }
                    var i, o, r = [];
                    G && r.push(ce[0]), U && r.push(ne[0]), H.bind("focus.jsp", function() {
                        t.focus()
                    }), t.attr("tabindex", 0).unbind("keydown.jsp keypress.jsp").bind("keydown.jsp", function(t) {
                        if (t.target === this || r.length && e(t.target).closest(r).length) {
                            var s = te,
                                a = J;
                            switch (t.keyCode) {
                                case 40:
                                case 38:
                                case 34:
                                case 32:
                                case 33:
                                case 39:
                                case 37:
                                    i = t.keyCode, n();
                                    break;
                                case 35:
                                    x(X - R), i = null;
                                    break;
                                case 36:
                                    x(0), i = null
                            }
                            return o = t.keyCode == i && s != te || a != J, !o
                        }
                    }).bind("keypress.jsp", function(t) {
                        return t.keyCode == i && n(), t.target === this || r.length && e(t.target).closest(r).length ? !o : void 0
                    }), O.hideFocus ? (t.css("outline", "none"), "hideFocus" in B[0] && t.attr("hideFocus", !0)) : (t.css("outline", ""), "hideFocus" in B[0] && t.attr("hideFocus", !1))
                }

                function q() {
                    t.attr("tabindex", "-1").removeAttr("tabindex").unbind("keydown.jsp keypress.jsp"), H.unbind(".jsp")
                }

                function N() {
                    if (location.hash && location.hash.length > 1) {
                        var t, n, i = escape(location.hash.substr(1));
                        try {
                            t = e("#" + i + ', a[name="' + i + '"]')
                        } catch (e) {
                            return
                        }
                        t.length && H.find(i) && (0 === B.scrollTop() ? n = setInterval(function() {
                            B.scrollTop() > 0 && (T(t, !0), e(document).scrollTop(B.position().top), clearInterval(n))
                        }, 50) : (T(t, !0), e(document).scrollTop(B.position().top)))
                    }
                }

                function L() {
                    e(document.body).data("jspHijack") || (e(document.body).data("jspHijack", !0), e(document.body).delegate('a[href*="#"]', "click", function(t) {
                        var n, i, o, r, s, a, l = this.href.substr(0, this.href.indexOf("#")),
                            c = location.href;
                        if (-1 !== location.href.indexOf("#") && (c = location.href.substr(0, location.href.indexOf("#"))), l === c) {
                            n = escape(this.href.substr(this.href.indexOf("#") + 1));
                            try {
                                i = e("#" + n + ', a[name="' + n + '"]')
                            } catch (e) {
                                return
                            }
                            i.length && (o = i.closest(".jspScrollable"), r = o.data("jsp"), r.scrollToElement(i, !0), o[0].scrollIntoView && (s = e(window).scrollTop(), a = i.offset().top, (s > a || a > s + e(window).height()) && o[0].scrollIntoView()), t.preventDefault())
                        }
                    }))
                }

                function z() {
                    var e, t, n, i, o, r = !1;
                    B.unbind("touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick").bind("touchstart.jsp", function(s) {
                        var a = s.originalEvent.touches[0];
                        e = k(), t = _(), n = a.pageX, i = a.pageY, o = !1, r = !0
                    }).bind("touchmove.jsp", function(s) {
                        if (r) {
                            var a = s.originalEvent.touches[0],
                                l = te,
                                c = J;
                            return we.scrollTo(e + n - a.pageX, t + i - a.pageY), o = o || Math.abs(n - a.pageX) > 5 || Math.abs(i - a.pageY) > 5, l == te && c == J
                        }
                    }).bind("touchend.jsp", function() {
                        r = !1
                    }).bind("click.jsp-touchclick", function() {
                        return o ? (o = !1, !1) : void 0
                    })
                }

                function W() {
                    var e = _(),
                        n = k();
                    t.removeClass("jspScrollable").unbind(".jsp"), H.unbind(".jsp"), t.replaceWith(ke.append(H.children())), ke.scrollTop(e), ke.scrollLeft(n), me && clearInterval(me)
                }
                var O, H, M, R, B, F, X, V, Y, U, G, Q, K, J, Z, ee, te, ne, ie, oe, re, se, ae, le, ce, ue, de, pe, he, fe, me, ge, ve, ye, we = this,
                    be = !0,
                    xe = !0,
                    Ce = !1,
                    Te = !1,
                    ke = t.clone(!1, !1).empty(),
                    _e = e.fn.mwheelIntent ? "mwheelIntent.jsp" : "mousewheel.jsp";
                "border-box" === t.css("box-sizing") ? (ge = 0, ve = 0) : (ge = t.css("paddingTop") + " " + t.css("paddingRight") + " " + t.css("paddingBottom") + " " + t.css("paddingLeft"), ve = (parseInt(t.css("paddingLeft"), 10) || 0) + (parseInt(t.css("paddingRight"), 10) || 0)), e.extend(we, {
                    reinitialise: function(t) {
                        t = e.extend({}, O, t), i(t)
                    },
                    scrollToElement: function(e, t, n) {
                        T(e, t, n)
                    },
                    scrollTo: function(e, t, n) {
                        C(e, n), x(t, n)
                    },
                    scrollToX: function(e, t) {
                        C(e, t)
                    },
                    scrollToY: function(e, t) {
                        x(e, t)
                    },
                    scrollToPercentX: function(e, t) {
                        C(e * (F - M), t)
                    },
                    scrollToPercentY: function(e, t) {
                        x(e * (X - R), t)
                    },
                    scrollBy: function(e, t, n) {
                        we.scrollByX(e, n), we.scrollByY(t, n)
                    },
                    scrollByX: function(e, t) {
                        var n = k() + Math[0 > e ? "floor" : "ceil"](e),
                            i = n / (F - M);
                        v(i * ee, t)
                    },
                    scrollByY: function(e, t) {
                        var n = _() + Math[0 > e ? "floor" : "ceil"](e),
                            i = n / (X - R);
                        m(i * K, t)
                    },
                    positionDragX: function(e, t) {
                        v(e, t)
                    },
                    positionDragY: function(e, t) {
                        m(e, t)
                    },
                    animate: function(e, t, n, i, o) {
                        var r = {};
                        r[t] = n, e.animate(r, {
                            duration: O.animateDuration,
                            easing: O.animateEase,
                            queue: !1,
                            step: i,
                            complete: o
                        })
                    },
                    getContentPositionX: function() {
                        return k()
                    },
                    getContentPositionY: function() {
                        return _()
                    },
                    getContentWidth: function() {
                        return F
                    },
                    getContentHeight: function() {
                        return X
                    },
                    getPercentScrolledX: function() {
                        return k() / (F - M)
                    },
                    getPercentScrolledY: function() {
                        return _() / (X - R)
                    },
                    getIsScrollableH: function() {
                        return G
                    },
                    getIsScrollableV: function() {
                        return U
                    },
                    getContentPane: function() {
                        return H
                    },
                    scrollToBottom: function(e) {
                        m(K, e)
                    },
                    hijackInternalLinks: e.noop,
                    destroy: function() {
                        W()
                    }
                }), i(n)
            }
            return t = e.extend({}, e.fn.jScrollPane.defaults, t), e.each(["arrowButtonSpeed", "trackClickSpeed", "keyboardSpeed"], function() {
                t[this] = t[this] || t.speed
            }), this.each(function() {
                var i = e(this),
                    o = i.data("jsp");
                o ? o.reinitialise(t) : (e("script", i).filter('[type="text/javascript"],:not([type])').remove(), o = new n(i, t), i.data("jsp", o))
            })
        }, e.fn.jScrollPane.defaults = {
            showArrows: !1,
            maintainPosition: !0,
            stickToBottom: !1,
            stickToRight: !1,
            clickOnTrack: !0,
            autoReinitialise: !1,
            autoReinitialiseDelay: 500,
            verticalDragMinHeight: 0,
            verticalDragMaxHeight: 99999,
            horizontalDragMinWidth: 0,
            horizontalDragMaxWidth: 99999,
            contentWidth: void 0,
            animateScroll: !1,
            animateDuration: 300,
            animateEase: "linear",
            hijackInternalLinks: !1,
            verticalGutter: 4,
            horizontalGutter: 4,
            mouseWheelSpeed: 3,
            arrowButtonSpeed: 0,
            arrowRepeatFreq: 50,
            arrowScrollOnHover: !1,
            trackClickSpeed: 0,
            trackClickRepeatFreq: 70,
            verticalArrowPositions: "split",
            horizontalArrowPositions: "split",
            enableKeyboardNavigation: !0,
            hideFocus: !1,
            keyboardSpeed: 0,
            initialDelay: 300,
            speed: 30,
            scrollPagePercent: .8
        }
    }),
    function(e) {
        "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e : e(jQuery)
    }(function(e) {
        function t(t) {
            var s = t || window.event,
                a = l.call(arguments, 1),
                c = 0,
                d = 0,
                p = 0,
                h = 0,
                f = 0,
                m = 0;
            if (t = e.event.fix(s), t.type = "mousewheel", "detail" in s && (p = s.detail * -1), "wheelDelta" in s && (p = s.wheelDelta), "wheelDeltaY" in s && (p = s.wheelDeltaY), "wheelDeltaX" in s && (d = s.wheelDeltaX * -1), "axis" in s && s.axis === s.HORIZONTAL_AXIS && (d = p * -1, p = 0), c = 0 === p ? d : p, "deltaY" in s && (p = s.deltaY * -1, c = p), "deltaX" in s && (d = s.deltaX, 0 === p && (c = d * -1)), 0 !== p || 0 !== d) {
                if (1 === s.deltaMode) {
                    var g = e.data(this, "mousewheel-line-height");
                    c *= g, p *= g, d *= g
                } else if (2 === s.deltaMode) {
                    var v = e.data(this, "mousewheel-page-height");
                    c *= v, p *= v, d *= v
                }
                if (h = Math.max(Math.abs(p), Math.abs(d)), (!r || h < r) && (r = h, i(s, h) && (r /= 40)), i(s, h) && (c /= 40, d /= 40, p /= 40), c = Math[c >= 1 ? "floor" : "ceil"](c / r), d = Math[d >= 1 ? "floor" : "ceil"](d / r), p = Math[p >= 1 ? "floor" : "ceil"](p / r), u.settings.normalizeOffset && this.getBoundingClientRect) {
                    var y = this.getBoundingClientRect();
                    f = t.clientX - y.left, m = t.clientY - y.top
                }
                return t.deltaX = d, t.deltaY = p, t.deltaFactor = r, t.offsetX = f, t.offsetY = m, t.deltaMode = 0, a.unshift(t, c, d, p), o && clearTimeout(o), o = setTimeout(n, 200), (e.event.dispatch || e.event.handle).apply(this, a)
            }
        }

        function n() {
            r = null
        }

        function i(e, t) {
            return u.settings.adjustOldDeltas && "mousewheel" === e.type && t % 120 === 0
        }
        var o, r, s = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"],
            a = "onwheel" in document || document.documentMode >= 9 ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"],
            l = Array.prototype.slice;
        if (e.event.fixHooks)
            for (var c = s.length; c;) e.event.fixHooks[s[--c]] = e.event.mouseHooks;
        var u = e.event.special.mousewheel = {
            version: "3.1.12",
            setup: function() {
                if (this.addEventListener)
                    for (var n = a.length; n;) this.addEventListener(a[--n], t, !1);
                else this.onmousewheel = t;
                e.data(this, "mousewheel-line-height", u.getLineHeight(this)), e.data(this, "mousewheel-page-height", u.getPageHeight(this))
            },
            teardown: function() {
                if (this.removeEventListener)
                    for (var n = a.length; n;) this.removeEventListener(a[--n], t, !1);
                else this.onmousewheel = null;
                e.removeData(this, "mousewheel-line-height"), e.removeData(this, "mousewheel-page-height")
            },
            getLineHeight: function(t) {
                var n = e(t),
                    i = n["offsetParent" in e.fn ? "offsetParent" : "parent"]();
                return i.length || (i = e("body")), parseInt(i.css("fontSize"), 10) || parseInt(n.css("fontSize"), 10) || 16
            },
            getPageHeight: function(t) {
                return e(t).height()
            },
            settings: {
                adjustOldDeltas: !0,
                normalizeOffset: !0
            }
        };
        e.fn.extend({
            mousewheel: function(e) {
                return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
            },
            unmousewheel: function(e) {
                return this.unbind("mousewheel", e)
            }
        })
    }), $(function() {
        function e() {
            $(".custom-scroll") && $(window).width() > 768 && $(".custom-scroll").bind("jsp-scroll-y", function(e, t, n, i) {
                $(".person-box__item-pic").css("top", t), $.each($(".person-box__item:not(.active)"), function() {
                    prev = $(this).prev().data("person"),
                        next = $(this).next().data("person");
                    var e = $(this).outerHeight() + $(this).position().top - 300;
                    $(this).position().top - 300 <= t && t <= e && (idPerson = $(this).data("person"), $("#" + idPerson).closest(".person-box__item").addClass("active"))
                })
            }).jScrollPane({
                showArrows: !0,
                animateEase: "easing"
            })
        }
        e(), $(window).resize(function() {
            e()
        })
    }), $(document).ready(function() {
        "use strict";
        $(".person-slider.owl-carousel").owlCarousel({
            autoPlay: !1,
            items: 1,
            itemsMobile: [479, 1],
            itemsDesktop: [1e3, 1],
            itemsDesktopSmall: [900, 1],
            itemsTablet: [600, 1],
            touchDrag: !0,
            navigation: !1,
            pagination: !0,
            navigationText: !1,
            margin: 0
        }), $(".owl-carousel.review-slider").owlCarousel({
            autoPlay: !1,
            items: 1,
            itemsMobile: [479, 1],
            itemsDesktop: [1e3, 1],
            itemsDesktopSmall: [900, 1],
            itemsTablet: [600, 1],
            touchDrag: !0,
            navigation: !0,
            pagination: !0,
            navigationText: !1,
            margin: 0
        })
    }), $(function() {
        function e() {
            if ($(".article-box__author").length > 0 && $(window).width() > 768) {
                var e = $(".article-box__author"),
                    t = $(e).height(),
                    n = ($(e).offset().top, $("footer").height()),
                    i = window.pageYOffset,
                    o = $(".article-box").offset().top + $(".article-box").height(),
                    r = i + n;
                $(".wrapper").height() + $(".header").height() + $(".footer").height();
                r >= o - t ? $(".article-box__author").addClass("fixed") : $(".article-box__author").removeClass("fixed")
            }
        }
        $(window).scroll(function() {
            e()
        })
    });