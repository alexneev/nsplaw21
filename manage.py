#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This is a manage file for nsp project.
"""

import os
import sys

__author__ = 'sobolevn'


def main():
    """ Main function. """
    from django.core.management import execute_from_command_line

    if 'test' in sys.argv:
        os.environ.setdefault('_DJANGO_TESTING', 'True')

    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nsp.settings')
    execute_from_command_line(sys.argv)

if __name__ == '__main__':
    main()
