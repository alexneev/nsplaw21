# -*- coding: utf-8 -*-
import logging

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.db.models import F
from djchoices import ChoiceItem
from djchoices import DjangoChoices

from landing.utils import find_plugin_on_page, text_to_list


from landing.forms import NewsCMSModelForm, NewsBlockChooseOneForm, CommentsCMSModelForm, PersonCMSmodelForm, \
    InformPluginForm
from landing.models import (
    RatingCMS, ExpertiseContainer, ExpertiseItem, NewsCMS,
    MainQuoteForServiceCMS, QuoteCMS, QuoteContainerCMS, FirstSlideCMS,
    WeDoMoreCMS, AboutUsCMS, WorldWideCMS, FactInNumberCMS, PublicationCMS,
    CommentsInMediaCMS, DoneProjectCMS, PersonCMS, AboutServiceCMS, SubSerivcesContainerCMS,
    ContactPeopleContainerCMS, ContactPersonCMS, NewsBlockContainerCMS, NewsItemCMS, BiographyContainerCMS,
    BiographyBlockCMS, UploadedFile, ContainerBlockCMS, NewsroomNewsCMS, ContactFormContainerCMS,
    ContactFormItemCMS, NewsInformCMS, SubServiceDetailCMS, SubServiceFeaturesContainerCMS,
    SubServiceFeaturesItemCMS, NewsPageTitleCMS, NewsPageDesctiptionCMS, NewsPageMainTextCMS, NewsPageQuoteCMS,
    NewsPagePersonCMS, NewsPageDateCMS, NewsPagePhotoCMS, NewsPageLinkCMS, ContactFormLatsItemCMS,
    NewsBlockChooseOneCMS, NewsRoomGenerateAllNewsCMS, SubServiceListCMS, FooterCMS, PersonPhotoCMS,
    YouTubeVideoContainerCMS, YouTubeVideoCMS, VideoCMS)

from cms.models.pagemodel import Page

logger = logging.getLogger(__name__)


# ----------------------------------------------------------------------
# Common section
# ----------------------------------------------------------------------


# logos below quotes
class RatingPlugin(CMSPluginBase):
    render_template = "landing/_plugins/common/ratings.html"
    model = RatingCMS
    module = "Common"
    name = "RatingsLogo"
    cache = True


# quote container
class QuotesContainerPlugin(CMSPluginBase):
    render_template = "landing/_plugins/common/quotes.html"
    model = QuoteContainerCMS
    module = "Common"
    name = "QuotesContainer"
    cache = True
    allow_children = True
    child_classes = ['QuotesPlugin']


# quote item
class QuotesPlugin(CMSPluginBase):
    render_template = "landing/_plugins/common/qoute_item.html"
    model = QuoteCMS
    module = "Common"
    name = "Quote"
    cache = True
    require_parent = True
    parent_classes = ['QuotesContainerPlugin']


class Footer(CMSPluginBase):
    render_template = "landing/_parts/footer.html"
    model = FooterCMS
    module = "Common"
    name = "Footer"
    cache = True

    MAX_SERVICES_IN_ROW = 5
    MAX_ROWS = 2

    def render(self, context, instance, placeholder):
        left_menu = Page.objects.filter(reverse_id='left-menu').first().children.all()
        right_menu = Page.objects.filter(reverse_id='right-menu').first().children.all()
        parent_page = Page.objects.filter(reverse_id="services").public().first()
        if Page.objects.filter(reverse_id="bondholders").first().is_published(context['LANGUAGE_CODE']):
            bondholders = Page.objects.filter(reverse_id="bondholders").first()
        else:
            bondholders = False
        sub_pages = parent_page.get_children()
        sub_pages = [
            sub_page for sub_page in sub_pages if sub_page.is_published(context['LANGUAGE_CODE'])
        ]
        count_of_sub_pages = len(sub_pages)
        first_row = []
        second_row = []

        elements_in_first_row = count_of_sub_pages // self.MAX_ROWS + 1 if count_of_sub_pages % self.MAX_ROWS \
            else count_of_sub_pages // self.MAX_ROWS
        for index, item in enumerate(sub_pages):
            first_row.append(item) if index + 1 <= elements_in_first_row else second_row.append(item)
        context['service_rows'] = [first_row, second_row]
        context['services'] = parent_page
        context['bondholders'] = bondholders
        context['left_menu'] = left_menu
        context['right_menu'] = right_menu
        return super(Footer, self).render(context, instance, placeholder)


class ContactFormContainerPlugin(CMSPluginBase):
    model = ContactFormContainerCMS
    render_template = "landing/_plugins/common/contact_form.html"
    module = "Common"
    name = "ContactForm"
    cache = True
    allow_children = True
    child_classes = ['ContactFormItemPlugin', 'ContactFormLastItemPlugin']


class ContactFormItemPlugin(CMSPluginBase):
    model = ContactFormItemCMS
    render_template = "landing/_plugins/common/contact_form_item.html"
    name = "ContactFormItem"
    require_parent = True
    parent_classes = ['ContactFormContainerPlugin']


class ContactFormLastItemPlugin(CMSPluginBase):
    model = ContactFormLatsItemCMS
    render_template = "landing/_plugins/common/contact_form_item_last.html"
    name = "ContactFormLastItem"
    require_parent = True
    parent_classes = ['ContactFormContainerPlugin']


class DownloadFormPlugin(CMSPluginBase):
    render_template = "landing/_plugins/common/download_file.html"


class NewsBlockContextPlugin(CMSPluginBase):
    module = "Common"
    name = "GenerateNewsBlock"
    render_template = "landing/_plugins/common/news_block_context.html"


class NewsBlockChooseOnePlugin(CMSPluginBase):
    model = NewsBlockChooseOneCMS
    form = NewsBlockChooseOneForm
    module = "Common"
    name = "NewsBlockChoose"
    render_template = "landing/_plugins/common/news_block_choose.html"

    def render(self, context, instance, placeholder):
        inform_plugin = find_plugin_on_page(instance.news_page, 'InformPlugin', lang_code=context['LANGUAGE_CODE'])
        try:
            plugin_instance = inform_plugin.get_plugin_instance()[0]
            context['news_data'] = {
                'url': instance.news_page.get_absolute_url(),
                'title': plugin_instance.title,
                'type_of_news': plugin_instance.type_of_news,
                'body_text': plugin_instance.body_text,
                'news_date': plugin_instance.news_date,
                'magazine': plugin_instance.magazine,
                'publication_file': plugin_instance.publication_file,
                'photo': plugin_instance.photo,
            }
        except AttributeError as e:
            logger.warning(e)
        return super(NewsBlockChooseOnePlugin, self).render(context, instance, placeholder)


# ----------------------------------------------------------------------
# Mainpage section
# ----------------------------------------------------------------------                                             \__|                 \______/


# promo movie
class PromoPlugin(CMSPluginBase):
    render_template = "landing/_plugins/index/promo.html"
    name = "Promo"
    cache = True


# container – title and description
class ExpertiseContainerPlugin(CMSPluginBase):
    render_template = "landing/_plugins/index/expertise_container.html"
    model = ExpertiseContainer
    module = "Main page"
    name = "ExpertiseContainer"
    cache = True
    allow_children = True
    child_classes = ['ExpertiseItemPlugin']


class ExpertiseItemPlugin(CMSPluginBase):
    render_template = "landing/_plugins/index/expertise_item.html"
    model = ExpertiseItem
    module = "Main page"
    name = "ExpertiseItem"
    cache = True
    require_parent = True
    parent_classes = ['ExpertiseContainerPlugin']
    allow_children = True
    child_classes = ['ExpertiseItemFilePlugin']


class ExpertiseItemFilePlugin(CMSPluginBase):
    render_template = "landing/_plugins/index/expertise_item_file.html"
    model = UploadedFile
    module = "Main page"
    name = "ExpertiseItemFile"
    cache = True
    require_parent = True
    parent_classes = ['ExpertiseItemPlugin']


class NewsPlugin(CMSPluginBase):
    render_template = "landing/_plugins/index/news_item.html"
    model = NewsCMS
    module = "Main page"
    name = "News"
    form = NewsCMSModelForm
    cache = True

    class TypeOfNews(DjangoChoices):
        SmallNews = ChoiceItem('Small news')
        BigNews = ChoiceItem('Big news')

    def render(self, context, instance, placeholder):
        news_item_big = (
            NewsInformCMS.objects.filter(
                language=context['LANGUAGE_CODE']
            ).filter(
                type_of_news__in=(self.TypeOfNews.SmallNews, self.TypeOfNews.BigNews)
            ).filter(
                mainpage_big=True
            ).filter(
                plugin_type='InformPlugin'
            ).filter(
                placeholder__page__publisher_is_draft=True
            ).order_by(
                '-news_date'
            ).first()
        )
        news_item_small = (
            NewsInformCMS.objects.filter(
                language=context['LANGUAGE_CODE']
            ).filter(
                type_of_news__in=(self.TypeOfNews.SmallNews, self.TypeOfNews.BigNews)
            ).filter(
                mainpage_small=True
            ).filter(
                plugin_type='InformPlugin'
            ).filter(
                placeholder__page__publisher_is_draft=True
            ).order_by(
                '-news_date'
            ).first()
        )
        if news_item_big:
            context['big_news'] = {
                'page_url': news_item_big.placeholder.page.get_absolute_url(),
                'photo_url': news_item_big.photo.url if news_item_big.photo else '',
                'news_date': news_item_big.news_date,
                'title': news_item_big.title,
                'body_text': news_item_big.body_text,
            }
        else:
            context['big_news'] = {}

        if news_item_small:
            context['small_news'] = {
                'page_url': news_item_small.placeholder.page.get_absolute_url(),
                'news_date': news_item_small.news_date,
                'title': news_item_small.title,
                'body_text': news_item_small.body_text,
            }
        else:
            context['small_news'] = {}

        super(NewsPlugin, self).render(context, instance, placeholder)
        return context


class PublicationsPlugin(CMSPluginBase):
    render_template = "landing/_plugins/index/publications.html"
    model = PublicationCMS
    module = "Main page"
    name = "Publications"
    # form = PublicationCMSModelForm
    cache = False


class CommentsInMediaPlugin(CMSPluginBase):
    render_template = "landing/_plugins/index/comments_in_media.html"
    model = CommentsInMediaCMS
    form = CommentsCMSModelForm
    module = "Main page"
    name = "CommentsImMedia"
    cache = True


class DoneProjectsPlugin(CMSPluginBase):
    render_template = "landing/_plugins/index/done_projects.html"
    model = DoneProjectCMS
    module = "Main page"
    name = "DoneProjects"
    cache = True


class VideoContainerPlugin(CMSPluginBase):
    render_template = "landing/_plugins/index/video_container.html"
    model = YouTubeVideoContainerCMS
    module = "Main page"
    name = "VideoBlock"
    cache = False
    allow_children = True
    child_classes = ['VideoPlugin']


class VideoPlugin(CMSPluginBase):
    render_template = "landing/_plugins/index/video_item.html"
    model = YouTubeVideoCMS
    module = "Main page"
    name = "Video"
    cache = False
    allow_parent = True
    parent_classes = ['VideoContainerPlugin']


class VideoBlockPlugin(CMSPluginBase):
    render_template = "landing/_plugins/index/video.html"
    model = VideoCMS
    module = "Main page"
    name = "VideoBlock-2"
    cache = True


class UtilsServiceModel(object):
    """
    Модель описания данных для рендера в плагине ServicesTabsPlugin
    """

    def __init__(self, quote, service_name, language, id):
        self.language = language
        self.active = False
        self.quote = quote
        self.service_name = service_name
        self.sub_services = {
            'first_column': [],
            'second_column': [],
        }
        self.id = id

    def set_active(self):
        self.active = True

    def set_subservices(self, sub_services):
        """
        в дизайне подуслуги разделяются на две колонки
        поэтому нечетные в первую колонку, четные – во вторую
        """
        sub_services = list(filter(
            lambda sub_service: sub_service.is_published(self.language), sub_services
        ))

        for i, sub_service in enumerate(sub_services):
            if i % 2 == 0:
                self.sub_services['first_column'].append(sub_service)
            else:
                self.sub_services['second_column'].append(sub_service)


class ServicesTabsPlugin(CMSPluginBase):
    render_template = "landing/_plugins/index/service_tabs.html"
    model = MainQuoteForServiceCMS
    module = "Main page"
    name = "ServicesTabs"
    cache = True

    # в context добавляется переменная с перечнем страниц услуги и подуслуг
    def render(self, context, instance, placeholder):
        context['all_services'] = []
        all_services = []

        # Django CMS дублирует модели и поэтому используется filter, а не get
        parent_page = Page.objects.filter(reverse_id='services').public().first()
        service_pages = [page for page in parent_page.children.all() if page.is_published(context['LANGUAGE_CODE'])]

        for index, service_page in enumerate(service_pages):
            if hasattr(service_page, 'pagequoteextension'):
                service = UtilsServiceModel(
                    service_page.pagequoteextension.quote,
                    service_page,
                    context['LANGUAGE_CODE'],
                    index
                )
                service.set_subservices(service_page.children.all())
                all_services.append(service)

        count_of_services = len(service_pages)
        if count_of_services > 5:
            first_row = []
            second_row = []
            elements_in_first_row = count_of_services // 2 + 1 if count_of_services % 2 \
                else count_of_services // 2
            for index, item in enumerate(all_services):
                if index + 1 > elements_in_first_row:
                    first_row.append(item)
                else:
                    second_row.append(item)
            context['all_services'] = [second_row, first_row]
        else:
            context['all_services'] = [all_services]

        # первая закладка активна
        if context['all_services']:
            context['all_services'][0][0].active = True
            context['all_services_content'] = all_services
        super(ServicesTabsPlugin, self).render(context, instance, placeholder)
        return context


# ----------------------------------------------------------------------
# About page section
# ----------------------------------------------------------------------

class FirstSlideAboutPlugin(CMSPluginBase):
    model = FirstSlideCMS
    render_template = "landing/_plugins/common/firstslide.html"
    module = "About_page"
    name = "FirstSlideAbout"
    cache = True


class WeDoMorePlugin(CMSPluginBase):
    model = WeDoMoreCMS
    render_template = "landing/_plugins/about/wedomore.html"
    module = "About_page"
    name = "WeDoMore"
    cache = True


class AboutUsPlugin(CMSPluginBase):
    model = AboutUsCMS
    render_template = "landing/_plugins/about/about_us.html"
    module = "About_page"
    name = "AboutUs"
    cache = True


class WorldWidePlugin(CMSPluginBase):
    model = WorldWideCMS
    render_template = "landing/_plugins/about/worldwide.html"
    module = "About_page"
    name = "WorldWide"
    cache = True


class FactInNumberPlugin(CMSPluginBase):
    model = FactInNumberCMS
    render_template = "landing/_plugins/about/factnumber.html"
    module = "About_page"
    name = "FactsAndNumbers"
    cache = True


# ----------------------------------------------------------------------
# Team page section
# ----------------------------------------------------------------------                                               \__|                 \______/


class FirstSlideTeamPlugin(CMSPluginBase):
    model = FirstSlideCMS
    render_template = "landing/_plugins/common/firstslide.html"
    module = "Team_page"
    name = "FirstSlideTeam"
    cache = True


class TeamBlockPlugin(CMSPluginBase):
    render_template = "landing/_plugins/team/team.html"
    module = "Team_page"
    name = "Team"
    cache = True
    allow_children = True
    child_classes = ['PersonPlugin']


class PersonPlugin(CMSPluginBase):
    model = PersonCMS
    render_template = "landing/_plugins/team/person.html"
    form = PersonCMSmodelForm
    module = "Team_page"
    name = "Person"
    cache = True
    allow_parent = True
    parent_classes = ['TeamBlockPlugin']


class BiographyContainerPlugin(CMSPluginBase):
    model = BiographyContainerCMS
    render_template = "landing/_plugins/team/biography_container.html"
    module = "Team_page"
    name = "BiographyContainer"
    cache = True
    allow_children = True
    child_classes = ['BiographyItemPlugin']


class BiographyItemPlugin(CMSPluginBase):
    model = BiographyBlockCMS
    render_template = "landing/_plugins/team/biography_item.html"
    module = "Team_page"
    name = "BiographyItem"
    cache = True
    allow_parent = True
    parent_classes = ['BiographyContainerPlugin']


class PersonPhotoPlugin(CMSPluginBase):
    model = PersonPhotoCMS
    render_template = "landing/_plugins/team/person_photo.html"
    module = "Team_page"
    name = "PersonPhotoItem"
    cache = True


# ----------------------------------------------------------------------
# Services section
# ----------------------------------------------------------------------


class AboutServicePlugin(CMSPluginBase):
    model = AboutServiceCMS
    render_template = "landing/_plugins/services/about_service.html"
    module = "Service_page"
    name = "About_service"
    cache = True


class SubSerivcesContainerPlugin(CMSPluginBase):
    model = SubSerivcesContainerCMS
    render_template = "landing/_plugins/services/sub_services.html"
    module = "Service_page"
    name = "Services"
    cache = True

    def render(self, context, instance, placeholder):
        context['sub_pages'] = []
        current_page = instance.placeholder.page_getter()
        sub_pages = current_page.children.all()
        for page in sub_pages:
            page_plugin = find_plugin_on_page(page, 'Subservice detail', lang_code=context['LANGUAGE_CODE'])
            if page_plugin:
                context['sub_pages'].append({
                    'url': page.get_absolute_url(),
                    'title': page_plugin.get_plugin_instance()[0].title,
                    'body_text': page_plugin.get_plugin_instance()[0].body_text
                })
        return super(SubSerivcesContainerPlugin, self).render(context, instance, placeholder)


# to do – define model and set to container
class DownloadsContainerPlugin(CMSPluginBase):
    render_template = "landing/_plugins/services/sub_services_item.html"
    module = "Service_page"
    name = "Downloads"
    cache = True
    allow_children = True
    child_classes = ['DownloadFilePlugin']


# to do – define model and set to plugin
class DownloadFilePlugin(CMSPluginBase):
    render_template = "landing/_plugins/services/sub_services_item.html"
    module = "Service_page"
    name = "Download File"
    cache = True
    allow_parent = True
    parent_classes = ['DownloadsContainerPlugin']


class ContactPeopleContainerPlugin(CMSPluginBase):
    model = ContactPeopleContainerCMS
    render_template = "landing/_plugins/services/contact_people_container.html"
    module = "Service_page"
    name = "Contact People Container"
    cache = True
    allow_children = True
    child_classes = ['ContactPersonPlugin']


class ContactPersonPlugin(CMSPluginBase):
    model = ContactPersonCMS
    render_template = "landing/_plugins/services/contact_person.html"
    module = "Service_page"
    name = "Contact Person"
    cache = True
    require_parent = True
    parent_classes = ['ContactPeopleContainerPlugin']


class NewsRightBlockContainerPlugin(CMSPluginBase):
    model = NewsBlockContainerCMS
    render_template = "landing/_plugins/services/news_container.html"
    module = "Service_page"
    name = "News Container"
    cache = True
    allow_children = True
    child_classes = ['NewsRightBlockPlugin']


class NewsRightBlockPlugin(CMSPluginBase):
    model = NewsItemCMS
    render_template = "landing/_plugins/services/news_block.html"
    module = "Service_page"
    name = "News Item"
    cache = True
    require_parent = True
    parent_classes = ['NewsRightBlockContainerPlugin']


# sub services plugins

class SubServiceDetailPlugin(CMSPluginBase):
    model = SubServiceDetailCMS
    render_template = "landing/_plugins/sub_services/subservice_detail.html"
    module = "Service_page"
    name = "Subservice detail"
    cache = True


class SubServiceListPlugin(CMSPluginBase):
    model = SubServiceListCMS
    render_template = "landing/_plugins/sub_services/subservice_list.html"
    module = "Service_page"
    name = "SubserviceList"
    cache = True

    def render(self, context, instance, placeholder):
        context['list'] = text_to_list(instance.list)
        return super(SubServiceListPlugin, self).render(context, instance, placeholder)


class SubServiceFeaturesContainerPlugin(CMSPluginBase):
    model = SubServiceFeaturesContainerCMS
    render_template = "landing/_plugins/sub_services/subservice_clientfeatures_container.html"
    module = "Service_page"
    name = "Subservice features container"
    cache = True
    allow_children = True
    child_classes = ['SubServiceFeaturesItemPlugin']


class SubServiceFeaturesItemPlugin(CMSPluginBase):
    model = SubServiceFeaturesItemCMS
    render_template = "landing/_plugins/sub_services/subservice_clientfeatures_item.html"
    module = "Service_page"
    name = "Subservice list item"
    cache = True
    require_parent = True
    parent_classes = ['SubServiceFeaturesContainerPlugin']


# ----------------------------------------------------------------------
# News page section
# ----------------------------------------------------------------------


class NewsBlockContainerPlugin(CMSPluginBase):
    model = ContainerBlockCMS
    render_template = "landing/_plugins/newsroom/block_container.html"
    module = "Newsroom"
    name = "NewsBlock"
    cache = True
    allow_children = True
    child_classes = ['NewsPublicationsItemPlugin']


class NewsPublicationsItemPlugin(CMSPluginBase):
    model = NewsroomNewsCMS
    render_template = "landing/_plugins/newsroom/newspublication_item.html"
    module = "Newsroom"
    name = "NewsItem"
    cache = True
    require_parent = True
    parent_classes = ['NewsBlockContainerPlugin']


class InformPlugin(CMSPluginBase):
    model = NewsInformCMS
    module = "Newsroom"
    render_template = "landing/_plugins/newsroom/inform.html"
    name = "InformPlugin"
    cache = True
    form = InformPluginForm


class TypeOfNews(DjangoChoices):
    SmallNews = ChoiceItem('Small news')
    BigNews = ChoiceItem('Big news')
    Publcication = ChoiceItem('Publication')
    SmallCommentInMedia = ChoiceItem('Small comment')
    BigCommentInMedia = ChoiceItem('Big comment')


def set_news_as_small(news_array, start_number=1):
    result_array = []
    for index, item in enumerate(news_array):
        if index > start_number and item['content'].type_of_news == 'Big news':
            item['content'].type_of_news = 'Small news'
        result_array.append(item)
    return result_array


class BaseNewsRoom(CMSPluginBase):
    COUNT_OF_ITEMS = None
    PARENT_PAGE_ID = None
    TYPE_OF_NEWS = [
        TypeOfNews.SmallNews, TypeOfNews.BigNews,
        TypeOfNews.Publcication, TypeOfNews.SmallCommentInMedia,
        TypeOfNews.BigCommentInMedia
    ]
    FEATURED = [True, False]

    def render(self, context, instance, placeholder):
        if not self.PARENT_PAGE_ID:
            self.PARENT_PAGE_ID = instance.parent_page_id
        context['news_pages'] = []
        news_parent_page = Page.objects.public().get(reverse_id=self.PARENT_PAGE_ID)
        context['news_parent_page'] = news_parent_page
        news_pages = news_parent_page.children.all()

        news_pages_by_id = {p.id: p for p in news_pages}

        news_items = self.get_news(page_ids=news_pages_by_id.keys(), language=context['LANGUAGE_CODE'])
        if self.COUNT_OF_ITEMS:
            news_items = news_items[:self.COUNT_OF_ITEMS]

        for item in news_items:
            news_page = news_pages_by_id[item.page_id]
            context['news_pages'].append({
                'content': item,
                'page_url': news_page.get_absolute_url(),
            })

        context['news_pages'] = set_news_as_small(context['news_pages'], 0)

        return super(BaseNewsRoom, self).render(context, instance, placeholder)

    def get_news(self, page_ids, language):
        news_items = (
            NewsInformCMS.objects
                .filter(placeholder__page__id__in=page_ids)
                .filter(language=language)
                .filter(type_of_news__in=self.TYPE_OF_NEWS)
                .filter(featured_news__in=self.FEATURED)
                .filter(plugin_type='InformPlugin')
                .order_by('-news_date')
                .annotate(page_id=F('placeholder__page__id'))
        )
        return news_items


class NewsRoomGenerateAllNewsPlugin(BaseNewsRoom):
    render_template = "landing/_plugins/newsroom/generate_all_news.html"
    name = "All_News"
    model = NewsRoomGenerateAllNewsCMS
    cache = True

    COUNT_OF_ITEMS = 10


class NewsSubPageGenerateAllNewsPlugin(BaseNewsRoom):
    render_template = "landing/_plugins/newsroom/generate_all_news_subpage.html"
    name = "All_News_Sub_Page"
    model = NewsRoomGenerateAllNewsCMS
    cache = True


class NewsRoomGenerateAllPublicationsPlugin(CMSPluginBase):
    render_template = "landing/_plugins/news_pages/additional_news.html"


class AdditionalNewsBlockPlugin(BaseNewsRoom):
    module = "News Page"
    render_template = "landing/_plugins/news_pages/additional_news.html"
    name = "AdditionalNews"
    cache = True

    COUNT_OF_ITEMS = 2
    PARENT_PAGE_ID = 'news_pages_section'
    TYPE_OF_NEWS = [TypeOfNews.BigNews, TypeOfNews.BigCommentInMedia]
    FEATURED = [True]


# news page parts


class NewsPageTitlePlugin(CMSPluginBase):
    model = NewsPageTitleCMS
    module = "News Page"
    render_template = "landing/_plugins/news_pages/title.html"
    name = "Title"
    cache = True


class NewsPageDesctiptionPlugin(CMSPluginBase):
    model = NewsPageDesctiptionCMS
    module = "News Page"
    render_template = "landing/_plugins/news_pages/description.html"
    name = "Description"
    cache = True


class NewsPageMainTextPlugin(CMSPluginBase):
    model = NewsPageMainTextCMS
    module = "News Page"
    render_template = "landing/_plugins/news_pages/main_text.html"
    name = "Main text"
    cache = True


class NewsPageQuotePlugin(CMSPluginBase):
    model = NewsPageQuoteCMS
    module = "News Page"
    render_template = "landing/_plugins/news_pages/qoutes.html"
    name = "Quotes"
    cache = True


class NewsPagePersonPlugin(CMSPluginBase):
    model = NewsPagePersonCMS
    module = "News Page"
    render_template = "landing/_plugins/news_pages/person.html"
    name = "Person"
    cache = True


class NewsPageDatePlugin(CMSPluginBase):
    model = NewsPageDateCMS
    module = "News Page"
    render_template = "landing/_plugins/news_pages/date.html"
    name = "Date"
    cache = True


class NewsPagePhotoPlugin(CMSPluginBase):
    model = NewsPagePhotoCMS
    module = "News Page"
    render_template = "landing/_plugins/news_pages/photo.html"
    name = "Photo"
    cache = True


class NewsPageLinkPlugin(CMSPluginBase):
    model = NewsPageLinkCMS
    module = "News Page"
    render_template = "landing/_plugins/news_pages/link.html"
    name = "Link"
    cache = True


# common
plugin_pool.register_plugin(RatingPlugin)
plugin_pool.register_plugin(QuotesContainerPlugin)
plugin_pool.register_plugin(QuotesPlugin)
plugin_pool.register_plugin(Footer)
plugin_pool.register_plugin(ContactFormContainerPlugin)
plugin_pool.register_plugin(ContactFormItemPlugin)
plugin_pool.register_plugin(ContactFormLastItemPlugin)
plugin_pool.register_plugin(DownloadFormPlugin)
plugin_pool.register_plugin(NewsBlockContextPlugin)
plugin_pool.register_plugin(NewsBlockChooseOnePlugin)

# main page
plugin_pool.register_plugin(PromoPlugin)
plugin_pool.register_plugin(ExpertiseContainerPlugin)
plugin_pool.register_plugin(ExpertiseItemPlugin)
plugin_pool.register_plugin(ExpertiseItemFilePlugin)
plugin_pool.register_plugin(NewsPlugin)
plugin_pool.register_plugin(PublicationsPlugin)
plugin_pool.register_plugin(CommentsInMediaPlugin)
plugin_pool.register_plugin(DoneProjectsPlugin)
plugin_pool.register_plugin(VideoContainerPlugin)
plugin_pool.register_plugin(VideoPlugin)
plugin_pool.register_plugin(VideoBlockPlugin)
plugin_pool.register_plugin(ServicesTabsPlugin)

# about page
plugin_pool.register_plugin(FirstSlideAboutPlugin)
plugin_pool.register_plugin(WeDoMorePlugin)
plugin_pool.register_plugin(AboutUsPlugin)
plugin_pool.register_plugin(WorldWidePlugin)
plugin_pool.register_plugin(FactInNumberPlugin)

# team page
plugin_pool.register_plugin(FirstSlideTeamPlugin)
plugin_pool.register_plugin(TeamBlockPlugin)
plugin_pool.register_plugin(PersonPlugin)
plugin_pool.register_plugin(BiographyContainerPlugin)
plugin_pool.register_plugin(BiographyItemPlugin)
plugin_pool.register_plugin(PersonPhotoPlugin)

# services
plugin_pool.register_plugin(AboutServicePlugin)
plugin_pool.register_plugin(SubSerivcesContainerPlugin)
plugin_pool.register_plugin(ContactPeopleContainerPlugin)
plugin_pool.register_plugin(ContactPersonPlugin)
plugin_pool.register_plugin(NewsRightBlockContainerPlugin)
plugin_pool.register_plugin(NewsRightBlockPlugin)
plugin_pool.register_plugin(SubServiceDetailPlugin)
plugin_pool.register_plugin(SubServiceListPlugin)
plugin_pool.register_plugin(SubServiceFeaturesContainerPlugin)
plugin_pool.register_plugin(SubServiceFeaturesItemPlugin)

# newsroom
plugin_pool.register_plugin(NewsBlockContainerPlugin)
plugin_pool.register_plugin(NewsPublicationsItemPlugin)
plugin_pool.register_plugin(NewsRoomGenerateAllNewsPlugin)
plugin_pool.register_plugin(NewsSubPageGenerateAllNewsPlugin)
plugin_pool.register_plugin(InformPlugin)

# news page
plugin_pool.register_plugin(NewsPageTitlePlugin)
plugin_pool.register_plugin(NewsPageDesctiptionPlugin)
plugin_pool.register_plugin(NewsPageMainTextPlugin)
plugin_pool.register_plugin(NewsPageQuotePlugin)
plugin_pool.register_plugin(NewsPagePersonPlugin)
plugin_pool.register_plugin(NewsPageDatePlugin)
plugin_pool.register_plugin(NewsPagePhotoPlugin)
plugin_pool.register_plugin(NewsPageLinkPlugin)
plugin_pool.register_plugin(AdditionalNewsBlockPlugin)

plugin_pool.register_plugin(NewsRoomGenerateAllPublicationsPlugin)
