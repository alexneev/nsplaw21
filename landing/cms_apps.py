# -*- coding: utf-8 -*-

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

__author__ = 'sobolevn'


class DownloadAppHook(CMSApp):
    app_name = 'download_app'
    name = 'DownloadAppHook'

    urls = [
        'download_app.urls'
    ]

apphook_pool.register(app=DownloadAppHook)
