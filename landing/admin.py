# -*- coding: utf-8 -*-

"""
landing admin module.
"""
from hvad.admin import TranslatableAdmin, TranslatableStackedInline
from cms.extensions import PageExtensionAdmin
from hvad.forms import TranslatableModelForm
from django.contrib import admin
from landing.models import (
    RatingBase,
    YoutubeVideo,
    QuoteBase,
    NewsCMS,
    PersonBase,
    NewsPaperBase,
    ProjectClientBase,
    UploadedFileBase,
    PageQuoteExtension,
)


class DownloadFileAdmin(admin.ModelAdmin):
    readonly_fields = ('file_uid',)


class QuoteExtensionAdmin(PageExtensionAdmin):
    pass


admin.site.register(PageQuoteExtension, QuoteExtensionAdmin)
admin.site.register(RatingBase, TranslatableAdmin)
admin.site.register(YoutubeVideo, TranslatableAdmin)
admin.site.register(QuoteBase, TranslatableAdmin)
# admin.site.register(NewsCMS)
admin.site.register(PersonBase, TranslatableAdmin)
admin.site.register(NewsPaperBase, TranslatableAdmin)
admin.site.register(ProjectClientBase, TranslatableAdmin)
admin.site.register(UploadedFileBase, TranslatableAdmin)

# Monkey-patch to limit page title length
from cms.models.pagemodel import Page

Page.__get_admin_tree_title = Page.get_admin_tree_title


def get_admin_tree_title(self):
    LIMIT = 80
    title = Page.__get_admin_tree_title(self)
    if len(title) > LIMIT:
        title = title[:LIMIT] + u'…'
    return title


Page.get_admin_tree_title = get_admin_tree_title
