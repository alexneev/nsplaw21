# -*- coding: utf-8 -*-

from django import template
from cms.models import Page

register = template.Library()

#ToDo: improve way to generate template
@register.simple_tag()
def is_page_published(page, language):
    if page.is_published(language):
        return "<a href='{}'>{}</a>".format(page.get_absolute_url(), page)
    return ''
