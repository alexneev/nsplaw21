# -*- coding: utf-8 -*-

from django import template
from cms.models import Page

register = template.Library()

MAX_SERVICES_IN_ROW = 5
MAX_ROWS = 2

@register.inclusion_tag('landing/_tags/show_services_menu.html', takes_context=True)
def show_services_menu(context, services_id, language):
    parent_page = Page.objects.filter(reverse_id=services_id).public().first()
    context['service_parent_page'] = parent_page
    sub_pages = parent_page.get_children()
    sub_pages = [page for page in sub_pages if page.is_published(language)]
    count_of_sub_pages = len(sub_pages)
    if count_of_sub_pages > MAX_SERVICES_IN_ROW:
        first_row = []
        second_row = []
        elements_in_first_row = count_of_sub_pages // MAX_ROWS + 1 if count_of_sub_pages % MAX_ROWS \
            else count_of_sub_pages // MAX_ROWS
        for index, item in enumerate(sub_pages):
            first_row.append(item) if index + 1 <= elements_in_first_row else second_row.append(item)
        context['service_rows'] = [first_row, second_row]
    else:
        context['service_rows'] = [sub_pages]
    print context['service_rows']
    return context
