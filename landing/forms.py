# -*- coding: utf-8 -*-

"""
landing forms module.
"""
import logging
from cms.models import Page
from django import forms
from landing.models import NewsCMS, NewsBlockChooseOneCMS, CommentsInMediaCMS, PersonCMS, NewsInformCMS
from download_app.models import FileDownloadRequest

__author__ = 'sobolevn'

logger = logging.getLogger(__name__)


CHOISES = {
    'news': [
        (u'Small news', 'Новость. Маленький блок'),
        (u'Big news', 'Новость. Большой блок'),
    ],
    'publication': [(u'Publication', 'Публикация')],
    'comments': [
        (u'Small comment', 'Комментарий. Маленький блок'),
        (u'Big comment', 'Комментарий. Большой блок')
    ]
}

EXCLUDE_FIELDS = {
    'news': ['magazine', 'publication_file'],
    'publication': ['magazine', 'publication_file'],
    'comments': ['magazine', 'publication_file']
}


# for main page comments plugin
class InformPluginForm(forms.ModelForm):
    class Meta:
        model = NewsInformCMS
        exclude = []

    def __init__(self, *args, **kwargs):
        super(InformPluginForm, self).__init__(*args, **kwargs)
        self.fields['type_of_news'].choices = CHOISES['news'] + CHOISES['publication'] + CHOISES['comments']


# for main page news plugin
class NewsCMSModelForm(forms.ModelForm):
    class Meta:
        model = NewsCMS
        exclude = [
            'big_news_title',
            'big_news_text',
            'big_news_page',
            'big_news_mp_photo',
            'big_news_date',
            'small_news_title',
            'small_news_text',
            'small_news_page',
            'small_news_date',
        ]


# for main page comments plugin
class CommentsCMSModelForm(forms.ModelForm):
    class Meta:
        model = CommentsInMediaCMS
        exclude = []

    def __init__(self, *args, **kwargs):
        super(CommentsCMSModelForm, self).__init__(*args, **kwargs)
        try:
            parent_page = Page.objects.filter(reverse_id='comments').first()
            self.fields['comment_page'].queryset = parent_page.children.all()
        except:
            self.fields['comment_page'].queryset = Page.objects.all()


# for team page person plugin
class PersonCMSmodelForm(forms.ModelForm):
    class Meta:
        model = PersonCMS
        exclude = []

    def __init__(self, *args, **kwargs):
        super(PersonCMSmodelForm, self).__init__(*args, **kwargs)
        try:
            parent_page = Page.objects.filter(reverse_id='team').first()
            self.fields['person'].queryset = parent_page.children.all() | Page.objects.filter(reverse_id='career')
        except Exception as e:
            print e
            self.fields['person'].queryset = Page.objects.all()


class NewsBlockChooseOneForm(forms.ModelForm):
    class Meta:
        model = NewsBlockChooseOneCMS
        exclude = []

    def __init__(self, *args, **kwargs):
        super(NewsBlockChooseOneForm, self).__init__(*args, **kwargs)
        try:
            parent_page = Page.objects.filter(reverse_id='news_pages_section').first()
            self.fields['news_page'].queryset = parent_page.children.all()
        except Exception:
            logger.exception('NewsBlockChooseOneForm')
            self.fields['news_page'].queryset = Page.objects.all()


# this form used by index.html
class FileDownloadRequestForm(forms.ModelForm):
    class Meta:
        model = FileDownloadRequest
        fields = [
            'email'
        ]

    def save(self, commit=True, requested_file=None):
        inst = super(FileDownloadRequestForm, self).save(commit=False)

        inst.requested_file = requested_file
        if commit:
            inst.save()

        return inst
