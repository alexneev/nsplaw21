# -*- coding: utf-8 -*-

from cms.api import create_page
from cms.models import Page
from cms.utils.urlutils import admin_reverse
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect


NEWS_TEMPLATES = {
    'news_pages_section': 'landing/news_page.html',
    'publications_section': 'landing/news_page.html',
    'comments': 'landing/news_page.html',
}

@login_required
def create_news_page(request):
    news_type = request.GET.get('type')
    assert news_type in NEWS_TEMPLATES
    parent_page = Page.objects.drafts().get(reverse_id=news_type)

    page = create_page(
        title='',
        template=NEWS_TEMPLATES[news_type],
        language=request.LANGUAGE_CODE,
        created_by='admin_views',
        parent=parent_page,
        in_navigation=True,
    )

    return redirect(admin_reverse('cms_page_change', args=[page.pk]))
