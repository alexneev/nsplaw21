import logging

logger = logging.getLogger(__name__)


def find_plugin_on_page(user_page, plugin_name, lang_code='ru'):
    """
    Find first plugin with plugin_name
    on the user_page
    """
    try:
        page_placeholders = user_page.placeholders.all()
    except Exception:
        logger.exception('Cannot find placeholder')
        return False
    for page_placeholder in page_placeholders:
        for page_plugin in page_placeholder.get_plugins_list():
            if page_plugin.get_plugin_name() == plugin_name and page_plugin.language == lang_code:
                return page_plugin
    return False


def get_latest(_list, count):
    if _list:
        try:
            return _list[0:count-1]
        except Exception:
            logger.exception('Cannot get latest')
            return _list
    return []


def text_to_list(string):
    """
    Split string by \n
    Return list
    """
    if '\n' in string:
        split_list = []
        for item in string.split('\n'):
            if item:
                split_list.append(item)
        return split_list
    else:
        return string
