# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from datetime import datetime
from django.conf import settings
from django.db.models import URLField
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from cms.models import Page
import uuid
from djchoices import DjangoChoices, ChoiceItem
from hvad.models import TranslatableModel, TranslatedFields

from django.db import models
from djangocms_text_ckeditor.fields import HTMLField

# from imagekit.models import ImageSpecField
# from imagekit.processors import ResizeToFill, resize

from cms.models.pluginmodel import CMSPlugin

from cms.extensions import PageExtension
from cms.extensions.extension_pool import extension_pool


# ----------------------------------------------------------------------
# Common section
# ----------------------------------------------------------------------


@python_2_unicode_compatible
class PageQuoteExtension(PageExtension):
    quote = models.ForeignKey('landing.QuoteBase', verbose_name=u'Цитата связанная со страницей')

    def __str__(self):
        return u'Цитата связанная со страницей'

    class Meta:
        verbose_name = u'Цитата связанная со страницей'
        verbose_name_plural = u'Цитаты связанные со страницами'


extension_pool.register(PageQuoteExtension)


@python_2_unicode_compatible
class UploadedFileBase(TranslatableModel):
    PRESENTATION = u"Презентация"
    DD = u"Due Diligense"
    CASESTUDY = u"Case study"
    PUBLICATION = u"Публикация"
    FILE_TYPE = (
        (PRESENTATION, u"Презентация"),
        (DD, u"Due Diligense"),
        (CASESTUDY, u"Case study"),
        (PUBLICATION, u"Публикация"),
    )
    file_object = models.FileField(upload_to='uploads/')
    file_type = models.CharField(choices=FILE_TYPE, max_length=15, default=PRESENTATION)
    translations = TranslatedFields(
        title=models.CharField(max_length=256, blank=False)
    )
    file_uid = models.UUIDField(default=uuid.uuid4,
                                editable=False,
                                unique=True)

    class Meta:
        verbose_name = u'Документ'
        verbose_name_plural = u'Документы'

    def __str__(self):
        title = u"{}".format(self.title)
        return title


@python_2_unicode_compatible
class RatingBase(TranslatableModel):
    # User fields:
    translations = TranslatedFields(
        title=models.CharField(max_length=256)
    )
    logo = models.ImageField(upload_to='uploads/')

    class Meta:
        verbose_name = u'Рейтинг'
        verbose_name_plural = u'Рейтинги'

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class RatingCMS(CMSPlugin):
    rating = models.ForeignKey(RatingBase)

    def __str__(self):
        return u"{}".format(self.rating.title)


@python_2_unicode_compatible
class QuoteContainerCMS(CMSPlugin):
    title = models.CharField(max_length=256)

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class QuoteBase(TranslatableModel):
    translations = TranslatedFields(
        text=models.TextField()
    )
    rating = models.ForeignKey(RatingBase)

    class Meta:
        verbose_name = u'Цитата'
        verbose_name_plural = u'Цитаты'

    def __str__(self):
        return u"{} : {}".format(self.text, self.rating.title)


@python_2_unicode_compatible
class QuoteCMS(CMSPlugin):
    quote = models.ForeignKey(QuoteBase)

    def __str__(self):
        return u"{}".format(self.quote)


@python_2_unicode_compatible
class FirstSlideCMS(CMSPlugin):
    # User fields:
    title = models.CharField(max_length=256)
    body_text = HTMLField(configuration='CKEDITOR_SETTINGS_1')
    backgound_image = models.ImageField(upload_to='uploads/')

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class PersonBase(TranslatableModel):
    # User fields:
    translations = TranslatedFields(
        first_name=models.CharField(max_length=256),
        last_name=models.CharField(max_length=256),
        job_position=models.CharField(max_length=256),
        specialization=models.CharField(max_length=256, blank=True),
    )

    photo = models.ImageField(upload_to='uploads/')
    email = models.EmailField(default='info@nsplaw.ru', blank=True)
    person_page = models.ForeignKey(
        Page, related_name='contact_person_field', blank=True, null=True
    )

    class Meta:
        verbose_name = u'Сотрудник'
        verbose_name_plural = u'Сотрудники'

    def __str__(self):
        return u"{} {}".format(self.first_name, self.last_name)


# form container
@python_2_unicode_compatible
class ContactFormContainerCMS(CMSPlugin):
    title = models.CharField(max_length=512)
    body_text = models.TextField()
    question_number = models.CharField(max_length=128)

    def __str__(self):
        return u"{}".format(self.title)


# form item
@python_2_unicode_compatible
class ContactFormItemCMS(CMSPlugin):
    question = models.CharField(max_length=256)
    error_message = models.CharField(max_length=256)

    def __str__(self):
        return u"{}".format(self.question)


# form last item
@python_2_unicode_compatible
class ContactFormLatsItemCMS(CMSPlugin):
    title = models.CharField(max_length=256)

    def __str__(self):
        return u"{}".format(self.title)


class NewsBlockChooseOneCMS(CMSPlugin):
    news_page = models.ForeignKey(
        Page, related_name='news_blocks', blank=True
    )


class FooterCMS(CMSPlugin):
    common_info = models.CharField(max_length=128, blank=True, default=u"ОБЩАЯ ИНФОРМАЦИЯ")
    address = models.TextField(blank=True)


# ----------------------------------------------------------------------
# Mainpage section
# ----------------------------------------------------------------------


@python_2_unicode_compatible
class ExpertiseContainer(CMSPlugin):
    # User fields:
    title = models.CharField(max_length=256)
    description = models.TextField()

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class ExpertiseItem(CMSPlugin):
    # User fields:
    title = models.CharField(max_length=256)
    photo = models.ImageField(upload_to='uploads/')

    def __str__(self):
        return u"{}".format(self.title)


# used by ExpertiseItemFilePlugin
@python_2_unicode_compatible
class UploadedFile(CMSPlugin):
    uploaded_file = models.ForeignKey(UploadedFileBase, related_name='uploaded_file_field')

    def __str__(self):
        return u"{}".format(self.uploaded_file.title)


class NewsCMS(CMSPlugin):
    # заголовок блока
    block_title = models.CharField(max_length=256)
    # заголовок новости с фотографией
    big_news_title = models.CharField(max_length=256)
    big_news_text = models.TextField()
    big_news_page = models.ForeignKey(
        Page, related_name='big_news_link_field', default=None, blank=True
    )
    big_news_mp_photo = models.ImageField(upload_to='uploads/', blank=True)
    big_news_date = models.DateField(_("Date"), default=datetime.now)
    # заголовок новости без фотографии
    small_news_title = models.CharField(max_length=256)
    small_news_text = models.TextField()
    small_news_page = models.ForeignKey(Page, related_name='small_news_link_field', default=None)
    small_news_date = models.DateField(_("Date"), default=datetime.now)


@python_2_unicode_compatible
class PublicationCMS(CMSPlugin):
    # заголовок блока
    block_title = models.CharField(max_length=256)
    # первая публикация
    first_publication_person = models.ForeignKey(PersonBase, related_name='first_person_field', default=None)
    first_publication_title = models.CharField(max_length=256)
    first_publication_file = models.ForeignKey(
        UploadedFileBase, related_name='first_file_field', default=None, null=True, blank=True
    )
    first_publication_link = models.URLField(blank=True)
    # вторая публикация
    second_publication_person = models.ForeignKey(PersonBase, related_name='second_person_field', default=None)
    second_publication_title = models.CharField(max_length=256)
    second_publication_file = models.ForeignKey(
        UploadedFileBase, related_name='second_file_field', default=None, null=True, blank=True
    )
    second_publication_link = models.URLField(blank=True)

    def __str__(self):
        first_pub = self.first_publication_title[0:25] if self.first_publication_title else 'None'
        second_pub = self.second_publication_title[0:25] if self.second_publication_title else 'None'
        return u"1: {}... 2: {}...".format(first_pub, second_pub)


@python_2_unicode_compatible
class NewsPaperBase(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(max_length=256)
    )
    logo = models.ImageField(upload_to='uploads/')

    class Meta:
        verbose_name = u'СМИ'
        verbose_name_plural = u'СМИ'

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class CommentsInMediaCMS(CMSPlugin):
    # block title
    block_title = models.CharField(max_length=256)
    # comment
    magazine = models.ForeignKey(NewsPaperBase, related_name='newspaper_field', default=None)
    comment_title = models.CharField(max_length=256)
    comment_text = models.TextField()
    comment_page = models.ForeignKey(Page, related_name='comment_page_field', default=None)

    def __str__(self):
        return u"{}".format(self.comment_title)


class ProjectClientBase(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(max_length=256)
    )

    class Meta:
        verbose_name = u'Клиент'
        verbose_name_plural = u'Клиенты'


@python_2_unicode_compatible
class DoneProjectCMS(CMSPlugin):
    # block title
    block_title = models.CharField(max_length=256)
    # project
    project_title = models.CharField(max_length=256)
    project_client = models.ForeignKey(ProjectClientBase, related_name='done_project_client_field', default=None)
    project_date = models.DateField(default=datetime.now)
    project_page = models.ForeignKey(Page, related_name='done_project_field', default=None)
    project_image = models.ImageField(upload_to='uploads/')

    def __str__(self):
        return u"{}".format(self.project_title)


@python_2_unicode_compatible
class YoutubeVideo(TranslatableModel):
    # Innerfields:
    translations = TranslatedFields(
        title=models.CharField(max_length=256)
    )
    url = models.CharField(max_length=256)

    class Meta:
        verbose_name = u'Ролик'
        verbose_name_plural = u'Ролики'

    def __str__(self):
        return u"{}".format(self.title)


class VideoCMS(CMSPlugin):
    # User fields:
    title = models.CharField(max_length=256)
    main_video = models.ForeignKey(YoutubeVideo, related_name='main_video')
    first_video = models.ForeignKey(YoutubeVideo, related_name='first_video')
    second_video = models.ForeignKey(YoutubeVideo, related_name='second_video')
    third_video = models.ForeignKey(YoutubeVideo, related_name='third_video')
    fourth_video = models.ForeignKey(YoutubeVideo, related_name='fourth_video')


@python_2_unicode_compatible
class YouTubeVideoContainerCMS(CMSPlugin):
    # User fields:
    title = models.CharField(max_length=256)
    main_video = models.ForeignKey(YoutubeVideo, related_name='main_video_container')

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class YouTubeVideoCMS(CMSPlugin):
    # User fields:
    video = models.ForeignKey(YoutubeVideo, related_name='small_video')

    def __str__(self):
        return u"{}".format(self.video.title)


class MainQuoteForServiceCMS(CMSPlugin):
    # User fields:
    title_subservices = models.CharField(max_length=256)
    first_quote = models.ForeignKey(QuoteBase, related_name='first_quote')
    second_quote = models.ForeignKey(QuoteBase, related_name='second_quote')
    third_quote = models.ForeignKey(QuoteBase, related_name='third_quote')


# ----------------------------------------------------------------------
# About page section
# ----------------------------------------------------------------------                                                  \__|                 \______/

@python_2_unicode_compatible
class WeDoMoreCMS(CMSPlugin):
    # User fields:
    title = models.CharField(max_length=256)
    body_text = HTMLField(configuration='CKEDITOR_SETTINGS_1')

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class AboutUsCMS(CMSPlugin):
    # User fields:
    title = models.CharField(max_length=256)
    body_text = HTMLField(configuration='CKEDITOR_SETTINGS_1')
    leftside_image = models.ImageField(upload_to='uploads/')

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class WorldWideCMS(CMSPlugin):
    # User fields:
    title = models.CharField(max_length=256)
    body_text = HTMLField(configuration='CKEDITOR_SETTINGS_1')

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class FactInNumberCMS(CMSPlugin):
    # User fields:
    title = models.CharField(max_length=256)
    body_text = models.TextField()

    def __str__(self):
        return u"{}".format(self.title)


# ----------------------------------------------------------------------
# Team page section
# ----------------------------------------------------------------------                                              \__|                 \______/


@python_2_unicode_compatible
class BiographyContainerCMS(CMSPlugin):
    # User
    name = models.CharField(max_length=256)
    job_position = models.CharField(max_length=256)
    email = models.CharField(max_length=128, blank=True, null=True)
    title = models.CharField(max_length=256)
    description = models.TextField()

    def __str__(self):
        return u"{}".format(self.name)


@python_2_unicode_compatible
class BiographyBlockCMS(CMSPlugin):
    # User
    title = models.CharField(max_length=256)
    description = HTMLField(configuration='CKEDITOR_SETTINGS_1')

    # photo = models.ImageField(upload_to='uploads/')

    def __str__(self):
        return u"{}".format(self.title)

        # team page


@python_2_unicode_compatible
class PersonPhotoCMS(CMSPlugin):
    # User
    photo = models.ImageField(upload_to='uploads/')
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return u"Photo active: {}".format(self.is_active)


# team page
@python_2_unicode_compatible
class PersonCMS(CMSPlugin):
    # User
    name = models.CharField(max_length=256)
    person_position = models.CharField(max_length=256)
    photo_gray = models.ImageField(upload_to='uploads/')
    photo_color = models.ImageField(upload_to='uploads/')
    person = models.ForeignKey(Page, related_name='person_field', default=None)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return u"{}".format(self.name)


# ----------------------------------------------------------------------
# Services section
# ----------------------------------------------------------------------


@python_2_unicode_compatible
class AboutServiceCMS(CMSPlugin):
    # User fields:
    title = models.CharField(max_length=256)
    sub_title = models.CharField(max_length=256)
    body_text = HTMLField(configuration='CKEDITOR_SETTINGS_1')

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class SubSerivcesContainerCMS(CMSPlugin):
    # User field:
    title = models.CharField(max_length=256)

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class SubSerivceCMS(CMSPlugin):
    # User field:
    title = models.CharField(max_length=256)
    body_text = models.TextField()
    sub_service = models.ForeignKey(Page, related_name='sub_service_field', default=None)

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class ContactPeopleContainerCMS(CMSPlugin):
    # User fields:
    title = models.CharField(max_length=256)

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class ContactPersonCMS(CMSPlugin):
    # User fields:
    contact_person = models.ForeignKey(PersonBase, related_name='contacnt_person_link_field')

    def __str__(self):
        return u"{} {}".format(self.contact_person.first_name, self.contact_person.last_name)


class NewsBlockContainerCMS(CMSPlugin):
    # User fields:
    title = models.CharField(max_length=256)

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class NewsItemCMS(CMSPlugin):
    # Inner fields:
    NEWS, DONE_PROJECT, PUBLICATION, EVENT = "Новость", "Завершенный проект", "Публикация", "Событие"
    NEWS_TYPE = (
        (NEWS, "Новость"),
        (DONE_PROJECT, "Завершенный проект"),
        (PUBLICATION, "Публикация"),
        (EVENT, "Событие")
    )
    # common fields for all types of news
    date = models.DateField(default=datetime.now)
    title = models.CharField(max_length=256)
    page = models.ForeignKey(Page, related_name='news_item_link_field')
    type_of_news = models.CharField(choices=NEWS_TYPE, max_length=35, default=NEWS)
    # need for done project and news
    photo = models.ImageField(upload_to='uploads/', blank=True)
    # need for news, event and comment in media
    body_text = models.TextField(blank=True)
    # need for a done project
    client_name = models.ForeignKey(
        ProjectClientBase, related_name='clientname_field', default=None, blank=True
    )
    # need for a comment in media
    magazine = models.ForeignKey(
        NewsPaperBase, related_name='newspaper_title_field', default=None, blank=True
    )

    def __str__(self):
        return u"{}".format(self.title)


# sub services plugins


@python_2_unicode_compatible
class SubServiceDetailCMS(CMSPlugin):
    title = models.CharField(max_length=256)
    subtitle = models.CharField(max_length=256)
    body_text = models.TextField()

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class SubServiceListCMS(CMSPlugin):
    title = models.CharField(max_length=256)
    list = models.TextField()

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class SubServiceFeaturesContainerCMS(CMSPlugin):
    title = models.CharField(max_length=256)

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class SubServiceFeaturesItemCMS(CMSPlugin):
    title = models.CharField(max_length=256)
    body_text = models.TextField()

    def __str__(self):
        return u"{}".format(self.title)


# ----------------------------------------------------------------------
# News page section
# ----------------------------------------------------------------------                                               |__/                 \______/

# newsroom page
# container models

@python_2_unicode_compatible
class ContainerBlockCMS(CMSPlugin):
    title = models.CharField(max_length=256)
    block_id = models.CharField(max_length=16)
    # title and href arg for "show more" button
    button_text = models.CharField(max_length=32, default='Show more')
    all_news_page = models.ForeignKey(Page, related_name='showmore_link')

    def __str__(self):
        return u"{}".format(self.title)


# models for a plugins
@python_2_unicode_compatible
class NewsroomNewsCMS(CMSPlugin):
    # minimum for a small news
    title = models.CharField(max_length=256)
    news_date = models.DateField(default=datetime.now)
    # for a big news
    is_big = models.BooleanField(default=False)
    body_text = models.TextField(blank=True)
    photo = models.ImageField(upload_to='uploads/', blank=True)
    news_page = models.ForeignKey(Page, related_name='news_page_field')

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class NewsroomDoneProjectCMS(CMSPlugin):
    # minimum for a small project description
    title = models.CharField(max_length=256)
    client_name = models.ForeignKey(ProjectClientBase, related_name='project_client_name_field')
    # for a big project description
    is_big = models.BooleanField(default=False)
    body_text = models.TextField(blank=True)
    photo = models.ImageField(upload_to='uploads/', blank=True)
    project_page = models.ForeignKey(Page, related_name='project_page_field')

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class NewsroomPublicationCMS(CMSPlugin):
    title = models.CharField(max_length=256)
    body_text = models.TextField(blank=True)
    publication_file = models.ForeignKey(UploadedFile, related_name='publication_file_field')

    def __str__(self):
        return u"{}".format(self.title)


# inner plugin for describe page in all news page
@python_2_unicode_compatible
class NewsInformCMS(CMSPlugin):
    class TypeOfNews(DjangoChoices):
        SmallNews = ChoiceItem('Small news')
        BigNews = ChoiceItem('Big news')
        # SmallProject = ChoiceItem('Small project')
        # BigProject = ChoiceItem('Big project')
        Publcication = ChoiceItem('Publication')
        SmallCommentInMedia = ChoiceItem('Small comment')
        BigCommentInMedia = ChoiceItem('Big comment')

    type_of_news = models.CharField(
        max_length=32,
        choices=TypeOfNews.choices,
    )
    title = models.CharField(max_length=512)
    body_text = models.TextField(blank=True)
    news_date = models.DateField(default=datetime.now)
    external_link = models.URLField(verbose_name='Внешняя ссылка', blank=True, default='')
    photo = models.ImageField(upload_to='uploads/', blank=True)
    featured_news = models.BooleanField(default=False)
    mainpage_big = models.BooleanField(default=False)
    mainpage_small = models.BooleanField(default=False)
    magazine = models.ForeignKey(
        NewsPaperBase, related_name='comment_id_media_field', blank=True, null=True
    )
    publication_file = models.ForeignKey(
        UploadedFileBase, related_name='publication_file_field', blank=True, null=True
    )

    def __str__(self):
        return u"{}".format(self.title)


# parts of the pages
@python_2_unicode_compatible
class NewsPageTitleCMS(CMSPlugin):
    title = models.CharField(max_length=512)

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class NewsPageDesctiptionCMS(CMSPlugin):
    body_text = models.TextField()

    def __str__(self):
        return u"{}".format(self.body_text)


@python_2_unicode_compatible
class NewsPageMainTextCMS(CMSPlugin):
    body_text = HTMLField(configuration='CKEDITOR_SETTINGS_1')

    def __str__(self):
        return u"{}".format(self.body_text)


@python_2_unicode_compatible
class NewsPageQuoteCMS(CMSPlugin):
    body_text = models.TextField()

    def __str__(self):
        return u"{}".format(self.body_text)


@python_2_unicode_compatible
class NewsPagePersonCMS(CMSPlugin):
    page_person = models.ForeignKey(PersonBase, related_name='person_field')

    def __str__(self):
        return u"{} {}".format(self.page_person.first_name, self.page_person.last_name)


@python_2_unicode_compatible
class NewsPageDateCMS(CMSPlugin):
    news_date = models.DateField(_("Date"), default=datetime.now)

    def __str__(self):
        return u"{}".format(self.news_date)


class NewsPagePhotoCMS(CMSPlugin):
    news_photo = models.ImageField(upload_to='uploads/')


@python_2_unicode_compatible
class NewsPageLinkCMS(CMSPlugin):
    title = models.CharField(max_length=256)
    external_link = models.CharField(max_length=512, blank=True)
    link_page = models.ForeignKey(
        Page, related_name='link_to_page_field', null=True, blank=True
    )
    link_file = models.ForeignKey(
        UploadedFileBase, related_name='link_to_file_field', null=True, blank=True
    )

    def __str__(self):
        return u"{}".format(self.title)


@python_2_unicode_compatible
class NewsRoomGenerateAllNewsCMS(CMSPlugin):
    parent_page_id = models.CharField(max_length=64)
    max_items = models.IntegerField()

    def __str__(self):
        return u"{}".format(self.parent_page_id)
