# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0012_newsinformcms_external_link'),
    ]

    operations = [
        migrations.AddField(
            model_name='publicationcms',
            name='first_publication_link',
            field=models.URLField(blank=True),
        ),
        migrations.AddField(
            model_name='publicationcms',
            name='second_publication_link',
            field=models.URLField(blank=True),
        ),
        migrations.AlterField(
            model_name='publicationcms',
            name='first_publication_file',
            field=models.ForeignKey(related_name='first_file_field', default=None, blank=True, to='landing.UploadedFileBase'),
        ),
        migrations.AlterField(
            model_name='publicationcms',
            name='second_publication_file',
            field=models.ForeignKey(related_name='second_file_field', default=None, blank=True, to='landing.UploadedFileBase'),
        ),
    ]
