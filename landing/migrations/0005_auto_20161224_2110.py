# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0004_auto_20161224_2054'),
    ]

    operations = [
        migrations.CreateModel(
            name='PersonBaseTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=256)),
                ('last_name', models.CharField(max_length=256)),
                ('job_position', models.CharField(max_length=256)),
                ('specialization', models.CharField(max_length=256, blank=True)),
                ('language_code', models.CharField(max_length=15, db_index=True)),
            ],
            options={
                'managed': True,
                'abstract': False,
                'db_table': 'landing_personbase_translation',
                'db_tablespace': '',
                'default_permissions': (),
            },
        ),
        migrations.RemoveField(
            model_name='personbase',
            name='first_name',
        ),
        migrations.RemoveField(
            model_name='personbase',
            name='job_position',
        ),
        migrations.RemoveField(
            model_name='personbase',
            name='last_name',
        ),
        migrations.RemoveField(
            model_name='personbase',
            name='specialization',
        ),
        migrations.AddField(
            model_name='personbasetranslation',
            name='master',
            field=models.ForeignKey(related_name='translations', editable=False, to='landing.PersonBase'),
        ),
        migrations.AlterUniqueTogether(
            name='personbasetranslation',
            unique_together=set([('language_code', 'master')]),
        ),
    ]
