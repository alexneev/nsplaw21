# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0009_auto_20170324_1000'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='newsinformcms',
            name='project_client',
        ),
        migrations.AddField(
            model_name='newsinformcms',
            name='mainpage_big',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='newsinformcms',
            name='mainpage_small',
            field=models.BooleanField(default=False),
        ),
    ]
