# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0013_urlconfrevision'),
        ('landing', '0006_auto_20161224_2138'),
    ]

    operations = [
        migrations.CreateModel(
            name='PersonPhotoCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('photo', models.ImageField(upload_to='uploads/')),
                ('is_active', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.RemoveField(
            model_name='biographyblockcms',
            name='photo',
        ),
        migrations.RemoveField(
            model_name='biographycontainercms',
            name='photo',
        ),
        migrations.AddField(
            model_name='biographycontainercms',
            name='email',
            field=models.CharField(max_length=128, null=True, blank=True),
        ),
    ]
