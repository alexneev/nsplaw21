# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0011_auto_20170702_1503'),
    ]

    operations = [
        migrations.AddField(
            model_name='newsinformcms',
            name='external_link',
            field=models.URLField(default='', verbose_name='\u0412\u043d\u0435\u0448\u043d\u044f\u044f \u0441\u0441\u044b\u043b\u043a\u0430', blank=True),
        ),
    ]
