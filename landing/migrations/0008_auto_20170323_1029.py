# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0013_urlconfrevision'),
        ('landing', '0007_auto_20170212_2204'),
    ]

    operations = [
        migrations.CreateModel(
            name='YouTubeVideoCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='YouTubeVideoContainerCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.AlterModelOptions(
            name='newspaperbase',
            options={'verbose_name': '\u0421\u041c\u0418', 'verbose_name_plural': '\u0421\u041c\u0418'},
        ),
        migrations.AlterModelOptions(
            name='personbase',
            options={'verbose_name': '\u0421\u043e\u0442\u0440\u0443\u0434\u043d\u0438\u043a', 'verbose_name_plural': '\u0421\u043e\u0442\u0440\u0443\u0434\u043d\u0438\u043a\u0438'},
        ),
        migrations.AlterModelOptions(
            name='projectclientbase',
            options={'verbose_name': '\u041a\u043b\u0438\u0435\u043d\u0442', 'verbose_name_plural': '\u041a\u043b\u0438\u0435\u043d\u0442\u044b'},
        ),
        migrations.AlterModelOptions(
            name='quotebase',
            options={'verbose_name': '\u0426\u0438\u0442\u0430\u0442\u0430', 'verbose_name_plural': '\u0426\u0438\u0442\u0430\u0442\u044b'},
        ),
        migrations.AlterModelOptions(
            name='ratingbase',
            options={'verbose_name': '\u0420\u0435\u0439\u0442\u0438\u043d\u0433', 'verbose_name_plural': '\u0420\u0435\u0439\u0442\u0438\u043d\u0433\u0438'},
        ),
        migrations.AlterModelOptions(
            name='uploadedfilebase',
            options={'verbose_name': '\u0414\u043e\u043a\u0443\u043c\u0435\u043d\u0442', 'verbose_name_plural': '\u0414\u043e\u043a\u0443\u043c\u0435\u043d\u0442\u044b'},
        ),
        migrations.AlterModelOptions(
            name='youtubevideo',
            options={'verbose_name': '\u0420\u043e\u043b\u0438\u043a', 'verbose_name_plural': '\u0420\u043e\u043b\u0438\u043a\u0438'},
        ),
        migrations.AddField(
            model_name='youtubevideocontainercms',
            name='main_video',
            field=models.ForeignKey(related_name='main_video_container', to='landing.YoutubeVideo'),
        ),
        migrations.AddField(
            model_name='youtubevideocms',
            name='video',
            field=models.ForeignKey(related_name='small_video', to='landing.YoutubeVideo'),
        ),
    ]
