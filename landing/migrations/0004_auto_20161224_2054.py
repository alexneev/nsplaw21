# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0003_footercms'),
    ]

    operations = [
        migrations.CreateModel(
            name='UploadedFileBaseTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=256)),
                ('language_code', models.CharField(max_length=15, db_index=True)),
            ],
            options={
                'managed': True,
                'abstract': False,
                'db_table': 'landing_uploadedfilebase_translation',
                'db_tablespace': '',
                'default_permissions': (),
            },
        ),
        migrations.RemoveField(
            model_name='uploadedfilebase',
            name='title',
        ),
        migrations.AddField(
            model_name='uploadedfilebasetranslation',
            name='master',
            field=models.ForeignKey(related_name='translations', editable=False, to='landing.UploadedFileBase'),
        ),
        migrations.AlterUniqueTogether(
            name='uploadedfilebasetranslation',
            unique_together=set([('language_code', 'master')]),
        ),
    ]
