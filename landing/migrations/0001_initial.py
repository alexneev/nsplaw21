# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import djangocms_text_ckeditor.fields
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0013_urlconfrevision'),
    ]

    operations = [
        migrations.CreateModel(
            name='AboutServiceCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('sub_title', models.CharField(max_length=256)),
                ('body_text', djangocms_text_ckeditor.fields.HTMLField()),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='AboutUsCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('body_text', djangocms_text_ckeditor.fields.HTMLField()),
                ('leftside_image', models.ImageField(upload_to='uploads/')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='BiographyBlockCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('description', djangocms_text_ckeditor.fields.HTMLField()),
                ('photo', models.ImageField(upload_to='uploads/')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='BiographyContainerCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('name', models.CharField(max_length=256)),
                ('job_position', models.CharField(max_length=256)),
                ('title', models.CharField(max_length=256)),
                ('description', models.TextField()),
                ('photo', models.ImageField(upload_to='uploads/')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='CommentsInMediaCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('block_title', models.CharField(max_length=256)),
                ('comment_title', models.CharField(max_length=256)),
                ('comment_text', models.TextField()),
                ('comment_page', models.ForeignKey(related_name='comment_page_field', default=None, to='cms.Page')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='ContactFormContainerCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=512)),
                ('body_text', models.TextField()),
                ('question_number', models.CharField(max_length=128)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='ContactFormItemCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('question', models.CharField(max_length=256)),
                ('error_message', models.CharField(max_length=256)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='ContactFormLatsItemCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='ContactPeopleContainerCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='ContactPersonCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='ContainerBlockCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('block_id', models.CharField(max_length=16)),
                ('button_text', models.CharField(default='Show more', max_length=32)),
                ('all_news_page', models.ForeignKey(related_name='showmore_link', to='cms.Page')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='DoneProjectCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('block_title', models.CharField(max_length=256)),
                ('project_title', models.CharField(max_length=256)),
                ('project_date', models.DateField(default=datetime.datetime.now)),
                ('project_image', models.ImageField(upload_to='uploads/')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='ExpertiseContainer',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('description', models.TextField()),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='ExpertiseItem',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('photo', models.ImageField(upload_to='uploads/')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='FactInNumberCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('body_text', models.TextField()),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='FirstSlideCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('body_text', djangocms_text_ckeditor.fields.HTMLField()),
                ('backgound_image', models.ImageField(upload_to='uploads/')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='MainQuoteForServiceCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title_subservices', models.CharField(max_length=256)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsBlockChooseOneCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('news_page', models.ForeignKey(related_name='news_blocks', blank=True, to='cms.Page')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsBlockContainerCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('block_title', models.CharField(max_length=256)),
                ('big_news_title', models.CharField(max_length=256)),
                ('big_news_text', models.TextField()),
                ('big_news_mp_photo', models.ImageField(upload_to='uploads/', blank=True)),
                ('big_news_date', models.DateField(default=datetime.datetime.now, verbose_name='Date')),
                ('small_news_title', models.CharField(max_length=256)),
                ('small_news_text', models.TextField()),
                ('small_news_date', models.DateField(default=datetime.datetime.now, verbose_name='Date')),
                ('big_news_page', models.ForeignKey(related_name='big_news_link_field', default=None, blank=True, to='cms.Page')),
                ('small_news_page', models.ForeignKey(related_name='small_news_link_field', default=None, to='cms.Page')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsInformCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('type_of_news', models.CharField(max_length=32, choices=[('Small news', b'SmallNews'), ('Big news', b'BigNews'), ('Small project', b'SmallProject'), ('Big project', b'BigProject'), ('Publication', b'Publcication'), ('Small comment', b'SmallCommentInMedia'), ('Big comment', b'BigCommentInMedia')])),
                ('title', models.CharField(max_length=512)),
                ('body_text', models.TextField(blank=True)),
                ('news_date', models.DateField(default=datetime.datetime.now)),
                ('photo', models.ImageField(upload_to='uploads/', blank=True)),
                ('featured_news', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsItemCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('date', models.DateField(default=datetime.datetime.now)),
                ('title', models.CharField(max_length=256)),
                ('type_of_news', models.CharField(default='\u041d\u043e\u0432\u043e\u0441\u0442\u044c', max_length=35, choices=[('\u041d\u043e\u0432\u043e\u0441\u0442\u044c', '\u041d\u043e\u0432\u043e\u0441\u0442\u044c'), ('\u0417\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u043d\u044b\u0439 \u043f\u0440\u043e\u0435\u043a\u0442', '\u0417\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u043d\u044b\u0439 \u043f\u0440\u043e\u0435\u043a\u0442'), ('\u041f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u044f', '\u041f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u044f'), ('\u0421\u043e\u0431\u044b\u0442\u0438\u0435', '\u0421\u043e\u0431\u044b\u0442\u0438\u0435')])),
                ('photo', models.ImageField(upload_to='uploads/', blank=True)),
                ('body_text', models.TextField(blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsPageDateCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('news_date', models.DateField(default=datetime.datetime.now, verbose_name='Date')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsPageDesctiptionCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('body_text', models.TextField()),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsPageLinkCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('external_link', models.CharField(max_length=512, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsPageMainTextCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('body_text', djangocms_text_ckeditor.fields.HTMLField()),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsPagePersonCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsPagePhotoCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('news_photo', models.ImageField(upload_to='uploads/')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsPageQuoteCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('body_text', models.TextField()),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsPageTitleCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=512)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsPaperBase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=256)),
                ('logo', models.ImageField(upload_to='uploads/')),
            ],
        ),
        migrations.CreateModel(
            name='NewsroomDoneProjectCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('is_big', models.BooleanField(default=False)),
                ('body_text', models.TextField(blank=True)),
                ('photo', models.ImageField(upload_to='uploads/', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsRoomGenerateAllNewsCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('parent_page_id', models.CharField(max_length=64)),
                ('max_items', models.IntegerField()),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsroomNewsCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('news_date', models.DateField(default=datetime.datetime.now)),
                ('is_big', models.BooleanField(default=False)),
                ('body_text', models.TextField(blank=True)),
                ('photo', models.ImageField(upload_to='uploads/', blank=True)),
                ('news_page', models.ForeignKey(related_name='news_page_field', to='cms.Page')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsroomPublicationCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('body_text', models.TextField(blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='PersonBase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=256)),
                ('last_name', models.CharField(max_length=256)),
                ('job_position', models.CharField(max_length=256)),
                ('specialization', models.CharField(max_length=256, blank=True)),
                ('photo', models.ImageField(upload_to='uploads/')),
                ('email', models.EmailField(default='info@nsplaw.ru', max_length=254, blank=True)),
                ('person_page', models.ForeignKey(related_name='contact_person_field', blank=True, to='cms.Page', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='PersonCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('name', models.CharField(max_length=256)),
                ('person_position', models.CharField(max_length=256)),
                ('photo_gray', models.ImageField(upload_to='uploads/')),
                ('photo_color', models.ImageField(upload_to='uploads/')),
                ('person', models.ForeignKey(related_name='person_field', default=None, to='cms.Page')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='ProjectClientBase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='PublicationCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('block_title', models.CharField(max_length=256)),
                ('first_publication_title', models.CharField(max_length=256)),
                ('second_publication_title', models.CharField(max_length=256)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='QuoteBase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='QuoteCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('quote', models.ForeignKey(to='landing.QuoteBase')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='QuoteContainerCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='RatingBase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=256)),
                ('logo', models.ImageField(upload_to='uploads/')),
            ],
        ),
        migrations.CreateModel(
            name='RatingCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('rating', models.ForeignKey(to='landing.RatingBase')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='SubSerivceCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('body_text', models.TextField()),
                ('sub_service', models.ForeignKey(related_name='sub_service_field', default=None, to='cms.Page')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='SubSerivcesContainerCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='SubServiceDetailCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('subtitle', models.CharField(max_length=256)),
                ('body_text', models.TextField()),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='SubServiceFeaturesContainerCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='SubServiceFeaturesItemCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('body_text', models.TextField()),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='SubServiceListCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('list', models.TextField()),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='UploadedFile',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='UploadedFileBase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file_object', models.FileField(upload_to='uploads/')),
                ('file_type', models.CharField(default='\u041f\u0440\u0435\u0437\u0435\u043d\u0442\u0430\u0446\u0438\u044f', max_length=15, choices=[('\u041f\u0440\u0435\u0437\u0435\u043d\u0442\u0430\u0446\u0438\u044f', '\u041f\u0440\u0435\u0437\u0435\u043d\u0442\u0430\u0446\u0438\u044f'), ('Due Diligense', 'Due Diligense'), ('Case study', 'Case study'), ('\u041f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u044f', '\u041f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u044f')])),
                ('title', models.CharField(max_length=256)),
                ('file_uid', models.UUIDField(default=uuid.uuid4, unique=True, editable=False)),
            ],
        ),
        migrations.CreateModel(
            name='VideoCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='WeDoMoreCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('body_text', djangocms_text_ckeditor.fields.HTMLField()),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='WorldWideCMS',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=256)),
                ('body_text', djangocms_text_ckeditor.fields.HTMLField()),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='YoutubeVideo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=256)),
                ('url', models.CharField(max_length=256)),
            ],
        ),
        migrations.AddField(
            model_name='videocms',
            name='first_video',
            field=models.ForeignKey(related_name='first_video', to='landing.YoutubeVideo'),
        ),
        migrations.AddField(
            model_name='videocms',
            name='fourth_video',
            field=models.ForeignKey(related_name='fourth_video', to='landing.YoutubeVideo'),
        ),
        migrations.AddField(
            model_name='videocms',
            name='main_video',
            field=models.ForeignKey(related_name='main_video', to='landing.YoutubeVideo'),
        ),
        migrations.AddField(
            model_name='videocms',
            name='second_video',
            field=models.ForeignKey(related_name='second_video', to='landing.YoutubeVideo'),
        ),
        migrations.AddField(
            model_name='videocms',
            name='third_video',
            field=models.ForeignKey(related_name='third_video', to='landing.YoutubeVideo'),
        ),
        migrations.AddField(
            model_name='uploadedfile',
            name='uploaded_file',
            field=models.ForeignKey(related_name='uploaded_file_field', to='landing.UploadedFileBase'),
        ),
        migrations.AddField(
            model_name='quotebase',
            name='rating',
            field=models.ForeignKey(to='landing.RatingBase'),
        ),
        migrations.AddField(
            model_name='publicationcms',
            name='first_publication_file',
            field=models.ForeignKey(related_name='first_file_field', default=None, to='landing.UploadedFileBase'),
        ),
        migrations.AddField(
            model_name='publicationcms',
            name='first_publication_person',
            field=models.ForeignKey(related_name='first_person_field', default=None, to='landing.PersonBase'),
        ),
        migrations.AddField(
            model_name='publicationcms',
            name='second_publication_file',
            field=models.ForeignKey(related_name='second_file_field', default=None, to='landing.UploadedFileBase'),
        ),
        migrations.AddField(
            model_name='publicationcms',
            name='second_publication_person',
            field=models.ForeignKey(related_name='second_person_field', default=None, to='landing.PersonBase'),
        ),
        migrations.AddField(
            model_name='newsroompublicationcms',
            name='publication_file',
            field=models.ForeignKey(related_name='publication_file_field', to='landing.UploadedFile'),
        ),
        migrations.AddField(
            model_name='newsroomdoneprojectcms',
            name='client_name',
            field=models.ForeignKey(related_name='project_client_name_field', to='landing.ProjectClientBase'),
        ),
        migrations.AddField(
            model_name='newsroomdoneprojectcms',
            name='project_page',
            field=models.ForeignKey(related_name='project_page_field', to='cms.Page'),
        ),
        migrations.AddField(
            model_name='newspagepersoncms',
            name='page_person',
            field=models.ForeignKey(related_name='person_field', to='landing.PersonBase'),
        ),
        migrations.AddField(
            model_name='newspagelinkcms',
            name='link_file',
            field=models.ForeignKey(related_name='link_to_file_field', blank=True, to='landing.UploadedFileBase', null=True),
        ),
        migrations.AddField(
            model_name='newspagelinkcms',
            name='link_page',
            field=models.ForeignKey(related_name='link_to_page_field', blank=True, to='cms.Page', null=True),
        ),
        migrations.AddField(
            model_name='newsitemcms',
            name='client_name',
            field=models.ForeignKey(related_name='clientname_field', default=None, blank=True, to='landing.ProjectClientBase'),
        ),
        migrations.AddField(
            model_name='newsitemcms',
            name='magazine',
            field=models.ForeignKey(related_name='newspaper_title_field', default=None, blank=True, to='landing.NewsPaperBase'),
        ),
        migrations.AddField(
            model_name='newsitemcms',
            name='page',
            field=models.ForeignKey(related_name='news_item_link_field', to='cms.Page'),
        ),
        migrations.AddField(
            model_name='newsinformcms',
            name='magazine',
            field=models.ForeignKey(related_name='comment_id_media_field', blank=True, to='landing.NewsPaperBase', null=True),
        ),
        migrations.AddField(
            model_name='newsinformcms',
            name='project_client',
            field=models.ForeignKey(related_name='project_client_field', blank=True, to='landing.ProjectClientBase', null=True),
        ),
        migrations.AddField(
            model_name='newsinformcms',
            name='publication_file',
            field=models.ForeignKey(related_name='publication_file_field', blank=True, to='landing.UploadedFileBase', null=True),
        ),
        migrations.AddField(
            model_name='mainquoteforservicecms',
            name='first_quote',
            field=models.ForeignKey(related_name='first_quote', to='landing.QuoteBase'),
        ),
        migrations.AddField(
            model_name='mainquoteforservicecms',
            name='second_quote',
            field=models.ForeignKey(related_name='second_quote', to='landing.QuoteBase'),
        ),
        migrations.AddField(
            model_name='mainquoteforservicecms',
            name='third_quote',
            field=models.ForeignKey(related_name='third_quote', to='landing.QuoteBase'),
        ),
        migrations.AddField(
            model_name='doneprojectcms',
            name='project_client',
            field=models.ForeignKey(related_name='done_project_client_field', default=None, to='landing.ProjectClientBase'),
        ),
        migrations.AddField(
            model_name='doneprojectcms',
            name='project_page',
            field=models.ForeignKey(related_name='done_project_field', default=None, to='cms.Page'),
        ),
        migrations.AddField(
            model_name='contactpersoncms',
            name='contact_person',
            field=models.ForeignKey(related_name='contacnt_person_link_field', to='landing.PersonBase'),
        ),
        migrations.AddField(
            model_name='commentsinmediacms',
            name='magazine',
            field=models.ForeignKey(related_name='newspaper_field', default=None, to='landing.NewsPaperBase'),
        ),
    ]
