# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0008_auto_20170323_1029'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newsinformcms',
            name='type_of_news',
            field=models.CharField(max_length=32, choices=[('\u041d\u043e\u0432\u043e\u0441\u0442\u044c. \u041c\u0430\u043b\u0435\u043d\u044c\u043a\u0438\u0439 \u0431\u043b\u043e\u043a', b'SmallNews'), ('\u041d\u043e\u0432\u043e\u0441\u0442\u044c. \u0411\u043e\u043b\u044c\u0448\u043e\u0439 \u0431\u043b\u043e\u043a', b'BigNews'), ('\u041f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u044f', b'Publcication'), ('\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439. \u041c\u0430\u043b\u0435\u043d\u044c\u043a\u0438\u0439 \u0431\u043b\u043e\u043a', b'SmallCommentInMedia'), ('\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439. \u0411\u043e\u043b\u044c\u0448\u043e\u0439 \u0431\u043b\u043e\u043a', b'BigCommentInMedia')]),
        ),
    ]
