# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0005_auto_20161224_2110'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewsPaperBaseTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=256)),
                ('language_code', models.CharField(max_length=15, db_index=True)),
            ],
            options={
                'managed': True,
                'abstract': False,
                'db_table': 'landing_newspaperbase_translation',
                'db_tablespace': '',
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='ProjectClientBaseTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=256)),
                ('language_code', models.CharField(max_length=15, db_index=True)),
            ],
            options={
                'managed': True,
                'abstract': False,
                'db_table': 'landing_projectclientbase_translation',
                'db_tablespace': '',
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='QuoteBaseTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField()),
                ('language_code', models.CharField(max_length=15, db_index=True)),
            ],
            options={
                'managed': True,
                'abstract': False,
                'db_table': 'landing_quotebase_translation',
                'db_tablespace': '',
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='RatingBaseTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=256)),
                ('language_code', models.CharField(max_length=15, db_index=True)),
            ],
            options={
                'managed': True,
                'abstract': False,
                'db_table': 'landing_ratingbase_translation',
                'db_tablespace': '',
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='YoutubeVideoTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=256)),
                ('language_code', models.CharField(max_length=15, db_index=True)),
            ],
            options={
                'managed': True,
                'abstract': False,
                'db_table': 'landing_youtubevideo_translation',
                'db_tablespace': '',
                'default_permissions': (),
            },
        ),
        migrations.RemoveField(
            model_name='newspaperbase',
            name='title',
        ),
        migrations.RemoveField(
            model_name='projectclientbase',
            name='title',
        ),
        migrations.RemoveField(
            model_name='quotebase',
            name='text',
        ),
        migrations.RemoveField(
            model_name='ratingbase',
            name='title',
        ),
        migrations.RemoveField(
            model_name='youtubevideo',
            name='title',
        ),
        migrations.AddField(
            model_name='youtubevideotranslation',
            name='master',
            field=models.ForeignKey(related_name='translations', editable=False, to='landing.YoutubeVideo'),
        ),
        migrations.AddField(
            model_name='ratingbasetranslation',
            name='master',
            field=models.ForeignKey(related_name='translations', editable=False, to='landing.RatingBase'),
        ),
        migrations.AddField(
            model_name='quotebasetranslation',
            name='master',
            field=models.ForeignKey(related_name='translations', editable=False, to='landing.QuoteBase'),
        ),
        migrations.AddField(
            model_name='projectclientbasetranslation',
            name='master',
            field=models.ForeignKey(related_name='translations', editable=False, to='landing.ProjectClientBase'),
        ),
        migrations.AddField(
            model_name='newspaperbasetranslation',
            name='master',
            field=models.ForeignKey(related_name='translations', editable=False, to='landing.NewsPaperBase'),
        ),
        migrations.AlterUniqueTogether(
            name='youtubevideotranslation',
            unique_together=set([('language_code', 'master')]),
        ),
        migrations.AlterUniqueTogether(
            name='ratingbasetranslation',
            unique_together=set([('language_code', 'master')]),
        ),
        migrations.AlterUniqueTogether(
            name='quotebasetranslation',
            unique_together=set([('language_code', 'master')]),
        ),
        migrations.AlterUniqueTogether(
            name='projectclientbasetranslation',
            unique_together=set([('language_code', 'master')]),
        ),
        migrations.AlterUniqueTogether(
            name='newspaperbasetranslation',
            unique_together=set([('language_code', 'master')]),
        ),
    ]
