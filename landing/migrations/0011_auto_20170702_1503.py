# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
        ('landing', '0010_auto_20170324_1005'),
    ]

    operations = [
        migrations.CreateModel(
            name='PageQuoteExtension',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('extended_object', models.OneToOneField(editable=False, to='cms.Page')),
                ('public_extension', models.OneToOneField(related_name='draft_extension', null=True, editable=False, to='landing.PageQuoteExtension')),
                ('quote', models.ForeignKey(verbose_name='\u0426\u0438\u0442\u0430\u0442\u0430 \u0441\u0432\u044f\u0437\u0430\u043d\u043d\u0430\u044f \u0441\u043e \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435\u0439', to='landing.QuoteBase')),
            ],
            options={
                'verbose_name': '\u0426\u0438\u0442\u0430\u0442\u0430 \u0441\u0432\u044f\u0437\u0430\u043d\u043d\u0430\u044f \u0441\u043e \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435\u0439',
                'verbose_name_plural': '\u0426\u0438\u0442\u0430\u0442\u044b \u0441\u0432\u044f\u0437\u0430\u043d\u043d\u044b\u0435 \u0441\u043e \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430\u043c\u0438',
            },
        ),
        migrations.AlterField(
            model_name='aboutservicecms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_aboutservicecms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='aboutuscms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_aboutuscms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='biographyblockcms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_biographyblockcms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='biographycontainercms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_biographycontainercms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='commentsinmediacms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_commentsinmediacms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='contactformcontainercms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_contactformcontainercms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='contactformitemcms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_contactformitemcms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='contactformlatsitemcms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_contactformlatsitemcms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='contactpeoplecontainercms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_contactpeoplecontainercms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='contactpersoncms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_contactpersoncms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='containerblockcms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_containerblockcms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='doneprojectcms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_doneprojectcms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='expertisecontainer',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_expertisecontainer', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='expertiseitem',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_expertiseitem', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='factinnumbercms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_factinnumbercms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='firstslidecms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_firstslidecms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='footercms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_footercms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='mainquoteforservicecms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_mainquoteforservicecms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='newsblockchooseonecms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_newsblockchooseonecms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='newsblockcontainercms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_newsblockcontainercms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='newscms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_newscms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='newsinformcms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_newsinformcms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='newsinformcms',
            name='type_of_news',
            field=models.CharField(max_length=32, choices=[('Small news', b'SmallNews'), ('Big news', b'BigNews'), ('Publication', b'Publcication'), ('Small comment', b'SmallCommentInMedia'), ('Big comment', b'BigCommentInMedia')]),
        ),
        migrations.AlterField(
            model_name='newsitemcms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_newsitemcms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='newspagedatecms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_newspagedatecms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='newspagedesctiptioncms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_newspagedesctiptioncms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='newspagelinkcms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_newspagelinkcms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='newspagemaintextcms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_newspagemaintextcms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='newspagepersoncms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_newspagepersoncms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='newspagephotocms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_newspagephotocms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='newspagequotecms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_newspagequotecms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='newspagetitlecms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_newspagetitlecms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='newsroomdoneprojectcms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_newsroomdoneprojectcms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='newsroomgenerateallnewscms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_newsroomgenerateallnewscms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='newsroomnewscms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_newsroomnewscms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='newsroompublicationcms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_newsroompublicationcms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='personcms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_personcms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='personphotocms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_personphotocms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='publicationcms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_publicationcms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='quotecms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_quotecms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='quotecontainercms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_quotecontainercms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='ratingcms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_ratingcms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='subserivcecms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_subserivcecms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='subserivcescontainercms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_subserivcescontainercms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='subservicedetailcms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_subservicedetailcms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='subservicefeaturescontainercms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_subservicefeaturescontainercms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='subservicefeaturesitemcms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_subservicefeaturesitemcms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='subservicelistcms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_subservicelistcms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='uploadedfile',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_uploadedfile', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='videocms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_videocms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='wedomorecms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_wedomorecms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='worldwidecms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_worldwidecms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='youtubevideocms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_youtubevideocms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='youtubevideocontainercms',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='landing_youtubevideocontainercms', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
    ]
