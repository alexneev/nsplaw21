# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0013_auto_20170904_0603'),
    ]

    operations = [
        migrations.AlterField(
            model_name='publicationcms',
            name='first_publication_file',
            field=models.ForeignKey(related_name='first_file_field', default=None, blank=True, to='landing.UploadedFileBase', null=True),
        ),
        migrations.AlterField(
            model_name='publicationcms',
            name='second_publication_file',
            field=models.ForeignKey(related_name='second_file_field', default=None, blank=True, to='landing.UploadedFileBase', null=True),
        ),
    ]
