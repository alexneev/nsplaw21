from cms.models import Page


def news(request):
    news_list = []
    news_pages_section = Page.objects.filter(reverse_id='news_pages_section').first()
    news_pages = news_pages_section.children.all()
    for page in news_pages:
        page_placeholders = page.placeholders.all()
        for page_placeholder in page_placeholders:
            for page_plugin in page_placeholder.get_plugins_list():
                if (page_plugin.get_plugin_name() == "InformPlugin" and
                        page_plugin.newsinformcms.featured_news):
                    news_list.append({
                        page.id: {
                            'url': page.get_absolute_url(),
                            'title': page_plugin.newsinformcms.title,
                            'type_of_news': page_plugin.newsinformcms.type_of_news,
                            'body_text': page_plugin.newsinformcms.body_text,
                            'news_date': page_plugin.newsinformcms.news_date,
                            'magazine': page_plugin.newsinformcms.magazine,
                            'publication_file': page_plugin.newsinformcms.publication_file,
                            'project_client': page_plugin.newsinformcms.project_client,
                        }})

    # print news_list
    return {
        'custom_context': {
            'username': str(request.user),
            'news_list': news_list
        }
    }


def active_menu(request):
    active_menu = ""
    try:
        if request.current_page.parent:
            if request.current_page.parent.parent:
                if request.current_page.parent.parent.parent:
                    active_menu = str(request.current_page.parent.parent)
                else:
                    active_menu = str(request.current_page.parent)
            else:
                active_menu = str(request.current_page)
        else:
            active_menu = "Mainpage"
    except Exception as e:
        print e

    return {
        'custom_context': {
            'username': str(request.user),
            'active_menu': active_menu
        }
    }


def utils_data(request):
    service_label = Page.objects.filter(reverse_id='services').first()
    return {
        'utils': {
            'username': str(request.user),
            'services': service_label
        }
    }
