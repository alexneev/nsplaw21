/**
 * Created by sobolevn on 17.11.16.
 */

'use strict';

function sendRequest(userData) {
    var request = $.ajax({
        method: "POST",
        url: '/ru/download/',
        dataType: 'json',
        data: userData
    });
    request.success(function (data, textStatus, jqXHR) {
        $("#popup-publication").removeClass('visible');
        $("#popup-publication-success").addClass('visible');
    });
}

(function (window, document, $) {

    $('.popup__form__btn').click(function () {
        var json_data = {
            'email': $('.popup__form__input').val(),
            'file': $('input[name="id-publication"]').val(),
            'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val()
        };
        sendRequest(json_data);
    });

    $('.close-popup').click(function () {
        $("#popup-publication-success").removeClass('visible');
        $('body').removeClass('no-scroll');
    });

    $('.btn-close').click(function () {
        $("#popup-publication-success").removeClass('visible');
        $('body').removeClass('no-scroll');
    });

})(window, document, jQuery);
