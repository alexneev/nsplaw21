window.addEventListener('load', function() {


  var buttons = toArray(document.querySelectorAll('.feedaback-form__field-btn'));
  if (buttons.length !== 0) {

      var countOfQuestions = buttons.length;
      var sendEmailButton = buttons[countOfQuestions-1];

      var agreeButton = document.querySelector('#check');

      // this condition checks is the language ru
      if (!agreeButton.checked) {
        sendEmailButton.style.opacity = '.5';
        sendEmailButton.disabled = true;
      };

      agreeButton.addEventListener('click', function (){
        if (sendEmailButton.disabled) {
          sendEmailButton.style.opacity = '1';
          sendEmailButton.disabled = false;
        } else {
          sendEmailButton.style.opacity = '.5';
          sendEmailButton.disabled = true;
        }

      });

      sendEmailButton.addEventListener('click', function() {
        var termsBlock = document.querySelector('#terms-block');
        if (termsBlock) {
          termsBlock.style.display = 'none';
        }
      });

  }

  function toArray(collection) {
      return Array.prototype.slice.call(collection);
  }

});
