# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from cms.toolbar_pool import toolbar_pool
from cms.toolbar_base import CMSToolbar
from cms.cms_toolbars import PageToolbar
from cms.extensions.toolbar import ExtensionToolbar
from django.utils.translation import ugettext_lazy as _
from .models import PageQuoteExtension
from cms.utils.permissions import has_page_change_permission
from django.core.urlresolvers import reverse, NoReverseMatch
from cms.utils import get_cms_setting
from cms.api import get_page_draft


class CustomPageToolbar(PageToolbar):
    def add_draft_live(self):
        if self.page:
            if self.toolbar.edit_mode and not self.title:
                self.add_page_settings_button()

            if self.page.has_change_permission(self.request) and self.page.is_published(self.current_lang):
                return self.add_draft_live_item()


class CreateNewsPage(CMSToolbar):
    def populate(self):
        menu = self.toolbar.get_or_create_menu(
            'landing_application',
            _(u'Добавить новостную страницу')
        )
        menu.add_modal_item(
            name=_(u'Новости и события'),
            url=reverse('add_news_page') + '?type=news_pages_section',
        )
        menu.add_modal_item(
            name=_(u'Публикации'),
            url=reverse('add_news_page') + '?type=publications_section',
        )
        menu.add_modal_item(
            name=_(u'Комментарии в СМИ'),
            url=reverse('add_news_page') + '?type=comments',
        )


@toolbar_pool.register
class QuoteExtensionToolbar(ExtensionToolbar):
    model = PageQuoteExtension

    def populate(self):
        # setup the extension toolbar with permissions and sanity checks
        current_page_menu = self._setup_extension_toolbar()

        # if it's all ok
        if current_page_menu:
            # retrieves the instance of the current extension (if any) and the toolbar item URL
            page_extension, url = self.get_page_extension_admin()
            if url:
                # adds a toolbar item in position 0 (at the top of the menu)
                current_page_menu.add_modal_item(_(u'Цитата страницы'), url=url,
                                                 disabled=not self.toolbar.edit_mode, position=0)
