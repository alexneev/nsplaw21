# nsplaw

Authors:

- @sobolevn
- @andreyermilov

## Development

Run `docker-compose up`

## Building

We are using `python2.7` to run this project.

```bash
pip install -r requirements.txt
DJANGO_SETTINGS_MODULE=nsp.settings DJANGO_SECRET_KEY="$YOU_KEY" python manage.py runserver
```

Or you can just place file `config/config.secret` into your project.
After that you won't need any king of env-variables.

You may also need to rename `development.py.template` into `development.py`.

## Send file by email

```
$ redis-server
$ python manage.py rqworker
```


## Deployment

```
python manage.py makemigrations djangocms_table
python manage.py migrate djangocms_table
```

### Production

We host our production server on Digital Ocean.

To deploy run:

```bash
pip install -r requirements/dev.txt  # only once

fab deploy
```

Extra options:

- `fab deploy:nginx` will run deploy as usual and also symlink `nginx.conf` and restart the `nginx` server.

#### Database deployment

To deploy your database to the remote database:

```bash
fab deploy_data
```

To receive data from the remote database:

```bash
fab receive_data
```
