# -*- coding: utf-8 -*-

import os

from fabric.api import (
    run, env, cd, roles, sudo, prefix, get, put, local
)

__author__ = 'sobolevn'

env.roledefs['production'] = ['82.202.226.60']
VIRTUALENV = '/home/static/nsplaw/.venv/bin'


def production_env():
    """Production environment."""

    env.key_filename = [
        os.path.join(os.environ['HOME'], '.ssh', 'id_rsa.pub')
    ]
    env.user = 'static'
    env.project_ = 'nsp'
    env.project_root = '/home/static/nsplaw'
    env.media_folder = '{}/media'.format(env.project_root)
    env.data_backup_file = 'data.bak.json'
    env.data_backup_tar = 'data.zip.bak'

    # configurations:
    env.uwsgi_conf = '/etc/uwsgi/sites/nsp.ini'
    env.nginx_conf = '/etc/nginx/sites-enabled/nsp.conf'


@roles('production')
def deploy(nginx=None):
    """
    Deploy main script, this task runs all the required
    operations to apply the local changes to the production env.
    """

    production_env()  # activating production env

    with cd(env.project_root):
        # working only with master:
        run('git pull origin master')

        # removing old compiled python files:
        run('find . -name "*.pyc" -print -delete')

        # removing old translations
        # run('find . -name "*.mo" -print -delete')

        # compiling new
        with prefix('source {}/activate'.format(VIRTUALENV)):
            # installing dependencies
            run('pip install -r requirements/base.txt')

            # migrating the database
            run('python manage.py migrate')

            # compiling new messages
            # run('python manage.py compilemessages')

            # collecting static
            run('python manage.py collectstatic --noinput')

            # compile messages from .po to .mo
            # run('find . -name \*.po -execdir msgfmt django.po -o django.mo \;')

        # deploying configuration:
        sudo('cat config/uwsgi.ini > {}'.format(env.uwsgi_conf))

        apps = ['uwsgi', ]
        if nginx:
            # to run this section pass an argument:
            # `fab deploy:nginx`
            sudo('cat config/nginx.conf > {}'.format(env.nginx_conf))

            apps.append('nginx')  # nginx will also be restarted

        # restarting the app:
        sudo('systemctl restart {}'.format(' '.join(apps)))


@roles('production')
def deploy_data():
    """
    This script deploys current local database data to the
    production env.
    """

    production_env()

    with cd(env.project_root):
        local('python manage.py dumpdata > {}'.format(
            env.data_backup_file))

        # archive:
        local('zip -X {0} {1}'.format(
            env.data_backup_tar, env.data_backup_file))

        # upload files:
        put(local_path=env.data_backup_tar,
            remote_path=env.data_backup_tar)

        # unzip:
        run('unzip -o {}'.format(env.data_backup_tar))

        # load:
        with prefix('source {}/activate'.format(VIRTUALENV)):
            # backup:
            run('python manage.py dumpdata > before_deploy.json.sql')
            # this will drop everything:
            run('python manage.py sqlflush | python manage.py dbshell')
            # loading new data:
            run('python manage.py loaddata {}'.format(env.data_backup_file))

        # cleaning up:
        _clean_up()


@roles('production')
def receive_data():
    """
    This script copies the production database to the local one.
    """

    production_env()

    with cd(env.project_root):
        with prefix('source {}/activate'.format(VIRTUALENV)):
            run('python manage.py dumpdata > {}'.format(env.data_backup_file))

        # run `unzip -o data.zip.bak` to unarchive:
        run('zip -X {0} {1}'.format(
            env.data_backup_tar, env.data_backup_file))

        # downloading:
        get(env.data_backup_tar, local_path=env.data_backup_tar)

        # unpacking:
        local('unzip -o {}'.format(env.data_backup_tar))

        # apply remote backup:
        local('python manage.py dumpdata > before_deploy.json.sql')
        # this will drop everything:
        local('python manage.py sqlflush | python manage.py dbshell')
        # loading new data:
        local('python manage.py loaddata {}'.format(env.data_backup_file))

        # cleaning up:
        _clean_up()


def _clean_up(*extras):
    files = [
        env.data_backup_file,
        env.data_backup_tar,
    ]

    if extras:
        files.extend(extras)

    run('rm -f {}'.format(
        ' '.join(files)))

    local('rm -f {}'.format(
        ' '.join(files)))


@roles('production')
def database(action=None):
    """
    This script copies the production database to the local one.
    """

    if action == 'load':

        production_env()

        with cd(env.project_root):

            # create database dump
            run('pg_dump nsp_server_u1 > db_dump.local')

            # downloading:
            get('db_dump.local', local_path='db_dump.local')

            # remove database dump from server
            run('rm db_dump.local')

            # create empty database
            local('dropdb nsp_server')
            local('createdb nsp_server')

            # load data from server database
            local('psql nsp_server < db_dump.local')

            # clean temporary file
            local('rm db_dump.local')

    if action == 'clean':

        local('dropdb nsp_server')
        local('createdb nsp_server')
        local('psql nsp_server < clean.db')
