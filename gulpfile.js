var gulp = require('gulp'),
  sass = require('gulp-sass'),
  watch = require('gulp-watch'),
  prefixer = require('gulp-autoprefixer'),
  cssmin = require('gulp-minify-css'),
  rename = require('gulp-rename'),
  uglify = require('gulp-uglify'),
  rigger = require('gulp-rigger'),
  imageop = require('gulp-image-optimization'),
  spritesmith = require('gulp-spritesmith'),
  size = require('gulp-filesize'),
  htmlhint = require("gulp-htmlhint"),
  changed = require('gulp-changed'),
  uncss = require('gulp-uncss'),
  cssbeautify = require('gulp-cssbeautify'),
  browserSync = require("browser-sync"),
  svgstore = require('gulp-svgstore'),
  svgmin = require('gulp-svgmin'),
  gpath = require('path'),
  cheerio = require('gulp-cheerio'),
  includes = require('gulp-file-include'),
  replace = require('gulp-replace-task'),
  imageminMozjpeg = require('imagemin-mozjpeg');
reload = browserSync.reload;

var path = {
  build: {
    folder: 'front/build/',
    js: 'landing/static/landing/js/',
    css: 'landing/static/landing/css/'
  },
  app: {
    js: 'front/js/common.js',
    style: 'front/sass/common.sass'
  },
  watch: {
    js: 'front/js/**/*.js',
    style: 'front/sass/**/*.sass'
  }
};


gulp.task('js:build', function () {
  gulp.src(path.app.js)
    .pipe(changed(path.build.js))
    .pipe(rigger())
    .pipe(uglify())
    .pipe(gulp.dest(path.build.js))
    .pipe(size())
    .pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
  gulp.src(path.app.style)
    .pipe(changed(path.build.css))
    .pipe(sass())
    .pipe(prefixer())
    .pipe(cssbeautify())
    .pipe(gulp.dest(path.build.css))
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(path.build.css))
    .pipe(size())
    .pipe(reload({stream: true}));
});


gulp.task('build', [
  'js:build',
  'style:build'
]);


gulp.task('watch', [], function () {
  watch([path.watch.style], function (event, cb) {
    gulp.start('style:build');
  });
  watch([path.watch.js], function (event, cb) {
    gulp.start('js:build');
  });
});

gulp.task('default', ['build', 'watch']);
