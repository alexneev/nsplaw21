# -*- coding: utf-8 -*-

from django import forms

from download_app.models import FileDownloadRequest

__author__ = 'sobolevn'


class FileDownloadRequestForm(forms.ModelForm):
    class Meta:
        model = FileDownloadRequest
        fields = [
            'email'
        ]

    def save(self, commit=True, requested_file=None):
        inst = super(FileDownloadRequestForm, self).save(commit=False)

        inst.requested_file = requested_file
        if commit:
            inst.save()

        return inst
