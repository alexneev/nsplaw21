# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0001_initial'),
        ('download_app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='filedownloadrequest',
            name='requested_file',
            field=models.ForeignKey(related_name='file_for_download_field', to='landing.UploadedFileBase'),
        ),
    ]
