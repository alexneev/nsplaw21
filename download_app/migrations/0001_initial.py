# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import djchoices.choices


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FileDownloadRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=254)),
                ('status', models.CharField(max_length=1, choices=[(b'N', b'New'), (b'S', b'Sent'), (b'E', b'Error')], validators=[djchoices.choices.ChoicesValidator({b'S': b'Sent', b'E': b'Error', b'N': b'New'})])),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
    ]
