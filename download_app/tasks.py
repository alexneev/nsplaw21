# -*- coding: utf-8 -*-

"""
You need to run:
    `python manage.py rqworker`

To make these tasks work.
"""

import os
import logging

from django.db import transaction
from django.conf import settings

from django_rq import job

from download_app.models import FileDownloadRequest
from download_app import utils

logger = logging.getLogger('server')

__author__ = 'sobolevn'


@job
def send_file_by_email(request_id):
    """

    Args:
        request_id: FileDownloadRequest's object id.
            It is required to get the object.
            Sending the object itself is not a good decision.
            http://stackoverflow.com/questions/15079176/should-django-model-object-instances-be-passed-to-celery

    Returns:

    """
    with transaction.atomic():
        file_request = FileDownloadRequest.objects.get(id=request_id)

        try:
            utils.send_file_email(
                file_request.email,
                file_request.requested_file.file_object.file
            )
        except Exception as ex:
            # On every exception:
            file_request.status = FileDownloadRequest.RequestStatus.Error
            logger.error(str(ex))
        else:
            file_request.status = FileDownloadRequest.RequestStatus.Sent

        file_request.save()

    return
