# -*- coding: utf-8 -*-

from django.db import models
from django.utils import timezone

from djchoices import DjangoChoices, ChoiceItem

from landing.models import UploadedFileBase

__author__ = 'sobolevn'


class FileDownloadRequest(models.Model):
    class RequestStatus(DjangoChoices):
        New = ChoiceItem('N')
        Sent = ChoiceItem('S')
        Error = ChoiceItem('E')
    requested_file = models.ForeignKey(UploadedFileBase, related_name='file_for_download_field')
    email = models.EmailField()

    # Utils:
    status = models.CharField(
        max_length=1,
        choices=RequestStatus.choices,
        validators=[RequestStatus.validator]
    )
    date_created = models.DateTimeField(default=timezone.now)
