# -*- coding: utf-8 -*-
import mimetypes
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from nsp.settings.components import emails
from django.core.mail import EmailMultiAlternatives
import re

__author__ = 'sobolevn'


def send_file_email(to, file_obj):
    subject = 'File request: ' + file_obj.name
    body = '<h1>Your file in an attachment</h1>'
    mimetype = mimetypes.guess_type(file_obj.name)[0]

    mail = EmailMultiAlternatives(
        subject=subject,
        body=body,
        to=[to, ]
    )

    mail.attach(
        filename=file_obj.name,
        content=file_obj.read(),
        mimetype=mimetype
    )

    html_content = '<p>This is an <strong>important</strong> message.</p>'

    mail.attach_alternative(
        html_content, "text/html"
    )

    mail.send()


def send_form_email(to, subject, html_content):
    """
    Function sends email.
    :param to: (str) email addr receiver
    :param subject: (str) email subj
    :param html_content: (str) email body
    :return: None
    """
    server = smtplib.SMTP(host=emails.EMAIL_HOST, port=emails.EMAIL_PORT)
    server.starttls()
    server.login(emails.EMAIL_HOST_USER, emails.EMAIL_HOST_PASSWORD)
    server.set_debuglevel(1)

    message = MIMEMultipart()
    message['From'] = emails.EMAIL_HOST_USER
    message['To'] = to
    message['Subject'] = subject
    message.attach(MIMEText(html_content, 'plain'))

    server.sendmail(emails.EMAIL_HOST_USER, to, message.as_string())
    server.quit()


def clean_data(string):
    regex = re.compile('\S')
    string = regex.findall(string)
    return ''.join(string)
