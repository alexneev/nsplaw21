# -*- coding: utf-8 -*-

"""
download_app views module.
"""

from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.http import JsonResponse
from datetime import datetime, date

from download_app.forms import FileDownloadRequestForm
from download_app.tasks import send_file_by_email
from download_app.utils import send_form_email
from landing.models import UploadedFileBase
import json
from download_app.utils import clean_data

from nsp.settings.components.emails import ADMIN_MAIL_ADDRESS
__author__ = 'sobolevn'

# Create your views here:


def index(request):
    if request.method == 'POST' and request.is_ajax():

        requested_file = get_object_or_404(
            UploadedFileBase, file_uid=request.POST['file']
        )

        file_request_form = FileDownloadRequestForm(data=request.POST)
        if file_request_form.is_valid():
            file_request = file_request_form.save(
                requested_file=requested_file)

            # What we do here is:
            # 1. Enqueue this operation in the redis-queue
            # 2. When the worker will process this task,
            # it will send the file to user
            send_file_by_email.delay(file_request.id)

            return JsonResponse({}, status=200)


def form_by_mail(request):
    if request.method == 'POST' and request.is_ajax():
        html_content = '<h2>Данные запроса:</h2>'
        json_data = json.loads(request.POST['userData'])
        for item in json_data:
            html_content += '<p>Вопрос: {}</p><p>Ответ: {}</p>'.format(
                item['field_name'].encode('utf-8'),
                item['field_data'].encode('utf-8')
            )
        send_form_email(ADMIN_MAIL_ADDRESS, "Запрос от {}".format(date.today()), html_content)
    return JsonResponse({}, status=200)
