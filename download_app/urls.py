# -*- coding: utf-8 -*-

"""
download_app URL routing.
"""

from django.conf.urls import url

from download_app import views as download_views

__author__ = 'sobolevn'

# Place your URLs here:

#TODO: define send form as mail as apphook
urlpatterns = [
    url(r'download/$', download_views.index, name='index'),
    url(r'formbymail/$', download_views.form_by_mail, name='form_by_mail'),
]
