 $('.main-accordion-tabs').on('click', 'li > a.tab-link', function(event) {
	
	if (!$(this).hasClass('is-active')) {

		if ($(window).width() > 768)
			event.preventDefault();

		var accordionTabs = $(this).closest('.main-accordion-tabs');
		accordionTabs.find('.is-open').removeClass('is-open').hide();

		$(this).next().toggleClass('is-open').show();
		accordionTabs.find('.is-active').removeClass('is-active');
		$(this).addClass('is-active');
	} else {
		event.preventDefault();
	}
});
