$(function(){
	
	$( window ).scroll(function() {
		FixedPicPersonOnScroll();
	});
	
	function FixedPicPersonOnScroll() {
			
		if ($('.article-box__author').length > 0 && ($(window).width() > 768)) {
						
			var el = $(".article-box__author");
			
			var hAuthor = $(el).height();
			var posAuthorOffset = $(el).offset().top;
			var footerH = $("footer").height();
			
			var wScroll = window.pageYOffset;
			var posArticle = $(".article-box").offset().top + $(".article-box").height();
			var windowPosWithFooter = wScroll + footerH;
			
			var all = $(".wrapper").height() + $(".header").height() + $(".footer").height()	
			
			if (windowPosWithFooter >= (posArticle - hAuthor)){
				$(".article-box__author").addClass('fixed');
			}
			else {
				$(".article-box__author").removeClass('fixed');
			}
			
		}
		
		
	}
	
});