$(function(){
	
	ScrollPaneInit();
	
	$( window ).resize(function() {
		ScrollPaneInit();
	});
	
	function ScrollPaneInit() {
			
		if ($('.custom-scroll') && $(window).width() > 768) {
			
			$('.custom-scroll').bind(
				'jsp-scroll-y',
				function(event, scrollPositionY, isAtTop, isAtBottom)
				{

					$(".person-box__item-pic").css('top', scrollPositionY);
					
					$.each($('.person-box__item:not(.active)'), function() {
						
						prev = $(this).prev().data("person");
						next = $(this).next().data("person");
						var personPosBottom = $(this).outerHeight() + $(this).position().top - 300;
						
						if (($(this).position().top - 300) <= scrollPositionY && 
							scrollPositionY <= personPosBottom)
						{
							
							idPerson = $(this).data("person");
							
							// $('.person-box__item').removeClass('active');
							$("#" + idPerson).closest('.person-box__item').addClass('active');
						}

					});
	
				}
			)
			.jScrollPane({
				showArrows:true,
				animateEase: "easing"
			});
		}
		
		
	}
	
});