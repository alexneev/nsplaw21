(function(){
	var ticker = function() {		
		$('.clock-wrapper').each(function(e){
			var now = new Date(),
				minutes = now.getMinutes(),
				hours = now.getHours();
			var _this = $(this);
			var timeZone = _this.data('zone');
			switch(timeZone){
				case 'moskow':
					break;
				case 'london':
					hours = hours - 3;
					break;
				case 'pekin':
					hours = hours + 5;
					break;
				case 'bern':
					hours = hours - 1;
					break;
			}
			_this.find(".clock__hours").attr('data-value', hours);
			_this.find(".clock__minutes").attr('data-value', minutes);

		});	
	}

	setInterval(ticker, 100);
})();
