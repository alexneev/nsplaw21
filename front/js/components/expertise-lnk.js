$(".expertise__lnk").on( "click", function(e) {
	e.preventDefault();
	
	var doc = $(this).data('expertise');
	var bg = $(this).data('bg');
	
	if(doc)	{
		$('.expertise-doc').removeClass('active');
		$('#'+doc).addClass('active');
		$(".expertise__lnk").parent().removeClass('active');
		$(this).parent().addClass('active');

		console.log($(this).closest("expertise-box"));

		$(".expertise-box").css('background', 'url('+bg+')');
		$(".expertise-box").css('background-size', 'cover');

	}
	
});
