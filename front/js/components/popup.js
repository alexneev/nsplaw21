$(document).on('click', '.open-popup-publication', function(){
	var thisPopup = $(this).data('name');
	var thisId = $(this).data('id');
	var scrollbarWidth = $(document).width();
	
	$('body').addClass('no-scroll');
	var scrollbarWidthNew = $(document).width();
	
	$('body').css({'padding-right': scrollbarWidthNew-scrollbarWidth, 'transition': "padding 0s"});
	
	$('input[name="id-publication"]').val(thisId);
	$("#popup-publication").addClass('visible');
	
});

$(document).on('click', '.popup-wrap, .btn-close, .close-popup', function(){
	$("#popup").removeClass('visible');
	$("#popup-publication").removeClass('visible');
	
	setTimeout(function(){
		$('body').removeClass('no-scroll');
		$('body').css({'padding-right': '0', 'transition': "padding 0s"});
		
	},500)
});

$(document).on('click', '.popup', function(e){
	e.stopPropagation();
	return;
});
