$(document).ready(function () {

	'use strict';

	$(".person-slider.owl-carousel").owlCarousel({
		autoPlay: false,
		items : 1,
		itemsMobile : [479, 1],
		itemsDesktop : [1000, 1], //5 items between 1000px and 901px
		itemsDesktopSmall : [900, 1], // betweem 900px and 601px
		itemsTablet: [600, 1],
		touchDrag: true,
		navigation: false,
		pagination: true,
		navigationText: false,
		margin: 0
	});

	$(".owl-carousel.review-slider").owlCarousel({
		autoPlay: false,
		items : 1,
		itemsMobile : [479, 1],
		itemsDesktop : [1000, 1], //5 items between 1000px and 901px
		itemsDesktopSmall : [900, 1], // betweem 900px and 601px
		itemsTablet: [600, 1],
		touchDrag: true,
		navigation: true,
		pagination: true,
		navigationText: false,
		margin: 0
	});


});
