$(document).on('click', '.show-menu', function(){
	$('.header').addClass('opened-menu')
	$(this).siblings(".main-nav").addClass('main-nav_showed');
	setTimeout(function(){
		$(".main-nav").addClass('main-nav_visible');
	},100)
});
$(document).on('click', '.close-main-nav', function(){
	closeMenu();
});
$(document).click(function(event) {
	if ($(event.target).closest(".show-menu").length || $(event.target).closest(".main-nav").length) return;
	closeMenu();
	event.stopPropagation();
});
function closeMenu() {
	$('.header').removeClass('opened-menu')
	$('.main-nav').removeClass('main-nav_visible');
	setTimeout(function(){
		$(".main-nav").removeClass('main-nav_showed');
	},500)
}

$(window).scroll(function(){
	var currentPos = $(this).scrollTop();
	var screenHeight = $(this).height();
	if ((currentPos > 28 && window.innerWidth > 1024) || (currentPos > 0 && window.innerWidth < 1025)) {
		$('.header-bottom').addClass("fixed header_dark");
	} else {
		$('.header-bottom').removeClass("fixed");
		if($('.header').hasClass('header-white')) {
			$('.header-bottom').removeClass("header_dark");
		}
	}

	if(currentPos > screenHeight && !$('.header-white').hasClass('header_dark')) {
		$('.header').addClass('header_dark');
	} else if(currentPos <= screenHeight && $('.header-white').hasClass('header_dark')) {
		$('.header').removeClass('header_dark');
	}

});
