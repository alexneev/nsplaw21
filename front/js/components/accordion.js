$('.js-accordion-trigger').bind('click', function(e){
	
	if(jQuery(this).parent().find('.js-accordion-content').hasClass('footer__nav') && $(window).width() < 768){
	
		jQuery(this).parent().find('.js-accordion-content').slideToggle('fast'); 
		// apply the toggle to the ul
		jQuery(this).parent().toggleClass('is-expanded');
		e.preventDefault();

	} 
	
	if ($(this).closest('.accordion_benefit').length){
		jQuery(this).parent().find('.js-accordion-content').slideToggle('fast'); 
		// apply the toggle to the ul
		jQuery(this).parent().toggleClass('is-expanded');
		e.preventDefault();
	}
});