// Bower components
	//= ../../bower_components/jquery/dist/jquery.js
	//= ../../bower_components/jquery-form-styler/jquery.formstyler.min.js
	//= ../../bower_components/owl/owl-carousel/owl.carousel.js
	//= components/packery.pkgd.min.js


$(document).ready(function(){
	// Custom components should lay in the folder 'components'
	//= components/menu.js
	//= components/accordion.js
	//= components/clock.js
	//= components/accordion-tabs.js
	//= components/expertise-lnk.js
	//= components/popup.js

	var hasIE = window.eval && eval("/*@cc_on 1;@*/") && (/msie (\d+)/i.exec(navigator.userAgent) || [,true])[1];

	if (hasIE < 11) {
		$('.old-popop').addClass('showed');
	}
	var res = navigator.userAgent.match(/; CPU.*OS (\d_\d)/);
	if(res) {
		var strVer = res[res.length-1];
		strVer = strVer.replace("_", ".");
		version = strVer * 1;
		if(version < 9) {
			$('.old-popop').addClass('showed');
		}
	}


	function videoCenter(e) {
		var center = e/2;

		if(word_1Width > center) {
			var wordPos = word_1Width - center;
			word_1.css('left', center-wordPos + 'px');
			word_2.css('left', center-wordPos+50 + 'px');
			word_3.css('left', center-wordPos+50+word_2Width+50 + 'px');

		} else if (word_1Width + 50 > center) {
		} else if (word_1Width+50+word_2Width > center) {
			var wordPos = word_1Width+50+word_2Width - center;

			word_2.css('left', center - word_2Width + wordPos + 'px');
			word_1.css('left', center - word_2Width + wordPos - 50 - word_1Width + 'px');
			word_3.css('left', center + wordPos + 50 + 'px');

		} else if(word_1Width+50+word_2Width+50 > center) {
		} else if(word_1Width+50+word_2Width+50+word_3Width > center) {
			var wordPos = word_1Width - center;
			word_1.css('left', center-wordPos + 'px');
			word_2.css('left', center-wordPos+50 + 'px');
			word_3.css('left', center-wordPos+50+word_2Width+50 + 'px');
		}

	}
	$('.promo-btn').click(function(){

		if($('.promo-btn').length && $(window).width() < 1024 && $(window).height() < 701) return

		var _this = $('.main-promo');

		if(!$('.main-promo.in-progress').length > 0)
			promoAnimate();

	})
	var durationVideo1 = 8500;
	var durationVideo2 = 8000;
	var durationVideo3 = 5000;
	var durationVideo4 = 5000;
	var durationVideo5 = 5000;
	var currentStep1,
		currentStep2

	function promoAnimate() {
		$('.main-promo').addClass('in-progress');
		// step1
		step1();
	}

	function step1() {
		var timer;
		var _this = $('.main-promo');
		_this.removeClass('step6').removeClass('step5').removeClass('step4').removeClass('step3').removeClass('step2');
		_this.addClass('step1').data('active', '1');
		$('.progress-bar__we').addClass('active');
		document.getElementById("video1").play();
		$('.progress-bar__we .progress-line').css('width','0');
		$('.progress-bar__do .progress-line').css('width','0');
		$('.progress-bar__more .progress-line').css('width','0');

		$("#video1").addClass('shown');
		$('.progress-bar__we .progress-line').animate({width: '90%'},durationVideo1,
			function(){
			if($('.progress-bar__we').hasClass('active')) {
				step2(_this);
				$('.progress-bar__we').removeClass('active');
			}
		});
	}

	function step2() {
		var timer;
		var _this = $('.main-promo');
		_this.removeClass('step6').removeClass('step5').removeClass('step4').removeClass('step3').addClass('step2').data('active', '2');
		$('.progress-bar__do').addClass('active');
		document.getElementById("video1").pause();
		document.getElementById("video1").currentTime = 0;
		document.getElementById("video2").play();
		$('video').removeClass('shown');
		$('.progress-bar__we .progress-line').css('width','90%');
		$('.progress-bar__do .progress-line').css('width','0');
		$('.progress-bar__more .progress-line').css('width','0');

		$("#video2").addClass('shown');
		$('.progress-bar__do .progress-line').animate({width: '90%'},durationVideo2,
			function(){
			if($('.progress-bar__do').hasClass('active')) {
				step3(_this);
				$('.progress-bar__do').removeClass('active');
			}
		});
	}

	function step3() {
		var timer;
		var _this = $('.main-promo');
		_this.removeClass('step2').removeClass('step6').removeClass('step5').removeClass('step4');
		_this.addClass('step3').data('active', '3');
		$('.progress-bar__more').addClass('active');
		document.getElementById("video2").pause();
		document.getElementById("video2").currentTime = 0;
		document.getElementById("video3").play();
		$('video').removeClass('shown');
		$('.progress-bar__we .progress-line').css('width','90%');
		$('.progress-bar__do .progress-line').css('width','90%');
		$('.progress-bar__more .progress-line').css('width','0');

		$("#video3").addClass('shown');
		$('.progress-bar__more .progress-line').animate({width: '33%'},durationVideo3, function(){
			if($('.progress-bar__more').hasClass('active')) {
				step4(_this);
			}
		});
	}

	function step4() {
		var _this = $('.main-promo');
		_this.removeClass('step3').removeClass('step6').removeClass('step5').removeClass('step2');
		_this.addClass('step4').data('active', '4');
		document.getElementById("video3").pause();
		document.getElementById("video3").currentTime = 0;
		document.getElementById("video4").play();
		$('video').removeClass('shown');

		$("#video4").addClass('shown');
		$('.progress-bar__more .progress-line').animate({width: '66%'},durationVideo4, function(){
			if($('.progress-bar__more').hasClass('active')) {
				step5(_this);
			}
		});
	}

	function step5() {
		var _this = $('.main-promo');
		_this.removeClass('step4').removeClass('step6').removeClass('step2').removeClass('step3');
		_this.addClass('step5').data('active', '5');
		document.getElementById("video4").pause();
		document.getElementById("video4").currentTime = 0;
		document.getElementById("video5").play();
		$('video').removeClass('shown');

		$("#video5").addClass('shown');
		$('.progress-bar__more .progress-line').animate({width: '90%'},durationVideo5, function(){
			if($('.progress-bar__more').hasClass('active')) {
				step6(_this);
				$('.progress-bar__more').removeClass('active');
			}
		});
	}

	function step6() {
		var _this = $('.main-promo');
		document.getElementById("video5").pause();
		document.getElementById("video5").currentTime = 0;
		$('video').removeClass('shown');

		setTimeout(function(){
			$('.main-promo').addClass('test');
			$('.main-promo').removeClass('test step5 step1');
			$('.main-promo').addClass('step6');
		}, 1000);

		setTimeout(function(){
			$('.main-promo').removeClass('in-progress');
			$('.progress-line').animate({width: '0'});
		}, 2000);
	}

	setTimeout(function(){
		$('.news-press__cell').packery({
			itemSelector: '.news-press__cell-item'
		});
	},500)

	// переключение по прогресс бару
 $('.progress-bar__item').click(function(){
  var currentProgress = $(this).data('progress');
  var activeProgress = $('.progress-bar__item.active').data('progress');
  var currentBlock = $('.main-promo').data('active');

  if(currentProgress > activeProgress) {
   $('.progress-bar__item.active .progress-line').stop(true).css('width', '90%');
   $('.progress-bar__item.active').removeClass('active');
   $(this).addClass('active');
   eval('step'+currentProgress + '()')

  } else if (currentProgress < activeProgress) {
   $('.progress-bar__item.active .progress-line').stop(true).css('width', '90%');
   $('.main-promo').removeClass('step' + currentBlock);
   $('.progress-bar__item.active').removeClass('active');
   $(this).addClass('active');
   eval('step'+currentProgress + '()')
  } else if (currentProgress == activeProgress) return;

 })


 // ------------

 var countOfQuestions = $('.form-steps__step-item').length;
 var globalCurrentStep = 1;

 $('.form-steps__btn-next, .nav-step-form').click(function(e){
	 e.preventDefault();
	 if (fieldValid()) {

		 $('.feedaback-form__page').css('display', 'block');

		 if (globalCurrentStep == countOfQuestions - 1) {
			 $('.feedaback-form__page').css('display', 'none');
			 var formData = [];
			 for (var i=1; i < countOfQuestions; i++) {
				 formData.push({
					 'field_name': $('label[for="field' + i + '"]').text(),
					 'field_data': $('input[name="field' + i + '"]').val()

				 });
			 };
			 sendRequest({
				 'csrfmiddlewaretoken': document.getElementsByName('csrfmiddlewaretoken')[0].value,
				 'userData': JSON.stringify(formData)
			 });
		 };

		 globalCurrentStep++;
		 setActiveStep('step' + globalCurrentStep);

	 };
 });

 function fieldValid() {
	 if (globalCurrentStep == countOfQuestions - 1) {
		 return true
	 };
	 var activeElement = $('input[name="field' + globalCurrentStep + '"]');
	 if (activeElement && activeElement.val().length > 2) {
		 return true;
	 } else {
		 return false;
	 }
 }

 function setActiveStep(nextStep) {
	 $('.form-steps__step-item, .nav-step-form').removeClass('active');
	 $('.error-mess').removeClass('active');

	 $('.form-steps__step-item[data-step="'+nextStep+'"]').addClass('active');
	 $('.nav-step-form[data-step="'+nextStep+'"]').addClass('active');
 }

 function validStep(currentElement) {
 }

 function sendRequest(userData) {
	 var request = $.ajax({
		 method: "POST",
		 url: '/ru/formbymail/',
		 dataType: 'json',
		 data: userData
	 });
 };

 // ------------

	// form-style
	setTimeout(function() {
		$('select').styler({
			onSelectOpened: function() {
				$(this).find('.jq-selectbox__trigger-arrow').addClass('opened');
			},
			onSelectClosed: function() {
				$(this).find('.jq-selectbox__trigger-arrow').removeClass('opened');
			}
		});
	}, 100);

});

// press center - active item on scroll
$(window).on('scroll', function(e){
	var currentPos = $(this).scrollTop();
	// активный пункт мню при скроле
	var currentScroll = $(this).scrollTop()+$('.menu-press').height()+$('.header-bottom').height();
	$('.menu-press__link').removeClass('active');
	$('.press-box').each(function(){
		var _this=$(this);
		var scrollPosition = _this.offset().top - $('.menu-press').outerHeight() -20;
		var scrollHeight = _this.outerHeight()+scrollPosition;
		if(currentScroll > scrollPosition && currentScroll < scrollHeight) {
			var item = _this.attr('id');
			$('.menu-press a[href="#'+item+'"]').addClass('active');
		}
	});

	if (currentPos > 106 && $('.menu-press').length && window.innerWidth >768) {
		$('.menu-press').addClass("fixed");
		$('.wrapper').css('padding-top', $('.menu-press').outerHeight()+90);
	} else {
		$('.menu-press').removeClass("fixed");
		$('.wrapper').css('padding-top', '0');
	}

});

function valid_email_address(email) {
	var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
	return pattern.test(email);
}

$(document).on('click', '#send-doc', function(){

	if (!valid_email_address($("#send-doc-email").val()))
	{
		$(".popup__form__error").addClass('active');
	}
	else
	{
		$(".popup__form__error").removeClass('active');

		/*
		// if send form ajax uncomment this

		$.ajax({
			url: '',
			data: $('#send-doc').serialize(),
			type: 'POST',
			success: function(data) {
				$('#popup-text').load('ajax/success-send-doc.html .wrapper');
			}
		});
		*/

		// if send form ajax comment this
		$('#popup-text').load('ajax/success-send-doc.html .wrapper');
	}
});


// плавный скролл к блоку
$(document).on('click','.menu-press a', function(e){
	var nameLink = $(this).attr('href');
	var height = $('.header-bottom').innerHeight() + $('.menu-press').innerHeight();
	var destination = $(nameLink).offset().top;
	var topBlock = height;
	destination = destination-topBlock;
	$('html, body').stop().animate({
		scrollTop: destination
	}, 1000);
	e.preventDefault();
});

// Custom scrool by jScrollPane
//= ../../bower_components/jScrollPane/script/jquery.jscrollpane.min.js
//= ../../bower_components/jScrollPane/script/jquery.mousewheel.js
//= components/custom-scroll.js

//= components/owl-slider.js
//= components/fixed-pic-onscroll.js
