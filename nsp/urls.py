# -*- coding: utf-8 -*-

"""
Main URL mapping configuration file.
Include other URLConfs from external apps using method include().

It is also a good practice to keep a single URL to the root index page.

This examples uses Django's default media
files serving technique in development.
"""

from django.conf.urls import include, url
from django.contrib import admin
from cms.sitemaps import CMSSitemap
from django.conf.urls.i18n import i18n_patterns
from django.conf import settings
from django.views.generic import TemplateView
from landing import admin_views


admin.autodiscover()

urlpatterns = i18n_patterns(
    '',

    url(r'^admin/add_news_page/$', admin_views.create_news_page, name='add_news_page'),

    # django-admin:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    # text and xml static files:
    url(r'^robots\.txt$', TemplateView.as_view(
        template_name='txt/robots.txt',
        content_type='text/plain'
    )),
    url(r'^humans\.txt$', TemplateView.as_view(
        template_name='txt/humans.txt',
        content_type='text/plain'
    )),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap',
        {'sitemaps': {'cmspages': CMSSitemap}}),
    url(r'^crossdomain\.xml$', TemplateView.as_view(
        template_name='txt/crossdomain.xml',
        content_type='application/xml'
    )),
    url(r'^manifest\.json$', TemplateView.as_view(
        template_name='txt/manifest.json',
        content_type='application/json'
    )),

    # django-cms:
    url(r'^', include('cms.urls')),
)

urlpatterns += [
    # rq:
    url(r'^django-rq/', include('django_rq.urls')),
]

# This urlconf is for development use only:
if settings.DEBUG:
    # Media files serving:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
