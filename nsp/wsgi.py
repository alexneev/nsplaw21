# -*- coding: utf-8 -*-

"""
WSGI config for nsp project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application


__author__ = 'sobolevn'

# This setting should be available via heroku config:
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nsp.settings')
application = get_wsgi_application()
