# -*- coding: utf-8 -*-

"""
This file determines all the settings used in production.
This file is required and if development.py is present these values are overridden.
"""

import os
import urlparse

import dj_database_url

from nsp.settings.components.common import (
    BASE_DIR,
    TEMPLATES,
    DATABASES,
)
from nsp.settings.components.rq import RQ_QUEUES


__author__ = 'sobolevn'


# Production flags:

DEBUG = True # False

FRONTEND_DEBUG = True # False

for template in TEMPLATES:
    template['OPTIONS']['debug'] = False


# Databases:
db_from_env = dj_database_url.config()

if db_from_env:
    # This means we are running on heroku
    DATABASES['default'].update(db_from_env)
# else: we are running from production, nothing special is required.


# Redis:
redis_url = os.environ.get('REDIS_URL')
if redis_url:
    # heroku
    redis_url = urlparse.urlparse(redis_url)
    RQ_QUEUES['default'] = {
        'HOST': redis_url.hostname,
        'PORT': redis_url.port,
        'DB': 0,
        'DEFAULT_TIMEOUT': 360,
    }
# else normal production, nothing special is required

# Network security and SSL:

ALLOWED_HOSTS = [
    '82.202.226.60',

    'nsplaw.ru',
    'www.nsplaw.ru',

    'nsplaw.com',
    'www.nsplaw.com',
]

SESSION_COOKIE_SECURE = False

CSRF_COOKIE_SECURE = False


# Static files:

# STATICFILES_DIRS = (
    # os.path.join(BASE_DIR, 'node_modules'),  # Adding NPM modules in development
# )


# Adding STATIC_ROOT to collect static files via 'collectstatic'
# STATIC_ROOT = os.path.join(BASE_DIR, 'static_root')

# Media path
# MEDIA_ROOT = os.path.join(BASE_DIR, 'media')


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATIC_URL = '/static/'

# Extra places for collectstatic to find static files.
STATICFILES_DIRS = (
    # os.path.join(PROJECT_ROOT, 'static'),
)

# Serving files in production:
# STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'


# Backups:
# DBBACKUP_SERVER_NAME = 'production'
