# -*- coding: utf-8 -*-

__author__ = 'sobolevn'


RQ_QUEUES = {
    'default': {
        'HOST': 'localhost',
        'PORT': 6379,
        'DB': 0,
        'DEFAULT_TIMEOUT': 360,
    },
}

RQ_SHOW_ADMIN_LINK = False
