# -*- coding: utf-8 -*-

"""
Django settings for nsp project.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their config, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""

import os
from os.path import dirname

import sentry_sdk

from nsp.settings.components import CONFIG, BASE_DIR
from sentry_sdk.integrations.django import DjangoIntegration

__author__ = 'sobolevn'

# Build paths inside the project like this: join(BASE_DIR, ...)


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = CONFIG['DJANGO_SECRET_KEY']

# DJANGOCMS_GOOGLEMAP_API_KEY = 'AIzaSyDrMFDpolZub52kKF-GGm02aug0exW-VQU'

SITE_ID = 1


INSTALLED_APPS = (
    # Styling:
    'djangocms_admin_style',
    'djangocms_text_ckeditor',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.messages',

    # Django-CMS:
    'cms',
    'menus',
    'sekizai',
    'treebeard',
    'djangocms_googlemap',

    # rq:
    'django_rq',

    # Amazon storage
    'storages',

    # your apps go here:
    'landing',
    'download_app',

    'debug_toolbar',

    # add hvad
    'hvad',

    'django_extensions',
)


MIDDLEWARE_CLASSES = (
    'django.middleware.cache.UpdateCacheMiddleware',
    'cms.middleware.utils.ApphookReloadMiddleware',

    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',

)

CMS_TOOLBARS = [
    'cms.cms_toolbars.PlaceholderToolbar',
    'cms.cms_toolbars.BasicToolbar',
    'landing.cms_toolbars.CustomPageToolbar',
    'landing.cms_toolbars.CreateNewsPage',
    'landing.cms_toolbars.QuoteExtensionToolbar'
]

ROOT_URLCONF = 'nsp.urls'

WSGI_APPLICATION = 'nsp.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': CONFIG['DJANGO_DATABASE_HOST'],
        'PORT': CONFIG['DJANGO_DATABASE_PORT'],
        'NAME': CONFIG['DJANGO_DATABASE_NAME'],
        'USER': CONFIG['DJANGO_DATABASE_USER'],
        'PASSWORD': CONFIG['DJANGO_DATABASE_PASSWORD'],
    }
}

sentry_sdk.init(
    dsn='https://5735f43befc240fbb695777856ced94d:efa8eb9367504f27a1bea7287337a9b4@sentry.io/158834',
    integrations=[DjangoIntegration()],
)

# Security
# https://docs.djangoproject.com/en/1.8/ref/settings/#csrf-cookie-httponly

CSRF_COOKIE_HTTPONLY = True

PROJECT_DIR = dirname(dirname(dirname(dirname(__file__))))
MEDIA_ROOT = os.path.join(PROJECT_DIR, 'media')


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'en'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LANGUAGES = (
    ('ru', u'Русский'),
    ('en', u'English'),
    ('zh-hans', u'中國'),
    ('ja', u'日本語'),
)

LOCALE_PATHS = (
    'landing/locale/',
)

# Static files (CSS, JavaScript, Images).
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'


# Templates:

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.core.context_processors.i18n',
                'django.core.context_processors.debug',
                'django.core.context_processors.request',
                'django.core.context_processors.media',
                'django.core.context_processors.csrf',
                'django.core.context_processors.tz',
                'sekizai.context_processors.sekizai',
                'django.core.context_processors.static',
                'cms.context_processors.cms_settings',

                # custom context processor
                'landing.context_processor.utils_data',
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
                'django.template.loaders.eggs.Loader',
            ],
        },
    },
]


# Media files
# Media-root is commonly changed in production (see development.py and production.py).

MEDIA_URL = '/media/'


# Django default authentication system.
# https://docs.djangoproject.com/en/1.8/topics/auth/

# AUTH_USER_MODEL = 'auth_app.SiteUser'

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

LOGIN_URL = '/login-form/'
LOGIN_REDIRECT_URL = '/logged-in/'
LOGIN_ERROR_URL = '/login-error/'

SESSION_COOKIE_HTTPONLY = True

SESSION_EXPIRE_AT_BROWSER_CLOSE = True


# Backups (django-dbbackups settings):
# http://django-dbbackup.readthedocs.org/en/latest/
# Note that PRODUCTION SERVER and DEVELOPMENT SERVER should have different names!

# DBBACKUP_STORAGE = 'dbbackup.storage.dropbox_storage'


# Logging
# https://docs.djangoproject.com/en/1.8/topics/logging/

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': 'server.log',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'DEBUG',
        },
        'server': {
            'handlers': ['file'],
            'level': 'DEBUG',
        },
    }
}


DEBUG_TOOLBAR_CONFIG = {
    # 'EXCLUDE_URLS' : ( '/admin' ,),
    # 'INTERCEPT_REDIRECTS' : True ,
}


