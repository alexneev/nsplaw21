# -*- coding: utf-8 -*-

from nsp.settings.components.common import CONFIG

__author__ = 'sobolevn'

# DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'



# Credentials
# AWS_ACCESS_KEY_ID = CONFIG['AWS_ACCESS_KEY_ID']
# AWS_SECRET_ACCESS_KEY = CONFIG['AWS_SECRET_ACCESS_KEY']
#
# AWS_STORAGE_BUCKET_NAME = 'nsplaw'

# Custom AWS
# AWS_DEFAULT_ACL = 'public-read'

# Uncomment to upload to Amazon with `python manage.py collectstatic`
# STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
