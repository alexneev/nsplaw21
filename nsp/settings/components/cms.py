# -*- coding: utf-8 -*-

"""
Settings for Django CMS.
"""


CMS_LANGUAGES = {
    'default': {
        'public': True,
        'hide_untranslated': True,
        'redirect_on_fallback': True,
    },
    1: [
        {
            'public': True,
            'code': 'ru',
            'hide_untranslated': False,
            'name': u'Русский',
            'redirect_on_fallback': True,
        },
        {
            'public': True,
            'code': 'en',
            'hide_untranslated': True,
            'name': 'English',
            'redirect_on_fallback': True,
        },
        {
            'public': True,
            'code': 'zh-hans',
            'hide_untranslated': True,
            'name': u'中文',
            'redirect_on_fallback': False,
        },
        {
            'public': True,
            'code': 'ja',
            'hide_untranslated': True,
            'name': u'日本語',
            'redirect_on_fallback': False,
        },
    ],
}

CMS_TEMPLATES = (
    ('landing/index.html', u'Главная'),
    ('landing/about.html', u'О фирме'),
    ('landing/company_service.html', u'Страница услуги'),
    ('landing/company_subservice.html', u'Страница подуслуги'),
    ('landing/team.html', u'Команда'),
    ('landing/person.html', u'Страница сотрудника'),
    ('landing/newsroom.html', u'Пресс-центра'),
    ('landing/newsroom_news.html', u'Страница со всеми новостями'),
    ('landing/news_page.html', u'Страница новости'),
    ('landing/contacts.html', u'Контакты'),
    ('landing/career.html', u'Карьера'),
    ('landing/video.html', u'Видео-канал'),
    ('landing/pvo.html', u'ПВО'),
)

CMS_PERMISSION = True

# set a placeholder for a specific plugin
CMS_PLACEHOLDER_CONF = {
    'Main services': {
        'plugins': ['ServicesTabsPlugin']
    },
    'Expertise': {
        'plugins': ['ExpertiseContainerPlugin']
    },
    'News': {
        'plugins': ['NewsPlugin']
    },
    'Press container': {
        'plugins': ['PublicationsPlugin', 'CommentsInMediaPlugin', 'DoneProjectsPlugin']
    },
    'Video block': {
        'plugins': ['VideoContainerPlugin']
    },
    'Quotes': {
        'plugins': ['QuotesContainerPlugin']
    },
    'Ratings': {
        'plugins': ['RatingPlugin']
    },
    'DownloadForm': {
        'plugins': ['DownloadFormPlugin']
    },
    'Footer': {
        'plugins': ['Footer']
    },
    'ContactForm': {
        'plugins': ['ContactFormContainerPlugin']
    },
    'ContentBlock': {
        'plugins': [
            'NewsPageDesctiptionPlugin', 'NewsPageMainTextPlugin',
            'NewsPageQuotePlugin', 'NewsPageDatePlugin', 'NewsPagePhotoPlugin',
            'NewsPageLinkPlugin', 'InformPlugin', 'TablePlugin'
        ]
    },
    'CareerBlock': {
        'plugins': [
            'NewsPageDesctiptionPlugin', 'NewsPageMainTextPlugin',
            'NewsPageQuotePlugin', 'NewsPagePersonPlugin', 'NewsPageDatePlugin',
            'NewsPagePhotoPlugin', 'NewsPageLinkPlugin', 'NewsPageTitlePlugin'
        ]
    },
    'FirstSlideAbout': {
        'plugins': ['FirstSlideAboutPlugin']
    },
    'WeDoMore': {
        'plugins': ['WeDoMorePlugin']
    },
    'AboutUs': {
        'plugins': ['AboutUsPlugin']
    },
    'WorldWide': {
        'plugins': ['WorldWidePlugin']
    },
    'FactInNumber': {
        'plugins': ['FactInNumberPlugin']
    },
    'FirstSlideTeam': {
        'plugins': ['FirstSlideTeamPlugin']
    },
    'Team': {
        'plugins': ['TeamBlockPlugin']
    },
    'BiographyBlock': {
        'plugins': ['BiographyContainerPlugin']
    },
    'QuotesBlock': {
        'plugins': ['QuotesContainerPlugin']
    },
    'About Service': {
        'plugins': ['AboutServicePlugin']
    },
    'Sub Services': {
        'plugins': ['SubSerivcesContainerPlugin']
    },
    'NewsRightBlock': {
        'plugins': ['NewsBlockChooseOnePlugin', 'AdditionalNewsBlockPlugin']
    },
    'SubServiceDetail': {
        'plugins': ['SubServiceDetailPlugin']
    },
    'SubServiceListBlock': {
        'plugins': ['SubServiceListPlugin']
    },
    'ContactPeople': {
        'plugins': ['ContactPeopleContainerPlugin']
    },
    'ClientFeatures': {
        'plugins': ['SubServiceFeaturesContainerPlugin']
    },
    'AdditionalNews': {
        'plugins': ['AdditionalNewsBlockPlugin']
    }
}

MIGRATION_MODULES = {
    'djangocms_table': 'djangocms_table.migrations_django'
}

# settings for a ckeditor
# nspenv/lib/python2.7/site-packages/djangocms_text_ckeditor/static/djangocms_text_ckeditor/ckeditor/skins
CKEDITOR_SETTINGS_1 = {
    'language': '{{ language }}',
    'skin': 'moono',
    'toolbar_HTMLField': [
        ['Undo', 'Redo'],
        ['ShowBlocks'],
        ['Bold', 'Italic', 'RemoveFormat', 'Link', 'NumberedList', 'BulletedList',],
    ]
}
