FROM python:2

ENV PYTHONUNBUFFERED 1

RUN mkdir /code
WORKDIR /code
ADD . /code/
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install sentry_sdk

